/*
|--------------------------------------------------------------------------
| Validating Environment Variables
|--------------------------------------------------------------------------
|
| In this file we define the rules for validating environment variables.
| By performing validation we ensure that your application is running in
| a stable environment with correct configuration values.
|
| This file is read automatically by the framework during the boot lifecycle
| and hence do not rename or move this file to a different location.
|
*/

import Env from '@ioc:Adonis/Core/Env'

export default Env.rules({
  HOST: Env.schema.string({format: 'host'}),
  PORT: Env.schema.number(),
  APP_KEY: Env.schema.string(),
  APP_NAME: Env.schema.string(),
  APP_LANGUAGE: Env.schema.string(),
  API_VERSION: Env.schema.string(),
  HASH_DRIVER: Env.schema.string(),
  DRIVE_DISK: Env.schema.enum(['local'] as const),
  NODE_ENV: Env.schema.enum(['development', 'geoalarme', 'test'] as const),
  DB_CONNECTION: Env.schema.string(),
  DB_HOST: Env.schema.string({format: 'host'}),
  DB_PORT: Env.schema.number(),
  DB_USER: Env.schema.string(),
  DB_PASSWORD: Env.schema.string.optional(),
  DB_DATABASE: Env.schema.string(),
  PUBLIC_VAPID_KEY: Env.schema.string(),
  PRIVATE_VAPID_KEY: Env.schema.string(),
  WEB_PUSH_CONTACT: Env.schema.string(),
  ECALL_VERSION: Env.schema.string(),
  ECALL_USERNAME: Env.schema.string(),
  ECALL_PASSWORD: Env.schema.string.optional(),
  ECALL_PHONE_NUMBER: Env.schema.string(),
})
