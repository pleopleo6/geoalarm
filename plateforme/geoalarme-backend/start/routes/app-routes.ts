import Route from "@ioc:Adonis/Core/Route";

Route.get('/*', async ({ view }) => {
  return view.render('mobile')
  // return view.render('desktop')
})
