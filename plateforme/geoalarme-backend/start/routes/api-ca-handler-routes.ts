import Route from "@ioc:Adonis/Core/Route";
import Env from "@ioc:Adonis/Core/Env";
// Import your custom middleware

Route.group(() => {
  // routes publiques
  Route.post('/protocol/v1', 'CAHandlerApiController.postHandler_v1');
  Route.get('/check/site/connexion', 'CAHandlerApiController.verify_connexion');
  Route.post('/save/lidar/data', 'CAHandlerApiController.save_lidar_data');
}).prefix(`/api/${Env.get('API_VERSION')}/CaHandler`)
