import Route from "@ioc:Adonis/Core/Route";
import Env from "@ioc:Adonis/Core/Env";


Route.post(`/api/${Env.get('API_VERSION')}/SensorHandler`, 'SensorHandlerApiController.decodeRequest');
