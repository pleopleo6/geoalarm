import Route from "@ioc:Adonis/Core/Route";
import Env from "@ioc:Adonis/Core/Env";

Route.group(() => {

  Route.get('/wording/:contextId/:langId', 'SharedApisController.getWording')

}).prefix(`/api/${Env.get('API_VERSION')}`)
