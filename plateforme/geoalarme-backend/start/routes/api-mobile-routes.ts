import Route from "@ioc:Adonis/Core/Route";
import Env from "@ioc:Adonis/Core/Env";
Route.group(() => {

  // routes publiques
  Route.group(() => {
    Route.post('/login', 'MobileApiController.login')
    Route.post('/logout', 'MobileApiController.logout')
    Route.get('/site/:id/camera/:cameraId/stream.jpg', 'MobileApiController.getSiteCameraStream')
  })

  // routes authentifiées
  Route.group(() => {

    Route.get('/sites', 'MobileApiController.getSites')
    Route.get('/site/:id', 'MobileApiController.getSite')
    Route.post('/site/:id/logs', 'MobileApiController.getSiteLogs')
    Route.get('/site/:id/devices/alarm', 'MobileApiController.getSiteAlarmDevices')
    Route.get('/site/:id/devices/signalisation', 'MobileApiController.getSiteSignalisationDevices')
    Route.get('/site/:id/devices/defaults', 'MobileApiController.getSiteDevicesInDefault')
    Route.get('/site/:id/device/:deviceId', 'MobileApiController.getSiteDevice')
    Route.post('/site/:id/command/:commandId', 'MobileApiController.callSiteCommand')

    Route.get('/site/:id/station/:stationId/devices/alarm', 'MobileApiController.getStationAlarmDevices')
    Route.get('/site/:id/station/:stationId/devices/signalisation', 'MobileApiController.getStationSignalisationDevices')
    Route.get('/site/:id/station/:stationId/device/:deviceId', 'MobileApiController.getStationDevice')

    Route.get('/site/:id/stations', 'MobileApiController.getStations')
    Route.get('/site/:id/station/:stationId', 'MobileApiController.getStation')
    Route.post('/site/:id/station/:stationId/logs', 'MobileApiController.getStationLogs')

    Route.get('/site/:id/cameras', 'MobileApiController.getSiteCameras')
    Route.get('/site/:id/camera/:cameraId', 'MobileApiController.getSiteCamera')

    Route.get('/site/:id/sensors', 'MobileApiController.getSiteSensors')
    Route.get('/site/:id/sensor/:sensorId', 'MobileApiController.getSiteSensor')
    Route.get('/site/:id/station/:stationId/sensors', 'MobileApiController.getStationSensors')
    Route.get('/site/:id/station/:stationId/sensor/:sensorId', 'MobileApiController.getStationSensor')

    Route.post('/preferences', 'MobileApiController.setUserPreferences')

    Route.post('/notify-subscription', 'MobileApiController.notifySubscription')

  }).middleware('auth')

}).prefix(`/api/${Env.get('API_VERSION')}/mobile`)
