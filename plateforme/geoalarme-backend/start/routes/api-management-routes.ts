import Route from "@ioc:Adonis/Core/Route";
import Env from "@ioc:Adonis/Core/Env";

Route.group(() => {

  // routes publiques
  Route.group(() => {

    // test - à supprimer
    Route.get('/', async () => ({context: 'management'}))

    Route.post('/login', 'ManagementApiController.login')
    Route.post('/logout', 'ManagementApiController.logout')


    // MANUEL INSERT
    Route.get('/addmanually', 'ManagementApiController.addManually')

  })

  // routes authentifiées
  Route.group(() => {

    // ROUTES À IMPLÉMENTER
    // ----
    // entreprise	POST
    // entreprise / { id } 	DELETE
    // entreprise / { id }	PUT
    // entreprises	GET
    // entreprise / { id } / users	POST
    // entreprise / { id_entreprise } / user / { id_user }	DELETE
    // entreprise / { id } / users	GET
    // user	POST
    // user / { id }	DELETE
    // user / { id }	PUT
    // roles	GET
    // role / { id }	DELETE
    // role / { id }	PUT
    // commands	GET
    // sites	GET
    // site / { id }	GET
    // site	POST
    // site / { id }	DELETE
    // site / { id }	PUT
    // site / { id } / stations	GET
    // site / { id } / station	POST
    // site / { id_site } /  station / { id }	DELETE
    // site / { id_site } /  station / { id }	GET
    // site / { id_site } /  station / { id }	PUT
    // site / { id } / objects	GET
    // site / { id } / station / { id } / objects	GET
    // site / { id_site } / station / { id } / device / { id }	GET
    // site / { id_site } / station / { id } / device	POST
    // site / { id_site } / station / { id } / device / { id }	DELETE
    // site / { id_site } / station / { id } / device / { id }	PUT
    // site / { id_site } / capteur / { id_capteur }	DELETE
    // site / { id_site } / capteur 	POST
    // site / { id_site } / capteur / { id_capteur }	PUT
    // site / { id_site } / capteur / { id_capteur }	GET
    // site / { id } / capteur	POST
    // site / { id } / cameras	GET
    // site / { id } / camera 	POST
    // site / { id_site } / camera / { id_camera }	DELETE
    // site / { id_site } / camera / { id_camera }	PUT
    // site / { id_site } / camera / { id_camera } / add	POST
    // site / { id_site } / camera / { id_camera } / remove	DELETE
    // site / { id } / stats	GET
    // capteurs / types	GET
    // capteurs / type	POST
    // capteurs / type / { id }	DELETE
    // capteurs / type / { id }	PUT
    // cameras / types	GET
    // cameras / type	POST
    // cameras / type / { id }	DELETE
    // cameras / type / { id }	PUT
    // alarm / types	GET
    // alarm / type	POST
    // alarm / type / { id }	DELETE
    // alarm / type / { id }	PUT
    // ...

  }).middleware('auth')


}).prefix(`/api/${Env.get('API_VERSION')}/management`)
