import Route from "@ioc:Adonis/Core/Route";
import Env from "@ioc:Adonis/Core/Env";

Route.group(() => {

  // routes publiques
  Route.group(() => {
    Route.post('/login', 'DesktopApiController.login')
    Route.post('/logout', 'DesktopApiController.logout')
  })

  // routes authentifiées
  Route.group(() => {
    Route.get('/sites', 'DesktopApiController.getSites')
    Route.get('/site/:id', 'DesktopApiController.getSite')
    Route.get('/site/:id/devices/:deviceId', 'DesktopApiController.getSiteDevice')
    Route.post('/site/:id/command/:commandId', 'DesktopApiController.callCommand')
    Route.post('/site/:id/distribution/', 'DesktopApiController.createSiteDistribution')
    Route.put('/site/:id/distribution/:distributionId', 'DesktopApiController.updateSiteDistribution')
    Route.delete('/site/:id/distribution/:distributionId', 'DesktopApiController.suppressSiteDistribution')

    Route.get('/devices', 'DesktopApiController.getDevices')
    Route.get('/devices/:deviceId', 'DesktopApiController.getDevice')

    // Route.get('/site/:id/objects', 'DesktopApiController.getSiteObjects')                // EN COURS
    // Route.get('/site/:id/station/:targetId', 'DesktopApiController.getSiteStation')      // EN COURS
    // Route.get('/site/:id/sensor/:targetId', 'DesktopApiController.getSiteSensor')        // EN COURS
    // Route.get('/site/:id/camera/:targetId', 'DesktopApiController.getSiteCamera')        // EN COURS
    // Route.get('/objects', 'DesktopApiController.getObjects')                             // EN COURS
    // Route.get('/station/:id', 'DesktopApiController.getStation')                         // EN COURS
    // Route.get('/sensor/:id', 'DesktopApiController.getSensor')                           // EN COURS
    // Route.get('/camera/:id', 'DesktopApiController.getCamera')                           // EN COURS
    // Route.get('/camera/:id/stream', 'DesktopApiController.getCameraStream')              // TODO ?
    Route.get('/users', 'DesktopApiController.getUsers')
    Route.get('/users/:id', 'DesktopApiController.getUser')
    Route.post('/users', 'DesktopApiController.createUser')                               // EN COURS
    Route.post('/users/:id', 'DesktopApiController.updateUser')                           // EN COURS
    Route.delete('/users/:id', 'DesktopApiController.suppressUser')                       // EN COURS

    Route.get('/logs', 'DesktopApiController.getLogs')                                   // EN COURS

  }).middleware('auth')

}).prefix(`/api/${Env.get('API_VERSION')}/desktop`)
