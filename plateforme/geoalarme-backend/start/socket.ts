import Ws from "../app/Services/websocket/Ws";
import CurrentWs from "../app/Models/CurrentWs";

// Boot Ws
Ws.boot();

// Define an array list of connected users
/**
 * Listen for incoming socket connections
 */
Ws.io.on('connection', async (socket) => {

  // @ts-ignore
  const userId = socket.handshake.query.userId;
  // console.log("connected ws : " + userId)

  if (userId) {
    // if user has already been connected and connexion hasnt closed, erase it from db
    const socket_connexion = await CurrentWs.findBy('userId', userId);
    // Delete the found record
    if (socket_connexion) {
      await socket_connexion.delete();
    }

    // Add the connected user to the list
    const currentWs = new CurrentWs();
    // @ts-ignore
    currentWs.userId = userId;
    await currentWs.save();

    socket.emit('geoalarme-ws-connection', {state: 'ws-connected'});
  }

  // Listen for the disconnect event
  socket.on('disconnect', async () => {
    // console.log("disconnected ws " + userId);
    if (userId) {
      // Find and delete the corresponding record from the table
      const currentWs = await CurrentWs.query().where('userId', userId).first();
      if (currentWs) {
        await currentWs.delete();
      }
    }
  });
});
