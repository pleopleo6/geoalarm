import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TLanguage extends BaseSchema {
    protected tableName = 'commands'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.boolean('is_global').nullable()
          table.integer('validation_level_id')
            .unsigned()
            .nullable()
            .references('command_validation_levels.id')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
