import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'signal_status'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('level').notNullable()
      table.string('description').notNullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
