import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'user_site_associations'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {

          table.integer('role_id')
            .unsigned()
            .notNullable()
            .references('roles.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
