import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TREntrepriseSite extends BaseSchema {
    protected tableName = 'entreprise_site_associations'

    public async up () {
      this.schema.dropTable(this.tableName)
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
