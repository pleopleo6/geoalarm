import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'command_pools'

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('state_id')
        .unsigned()
        .notNullable()
        .references('command_states.id')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
