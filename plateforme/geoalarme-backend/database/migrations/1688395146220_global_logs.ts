import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TLogGlobal extends BaseSchema {
    protected tableName = 'global_logs'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('log_type_id')
            .unsigned()
            .notNullable()
            .references('log_types.id')

          table.text('log_data').notNullable()

          table.timestamp('log_timestamp', { useTz: true }).notNullable()

          table.text('actor').notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
