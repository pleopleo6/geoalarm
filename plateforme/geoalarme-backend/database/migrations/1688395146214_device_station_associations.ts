import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrDeviceStation extends BaseSchema {
    protected tableName = 'device_station_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('device_id')
            .unsigned()
            .notNullable()
            .references('devices.id')

          table.integer('station_id')
            .unsigned()
            .notNullable()
            .references('stations.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
