import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'device_datas'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('alarm_status_id')

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
