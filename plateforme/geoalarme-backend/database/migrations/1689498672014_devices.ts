import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'devices'

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('alarm_status_id')
      table.dropColumn('default_status_id')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
