import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'site_status'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('level_num')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
