import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDeviceData extends BaseSchema {
    protected tableName = 'device_datas'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.integer('device_data_association_id')
            .unsigned()
            .notNullable()
            .references('device_data_associations.id')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
