import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TEntreprise extends BaseSchema {
    protected tableName = 'entreprises'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.text('city').notNullable()
          table.text('country').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
