import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'device_alarm_associations'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('device_id')
        .unsigned()
        .notNullable()
        .references('devices.id')

      table.integer('alarm_id')
        .unsigned()
        .notNullable()
        .references('alarm_status.id')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
