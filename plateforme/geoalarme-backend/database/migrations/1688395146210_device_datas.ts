import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDeviceData extends BaseSchema {
    protected tableName = 'device_datas'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('alarm_status_id')
            .unsigned()
            .notNullable()
            .references('alarm_status.id')

          table.text('data').notNullable()

          table.timestamp('timestamp', { useTz: true }).notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
