import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrUserSendType extends BaseSchema {
    protected tableName = 'user_send_type_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('user_id')
            .unsigned()
            .notNullable()
            .references('users.id')

          table.integer('event_type_id')
            .unsigned()
            .notNullable()
            .references('event_types.id')

          table.integer('communication_type_id')
            .unsigned()
            .notNullable()
            .references('communication_types.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
