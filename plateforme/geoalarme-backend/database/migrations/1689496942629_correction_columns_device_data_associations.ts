import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'device_data_associations'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.dropColumn('device_data_id')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
