import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TTypeSensor extends BaseSchema {
    protected tableName = 'sensor_types'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('decoder').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
