import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'site_default_associations'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.timestamp('timestamp', { useTz: true }).notNullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
