import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrDeviceData extends BaseSchema {
    protected tableName = 'device_data_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('device_id')
            .unsigned()
            .notNullable()
            .references('devices.id')

          table.integer('device_data_id')
            .unsigned()
            .notNullable()
            .references('device_datas.id')

          table.integer('data_type_id')
            .unsigned()
            .notNullable()
            .references('data_types.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
