import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TLanguage extends BaseSchema {
    protected tableName = 'languages'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.text('code').notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
