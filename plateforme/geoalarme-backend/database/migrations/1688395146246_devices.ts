import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDevice extends BaseSchema {
    protected tableName = 'devices'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('station_id')
          table.text('device_id').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
