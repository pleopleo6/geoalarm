import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrSensorData extends BaseSchema {
    protected tableName = 'sensor_data_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('sensor_id')
            .unsigned()
            .notNullable()
            .references('sensors.id')

          table.integer('sensor_data_id')
            .unsigned()
            .notNullable()
            .references('sensor_datas.id')

          table.integer('data_type_id')
            .unsigned()
            .notNullable()
            .references('data_types.id')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
