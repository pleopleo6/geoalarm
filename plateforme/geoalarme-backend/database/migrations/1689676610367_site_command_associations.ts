import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'site_command_associations'

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.increments('id').primary()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
