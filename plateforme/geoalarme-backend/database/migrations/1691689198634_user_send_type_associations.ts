import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'user_send_type_associations'

  public async up() {
    this.schema.dropTable(this.tableName)
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
