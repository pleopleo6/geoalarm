import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TTypeCamera extends BaseSchema {
    protected tableName = 'camera_types'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('name').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
