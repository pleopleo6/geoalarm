import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TWording extends BaseSchema {
    protected tableName = 'wordings'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.text('name').notNullable()

          table.text('value').notNullable()

          table.integer('language_id')
            .unsigned()
            .notNullable()
            .references('languages.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
