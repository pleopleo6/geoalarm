import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrSiteSensor extends BaseSchema {
    protected tableName = 'site_sensor_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('site_id')
            .unsigned()
            .notNullable()
            .references('sites.id')

          table.integer('sensor_id')
            .unsigned()
            .notNullable()
            .references('sensors.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
