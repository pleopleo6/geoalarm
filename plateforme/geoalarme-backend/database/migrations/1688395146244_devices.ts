import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDevice extends BaseSchema {
    protected tableName = 'devices'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.text('station_id').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
