import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TEventTypeType extends BaseSchema {
    protected tableName = 'event_type_types'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.text('name').notNullable()

          table.integer('label_id')
            .unsigned()
            .nullable()
            .references('wordings.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
