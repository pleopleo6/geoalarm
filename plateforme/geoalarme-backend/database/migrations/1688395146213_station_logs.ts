import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TLogStation extends BaseSchema {
    protected tableName = 'station_logs'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('station_id')
            .unsigned()
            .notNullable()
            .references('stations.id')

          table.integer('log_type_id')
            .unsigned()
            .notNullable()
            .references('log_types.id')

          table.text('log_data').notNullable()

          table.timestamp('log_timestamp', { useTz: true }).notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
