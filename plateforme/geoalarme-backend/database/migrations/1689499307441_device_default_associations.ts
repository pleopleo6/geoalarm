import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'device_default_associations'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('device_id')
        .unsigned()
        .notNullable()
        .references('devices.id')

      table.integer('default_id')
        .unsigned()
        .notNullable()
        .references('default_status.id')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
