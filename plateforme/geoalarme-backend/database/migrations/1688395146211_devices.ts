import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDevice extends BaseSchema {
    protected tableName = 'devices'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('type_id')
            .unsigned()
            .notNullable()
            .references('device_types.id')

          // table.bigInteger('FK_device_data').notNullable()
          // utilité à vérifier
          // ...

          table.integer('status_id')
            .unsigned()
            .notNullable()
            .references('device_status.id')

          table.integer('alarm_status_id')
            .unsigned()
            .notNullable()
            .references('alarm_status.id')

          table.integer('default_status_id')
            .unsigned()
            .notNullable()
            .references('default_status.id')

          table.text('name').notNullable()
          table.specificType('latitude', 'double precision').notNullable()
          table.specificType('longitude', 'double precision').notNullable()
          table.specificType('altitude', 'double precision').notNullable()
          table.specificType('position', 'point')

          table.timestamp('created_at', { useTz: true }).notNullable()
          table.timestamp('updated_at', { useTz: true }).notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
