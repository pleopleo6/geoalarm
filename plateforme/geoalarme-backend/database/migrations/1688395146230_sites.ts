import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'sites'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('city')
          table.dropColumn('district')
          table.dropColumn('country')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
