import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'device_datas'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table
        .integer('device_data_id')
        .unsigned()
        .references('device_data_associations.id')
        .onDelete('cascade')
        .notNullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
