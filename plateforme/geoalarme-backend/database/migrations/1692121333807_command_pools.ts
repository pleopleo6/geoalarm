import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'command_pools'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.text('uid').notNullable().unique()
      table.integer('command_id')
        .unsigned()
        .notNullable()
        .references('commands.id')
      table.integer('site_id')
        .unsigned()
        .notNullable()
        .references('sites.id')
      table.timestamp('timestamp', { useTz: true }).notNullable()
      table.json('params').notNullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
