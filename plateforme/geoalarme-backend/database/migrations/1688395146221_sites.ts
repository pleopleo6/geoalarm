import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'sites'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('status_id')
            .unsigned()
            .notNullable()
            .references('site_status.id')

          table.integer('alarm_status_id')
            .unsigned()
            .notNullable()
            .references('alarm_status.id')

          table.integer('default_status_id')
            .unsigned()
            .notNullable()
            .references('default_status.id')

          table.text('name').notNullable()
          table.specificType('area', 'polygon').nullable()
          table.text('site_id').notNullable()
          table.bigInteger('city').notNullable()
          table.bigInteger('district').notNullable()
          table.bigInteger('country').notNullable()

          table.timestamp('last_connexion', { useTz: true }).notNullable().comment("trigger")

          table.timestamp('created_at', { useTz: true }).notNullable()
          table.timestamp('updated_at', { useTz: true }).notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
