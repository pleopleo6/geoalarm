import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrUserSendType extends BaseSchema {
    protected tableName = 'user_send_type_associations'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.increments('id').primary()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
