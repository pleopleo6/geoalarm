import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TMessage extends BaseSchema {
    protected tableName = 'messages'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.text('name').notNullable()

          table.integer('language_id')
            .unsigned()
            .notNullable()
            .references('languages.id')

          table.integer('content_id')
            .unsigned()
            .notNullable()
            .references('wordings.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
