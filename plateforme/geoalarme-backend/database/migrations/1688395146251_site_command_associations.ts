import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrSiteCommand extends BaseSchema {
    protected tableName = 'site_command_associations'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {

          table.bigInteger('command_count').notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
