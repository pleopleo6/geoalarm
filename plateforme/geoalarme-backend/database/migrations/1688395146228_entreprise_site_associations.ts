import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TREntrepriseSite extends BaseSchema {
    protected tableName = 'entreprise_site_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('entreprise_id')
            .unsigned()
            .notNullable()
            .references('entreprises.id')

          table.integer('site_id')
            .unsigned()
            .notNullable()
            .references('sites.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
