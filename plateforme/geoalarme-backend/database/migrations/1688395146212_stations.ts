import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TStation extends BaseSchema {
    protected tableName = 'stations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('type_id')
            .unsigned()
            .notNullable()
            .references('station_types.id')

          table.integer('status_id')
            .unsigned()
            .notNullable()
            .references('station_status.id')

          table.integer('default_status_id')
            .unsigned()
            .notNullable()
            .references('default_status.id')

          table.text('name').notNullable()
          table.specificType('location', 'point').nullable()
          table.specificType('latitude', 'double precision').notNullable()
          table.specificType('longitude', 'double precision').notNullable()
          table.specificType('altitude', 'double precision').notNullable()

          table.timestamp('created_at', { useTz: true }).notNullable()
          table.timestamp('updated_at', { useTz: true }).notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
