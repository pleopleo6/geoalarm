import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'site_logs'

  public async up () {

    this.schema.createTable(this.tableName, (table) => {

      table.increments('id').primary()

      table.integer('site_id')
        .unsigned()
        .notNullable()
        .references('sites.id')

      table.integer('log_type_id')
        .unsigned()
        .notNullable()
        .references('log_types.id')

      table.jsonb('log_data').nullable()

      table.integer('actor').notNullable()

      table.integer('actor_type_id')
        .unsigned()
        .notNullable()
        .references('log_actor_types.id')

      table.timestamp('log_timestamp', {useTz: true}).notNullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
