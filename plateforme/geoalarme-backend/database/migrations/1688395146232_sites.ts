import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'sites'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('last_connexion')
          table.dropColumn('created_at')
          table.dropColumn('updated_at')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
