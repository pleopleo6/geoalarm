import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AlarmStatus extends BaseSchema {
    protected tableName = 'alarm_status'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.bigInteger('level').notNullable()
          table.text('description').nullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
