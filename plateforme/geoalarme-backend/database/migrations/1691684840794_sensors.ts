import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'sensors'

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('app_key')
      table.dropColumn('app_eui')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
