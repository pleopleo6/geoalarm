import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TEntreprise extends BaseSchema {
    protected tableName = 'entreprises'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.text('adresse_2').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
