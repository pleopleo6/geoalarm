import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'sensor_data_associations'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {

      table.increments('id').primary()
      table.dropColumn('sensor_data_id')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
