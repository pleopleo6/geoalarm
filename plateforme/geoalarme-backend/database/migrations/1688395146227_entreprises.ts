import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TEntreprise extends BaseSchema {
    protected tableName = 'entreprises'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('name').notNullable()
          table.text('adresse').notNullable()
          table.bigInteger('npa').notNullable()
          table.text('phone').notNullable()
          table.text('email').notNullable()
          table.timestamp('created_at', { useTz: true }).notNullable()
          table.timestamp('updated_at', { useTz: true }).notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
