import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TEventType extends BaseSchema {
    protected tableName = 'event_types'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('type_id')
            .unsigned()
            .notNullable()
            .references('event_type_types.id')

          table.bigInteger('level').notNullable()

          table.integer('label_id')
            .unsigned()
            .nullable()
            .references('wordings.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
