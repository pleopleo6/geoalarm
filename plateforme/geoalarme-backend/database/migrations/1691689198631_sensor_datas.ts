import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'sensor_datas'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.double('data').alter(); // Change the type of the 'data' column to double
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
