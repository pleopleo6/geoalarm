import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TLanguage extends BaseSchema {
    protected tableName = 'commands'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.text('labelId').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
