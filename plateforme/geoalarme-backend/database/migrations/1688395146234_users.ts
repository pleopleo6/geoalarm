import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TUser extends BaseSchema {
    protected tableName = 'users'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('role_id')
          table.integer('entreprise_id')
            .unsigned()
            .notNullable()
            .references('entreprises.id')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
