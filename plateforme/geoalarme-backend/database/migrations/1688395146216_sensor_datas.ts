import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSensorData extends BaseSchema {
    protected tableName = 'sensor_datas'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('data').notNullable()
          table.timestamp('timestamp', { useTz: true }).notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
