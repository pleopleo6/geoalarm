import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrSiteCommand extends BaseSchema {
    protected tableName = 'site_command_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('site_id')
            .unsigned()
            .notNullable()
            .references('sites.id')

          table.integer('command_id')
            .unsigned()
            .notNullable()
            .references('commands.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
