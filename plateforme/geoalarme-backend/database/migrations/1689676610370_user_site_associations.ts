import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'user_site_associations'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('entreprise_id')
          table.integer('site_id')
            .unsigned()
            .notNullable()
            .references('sites.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
