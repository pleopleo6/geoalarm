import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrStationSite extends BaseSchema {
    protected tableName = 'station_site_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('station_id')
            .unsigned()
            .notNullable()
            .references('stations.id')

          table.integer('site_id')
            .unsigned()
            .notNullable()
            .references('sites.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
