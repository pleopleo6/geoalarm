import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TCamera extends BaseSchema {
    protected tableName = 'cameras'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('type_id')
            .unsigned()
            .notNullable()
            .references('camera_types.id')

          table.specificType('location', 'point').nullable()
          table.text('url').notNullable()
          table.bigInteger('port').notNullable()
          table.specificType('latitude', 'double precision').notNullable()
          table.specificType('longitude', 'double precision').notNullable()
          table.specificType('altitude', 'double precision').notNullable()
          table.text('username').notNullable().comment("potentiellement dans fichier de config")
          table.text('password').notNullable().comment("potentiellement dans fichier de config")

          table.timestamp('created_at', { useTz: true }).notNullable()
          table.timestamp('updated_at', { useTz: true }).notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
