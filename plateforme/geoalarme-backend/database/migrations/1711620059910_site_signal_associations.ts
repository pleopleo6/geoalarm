import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'site_signal_associations'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.timestamp('timestamp', {useTz: true}).notNullable()
      table.integer('site_id')
        .unsigned()
        .notNullable()
        .references('sites.id')

      table.integer('signal_id')
        .unsigned()
        .notNullable()
        .references('signal_status.id')

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
