import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDefaultStatus extends BaseSchema {
    protected tableName = 'default_status'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.bigInteger('level').notNullable()
          table.text('description').nullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
