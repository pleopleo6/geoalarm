import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'sites'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.string('city').notNullable()
          table.string('district').notNullable()
          table.string('country').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
