import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'sites'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.timestamp('last_connexion', { useTz: true }).notNullable().comment("trigger")
          table.timestamp('created_at', { useTz: true }).notNullable()
          table.timestamp('updated_at', { useTz: true }).notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
