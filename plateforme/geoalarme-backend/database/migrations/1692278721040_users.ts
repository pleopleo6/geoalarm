import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TUser extends BaseSchema {
    protected tableName = 'users'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.string('webpush_subscription', 512).nullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
