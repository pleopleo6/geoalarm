import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'sensor_datas'

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table
        .integer('sensor_data_id')
        .unsigned()
        .references('sensor_data_associations.id')
        .onDelete('cascade')
        .notNullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName);
  }
}
