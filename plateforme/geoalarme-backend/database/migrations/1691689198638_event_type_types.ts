import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'event_type_types'

  public async up() {
    this.schema.dropTable(this.tableName)
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
