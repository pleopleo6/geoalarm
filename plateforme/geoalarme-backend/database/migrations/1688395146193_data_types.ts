import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDataType extends BaseSchema {
    protected tableName = 'data_types'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('name').notNullable()
          table.text('unity').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
