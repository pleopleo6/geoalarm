import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TStationType extends BaseSchema {
    protected tableName = 'station_types'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('name').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
