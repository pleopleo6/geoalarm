import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TStation extends BaseSchema {
    protected tableName = 'stations'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('device_id')
          table.text('station_id').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
