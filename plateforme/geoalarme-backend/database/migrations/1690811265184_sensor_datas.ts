import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDeviceData extends BaseSchema {
    protected tableName = 'sensor_datas'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('sensor_data_associations_id')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
