import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TCommand extends BaseSchema {
    protected tableName = 'commands'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.text('name').notNullable()

          table.text('command').notNullable()

          table.integer('role_id')
            .unsigned()
            .notNullable()
            .references('roles.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
