import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TStatusStation extends BaseSchema {
    protected tableName = 'station_status'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('name').notNullable()
          table.text('description').nullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
