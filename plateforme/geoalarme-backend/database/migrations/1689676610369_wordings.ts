import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
    protected tableName = 'wordings'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.integer('context_id')
            .unsigned()
            .nullable()
            .references('wording_categs.id')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
