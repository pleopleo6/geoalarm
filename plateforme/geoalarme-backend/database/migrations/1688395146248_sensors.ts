import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSensor extends BaseSchema {
    protected tableName = 'sensors'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.text('sensor_id').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
