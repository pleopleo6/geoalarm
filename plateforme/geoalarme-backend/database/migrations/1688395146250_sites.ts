import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSite extends BaseSchema {
    protected tableName = 'sites'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.specificType('latitude', 'double precision').notNullable()
          table.specificType('longitude', 'double precision').notNullable()
          table.specificType('altitude', 'double precision').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
