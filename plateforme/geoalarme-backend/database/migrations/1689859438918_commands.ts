import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TLanguage extends BaseSchema {
    protected tableName = 'commands'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('labelId')
          table.dropColumn('descriptionId')
          //table.text('label_id').notNullable()
          //table.text('description_id').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
