import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TDataType extends BaseSchema {
    protected tableName = 'data_types'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.dropColumn('abbr')
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
