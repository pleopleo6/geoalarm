import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'site_alarm_associations'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('site_id')
        .unsigned()
        .notNullable()
        .references('sites.id')

      table.integer('alarm_id')
        .unsigned()
        .notNullable()
        .references('alarm_status.id')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
