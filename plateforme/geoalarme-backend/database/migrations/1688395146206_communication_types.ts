import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TCommunicationType extends BaseSchema {
    protected tableName = 'communication_types'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.bigInteger('name').notNullable()

          table.integer('label_id')
            .unsigned()
            .nullable()
            .references('wordings.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
