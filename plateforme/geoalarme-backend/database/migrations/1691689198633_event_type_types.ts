import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'event_type_types'

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropForeign('labelId', 'event_type_types_label_id_foreign')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
