import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TSensor extends BaseSchema {
    protected tableName = 'sensors'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.increments('id').primary()

          table.integer('type_id')
            .unsigned()
            .notNullable()
            .references('sensor_types.id')

          table.specificType('location', 'point').nullable()
          table.text('dev_eui').notNullable()
          table.text('app_eui').notNullable()
          table.text('app_key').notNullable()
          table.text('name').notNullable()
          table.specificType('latitude', 'double precision').notNullable()
          table.specificType('longitude', 'double precision').notNullable()
          table.specificType('altitude', 'double precision').notNullable()

          table.timestamp('created_at', { useTz: true }).notNullable()
          table.timestamp('updated_at', { useTz: true }).notNullable()

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
