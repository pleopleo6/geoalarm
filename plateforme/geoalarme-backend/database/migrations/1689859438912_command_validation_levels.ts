import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TLanguage extends BaseSchema {
    protected tableName = 'command_validation_levels'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {
          table.increments('id').primary()
          table.text('name').notNullable()
          table.json('params').nullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
