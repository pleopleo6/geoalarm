import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TRUserSite extends BaseSchema {
    protected tableName = 'user_site_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('user_id')
            .unsigned()
            .notNullable()
            .references('users.id')

          table.integer('entreprise_id')
            .unsigned()
            .notNullable()
            .references('entreprises.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
