import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TCamera extends BaseSchema {
    protected tableName = 'cameras'

    public async up () {
        this.schema.alterTable(this.tableName, (table) => {
          table.text('name').notNullable()
        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
