import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'event_types'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {

      table.increments('id').primary()
      
      table.integer('level').nullable()

      table.text('label_name').notNullable()

      table.integer('type_id')
        .unsigned()
        .notNullable()
        .references('event_type_types.id')

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
