import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TrSiteCamera extends BaseSchema {
    protected tableName = 'site_camera_associations'

    public async up () {
        this.schema.createTable(this.tableName, (table) => {

          table.integer('site_id')
            .unsigned()
            .notNullable()
            .references('sites.id')

          table.integer('camera_id')
            .unsigned()
            .notNullable()
            .references('cameras.id')

        })
    }

    public async down () {
        this.schema.dropTable(this.tableName)
    }
}
