import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeviceType from "../../app/Models/DeviceType";

export default class extends BaseSeeder {
  public async run() {
    await DeviceType.createMany([
      {
        name: "Système de mesure booléen",
      },
      {
        name: "Système de mesure analogique",
      },
      {
        name: "Système de signalisation booléen",
      },
      {
        name: "Système de signalisation 3 niveaux",
      },
      {
        name: "tension batterie",
      },
      {
        name: "niveau communication",
      },
      {
        name: "flag communication",
      },
      {
        name: "alimentation réseau",
      },
    ])
  }
}
