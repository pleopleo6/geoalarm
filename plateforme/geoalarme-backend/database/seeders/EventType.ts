import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import EventType from "../../app/Models/EventType";

export default class extends BaseSeeder {
  public async run() {
    await EventType.createMany([
      // alarms
      {
        typeId: 1,
        level: 1,
        labelName: 'distribution_list_alarm_level_1',
      },
      {
        typeId: 1,
        level: 2,
        labelName: 'distribution_list_alarm_level_2',
      },
      {
        typeId: 1,
        level: 3,
        labelName: 'distribution_list_alarm_level_3',
      },
      {
        typeId: 1,
        level: 4,
        labelName: 'distribution_list_alarm_level_4',
      },
      // technical defaults
      {
        typeId: 2,
        level: 1,
        labelName: 'distribution_list_technical_default_1',
      },
      {
        typeId: 2,
        level: 2,
        labelName: 'distribution_list_technical_default_2',
      },
      {
        typeId: 2,
        level: 3,
        labelName: 'distribution_list_technical_default_3',
      },
      {
        typeId: 2,
        level: 4,
        labelName: 'distribution_list_technical_default_4',
      },

      {
        typeId: 3,
        level: 0,
        labelName: 'distribution_list_site_status_level_0',
      },
      {
        typeId: 3,
        level: 1,
        labelName: 'distribution_list_site_status_level_1',
      },
      {
        typeId: 3,
        level: 2,
        labelName: 'distribution_list_site_status_level_2',
      },
      {
        typeId: 3,
        level: 3,
        labelName: 'distribution_list_site_status_level_3',
      },

    ])
  }
}
