import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import UserSiteAssociation from "../../app/Models/UserSiteAssociation";

export default class extends BaseSeeder {
  public async run() {
    await UserSiteAssociation.createMany([
        {
          userId: 2,
          siteId: 3,
          roleId: 2,
        },
        {
          userId: 5,
          siteId: 3,
          roleId: 3,
        },
        {
          userId: 8,
          siteId: 3,
          roleId: 4,
        },
        {
          userId: 5,
          siteId: 3,
          roleId: 2,
        },
        {
          userId: 2,
          siteId: 2,
          roleId: 1,
        },
        {
          userId: 10,
          siteId: 2,
          roleId: 1,
        },
        {
          userId: 10,
          siteId: 1,
          roleId: 2,
        },
        {
          userId: 10,
          siteId: 3,
          roleId: 2,
        },
        {
          userId: 11,
          siteId: 2,
          roleId: 1,
        },
        {
          userId: 11,
          siteId: 1,
          roleId: 1,
        },
        {
          userId: 11,
          siteId: 3,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 2,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 1,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 3,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 2,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 1,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 3,
          roleId: 1,
        },
        {
          userId: 14,
          siteId: 2,
          roleId: 1,
        },
        {
          userId: 14,
          siteId: 1,
          roleId: 1,
        },
        {
          userId: 14,
          siteId: 3,
          roleId: 1,
        },
        {
          userId: 15,
          siteId: 2,
          roleId: 1,
        },
        {
          userId: 15,
          siteId: 1,
          roleId: 1,
        },
        {
          userId: 15,
          siteId: 3,
          roleId: 1,
        },
        {
          userId: 16,
          siteId: 2,
          roleId: 3,
        },
        {
          userId: 16,
          siteId: 3,
          roleId: 3,
        },
        /*{
          userId: 17,
          siteId: 2,
          roleId: 3,
        },
        {
          userId: 17,
          siteId: 3,
          roleId: 3,
        },*/
        {
          userId: 18,
          siteId: 1,
          roleId: 2,
        },
        {
          userId: 19,
          siteId: 1,
          roleId: 3,
        },


        // new data
        {
          userId: 11,
          siteId: 4,
          roleId: 1,
        },
        {
          userId: 11,
          siteId: 5,
          roleId: 1,
        },
        {
          userId: 11,
          siteId: 6,
          roleId: 1,
        },
        {
          userId: 11,
          siteId: 7,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 4,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 5,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 6,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 7,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 4,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 5,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 6,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 7,
          roleId: 1,
        },

        //stbarth
        {
          userId: 22,
          siteId: 4,
          roleId: 2,
        }, {
          userId: 23,
          siteId: 4,
          roleId: 3,
        }, {
          userId: 24,
          siteId: 4,
          roleId: 4,
        }
        // ech
        ,
        {
          userId: 25,
          siteId: 5,
          roleId: 2,
        }, {
          userId: 26,
          siteId: 5,
          roleId: 2,
        }, {
          userId: 27,
          siteId: 5,
          roleId: 3,
        },

        {
          userId: 28,
          siteId: 6,
          roleId: 2,
        }, {
          userId: 28,
          siteId: 7,
          roleId: 2,
        },

        {
          userId: 29,
          siteId: 6,
          roleId: 2,
        }, {
          userId: 29,
          siteId: 7,
          roleId: 2,
        },
        {
          userId: 13,
          siteId: 8,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 8,
          roleId: 1,
        },
        {
          userId: 11,
          siteId: 8,
          roleId: 1,
        },
        {
          userId: 13,
          siteId: 9,
          roleId: 1,
        },
        {
          userId: 12,
          siteId: 9,
          roleId: 1,
        },
        {
          userId: 11,
          siteId: 9,
          roleId: 1,
        },
        {userId: 30, siteId: 9, roleId: 2},  // Julien MONNEY - Administrateur
        {userId: 31, siteId: 9, roleId: 3},  // Laurent BERTHOUY - Technicien
        {userId: 32, siteId: 9, roleId: 3},  // Bertrand PERRIN - Technicien
        {userId: 33, siteId: 9, roleId: 3},  // Philippe SCHLUP - Technicien
        {userId: 34, siteId: 9, roleId: 3}, // René MARCLAY - Technicien
        {userId: 35, siteId: 9, roleId: 3}, // Valentin CLÉMENT - Technicien
        {userId: 36, siteId: 9, roleId: 3}, // Atelier MANU - Technicien
        {userId: 37, siteId: 9, roleId: 3}, // TP RÉSERVE - Technicien
        {userId: 38, siteId: 9, roleId: 3}, // tel piquet tp - Technicien
        {userId: 39, siteId: 9, roleId: 3}, // Thierry MARCLAY - Technicien
        {userId: 40, siteId: 9, roleId: 4}, // PIDM - Observateur
        {userId: 41, siteId: 9, roleId: 3}, // Yves Rey-Bellet - Technicien
        {userId: 42, siteId: 9, roleId: 2},  // John Monney - Administrateur

        {userId: 45, siteId: 1, roleId: 1},
        {userId: 45, siteId: 2, roleId: 1},
        {userId: 45, siteId: 3, roleId: 1},
        {userId: 45, siteId: 4, roleId: 1},
        {userId: 45, siteId: 5, roleId: 1},
        {userId: 45, siteId: 6, roleId: 1},
        {userId: 45, siteId: 7, roleId: 1},
        {userId: 45, siteId: 8, roleId: 1},
        {userId: 45, siteId: 9, roleId: 1},
        {userId: 11, siteId: 10, roleId: 1},
        {userId: 12, siteId: 10, roleId: 1},

      ]
    )
  }
}
