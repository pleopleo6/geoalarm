import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import SiteDefaultAssociation from '../../app/Models/SiteDefaultAssociation';

export default class extends BaseSeeder {
  public async run () {
    await SiteDefaultAssociation.createMany([
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
      {
        siteId: 3,
        defaultId: 1,
      },
    ])
  }
}
