import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeviceDefaultAssociation from "../../app/Models/DeviceDefaultAssociation";

export default class extends BaseSeeder {
  public async run() {
    await DeviceDefaultAssociation.createMany([
      {
        deviceId: 1,
        defaultId: 1,
      },
    ])
  }
}
