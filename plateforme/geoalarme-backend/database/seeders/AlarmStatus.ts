import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import AlarmStatus from "../../app/Models/AlarmStatus";

export default class extends BaseSeeder {
  public async run() {
    await AlarmStatus.createMany([
      {
        id: 0,
        level: 0,
        description: 'Off',
      },
      {
        id: 1,
        level: 1,
        description: 'Aucun danger',
      },
      {
        id: 2,
        level: 2,
        description: 'Danger prononcé',
      },
      {
        id: 3,
        level: 3,
        description: 'Danger imminent',
      },
      {
        id: 4,
        level: 4,
        description: 'Evénement en cours',
      },
      {
        id: 5,
        level: 11,
        description: 'Aucun danger - forcé',
      },
      {
        id: 6,
        level: 12,
        description: 'Danger prononcé - forcé',
      },
      {
        id: 7,
        level: 13,
        description: 'Danger imminent - forcé',
      },
      {
        id: 8,
        level: 14,
        description: 'Evénement en cours - forcé',
      },
    ])
  }
}
