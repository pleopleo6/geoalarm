import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeviceStationAssociation from "../../app/Models/DeviceStationAssociation";

export default class extends BaseSeeder {
  public async run() {
    await DeviceStationAssociation.createMany([
      {
        deviceId: 1,
        stationId: 2
      },
      {
        deviceId: 2,
        stationId: 5
      },
      {
        deviceId: 3,
        stationId: 7
      },
      {
        deviceId: 4,
        stationId: 8
      },
      {
        deviceId: 5,
        stationId: 10
      },
      {
        deviceId: 6,
        stationId: 11
      },
      {
        deviceId: 7,
        stationId: 13
      },
      {
        deviceId: 8,
        stationId: 14
      },
      {
        deviceId: 9,
        stationId: 16
      },
      {
        deviceId: 10,
        stationId: 1
      },
      {
        deviceId: 11,
        stationId: 3
      },
      {
        deviceId: 12,
        stationId: 4
      },
      {
        deviceId: 13,
        stationId: 6
      },
      {
        deviceId: 14,
        stationId: 7
      },
      {
        deviceId: 15,
        stationId: 9
      },
      {
        deviceId: 16,
        stationId: 10
      },
      {
        deviceId: 17,
        stationId: 12
      },
      {
        deviceId: 18,
        stationId: 13
      },
      {
        deviceId: 19,
        stationId: 15
      },
      {
        deviceId: 20,
        stationId: 2
      },
      {
        deviceId: 21,
        stationId: 5
      },
      {
        deviceId: 22,
        stationId: 3
      },
      {
        deviceId: 23,
        stationId: 4
      },
      {
        deviceId: 24,
        stationId: 6
      },
      {
        deviceId: 25,
        stationId: 7
      },
      {
        deviceId: 26,
        stationId: 8
      },
      {
        deviceId: 27,
        stationId: 9
      },
      {
        deviceId: 28,
        stationId: 10
      },
      {
        deviceId: 29,
        stationId: 11
      },
      {
        deviceId: 30,
        stationId: 12
      },
      {
        deviceId: 31,
        stationId: 13
      },
      {
        deviceId: 32,
        stationId: 14
      },
      {
        deviceId: 33,
        stationId: 15
      },
      {
        deviceId: 34,
        stationId: 16
      },
      {
        deviceId: 35,
        stationId: 17
      },
      {
        deviceId: 36,
        stationId: 1
      },
      {
        deviceId: 37,
        stationId: 1
      },
      {
        deviceId: 38,
        stationId: 17
      },
      {
        deviceId: 39,
        stationId: 2
      },
      {
        deviceId: 40,
        stationId: 5
      },
      {
        deviceId: 41,
        stationId: 3
      },
      {
        deviceId: 42,
        stationId: 4
      },
      {
        deviceId: 43,
        stationId: 6
      },
      {
        deviceId: 44,
        stationId: 7
      },
      {
        deviceId: 45,
        stationId: 8
      },
      {
        deviceId: 46,
        stationId: 9
      },
      {
        deviceId: 47,
        stationId: 10
      },
      {
        deviceId: 48,
        stationId: 11
      },
      {
        deviceId: 49,
        stationId: 12
      },
      {
        deviceId: 50,
        stationId: 13
      },
      {
        deviceId: 51,
        stationId: 14
      },
      {
        deviceId: 52,
        stationId: 15
      },
      {
        deviceId: 53,
        stationId: 16
      },
      {
        deviceId: 54,
        stationId: 1
      },
      {
        deviceId: 55,
        stationId: 17
      },
      {
        deviceId: 56,
        stationId: 2
      },
      {
        deviceId: 57,
        stationId: 5
      },
      {
        deviceId: 58,
        stationId: 3
      },
      {
        deviceId: 59,
        stationId: 4
      },
      {
        deviceId: 60,
        stationId: 6
      },
      {
        deviceId: 61,
        stationId: 7
      },
      {
        deviceId: 62,
        stationId: 8
      },
      {
        deviceId: 63,
        stationId: 9
      },
      {
        deviceId: 64,
        stationId: 10
      },
      {
        deviceId: 65,
        stationId: 11
      },
      {
        deviceId: 66,
        stationId: 12
      },
      {
        deviceId: 67,
        stationId: 13
      },
      {
        deviceId: 68,
        stationId: 14
      },
      {
        deviceId: 69,
        stationId: 15
      },
      {
        deviceId: 70,
        stationId: 16
      },
      {
        deviceId: 71,
        stationId: 1
      },
      {
        deviceId: 72,
        stationId: 17
      },
      {
        deviceId: 73,
        stationId: 18
      },
      {
        deviceId: 74,
        stationId: 18
      },
      {
        deviceId: 75,
        stationId: 19
      },
      {
        deviceId: 76,
        stationId: 19
      },
      {
        deviceId: 77,
        stationId: 19
      },
      {
        deviceId: 78,
        stationId: 19
      },
      {
        deviceId: 79,
        stationId: 20
      },
      {
        deviceId: 80,
        stationId: 20
      },
      {
        deviceId: 81,
        stationId: 20
      },
      {
        deviceId: 82,
        stationId: 20
      },
      {
        deviceId: 83,
        stationId: 21
      },
      {
        deviceId: 84,
        stationId: 21
      },
      {
        deviceId: 85,
        stationId: 21
      },
      {
        deviceId: 86,
        stationId: 21
      },
      {
        deviceId: 87,
        stationId: 22
      },
      {
        deviceId: 88,
        stationId: 22
      },
      {
        deviceId: 89,
        stationId: 22
      },
      {
        deviceId: 90,
        stationId: 22
      },
      {
        deviceId: 91,
        stationId: 23
      },
      {
        deviceId: 92,
        stationId: 23
      },
      {
        deviceId: 93,
        stationId: 23
      },
      {
        deviceId: 94,
        stationId: 23
      },
      {
        deviceId: 95,
        stationId: 24
      },
      {
        deviceId: 96,
        stationId: 24
      },
      {
        deviceId: 97,
        stationId: 24
      },
      {
        deviceId: 98,
        stationId: 24
      },
      {
        deviceId: 99,
        stationId: 25
      },
      {
        deviceId: 100,
        stationId: 25
      },
      {
        deviceId: 101,
        stationId: 25
      },
      {
        deviceId: 102,
        stationId: 25
      },
      {
        deviceId: 103,
        stationId: 26
      },
      {
        deviceId: 104,
        stationId: 26
      },
      {
        deviceId: 105,
        stationId: 26
      },
      {
        deviceId: 106,
        stationId: 26
      },
      {
        deviceId: 107,
        stationId: 27
      },
      {
        deviceId: 108,
        stationId: 27
      },
      {
        deviceId: 109,
        stationId: 28
      },
      /*
      {
        deviceId: 110,
        stationId: 28
      },
      {
        deviceId: 111,
        stationId: 28
      },

       */
      {
        deviceId: 112,
        stationId: 28
      },
      {
        deviceId: 113,
        stationId: 27
      },
      {
        deviceId: 114,
        stationId: 27
      },
      {
        deviceId: 115,
        stationId: 29
      },
      /*
      {
        deviceId: 116,
        stationId: 29
      },
      {
        deviceId: 117,
        stationId: 29
      },
      {
        deviceId: 118,
        stationId: 29
      },

       */
      {
        deviceId: 119,
        stationId: 30
      },
      /*
      {
      deviceId: 120,
      stationId: 30
      },
      {
      deviceId: 121,
      stationId: 30
      },

       */
      {
        deviceId: 122,
        stationId: 30
      },
      {
        deviceId: 123,
        stationId: 27
      },
      {
        deviceId: 124,
        stationId: 27
      },
      {
        deviceId: 125,
        stationId: 27
      },
      {
        deviceId: 126,
        stationId: 31
      },
      /*
      {
      deviceId: 127,
      stationId: 31
      },
      {
      deviceId: 128,
      stationId: 31
      },
      {
      deviceId: 129,
      stationId: 31
      },
      {
      deviceId: 130,
      stationId: 32
      },
      {
      deviceId: 131,
      stationId: 32
      },

       */
      {
        deviceId: 132,
        stationId: 32
      },
      {
        deviceId: 133,
        stationId: 18
      },
      {
        deviceId: 134,
        stationId: 27
      },
      {
        deviceId: 135,
        stationId: 1
      },


// NEW DATAS
      {
        deviceId: 136,
        stationId: 33
      },
      {
        deviceId: 137,
        stationId: 33
      },
      {
        deviceId: 138,
        stationId: 33
      },
      {
        deviceId: 139,
        stationId: 34
      },
      {
        deviceId: 140,
        stationId: 34
      },
      {
        deviceId: 141,
        stationId: 34
      },
      {
        deviceId: 142,
        stationId: 34
      },
      {
        deviceId: 143,
        stationId: 35
      },
      {
        deviceId: 144,
        stationId: 35
      },
      {
        deviceId: 145,
        stationId: 35
      },
      {
        deviceId: 146,
        stationId: 35
      },
      {
        deviceId: 147,
        stationId: 36
      },
      {
        deviceId: 148,
        stationId: 36
      },
      {
        deviceId: 149,
        stationId: 36
      },
      {
        deviceId: 150,
        stationId: 36
      },
      {
        deviceId: 151,
        stationId: 37
      },
      {
        deviceId: 152,
        stationId: 37
      },
      {
        deviceId: 153,
        stationId: 38
      },
      {
        deviceId: 154,
        stationId: 38
      },
      {
        deviceId: 155,
        stationId: 38
      },
      {
        deviceId: 156,
        stationId: 38
      },
      {
        deviceId: 157,
        stationId: 39
      },
      {
        deviceId: 158,
        stationId: 39
      },
      {
        deviceId: 159,
        stationId: 39
      },
      {
        deviceId: 160,
        stationId: 39
      },
      {
        deviceId: 161,
        stationId: 40
      },
      {
        deviceId: 162,
        stationId: 40
      },
      {
        deviceId: 163,
        stationId: 41
      },
      {
        deviceId: 164,
        stationId: 41
      },
      {
        deviceId: 165,
        stationId: 41
      },
      {
        deviceId: 166,
        stationId: 41
      },
      {
        deviceId: 167,
        stationId: 41
      },
      {
        deviceId: 168,
        stationId: 41
      },
      {
        deviceId: 169,
        stationId: 42
      },
      {
        deviceId: 170,
        stationId: 42
      },
      {
        deviceId: 171,
        stationId: 42
      },
      {
        deviceId: 172,
        stationId: 42
      },
      {
        deviceId: 173,
        stationId: 43
      },
      {
        deviceId: 174,
        stationId: 43
      },
      {
        deviceId: 175,
        stationId: 44
      },
      {
        deviceId: 176,
        stationId: 44
      },
      {
        deviceId: 177,
        stationId: 44
      },
      {
        deviceId: 178,
        stationId: 44
      },
      {
        deviceId: 179,
        stationId: 44
      },
      {
        deviceId: 180,
        stationId: 45
      },
      {
        deviceId: 181,
        stationId: 45
      },
      {
        deviceId: 182,
        stationId: 45
      },
      {
        deviceId: 183,
        stationId: 45
      },
      {
        deviceId: 184,
        stationId: 46
      },
      {
        deviceId: 185,
        stationId: 46
      },
      {deviceId: 186, stationId: 47},
      {deviceId: 187, stationId: 47},
      {deviceId: 188, stationId: 47},
      {deviceId: 189, stationId: 47},
      {deviceId: 190, stationId: 48},
      {deviceId: 191, stationId: 48},
      {deviceId: 192, stationId: 48},
      {deviceId: 193, stationId: 48},
      {deviceId: 194, stationId: 48},
      {deviceId: 139, stationId: 34,},
      {deviceId: 140, stationId: 34,},
      {deviceId: 141, stationId: 34,},
      {deviceId: 136, stationId: 33,},
      {deviceId: 137, stationId: 33,},
      {deviceId: 138, stationId: 33,},

      {
        deviceId: 195,
        stationId: 49,
      },
      {
        deviceId: 196,
        stationId: 49,
      },
      {
        deviceId: 197,
        stationId: 49,
      },
      {
        deviceId: 198,
        stationId: 49,
      },

// Devices id 601 to 611: station is 50
      {
        deviceId: 199,
        stationId: 50,
      },
      {
        deviceId: 200,
        stationId: 50,
      },
      {
        deviceId: 201,
        stationId: 50,
      },
      {
        deviceId: 202,
        stationId: 50,
      },
      {
        deviceId: 203,
        stationId: 50,
      },
      {
        deviceId: 204,
        stationId: 50,
      },
      {
        deviceId: 205,
        stationId: 50,
      },
      {
        deviceId: 206,
        stationId: 50,
      },
      {
        deviceId: 207,
        stationId: 50,
      },
      {
        deviceId: 208,
        stationId: 50,
      },
      {
        deviceId: 209,
        stationId: 50,
      },

// Devices id 612 to 617: station is 51
      {
        deviceId: 210,
        stationId: 51,
      },
      {
        deviceId: 211,
        stationId: 51,
      },
      {
        deviceId: 212,
        stationId: 51,
      },
      {
        deviceId: 213,
        stationId: 51,
      },
      {
        deviceId: 214,
        stationId: 51,
      },
      {
        deviceId: 215,
        stationId: 51,
      },

// Devices id 618 to 623: station is 52
      {
        deviceId: 216,
        stationId: 52,
      },
      {
        deviceId: 217,
        stationId: 52,
      },
      {
        deviceId: 218,
        stationId: 52,
      },
      {
        deviceId: 219,
        stationId: 52,
      },
      {
        deviceId: 220,
        stationId: 52,
      },
      {
        deviceId: 221,
        stationId: 52,
      },

// Devices id 624 to 628: station is 53
      {
        deviceId: 222,
        stationId: 53,
      },
      {
        deviceId: 223,
        stationId: 53,
      },
      {
        deviceId: 224,
        stationId: 53,
      },
      {
        deviceId: 225,
        stationId: 53,
      },
      {
        deviceId: 226,
        stationId: 53,
      },

// Devices id 629 to 632: station is 54
      {
        deviceId: 227,
        stationId: 55,
      },
      {
        deviceId: 228,
        stationId: 55,
      },
      {
        deviceId: 229,
        stationId: 55,
      },
      {
        deviceId: 230,
        stationId: 55,
      },

// Device id 633: station is 55
      {
        deviceId: 231,
        stationId: 54,
      },
      {
        deviceId: 232,
        stationId: 47,
      }, {
        deviceId: 233,
        stationId: 47,
      }, {
        deviceId: 234,
        stationId: 47,
      },
      {
        deviceId: 235,
        stationId: 37,
      },

//
      {deviceId: 242, stationId: 57},
      {deviceId: 243, stationId: 57},
      {deviceId: 244, stationId: 57},
      {deviceId: 245, stationId: 57},
      {deviceId: 246, stationId: 57},
      {deviceId: 247, stationId: 57},
      {deviceId: 248, stationId: 57},
      {deviceId: 249, stationId: 57},

      // FG_SM1 devices linked to station 58
      {deviceId: 250, stationId: 58},
      {deviceId: 251, stationId: 58},
      {deviceId: 252, stationId: 58},
      {deviceId: 253, stationId: 58},
      {deviceId: 254, stationId: 58},
      {deviceId: 255, stationId: 58},
      {deviceId: 256, stationId: 58},
      {deviceId: 257, stationId: 58},
      {deviceId: 258, stationId: 58},
      {deviceId: 259, stationId: 58},

      // FG_SM2 devices linked to station 59
      {deviceId: 260, stationId: 59},
      {deviceId: 261, stationId: 59},
      {deviceId: 262, stationId: 59},
      {deviceId: 263, stationId: 59},
      {deviceId: 264, stationId: 59},
      {deviceId: 265, stationId: 59},
      {deviceId: 266, stationId: 59},
      {deviceId: 267, stationId: 59},
      {deviceId: 268, stationId: 59},

      // FG_F2 devices linked to station 60
      {deviceId: 269, stationId: 60},
      {deviceId: 270, stationId: 60},
      {deviceId: 271, stationId: 60},
      {deviceId: 272, stationId: 60},
      {deviceId: 273, stationId: 60},


    ])
  }
}
