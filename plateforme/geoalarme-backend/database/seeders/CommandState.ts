import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import CommandState from "../../app/Models/CommandState";


export default class extends BaseSeeder {
  public async run() {
    await CommandState.createMany([
      {
        id: 1,
        name: 'stored'
      },
      {
        id: 2,
        name: 'waiting'
      },
      {
        id: 3,
        name: 'confirmed'
      },
      {
        id: 4,
        name: 'rejected'
      },
      {
        id: 5,
        name: 'timeout'
      },
    ])
  }
}
