import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import Sensor from "../../app/Models/Sensor";

export default class extends BaseSeeder {
  public async run() {
    await Sensor.createMany([
      {
        id : 1,
        typeId : 1,
        name : "Station météo Vens",
        latitude: 46.081,
        longitude: 7.15575,
        altitude: 893,
        createdAt : "2023-08-09 10:51:12.978+02",
        updatedAt : "2023-08-09 10:51:12.978+02",
        sensorId : "0004A30B01078966",
      },
      {
        id : 2,
        typeId : 1,
        name : "Station météo La Fouly",
        latitude: 46.081,
        longitude: 7.15575,
        altitude: 893,
        createdAt : "2023-08-09 10:51:12.978+02",
        updatedAt : "2023-08-09 10:51:12.978+02",
        sensorId : "0004A30B01078E93",
      }
    ])
  }
}
