import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'


import SensorDataAssociation from "../../app/Models/SensorDataAssociation";

export default class extends BaseSeeder {
  public async run() {
    await SensorDataAssociation.createMany([
      {
        sensorId: 1,
        dataTypeId: 1,
      }, {
        sensorId: 1,
        dataTypeId: 2,
      }, {
        sensorId: 1,
        dataTypeId: 3,
      }, {
        sensorId: 1,
        dataTypeId: 4,
      }, {
        sensorId: 1,
        dataTypeId: 5,
      }, {
        sensorId: 1,
        dataTypeId: 6,
      }, {
        sensorId: 1,
        dataTypeId: 7,
      },
      {
        sensorId: 2,
        dataTypeId: 1,
      }, {
        sensorId: 2,
        dataTypeId: 2,
      }, {
        sensorId: 2,
        dataTypeId: 3,
      }, {
        sensorId: 2,
        dataTypeId: 4,
      }, {
        sensorId: 2,
        dataTypeId: 5,
      }, {
        sensorId: 2,
        dataTypeId: 6,
      }, {
        sensorId: 2,
        dataTypeId: 7,
      }
    ])
  }
}
