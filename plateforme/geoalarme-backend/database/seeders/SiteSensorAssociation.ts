import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import SiteSensorAssociation from "../../app/Models/SiteSensorAssociation";

export default class extends BaseSeeder {
  public async run () {
    await SiteSensorAssociation.createMany([
      {
        siteId: 3,
        sensorId: 2,
      },
      {
        siteId: 1,
        sensorId: 1,
      },
    ])
  }
}
