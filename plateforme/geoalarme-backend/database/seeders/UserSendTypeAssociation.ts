import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import UserSendTypeAssociation from "../../app/Models/UserSendTypeAssociation";

export default class extends BaseSeeder {
  public async run() {
    await UserSendTypeAssociation.createMany([
      {
        siteId: 3,
        userId: 2,
        data: {
          "eventIds": [1, 2, 3, 4, 5, 6],
          "messageIds": [1, 2, 3]
        },
      },
      {
        siteId: 3,
        userId: 2,
        data: {
          "eventIds": [3, 6],
          "messageIds": [4]
        },
      },
      {
        siteId: 9,
        userId: 11,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 12,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 30,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 31,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 32,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 33,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 34,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 35,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 36,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 37,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 38,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 39,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 40,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 41,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },
      {
        siteId: 9,
        userId: 42,
        data: {
          "eventIds": [1, 7],
          "messageIds": [0]
        },
      },

    ])
  }
}
