import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import EventTypeType from "../../app/Models/EventTypeType";

export default class extends BaseSeeder {
  public async run() {
    await EventTypeType.createMany([
      {
        name: 'distribution_list_technical_alarm',
        labelName: 'distribution_list_alarms',
      },
      {
        name: 'distribution_list_technical_default',
        labelName: 'distribution_list_technical_defaults',
      }, {
        id: 3,
        name: 'distribution_list_site_status',
        labelName: 'distribution_list_site_status',
      }
    ])
  }
}
