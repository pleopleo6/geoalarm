import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DefaultStatus from "../../app/Models/DefaultStatus";

export default class extends BaseSeeder {
  public async run() {
    await DefaultStatus.createMany([
      {
        id: 0,
        level: 0,
        description: 'Off',
      },
      {
        id: 1,
        level: 1,
        description: 'Aucun défaut',
      },
      {
        id: 2,
        level: 2,
        description: 'Défaut mineur',
      },
      {
        id: 3,
        level: 3,
        description: 'Défaut critique',
      },
      {
        id: 4,
        level: 4,
        description: 'Station en défaut',
      },
    ])
  }
}
