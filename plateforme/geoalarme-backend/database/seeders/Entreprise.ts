import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Entreprise from "../../app/Models/Entreprise";

export default class extends BaseSeeder {
  public async run() {
    await Entreprise.createMany([
      {
        id: 1,
        name: 'DMTE / Arr. 3',
        adresse: 'Rue du Léman 29 Bis',
        adresse_2: 'Case Postale 912',
        npa: 1920,
        city: 'Martigny 1',
        country: '027 607 11 22',
        phone: '027 607 11 22',
        email: 'contact@entreprise-test-3.com',
      },
      {
        id: 2,
        name: 'DMTE / Arr. 2',
        adresse: 'Rue du Léman 29 Bis',
        adresse_2: 'Case Postale 912',
        npa: 1920,
        city: 'Martigny 1',
        country: '027 607 11 22',
        phone: '027 607 11 22',
        email: 'contact@entreprise-test-2.com',
      },
      {
        id: 3,
        name: 'DMTE / Arr. 1',
        adresse: 'Rue du Léman 29 Bis',
        adresse_2: 'Case Postale 912',
        npa: 1920,
        city: 'Martigny 1',
        country: '027 607 11 22',
        phone: '027 607 11 22',
        email: 'contact@entreprise-test-1.com',
      },
      {
        id: 4,
        name: 'DGMR',
        adresse: 'Place de la Riponne 10',
        adresse_2: '-',
        npa: 1014,
        city: 'Lausanne',
        country: 'Switzerland',
        phone: '021 316 71 10',
        email: '-',
      },
      {
        id: 5,
        name: 'GEOAZIMUT',
        adresse: 'Route de Matran 220',
        adresse_2: '-',
        npa: 1752 ,
        city: 'Villars-sur-Glâne',
        country: 'Switzerland',
        phone: '026 422 14 76',
        email: '-',
      },
      {
        id: 6,
        name: 'Commune de Champéry',
        adresse: 'Rue du Village 46 - CP 54',
        adresse_2: 'CP 54',
        npa: 1874 ,
        city: 'Champéry',
        country: 'Switzerland',
        phone: '024 479 09 09',
        email: '-',
      }
    ])
  }
}
