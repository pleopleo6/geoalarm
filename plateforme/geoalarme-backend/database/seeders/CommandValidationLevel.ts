import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import CommandValidationLevel from "../../app/Models/CommandValidationLevel";

export default class extends BaseSeeder {
  public async run () {
    await CommandValidationLevel.createMany([
      {
        name: 'none',
        params: {},
      },
      {
        name: 'user_confirmation',
        params: {},
      },
    ])
  }
}
