import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeviceAlarmAssociation from "../../app/Models/DeviceAlarmAssociation";

export default class extends BaseSeeder {
  public async run() {
    await DeviceAlarmAssociation.createMany([
      {
        deviceId: 1,
        alarmId: 3,
      },
    ])
  }
}
