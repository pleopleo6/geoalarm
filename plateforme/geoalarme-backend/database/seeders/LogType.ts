import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import LogType from "../../app/Models/LogType";

export default class extends BaseSeeder {
  public async run() {
    await LogType.createMany([
      {
        name: 'Envoie de commande',
        description: 'Envoie de commande à une centrale',
      },
      {
        name: 'Reception de trame',
        description: 'Reception de trame de centrale',
      },
      {
        name: 'Enclanchement centrale alarme',
        description: 'Enclanchement centrale alarme',
      },
      {
        name: 'Commande envoyée',
        description: 'Envoie de commande',
      }
      ,
      {
        name: 'Connexion utilisateur',
        description: 'Connexion user',
      }
      ,
      {
        name: 'Deconnexion utilisateur',
        description: 'Deconnexion user',
      },
      {
        name: 'Reset centrale alarme',
        description: 'Reset centrale alarme',
      },
      {
        name: 'Orange centrale alarme',
        description: 'Orange',
      },
      {
        name: 'Modification utilisateur',
        description: 'Modif',
      },
      {
        name: 'Delete',
        description: 'Delete',
      },
      {
        name: 'Site Alarm',
        description: 'Site se trouve en alarme',
      },
      {
        name: 'Site Défaut',
        description: 'Site se trouve en défaut',
      },
      {
        name: 'Commande reçue',
        description: 'Réception de commande',
      },
      {
        name: 'Device Alarm',
        description: 'Device se trouve en alarme',
      },
      {
        name: 'Device Défaut',
        description: 'Site se trouve en défaut',
      },
    ])
  }
}
