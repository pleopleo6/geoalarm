import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DataType from "../../app/Models/DataType";

export default class extends BaseSeeder {
  public async run() {
    await DataType.createMany([
      {id: 1, name: 'Temp. air', unity: '°C', abbr: 'Temp'},
      {id: 2, name: 'Niv. batterie', unity: '%', abbr: 'Battery Level'},
      {id: 3, name: 'Pluviométrie', unity: 'mm', abbr: 'Rainfall'},
      {id: 4, name: 'Humidité air', unity: '%HR', abbr: 'Humidity'},
      {id: 5, name: 'Irradiance solaire', unity: 'W/m^2', abbr: 'Solar Irradiance'},
      {id: 6, name: 'Pression atm.', unity: 'hPa', abbr: 'Atmospheric Pressure'},
      {id: 7, name: 'rain_count', unity: 'mm', abbr: 'Rain Count'},
      {id: 8, name: 'Tension réseau', unity: 'V', abbr: 'MainV'},
      {id: 9, name: 'Tension batterie', unity: 'V', abbr: 'BattV'},
      {id: 10, name: 'Niveau Com', unity: 'dBi', abbr: 'Comlvl'},
      {id: 11, name: 'État', unity: '', abbr: 'Conflag'},
      {id: 12, name: 'État', unity: '', abbr: 'Feux'},
      {id: 13, name: 'État', unity: '', abbr: 'Câble'},
      {id: 14, name: 'État', unity: '', abbr: 'Caméra'},
      {id: 15, name: 'Niveau d\'eau', unity: 'm', abbr: 'WaterLvl'},
      {id: 16, name: 'État', unity: 'm', abbr: 'MainV'},
      {id: 17, name: 'État', unity: '', abbr: 'LiDAR status'},
    ])
  }
}
