import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import SignalStatus from "../../app/Models/SignalStatus";

export default class extends BaseSeeder {
  public async run() {
    await SignalStatus.createMany([
      {id: 1, level: 'SGNLOFF', levelNum: 0, description: 'Tout est éteint'},
      {id: 2, level: 'SGNLALM1', levelNum: 1, description: 'Signalisation pour alarme niveau 1'},
      {id: 3, level: 'SGNLALM2', levelNum: 2, description: 'Signalisation pour alarme niveau 2'},
      {id: 4, level: 'SGNLALM3', levelNum: 3, description: 'Signalisation pour alarme niveau 3'},
      {id: 5, level: 'SGNLALM4', levelNum: 4, description: 'Signalisation pour alarme niveau 4'},
      {id: 6, level: 'SGNLPAUSE', levelNum: 10, description: 'Signalisation pour arrêt temporaire (feu orange)'}
    ])
  }
}
