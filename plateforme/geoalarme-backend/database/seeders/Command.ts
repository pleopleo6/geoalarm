import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Command from "../../app/Models/Command";

export default class extends BaseSeeder {
  public async run() {
    await Command.createMany([

      // COMMANDES SUPER-ADMIN
      {
        innerId: 'A01',
        name: 'SITE_INIT',
        labelId: 'site_init_command_label',
        descriptionId: 'site_init_command_label',
        command: 'SITE_INIT',
        roleId: 1,
        isGlobal: true,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'A02',
        name: 'SITE_STATUS',
        labelId: 'site_status_command_label',
        descriptionId: 'site_status_command_desc',
        command: 'SITE_STATUS',
        roleId: 1,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'A03',
        name: 'STATION_STATUS',
        labelId: 'station_status_command_label',
        descriptionId: 'station_status_command_desc',
        command: 'STATION_STATUS',
        roleId: 1,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
          "station": 0,
          "param": 0,
        },
      },
      {
        innerId: 'A03',
        name: 'DEVICE_STATUS',
        labelId: 'device_status_command_label',
        descriptionId: 'device_status_command_desc',
        command: 'DEVICE_STATUS',
        roleId: 1,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
          "station": 0,
          "device": 0,
          "param": 0,
        },
      },
      {
        innerId: 'B01',
        name: 'RESET',
        labelId: 'reset_command_label',
        descriptionId: 'reset_command_desc',
        command: 'RESET',
        roleId: 1,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'B02',
        name: 'ORANGE_STATUS',
        labelId: 'orange_command_label',
        descriptionId: 'orange_command_desc',
        command: 'ORANGE_STATUS',
        roleId: 1,
        isGlobal: false,
        priority: 2,
        validationLevelId: 1,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'B03',
        name: 'ALARME',
        labelId: 'alarme_command_label',
        descriptionId: 'alarme_command_desc',
        command: 'ALARME',
        roleId: 1,
        isGlobal: false,
        priority: 3,
        validationLevelId: 2,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'B04',
        name: 'ALM_QUITTANCE',
        labelId: 'alm_quittance_command_label',
        descriptionId: 'alm_quittance_command_desc',
        command: 'ALM_QUITTANCE',
        roleId: 1,
        isGlobal: false,
        priority: 4,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'B05',
        name: 'DEFAULT_QUITTANCE',
        labelId: 'default_quittance_command_label',
        descriptionId: 'default_quittance_command_desc',
        command: 'DEFAULT_QUITTANCE',
        roleId: 1,
        isGlobal: false,
        priority: 4,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },

      // COMMANDES ADMIN
      {
        innerId: 'A01',
        name: 'SITE_INIT',
        labelId: 'site_init_command_label',
        descriptionId: 'site_init_command_label',
        command: 'SITE_INIT',
        roleId: 2,
        isGlobal: true,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'A02',
        name: 'SITE_STATUS',
        labelId: 'site_status_command_label',
        descriptionId: 'site_status_command_desc',
        command: 'SITE_STATUS',
        roleId: 2,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'A03',
        name: 'STATION_STATUS',
        labelId: 'station_status_command_label',
        descriptionId: 'station_status_command_desc',
        command: 'STATION_STATUS',
        roleId: 2,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
          "station": 0,
          "param": 0,
        },
      },
      {
        innerId: 'A03',
        name: 'DEVICE_STATUS',
        labelId: 'device_status_command_label',
        descriptionId: 'device_status_command_desc',
        command: 'DEVICE_STATUS',
        roleId: 2,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
          "station": 0,
          "device": 0,
          "param": 0,
        },
      },
      {
        innerId: 'B01',
        name: 'RESET',
        labelId: 'reset_command_label',
        descriptionId: 'reset_command_desc',
        command: 'RESET',
        roleId: 2,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'B02',
        name: 'ORANGE_STATUS',
        labelId: 'orange_command_label',
        descriptionId: 'orange_command_desc',
        command: 'ORANGE_STATUS',
        roleId: 2,
        isGlobal: false,
        priority: 2,
        validationLevelId: 1,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'B03',
        name: 'ALARME',
        labelId: 'alarme_command_label',
        descriptionId: 'alarme_command_desc',
        command: 'ALARME',
        roleId: 2,
        isGlobal: false,
        priority: 3,
        validationLevelId: 2,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'B04',
        name: 'ALM_QUITTANCE',
        labelId: 'alm_quittance_command_label',
        descriptionId: 'alm_quittance_command_desc',
        command: 'ALM_QUITTANCE',
        roleId: 2,
        isGlobal: false,
        priority: 4,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'B05',
        name: 'DEFAULT_QUITTANCE',
        labelId: 'default_quittance_command_label',
        descriptionId: 'default_quittance_command_desc',
        command: 'DEFAULT_QUITTANCE',
        roleId: 2,
        isGlobal: false,
        priority: 4,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },

      // COMMANDES OPERATOR
      {
        innerId: 'B01',
        name: 'RESET',
        labelId: 'reset_command_label',
        descriptionId: 'reset_command_desc',
        command: 'RESET',
        roleId: 3,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": null
        },
      },
      {
        innerId: 'B02',
        name: 'ORANGE_STATUS',
        labelId: 'orange_command_label',
        descriptionId: 'orange_command_desc',
        command: 'ORANGE_STATUS',
        roleId: 3,
        isGlobal: false,
        priority: 2,
        validationLevelId: 1,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'B03',
        name: 'ALARME',
        labelId: 'alarme_command_label',
        descriptionId: 'alarme_command_desc',
        command: 'ALARME',
        roleId: 3,
        isGlobal: false,
        priority: 3,
        validationLevelId: 2,
        params: {
          "uid": '',
          "param": 0,
        },
      },
      {
        innerId: 'B04',
        name: 'ALM_QUITTANCE',
        labelId: 'alm_quittance_command_label',
        descriptionId: 'alm_quittance_command_desc',
        command: 'ALM_QUITTANCE',
        roleId: 3,
        isGlobal: false,
        priority: 4,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'B05',
        name: 'DEFAULT_QUITTANCE',
        labelId: 'default_quittance_command_label',
        descriptionId: 'default_quittance_command_desc',
        command: 'DEFAULT_QUITTANCE',
        roleId: 3,
        isGlobal: false,
        priority: 4,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'A04',
        name: 'SITE_REFRESH',
        labelId: 'site_refresh_command_label',
        descriptionId: 'site_refresh_command_desc',
        command: 'SITE_REFRESH',
        roleId: 1,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'A04',
        name: 'SITE_REFRESH',
        labelId: 'site_refresh_command_label',
        descriptionId: 'site_refresh_command_desc',
        command: 'SITE_REFRESH',
        roleId: 2,
        isGlobal: true,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'A04',
        name: 'SITE_REFRESH',
        labelId: 'site_refresh_command_label',
        descriptionId: 'site_refresh_command_desc',
        command: 'SITE_REFRESH',
        roleId: 3,
        isGlobal: false,
        priority: 1,
        validationLevelId: 1,
        params: {
          "uid": '',
        },
      },
      {
        innerId: 'A04',
        name: 'SITE_ALMLVL',
        labelId: 'alarme_lvl_command_label',
        descriptionId: 'alarme_lvl_command_label_desc',
        command: 'SITE_ALMLVL',
        roleId: 1,
        isGlobal: false,
        priority: 2,
        validationLevelId: 2,
        params: {
          "uid": '',
          "param": 0
        },
      },
      {
        innerId: 'A04',
        name: 'SITE_ALMLVL',
        labelId: 'alarme_lvl_command_label',
        descriptionId: 'alarme_lvl_command_label_desc',
        command: 'SITE_ALMLVL',
        roleId: 2,
        isGlobal: false,
        priority: 2,
        validationLevelId: 2,
        params: {
          "uid": '',
          "param": 0
        },
      },
      {
        innerId: 'A05',
        name: 'SITE_SGNLLVL',
        labelId: 'sgnl_lvl_command_label',
        descriptionId: 'sgnl_lvl_command_label_desc',
        command: 'SITE_SGNLLVL',
        roleId: 1,
        isGlobal: false,
        priority: 2,
        validationLevelId: 1,
        params: {
          "uid": '',
          "param": 0
        },
      },
      {
        innerId: 'A05',
        name: 'SITE_SGNLLVL',
        labelId: 'sgnl_lvl_command_label',
        descriptionId: 'sgnl_lvl_command_label_desc',
        command: 'SITE_SGNLLVL',
        roleId: 2,
        isGlobal: false,
        priority: 2,
        validationLevelId: 1,
        params: {
          "uid": '',
          "param": 0
        },
      }
    ])
  }
}
