import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import CommunicationType from "../../app/Models/CommunicationType";

export default class extends BaseSeeder {
  public async run() {
    await CommunicationType.createMany([
      {
        name: 'dailyMessage',
        labelName: 'distribution_list_daily_message',
      },
      {
        name: 'email',
        labelName: 'distribution_list_message_type_email',
      },
      {
        name: 'notification',
        labelName: 'distribution_list_message_type_notification',
      },
      {
        name: 'sms',
        labelName: 'distribution_list_message_type_sms',
      },
      {
        name: 'vocalSms',
        labelName: 'distribution_list_message_type_vocal_sms',
      },
      {
        name: 'threema',
        labelName: 'distribution_list_message_type_threema',
      },
    ])
  }
}
