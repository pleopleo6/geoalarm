import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeviceDataAssociation from "../../app/Models/DeviceDataAssociation"

export default class extends BaseSeeder {
  public async run() {
    await DeviceDataAssociation.createMany([
      {
        deviceId: 1,
        dataTypeId: 12
      },
      {
        deviceId: 2,
        dataTypeId: 12
      },
      {
        deviceId: 3,
        dataTypeId: 12
      },
      {
        deviceId: 4,
        dataTypeId: 12
      },
      {
        deviceId: 5,
        dataTypeId: 12
      },
      {
        deviceId: 6,
        dataTypeId: 12
      },
      {
        deviceId: 7,
        dataTypeId: 12
      },
      {
        deviceId: 8,
        dataTypeId: 12
      },
      {
        deviceId: 9,
        dataTypeId: 12
      },
      {
        deviceId: 10,
        dataTypeId: 12
      },
      {
        deviceId: 11,
        dataTypeId: 13
      },
      {
        deviceId: 12,
        dataTypeId: 13
      },
      {
        deviceId: 13,
        dataTypeId: 13
      },
      {
        deviceId: 14,
        dataTypeId: 13
      },
      {
        deviceId: 15,
        dataTypeId: 13
      },
      {
        deviceId: 16,
        dataTypeId: 13
      },
      {
        deviceId: 17,
        dataTypeId: 13
      },
      {
        deviceId: 18,
        dataTypeId: 13
      },
      {
        deviceId: 19,
        dataTypeId: 13
      },
      {
        deviceId: 20,
        dataTypeId: 9
      },
      {
        deviceId: 21,
        dataTypeId: 9
      },
      {
        deviceId: 22,
        dataTypeId: 9
      },
      {
        deviceId: 23,
        dataTypeId: 9
      },
      {
        deviceId: 24,
        dataTypeId: 9
      },
      {
        deviceId: 25,
        dataTypeId: 9
      },
      {
        deviceId: 26,
        dataTypeId: 9
      },
      {
        deviceId: 27,
        dataTypeId: 9
      },
      {
        deviceId: 28,
        dataTypeId: 9
      },
      {
        deviceId: 29,
        dataTypeId: 9
      },
      {
        deviceId: 30,
        dataTypeId: 9
      },
      {
        deviceId: 31,
        dataTypeId: 9
      },
      {
        deviceId: 32,
        dataTypeId: 9
      },
      {
        deviceId: 33,
        dataTypeId: 9
      },
      {
        deviceId: 34,
        dataTypeId: 9
      },
      {
        deviceId: 35,
        dataTypeId: 9
      },
      {
        deviceId: 36,
        dataTypeId: 9
      },
      {
        deviceId: 37,
        dataTypeId: 8
      },
      {
        deviceId: 38,
        dataTypeId: 8
      },
      {
        deviceId: 39,
        dataTypeId: 10
      },
      {
        deviceId: 40,
        dataTypeId: 10
      },
      {
        deviceId: 41,
        dataTypeId: 10
      },
      {
        deviceId: 42,
        dataTypeId: 10
      },
      {
        deviceId: 43,
        dataTypeId: 10
      },
      {
        deviceId: 44,
        dataTypeId: 10
      },
      {
        deviceId: 45,
        dataTypeId: 10
      },
      {
        deviceId: 46,
        dataTypeId: 10
      },
      {
        deviceId: 47,
        dataTypeId: 10
      },
      {
        deviceId: 48,
        dataTypeId: 10
      },
      {
        deviceId: 49,
        dataTypeId: 10
      },
      {
        deviceId: 50,
        dataTypeId: 10
      },
      {
        deviceId: 51,
        dataTypeId: 10
      },
      {
        deviceId: 52,
        dataTypeId: 10
      },
      {
        deviceId: 53,
        dataTypeId: 10
      },
      {
        deviceId: 54,
        dataTypeId: 10
      },
      {
        deviceId: 55,
        dataTypeId: 10
      },
      {
        deviceId: 56,
        dataTypeId: 11
      },
      {
        deviceId: 57,
        dataTypeId: 11
      },
      {
        deviceId: 58,
        dataTypeId: 11
      },
      {
        deviceId: 59,
        dataTypeId: 11
      },
      {
        deviceId: 60,
        dataTypeId: 11
      },
      {
        deviceId: 61,
        dataTypeId: 11
      },
      {
        deviceId: 62,
        dataTypeId: 11
      },
      {
        deviceId: 63,
        dataTypeId: 11
      },
      {
        deviceId: 64,
        dataTypeId: 11
      },
      {
        deviceId: 65,
        dataTypeId: 11
      },
      {
        deviceId: 66,
        dataTypeId: 11
      },
      {
        deviceId: 67,
        dataTypeId: 11
      },
      {
        deviceId: 68,
        dataTypeId: 11
      },
      {
        deviceId: 69,
        dataTypeId: 11
      },
      {
        deviceId: 70,
        dataTypeId: 11
      },
      {
        deviceId: 71,
        dataTypeId: 11
      },
      {
        deviceId: 72,
        dataTypeId: 11
      },
      {
        deviceId: 73,
        dataTypeId: 8
      },
      {
        deviceId: 74,
        dataTypeId: 9
      },
      {
        deviceId: 75,
        dataTypeId: 13
      },
      {
        deviceId: 76,
        dataTypeId: 9
      },
      {
        deviceId: 77,
        dataTypeId: 10
      },
      {
        deviceId: 78,
        dataTypeId: 11
      },
      {
        deviceId: 79,
        dataTypeId: 13
      },
      {
        deviceId: 80,
        dataTypeId: 9
      },
      {
        deviceId: 81,
        dataTypeId: 10
      },
      {
        deviceId: 82,
        dataTypeId: 11
      },
      {
        deviceId: 83,
        dataTypeId: 12
      },
      {
        deviceId: 84,
        dataTypeId: 9
      },
      {
        deviceId: 85,
        dataTypeId: 10
      },
      {
        deviceId: 86,
        dataTypeId: 11
      },
      {
        deviceId: 87,
        dataTypeId: 12
      },
      {
        deviceId: 88,
        dataTypeId: 9
      },
      {
        deviceId: 89,
        dataTypeId: 10
      },
      {
        deviceId: 90,
        dataTypeId: 11
      },
      {
        deviceId: 91,
        dataTypeId: 12
      },
      {
        deviceId: 92,
        dataTypeId: 9
      },
      {
        deviceId: 93,
        dataTypeId: 10
      },
      {
        deviceId: 94,
        dataTypeId: 11
      },
      {
        deviceId: 95,
        dataTypeId: 12
      },
      {
        deviceId: 96,
        dataTypeId: 9
      },
      {
        deviceId: 97,
        dataTypeId: 10
      },
      {
        deviceId: 98,
        dataTypeId: 11
      },
      {
        deviceId: 99,
        dataTypeId: 14
      },
      {
        deviceId: 100,
        dataTypeId: 9
      },
      {
        deviceId: 101,
        dataTypeId: 10
      },
      {
        deviceId: 102,
        dataTypeId: 11
      },
      {
        deviceId: 103,
        dataTypeId: 14
      },
      {
        deviceId: 104,
        dataTypeId: 9
      },
      {
        deviceId: 105,
        dataTypeId: 10
      },
      {
        deviceId: 106,
        dataTypeId: 11
      },
      {
        deviceId: 107,
        dataTypeId: 8
      },
      {
        deviceId: 108,
        dataTypeId: 9
      },
      {
        deviceId: 109,
        dataTypeId: 13
      },
      /*
      {
        deviceId: 110,
        dataTypeId: 9
      },
      {
        deviceId: 111,
        dataTypeId: 10
      },
       */
      {
        deviceId: 112,
        dataTypeId: 11
      },
      {
        deviceId: 113,
        dataTypeId: 13
      },
      {
        deviceId: 114,
        dataTypeId: 12
      },
      {
        deviceId: 115,
        dataTypeId: 14
      },
      {
        deviceId: 116,
        dataTypeId: 9
      },
      {
        deviceId: 117,
        dataTypeId: 10
      },
      {
        deviceId: 118,
        dataTypeId: 11
      },
      /*
       */
      {
        deviceId: 119,
        dataTypeId: 13
      },
      /*
      {
      deviceId: 120,
      dataTypeId: 9
      },
      {
      deviceId: 121,
      dataTypeId: 10
      },

       */
      {
        deviceId: 122,
        dataTypeId: 11
      },
      {
        deviceId: 123,
        dataTypeId: 13
      },
      {
        deviceId: 124,
        dataTypeId: 12
      },
      {
        deviceId: 125,
        dataTypeId: 12
      },
      {
        deviceId: 126,
        dataTypeId: 14
      },
      /*
      {
      deviceId: 127,
      dataTypeId: 9
      },
      {
      deviceId: 128,
      dataTypeId: 10
      },
      {
      deviceId: 129,
      dataTypeId: 11
      },
      {
      deviceId: 130,
      dataTypeId: 9
      },
      {
      deviceId: 131,
      dataTypeId: 10
      },
      /*
       */
      {
        deviceId: 132,
        dataTypeId: 11
      }
      ,
      {
        deviceId: 133,
        dataTypeId: 13
      }
      ,
      {
        deviceId: 134,
        dataTypeId: 13
      }
      ,
      {
        deviceId: 135,
        dataTypeId: 13
      },


// NEW DATA
      {
        deviceId: 136,
        dataTypeId: 15
      },
      {
        deviceId: 137,
        dataTypeId: 8
      },
      {
        deviceId: 138,
        dataTypeId: 9
      },

      {
        deviceId: 139,
        dataTypeId: 12
      },
      {
        deviceId: 140,
        dataTypeId: 9
      },
      {
        deviceId: 141,
        dataTypeId: 10
      },
      {
        deviceId: 142,
        dataTypeId: 11
      },
      {
        deviceId: 143,
        dataTypeId: 12
      },
      {
        deviceId: 144,
        dataTypeId: 9
      },
      {
        deviceId: 145,
        dataTypeId: 10
      },
      {
        deviceId: 146,
        dataTypeId: 11
      },
      {
        deviceId: 147,
        dataTypeId: 14
      },
      {
        deviceId: 148,
        dataTypeId: 9
      },
      {
        deviceId: 149,
        dataTypeId: 10
      },
      {
        deviceId: 150,
        dataTypeId: 11
      },
      {
        deviceId: 151,
        dataTypeId: 12
      },
      {
        deviceId: 152,
        dataTypeId: 9
      },
      {
        deviceId: 153,
        dataTypeId: 13
      },
      {
        deviceId: 154,
        dataTypeId: 9
      },
      {
        deviceId: 155,
        dataTypeId: 10
      },
      {
        deviceId: 156,
        dataTypeId: 11
      },
      {
        deviceId: 157,
        dataTypeId: 12
      },
      {
        deviceId: 158,
        dataTypeId: 9
      },
      {
        deviceId: 159,
        dataTypeId: 10
      },
      {
        deviceId: 160,
        dataTypeId: 11
      },
      {
        deviceId: 161,
        dataTypeId: 12
      },
      {
        deviceId: 162,
        dataTypeId: 9
      },
      {
        deviceId: 163,
        dataTypeId: 15
      },
      {
        deviceId: 164,
        dataTypeId: 13
      },
      {
        deviceId: 165,
        dataTypeId: 13
      },
      {
        deviceId: 166,
        dataTypeId: 9
      },
      {
        deviceId: 167,
        dataTypeId: 10
      },
      {
        deviceId: 168,
        dataTypeId: 11
      },
      {
        deviceId: 169,
        dataTypeId: 12
      },
      {
        deviceId: 170,
        dataTypeId: 9
      },
      {
        deviceId: 171,
        dataTypeId: 10
      },
      {
        deviceId: 172,
        dataTypeId: 11
      },
      {
        deviceId: 173,
        dataTypeId: 14
      },
      {
        deviceId: 174,
        dataTypeId: 9
      },
      {
        deviceId: 175,
        dataTypeId: 13
      },
      {
        deviceId: 176,
        dataTypeId: 13
      },
      {
        deviceId: 177,
        dataTypeId: 15
      },
      {
        deviceId: 178,
        dataTypeId: 12
      },
      {
        deviceId: 179,
        dataTypeId: 9
      },
      {
        deviceId: 180,
        dataTypeId: 12
      },
      {
        deviceId: 181,
        dataTypeId: 9
      },
      {
        deviceId: 182,
        dataTypeId: 10
      },
      {
        deviceId: 183,
        dataTypeId: 11
      },
      {
        deviceId: 184,
        dataTypeId: 14
      },
      {
        deviceId: 185,
        dataTypeId: 9
      },

//
      {deviceId: 186, dataTypeId: 12},
      {deviceId: 187, dataTypeId: 12},
      {deviceId: 188, dataTypeId: 8},
      {deviceId: 189, dataTypeId: 9},
      {deviceId: 190, dataTypeId: 13},
      {deviceId: 191, dataTypeId: 8},
      {deviceId: 192, dataTypeId: 9},
      {deviceId: 193, dataTypeId: 10},
      {deviceId: 194, dataTypeId: 11},
      {deviceId: 195, dataTypeId: 8},
      {deviceId: 196, dataTypeId: 9},
      {deviceId: 197, dataTypeId: 10},
      {deviceId: 198, dataTypeId: 11},
      {deviceId: 199, dataTypeId: 13},
      {deviceId: 200, dataTypeId: 13},
      {deviceId: 201, dataTypeId: 17},
      {deviceId: 202, dataTypeId: 17},
      {deviceId: 203, dataTypeId: 17},
      {deviceId: 204, dataTypeId: 14},
      {deviceId: 205, dataTypeId: 12},
      {deviceId: 206, dataTypeId: 8},
      {deviceId: 207, dataTypeId: 9},
      {deviceId: 208, dataTypeId: 10},
      {deviceId: 209, dataTypeId: 11},
      {deviceId: 210, dataTypeId: 12},
      {deviceId: 211, dataTypeId: 12},
      {deviceId: 212, dataTypeId: 8},
      {deviceId: 213, dataTypeId: 9},
      {deviceId: 214, dataTypeId: 10},
      {deviceId: 215, dataTypeId: 11},
      {deviceId: 216, dataTypeId: 12},
      {deviceId: 217, dataTypeId: 12},
      {deviceId: 218, dataTypeId: 8},
      {deviceId: 219, dataTypeId: 9},
      {deviceId: 220, dataTypeId: 10},
      {deviceId: 221, dataTypeId: 11},
      {deviceId: 222, dataTypeId: 12},
      {deviceId: 223, dataTypeId: 8},
      {deviceId: 224, dataTypeId: 9},
      {deviceId: 225, dataTypeId: 10},
      {deviceId: 226, dataTypeId: 11},
      {deviceId: 227, dataTypeId: 8},
      {deviceId: 228, dataTypeId: 9},
      {deviceId: 229, dataTypeId: 10},
      {deviceId: 230, dataTypeId: 11},
      {deviceId: 231, dataTypeId: 14},
      {deviceId: 232, dataTypeId: 14},
      {deviceId: 233, dataTypeId: 11},
      {deviceId: 234, dataTypeId: 14},
      {deviceId: 234, dataTypeId: 14},
      {deviceId: 235, dataTypeId: 14},
      {id: 242, deviceId: 242, dataTypeId: 12},  // FG_CA_F1_CMDALM
      {id: 243, deviceId: 243, dataTypeId: 12},  // FG_CA_F1_BTALM
      {id: 244, deviceId: 244, dataTypeId: 12},  // FG_CA_F1_FEUX
      {id: 245, deviceId: 245, dataTypeId: 12},  // FG_CA_F1_SIRENE
      {id: 246, deviceId: 246, dataTypeId: 16},  // FG_CA_F1_MAINV
      {id: 247, deviceId: 247, dataTypeId: 9},   // FG_CA_F1_BATTV
      {id: 248, deviceId: 248, dataTypeId: 10},  // FG_CA_F1_COMLVL
      {id: 249, deviceId: 249, dataTypeId: 11},  // FG_CA_F1_COMFLAG

      {id: 250, deviceId: 250, dataTypeId: 13},  // FG_SM1_CABLE
      {id: 251, deviceId: 251, dataTypeId: 17},  // FG_SM1_LIDAR1
      {id: 252, deviceId: 252, dataTypeId: 17},  // FG_SM1_LIDAR2
      {id: 253, deviceId: 253, dataTypeId: 17},  // FG_SM1_LIDAR3
      {id: 254, deviceId: 254, dataTypeId: 17},  // FG_SM1_LIDAR4
      {id: 255, deviceId: 255, dataTypeId: 17},  // FG_SM1_LIDAR5
      {id: 256, deviceId: 256, dataTypeId: 17},  // FG_SM1_LIDAR6
      {id: 257, deviceId: 257, dataTypeId: 9},   // FG_SM1_BATTV
      {id: 258, deviceId: 258, dataTypeId: 10},  // FG_SM1_COMLVL
      {id: 259, deviceId: 259, dataTypeId: 11},  // FG_SM1_COMFLAG

      {id: 260, deviceId: 260, dataTypeId: 17},  // FG_SM2_LIDAR1
      {id: 261, deviceId: 261, dataTypeId: 17},  // FG_SM2_LIDAR2
      {id: 262, deviceId: 262, dataTypeId: 17},  // FG_SM2_LIDAR3
      {id: 263, deviceId: 263, dataTypeId: 17},  // FG_SM2_LIDAR4
      {id: 264, deviceId: 264, dataTypeId: 17},  // FG_SM2_LIDAR5
      {id: 265, deviceId: 265, dataTypeId: 17},  // FG_SM2_LIDAR6
      {id: 266, deviceId: 266, dataTypeId: 9},   // FG_SM2_BATTV
      {id: 267, deviceId: 267, dataTypeId: 10},  // FG_SM2_COMLVL
      {id: 268, deviceId: 268, dataTypeId: 11},  // FG_SM2_COMFLAG

      {id: 269, deviceId: 269, dataTypeId: 12},  // FG_F2_FEUX
      {id: 270, deviceId: 270, dataTypeId: 12},  // FG_F2_SIRENE
      {id: 271, deviceId: 271, dataTypeId: 9},   // FG_F2_BATTV
      {id: 272, deviceId: 272, dataTypeId: 10},  // FG_F2_COMLVL
      {id: 273, deviceId: 273, dataTypeId: 11},  // FG_F2_COMFLAG

    ])
  }
}
