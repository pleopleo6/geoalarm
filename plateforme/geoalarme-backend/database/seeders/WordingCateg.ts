import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import WordingCateg from '../../app/Models/WordingCateg';

export default class extends BaseSeeder {
  public async run () {
    await WordingCateg.createMany([
      {
        name: 'any',
      },
      {
        name: 'mobile',
      },
      {
        name: 'desktop',
      },
      {
        name: 'back-office',
      },
    ])
  }
}
