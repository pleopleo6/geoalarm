import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import SiteAlarmAssociation from '../../app/Models/SiteAlarmAssociation';

export default class extends BaseSeeder {
  public async run () {
    await SiteAlarmAssociation.createMany([
      {
        siteId: 3,
        alarmId: 3,
      },
      {
        siteId: 3,
        alarmId: 2,
      },
      {
        siteId: 3,
        alarmId: 2,
      },
      {
        siteId: 3,
        alarmId: 2,
      },
      {
        siteId: 3,
        alarmId: 1,
      },
      {
        siteId: 3,
        alarmId: 1,
      },
      {
        siteId: 3,
        alarmId: 1,
      },
    ])
  }
}
