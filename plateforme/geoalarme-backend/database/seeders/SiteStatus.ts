import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import SiteStatus from "../../app/Models/SiteStatus";

export default class extends BaseSeeder {
  public async run() {
    await SiteStatus.createMany([
      {
        name: "Actif",
        levelNum: 1
      },
      {
        name: "Inactif",
        levelNum: 0
      },
      {
        name: "Hors-service",
        levelNum: 2
      },
      {
        name: "Inatteignable",
        levelNum: 3
      },
    ])
  }
}
