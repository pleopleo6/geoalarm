import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import StationStatus from "../../app/Models/StationStatus";

export default class extends BaseSeeder {
  public async run () {
    await StationStatus.createMany([
      {
        id : 0,
        name: "Inactif",
      },
      {
        id : 1,
        name: "Fonctionnel",
      },
      {
        id : 2,
        name: 'Désactivé',
      },
      {
        id : 3,
        name: 'Hors-service',
      },
    ])
  }
}
