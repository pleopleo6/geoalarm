import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Language from "../../app/Models/Language";

export default class extends BaseSeeder {
  public async run () {
    await Language.createMany([
      {
        code: 'FR',
      },
      {
        code: 'EN',
      },
      {
        code: 'DE',
      },
      {
        code: 'IT',
      }
    ])
  }
}
