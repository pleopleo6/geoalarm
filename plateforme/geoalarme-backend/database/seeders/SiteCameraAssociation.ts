import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import SiteCameraAssociation from "../../app/Models/SiteCameraAssociation";

export default class extends BaseSeeder {
  public async run() {
    await SiteCameraAssociation.createMany([
      {
        siteId: 2,
        cameraId: 1,
      },
      {
        siteId: 2,
        cameraId: 2,
      },
      {
        siteId: 3,
        cameraId: 3,
      },
      {
        siteId: 3,
        cameraId: 4,
      },


      {
        siteId: 4,
        cameraId: 5,
      },
      {
        siteId: 6,
        cameraId: 6,
      },
      {
        siteId: 7,
        cameraId: 7,
      },
      {
        siteId: 5,
        cameraId: 8,
      },

      {
        siteId: 9,
        cameraId: 9,
      },
      {
        siteId: 9,
        cameraId: 10,
      },
      {
        siteId: 9,
        cameraId: 11,
      },
      {
        siteId: 10,
        cameraId: 12,
      },
      {
        siteId: 1,
        cameraId: 13,
      },
    ])
  }
}
