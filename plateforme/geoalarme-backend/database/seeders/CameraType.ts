import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import CameraType from "../../app/Models/CameraType";

export default class extends BaseSeeder {
  public async run() {
    await CameraType.createMany([
      {
        name: 'Mobotix',
      },
      {
        name: 'Axis',
      },
      {
        name: 'Daheng',
      },
    ])
  }
}
