import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeviceStatus from "../../app/Models/DeviceStatus";

export default class extends BaseSeeder {
  public async run() {
    await DeviceStatus.createMany([
      {
        id : 0,
        name: "Inactif",
      },
      {
        id : 1,
        name: "Fonctionnel",
      },
      {
        id : 2,
        name: "Désactivé",
      },
      {
        id : 3,
        name: "Hors-service",
      },

    ])
  }
}
