import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import SiteStationAssociation from "../../app/Models/SiteStationAssociation";

export default class extends BaseSeeder {
  public async run() {
    await SiteStationAssociation.createMany([
      {
        stationId: 1,
        siteId: 1,
      },
      {
        stationId: 2,
        siteId: 1,
      },
      {
        stationId: 3,
        siteId: 1,
      },
      {
        stationId: 4,
        siteId: 1,
      },
      {
        stationId: 5,
        siteId: 1,
      },
      {
        stationId: 6,
        siteId: 1,
      },
      {
        stationId: 7,
        siteId: 1,
      },
      {
        stationId: 8,
        siteId: 1,
      },
      {
        stationId: 9,
        siteId: 1,
      },
      {
        stationId: 10,
        siteId: 1,
      },
      {
        stationId: 11,
        siteId: 1,
      },
      {
        stationId: 12,
        siteId: 1,
      },
      {
        stationId: 13,
        siteId: 1,
      },
      {
        stationId: 14,
        siteId: 1,
      },
      {
        stationId: 15,
        siteId: 1,
      },
      {
        stationId: 16,
        siteId: 1,
      },
      {
        stationId: 17,
        siteId: 1,
      },
      {
        stationId: 18,
        siteId: 2,
      },
      {
        stationId: 19,
        siteId: 2,
      },
      {
        stationId: 20,
        siteId: 2,
      },
      {
        stationId: 21,
        siteId: 2,
      },
      {
        stationId: 22,
        siteId: 2,
      },
      {
        stationId: 23,
        siteId: 2,
      },
      {
        stationId: 24,
        siteId: 2,
      },
      {
        stationId: 25,
        siteId: 2,
      },
      {
        stationId: 26,
        siteId: 2,
      },
      {
        stationId: 27,
        siteId: 3,
      },
      {
        stationId: 28,
        siteId: 3,
      },
      {
        stationId: 29,
        siteId: 3,
      },
      {
        stationId: 30,
        siteId: 3,
      },
      {
        stationId: 31,
        siteId: 3,
      },
      {
        stationId: 32,
        siteId: 3,
      },

      // st barth
      {
        stationId: 33,
        siteId: 4,
      },
      {
        stationId: 34,
        siteId: 4,
      },
      {
        stationId: 35,
        siteId: 4,
      },
      {
        stationId: 36,
        siteId: 4,
      },


      // Echerche
      {
        stationId: 37,
        siteId: 5,
      },
      {
        stationId: 38,
        siteId: 5,
      },
      {
        stationId: 39,
        siteId: 5,
      },

      // FRACHEY
      {
        stationId: 40,
        siteId: 6,
      },
      {
        stationId: 41,
        siteId: 6,
      },
      {
        stationId: 42,
        siteId: 6,
      },
      {
        stationId: 43,
        siteId: 6,
      },

      // PISSOT
      {
        stationId: 44,
        siteId: 7,
      },
      {
        stationId: 45,
        siteId: 7,
      },
      {
        stationId: 47,
        siteId: 8,
      },
      {
        stationId: 48,
        siteId: 8,
      },
      {
        stationId: 49,
        siteId: 9,
      },
      {
        stationId: 50,
        siteId: 9,
      },
      {
        stationId: 51,
        siteId: 9,
      },
      {
        stationId: 52,
        siteId: 9,
      },
      {
        stationId: 53,
        siteId: 9,
      },
      {
        stationId: 54,
        siteId: 9,
      },
      {
        stationId: 55,
        siteId: 9,
      },

      {
        stationId: 56,
        siteId: 8,
      }
      ,

      {
        stationId: 57,
        siteId: 10,
      },

      {
        stationId: 58,
        siteId: 10,
      },

      {
        stationId: 59,
        siteId: 10,
      },

      {
        stationId: 60,
        siteId: 10,
      }
    ])
  }
}
