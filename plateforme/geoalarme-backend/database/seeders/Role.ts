import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Role from "../../app/Models/Role";

export default class extends BaseSeeder {
  public async run () {
    await Role.createMany([
      {
        name: 'super-administrator',
        code: 'ROLE_SUPER_ADMIN',
        description: 'Utilisateur super-admin',
      },
      {
        name: 'administrator',
        code: 'ROLE_SITE_ADMIN',
        description: 'Utilisateur admin',
      },
      {
        name: 'operator',
        code: 'ROLE_OPERATOR',
        description: 'Utilisateur operateur',
      },
      {
        name: 'observer',
        code: 'ROLE_OBSERVER',
        description: 'Utilisateur observateur',
      }
    ])
  }
}
