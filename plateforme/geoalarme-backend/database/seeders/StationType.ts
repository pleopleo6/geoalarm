import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import StationType from "../../app/Models/StationType";

export default class extends BaseSeeder {
  public async run () {
    await StationType.createMany([
      {
        id : 1,
        name: "Centrale d'alarme",
      },
      {
        name: 'Station de mesure',
      },
      {
        name: 'Signalisation',
      },
      {
        name: 'Caméra',
      },
      {
        name: 'Autre',
      }
    ])
  }
}
