import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeviceData from "../../app/Models/DeviceData";

export default class extends BaseSeeder {
  public async run () {

    const { DateTime } = require('luxon');
    const currentTime = DateTime.local();

    await DeviceData.createMany([
      {
        data: 34,
        timestamp: currentTime,
        device_data_association_id: 1
      },
      {
        data: 89,
        timestamp: currentTime,
        device_data_association_id: 2
      },
      {
        data: 56,
        timestamp: currentTime,
        device_data_association_id: 3
      },
      {
        data: 10,
        timestamp: currentTime,
        device_data_association_id: 4
      },
      {
        data: 72,
        timestamp: currentTime,
        device_data_association_id: 5
      },
      {
        data: 41,
        timestamp: currentTime,
        device_data_association_id: 6
      },
      {
        data: 17,
        timestamp: currentTime,
        device_data_association_id: 7
      },
      {
        data: 93,
        timestamp: currentTime,
        device_data_association_id: 8
      },
      {
        data: 25,
        timestamp: currentTime,
        device_data_association_id: 9
      },
      {
        data: 62,
        timestamp: currentTime,
        device_data_association_id: 10
      },
      {
        data: 88,
        timestamp: currentTime,
        device_data_association_id: 11
      },
      {
        data: 16,
        timestamp: currentTime,
        device_data_association_id: 12
      },
      {
        data: 59,
        timestamp: currentTime,
        device_data_association_id: 13
      },
      {
        data: 49,
        timestamp: currentTime,
        device_data_association_id: 14
      },
      {
        data: 83,
        timestamp: currentTime,
        device_data_association_id: 15
      },
      {
        data: 30,
        timestamp: currentTime,
        device_data_association_id: 16
      },
      {
        data: 60,
        timestamp: currentTime,
        device_data_association_id: 17
      },
      {
        data: 57,
        timestamp: currentTime,
        device_data_association_id: 18
      },
      {
        data: 14,
        timestamp: currentTime,
        device_data_association_id: 19
      },
      {
        data: 20,
        timestamp: currentTime,
        device_data_association_id: 20
      },
      {
        data: 73,
        timestamp: currentTime,
        device_data_association_id: 21
      },
      {
        data: 28,
        timestamp: currentTime,
        device_data_association_id: 22
      },
      {
        data: 94,
        timestamp: currentTime,
        device_data_association_id: 23
      },
      {
        data: 3,
        timestamp: currentTime,
        device_data_association_id: 24
      },
      {
        data: 40,
        timestamp: currentTime,
        device_data_association_id: 25
      },
      {
        data: 54,
        timestamp: currentTime,
        device_data_association_id: 26
      },
      {
        data: 75,
        timestamp: currentTime,
        device_data_association_id: 27
      },
      {
        data: 50,
        timestamp: currentTime,
        device_data_association_id: 28
      },
      {
        data: 38,
        timestamp: currentTime,
        device_data_association_id: 29
      },
      {
        data: 81,
        timestamp: currentTime,
        device_data_association_id: 30
      },
      {
        data: 84,
        timestamp: currentTime,
        device_data_association_id: 31
      },
      {
        data: 1,
        timestamp: currentTime,
        device_data_association_id: 32
      },
      {
        data: 61,
        timestamp: currentTime,
        device_data_association_id: 33
      },
      {
        data: 63,
        timestamp: currentTime,
        device_data_association_id: 34
      },
      {
        data: 64,
        timestamp: currentTime,
        device_data_association_id: 35
      },
      {
        data: 79,
        timestamp: currentTime,
        device_data_association_id: 36
      },
      {
        data: 32,
        timestamp: currentTime,
        device_data_association_id: 37
      },
      {
        data: 5,
        timestamp: currentTime,
        device_data_association_id: 38
      },
      {
        data: 77,
        timestamp: currentTime,
        device_data_association_id: 39
      },
      {
        data: 39,
        timestamp: currentTime,
        device_data_association_id: 40
      },
      {
        data: 87,
        timestamp: currentTime,
        device_data_association_id: 41
      },
      {
        data: 42,
        timestamp: currentTime,
        device_data_association_id: 42
      },
      {
        data: 66,
        timestamp: currentTime,
        device_data_association_id: 43
      },
      {
        data: 29,
        timestamp: currentTime,
        device_data_association_id: 44
      },
      {
        data: 90,
        timestamp: currentTime,
        device_data_association_id: 45
      },
      {
        data: 86,
        timestamp: currentTime,
        device_data_association_id: 46
      },
      {
        data: 12,
        timestamp: currentTime,
        device_data_association_id: 47
      },
      {
        data: 71,
        timestamp: currentTime,
        device_data_association_id: 48
      },
      {
        data: 52,
        timestamp: currentTime,
        device_data_association_id: 49
      },
      {
        data: 68,
        timestamp: currentTime,
        device_data_association_id: 50
      },
      {
        data: 22,
        timestamp: currentTime,
        device_data_association_id: 51
      },
      {
        data: 4,
        timestamp: currentTime,
        device_data_association_id: 52
      },
      {
        data: 13,
        timestamp: currentTime,
        device_data_association_id: 53
      },
      {
        data: 97,
        timestamp: currentTime,
        device_data_association_id: 54
      },
      {
        data: 98,
        timestamp: currentTime,
        device_data_association_id: 55
      },
      {
        data: 11,
        timestamp: currentTime,
        device_data_association_id: 56
      },
      {
        data: 15,
        timestamp: currentTime,
        device_data_association_id: 57
      },
      {
        data: 76,
        timestamp: currentTime,
        device_data_association_id: 58
      },
      {
        data: 8,
        timestamp: currentTime,
        device_data_association_id: 59
      },
      {
        data: 74,
        timestamp: currentTime,
        device_data_association_id: 60
      },
      {
        data: 67,
        timestamp: currentTime,
        device_data_association_id: 61
      },
      {
        data: 65,
        timestamp: currentTime,
        device_data_association_id: 62
      },
      {
        data: 36,
        timestamp: currentTime,
        device_data_association_id: 63
      },
      {
        data: 37,
        timestamp: currentTime,
        device_data_association_id: 64
      },
      {
        data: 55,
        timestamp: currentTime,
        device_data_association_id: 65
      },
      {
        data: 69,
        timestamp: currentTime,
        device_data_association_id: 66
      },
      {
        data: 2,
        timestamp: currentTime,
        device_data_association_id: 67
      },
      {
        data: 9,
        timestamp: currentTime,
        device_data_association_id: 68
      },
      {
        data: 91,
        timestamp: currentTime,
        device_data_association_id: 69
      },
      {
        data: 18,
        timestamp: currentTime,
        device_data_association_id: 70
      },
      {
        data: 70,
        timestamp: currentTime,
        device_data_association_id: 71
      },
      {
        data: 53,
        timestamp: currentTime,
        device_data_association_id: 72
      },
      {
        data: 78,
        timestamp: currentTime,
        device_data_association_id: 73
      },
      {
        data: 85,
        timestamp: currentTime,
        device_data_association_id: 74
      },
      {
        data: 21,
        timestamp: currentTime,
        device_data_association_id: 75
      },
      {
        data: 48,
        timestamp: currentTime,
        device_data_association_id: 76
      },
      {
        data: 47,
        timestamp: currentTime,
        device_data_association_id: 77
      },
      {
        data: 92,
        timestamp: currentTime,
        device_data_association_id: 78
      },
      {
        data: 26,
        timestamp: currentTime,
        device_data_association_id: 79
      },
      {
        data: 46,
        timestamp: currentTime,
        device_data_association_id: 80
      },
      {
        data: 95,
        timestamp: currentTime,
        device_data_association_id: 81
      },
      {
        data: 27,
        timestamp: currentTime,
        device_data_association_id: 82
      },
      {
        data: 23,
        timestamp: currentTime,
        device_data_association_id: 83
      },
      {
        data: 6,
        timestamp: currentTime,
        device_data_association_id: 84
      },
      {
        data: 96,
        timestamp: currentTime,
        device_data_association_id: 85
      },
      {
        data: 19,
        timestamp: currentTime,
        device_data_association_id: 86
      },
      {
        data: 44,
        timestamp: currentTime,
        device_data_association_id: 87
      },
      {
        data: 35,
        timestamp: currentTime,
        device_data_association_id: 88
      },
      {
        data: 80,
        timestamp: currentTime,
        device_data_association_id: 89
      },
      {
        data: 51,
        timestamp: currentTime,
        device_data_association_id: 90
      },
      {
        data: 7,
        timestamp: currentTime,
        device_data_association_id: 91
      },
      {
        data: 43,
        timestamp: currentTime,
        device_data_association_id: 92
      },
      {
        data: 58,
        timestamp: currentTime,
        device_data_association_id: 93
      },
      {
        data: 33,
        timestamp: currentTime,
        device_data_association_id: 94
      },
      {
        data: 31,
        timestamp: currentTime,
        device_data_association_id: 95
      },
      {
        data: 100,
        timestamp: currentTime,
        device_data_association_id: 96
      },
      {
        data: 24,
        timestamp: currentTime,
        device_data_association_id: 97
      },
      {
        data: 64,
        timestamp: currentTime,
        device_data_association_id: 98
      },
      {
        data: 50,
        timestamp: currentTime,
        device_data_association_id: 99
      },
      {
        data: 20,
        timestamp: currentTime,
        device_data_association_id: 100
      },
      {
        data: 70,
        timestamp: currentTime,
        device_data_association_id: 101
      },
      {
        data: 90,
        timestamp: currentTime,
        device_data_association_id: 102
      },
      {
        data: 80,
        timestamp: currentTime,
        device_data_association_id: 103
      },
      {
        data: 60,
        timestamp: currentTime,
        device_data_association_id: 104
      },
      {
        data: 40,
        timestamp: currentTime,
        device_data_association_id: 105
      },
      {
        data: 30,
        timestamp: currentTime,
        device_data_association_id: 106
      },
      {
        data: 10,
        timestamp: currentTime,
        device_data_association_id: 107
      },
      {
        data: 25,
        timestamp: currentTime,
        device_data_association_id: 108
      },
      {
        data: 45,
        timestamp: currentTime,
        device_data_association_id: 109
      },
      {
        data: 65,
        timestamp: currentTime,
        device_data_association_id: 110
      },
      {
        data: 95,
        timestamp: currentTime,
        device_data_association_id: 111
      },
      {
        data: 85,
        timestamp: currentTime,
        device_data_association_id: 112
      },
      {
        data: 75,
        timestamp: currentTime,
        device_data_association_id: 113
      },
      {
        data: 55,
        timestamp: currentTime,
        device_data_association_id: 114
      },
      {
        data: 35,
        timestamp: currentTime,
        device_data_association_id: 115
      },
      {
        data: 15,
        timestamp: currentTime,
        device_data_association_id: 116
      },
      {
        data: 5,
        timestamp: currentTime,
        device_data_association_id: 117
      },
      {
        data: 52,
        timestamp: currentTime,
        device_data_association_id: 118
      },
      {
        data: 72,
        timestamp: currentTime,
        device_data_association_id: 119
      },
      {
        data: 92,
        timestamp: currentTime,
        device_data_association_id: 120
      },
      {
        data: 88,
        timestamp: currentTime,
        device_data_association_id: 121
      },
      {
        data: 68,
        timestamp: currentTime,
        device_data_association_id: 122
      },
      {
        data: 48,
        timestamp: currentTime,
        device_data_association_id: 123
      },
      {
        data: 28,
        timestamp: currentTime,
        device_data_association_id: 124
      },
      {
        data: 8,
        timestamp: currentTime,
        device_data_association_id: 125
      },
      {
        data: 18,
        timestamp: currentTime,
        device_data_association_id: 126
      },
      {
        data: 38,
        timestamp: currentTime,
        device_data_association_id: 127
      },
      {
        data: 58,
        timestamp: currentTime,
        device_data_association_id: 128
      },
      {
        data: 78,
        timestamp: currentTime,
        device_data_association_id: 129
      },
      {
        data: 98,
        timestamp: currentTime,
        device_data_association_id: 130
      },
      {
        data: 97,
        timestamp: currentTime,
        device_data_association_id: 131
      },
      {
        data: 87,
        timestamp: currentTime,
        device_data_association_id: 132
      }
    ])
  }
}
