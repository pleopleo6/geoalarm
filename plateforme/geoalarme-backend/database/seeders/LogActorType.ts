import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import LogActorType from "../../app/Models/LogActorType";

export default class extends BaseSeeder {
  public async run () {
    await LogActorType.createMany([
      {
        name: 'user',
        description: 'log an action that have a user as origin',
      },
      {
        name: 'device',
        description: 'log an action that have a device as origin',
      },
      {
        name: 'station',
        description: 'log an action that have a station as origin',
      },
      {
        name: 'site',
        description: 'log an action that have a site as origin',
      },
    ])
  }
}
