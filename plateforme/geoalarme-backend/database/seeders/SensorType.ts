import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import SensorType from "../../app/Models/SensorType";

export default class extends BaseSeeder {
  public async run() {
    await SensorType.createMany([
      {
        decoder: "Barani_MeteoHelix",
      },
    ])
  }
}
