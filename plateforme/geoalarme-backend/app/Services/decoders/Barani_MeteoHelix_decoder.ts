import SensorDataAssociation from "../../Models/SensorDataAssociation";
import db_constants from "../../../config/constants";
import SensorData from "../../Models/SensorData";

export default class Barani_MeteoHelix_decoder {
  public static async decodeAndSave(payload: string[], capteur: any) {

    let payloadBin = '';
    for (const hex of payload) {
      payloadBin += parseInt(hex, 16).toString(2).padStart(4, '0');
    }

    //////////////////////////////
    // BATTERY: SAVING & DECODING
    //////////////////////////////
    const battery = (parseInt(payloadBin.substring(2, 7), 2)) * 0.05 + 3;
    console.log("battery " + battery);
    await this.saveSensorData(capteur.id, db_constants.DT_BATTERY, battery);

    //////////////////////////////
    // TEMPERATURE: SAVING & DECODING
    //////////////////////////////
    const temperature = (parseInt(payloadBin.substring(10, 18), 2) + 256 * parseInt(payloadBin.slice(7, 10), 2)) * 0.1 - 100;
    console.log("temperature " + temperature);
    await this.saveSensorData(capteur.id, db_constants.DT_TEMPERATURE, temperature);

    //////////////////////////////
    // AIR HUMIDITY: SAVING & DECODING
    //////////////////////////////
    const airHumidity = parseInt(payloadBin.slice(30, 39), 2) * 0.2;
    console.log("airHumidity " + airHumidity);
    await this.saveSensorData(capteur.id, db_constants.DT_AIR_HUMIDITY, airHumidity);

    //////////////////////////////
    // PRESSURE: SAVING & DECODING
    //////////////////////////////
    const pressure = ((parseInt(payloadBin.slice(39, 53), 2)) * 5 + 50000) * 0.01;
    console.log("pressure " + pressure);
    await this.saveSensorData(capteur.id, db_constants.DT_PRESSURE, pressure);

    //////////////////////////////
    // IRRADIATION: SAVING & DECODING
    //////////////////////////////
    const irradiation = parseInt(payloadBin.slice(53, 63), 2) * 2;
    console.log("irradiation " + irradiation);
    await this.saveSensorData(capteur.id, db_constants.DT_IRRADIATION, irradiation);

    //////////////////////////////
    // RAIN COUNT: SAVING & DECODING
    //////////////////////////////
    const rainCnt = (parseInt(payloadBin.slice(72, 80), 2)) * 0.2;
    console.log("rainCnt " + rainCnt);
    await this.saveSensorData(capteur.id, db_constants.DT_RAIN_COUNT, rainCnt);

    //////////////////////////////
    // ACTUAL RAIN: SAVING & DECODING
    //////////////////////////////

    // 1. get rain count type link
    const rainCountLink = await SensorDataAssociation.query()
      .where('sensorId', capteur.id)
      .where('dataTypeId', db_constants.DT_RAIN_COUNT)
      .first();
    // 2. get last rain count
    if (rainCountLink) {
      const lastRainCnt = await SensorData.query().where('sensor_data_association_id', rainCountLink.id)
        .orderBy('timestamp', 'desc').first();

      let lastRainCntVal = 0;
      if (lastRainCnt) {
        // @ts-ignore
        lastRainCntVal = parseFloat(lastRainCnt.data);
      }

      // 3. Get right rain value
      let actualRain = 0;
      if (rainCnt < lastRainCntVal) {
        actualRain = 256 + rainCnt - lastRainCntVal;
      } else {
        actualRain = rainCnt - lastRainCntVal;
      }

      // 4. Save actual rain
      console.log("actualRain " + actualRain);
      await this.saveSensorData(capteur.id, db_constants.DT_RAIN, actualRain);
    }
  }

  //////////////////////////////
  // PRIVATE METHODS
  //////////////////////////////
  private static async saveSensorData(sensorId: number, dataTypeId: number, data: number) {
    let data_round = data.toFixed(3)
    const sensorDataAssociation = await SensorDataAssociation.query()
      .where('sensorId', sensorId)
      .where('dataTypeId', dataTypeId)
      .first();
    if (sensorDataAssociation) {
      const sensorData = new SensorData();
      sensorData.sensor_data_association_id = sensorDataAssociation.id;
      // @ts-ignore
      sensorData.data = data_round;
      await sensorData.save();
    }
  }
}
