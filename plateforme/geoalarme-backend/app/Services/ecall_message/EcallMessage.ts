import axios from "axios";
import Env from '@ioc:Adonis/Core/Env';

class ECallService {

  private baseUrl: string;
  private authHeader: string;
  private booted = false;
  private fromNumber: string;


  public boot() {
    /** Ignore multiple calls to the boot method */
    if (this.booted) {
      return;
    }

    const apiVersion = Env.get('ECALL_VERSION');
    const username = Env.get('ECALL_USERNAME');
    const password = Env.get('ECALL_PASSWORD');
    this.fromNumber = Env.get('ECALL_PHONE_NUMBER');

    this.baseUrl = `https://rest.ecall.ch/api/${apiVersion}`;
    this.authHeader = `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`;
    this.booted = true;
  }

  public async sendMessage(to: string[], content: string) {
    try {
      const response = await axios.post(`${this.baseUrl}/message`, {
        channel: 'Sms',
        from: this.fromNumber,
        toList: to.map((address) => ({type: 'Number', address})),
        content: {
          type: 'Text',
          text: content,
        },
      }, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: this.authHeader,
        },
      });

      return response.data;
    } catch (error) {
      throw this.handleApiError(error);
    }
  }

  private handleApiError(error: any) {
    if (error.response) {
      const {status, data} = error.response;
      throw {status, data};
    } else if (error.request) {
      throw {status: 500, data: 'No response from the server'};
    } else {
      throw {status: 500, data: 'Request failed before reaching the server'};
    }
  }

}

export default new ECallService();
