import {Server} from 'socket.io'
import AdonisServer from '@ioc:Adonis/Core/Server'
import User from "../../Models/User";

class Ws {

  public io: Server
  private booted = false

  public boot() {

    /** Ignore multiple calls to the boot method */
    if (this.booted) {
      return
    }

    this.booted = true
    this.io = new Server(AdonisServer.instance!, {
      cors: {
        origin: '*'
      }
    })
  }

  /**
   * Emits a targeted message if a user is defined,
   or a global message if no user is defined
   * */
  public emit(message: Record<string, Record<string, string | number | boolean | any>> | string, target?: User) {
    this.io.emit(target ? `geoalarme-ws-user-${target.id}` : 'geoalarme-ws-global', message);
  }

  public emitCommand(message: Record<string, any>, target?: User) {
    this.io.emit(target ? `geoalarme-ws-user-${target.id}` : 'geoalarme-ws-global', message);
  }
}

export default new Ws()
