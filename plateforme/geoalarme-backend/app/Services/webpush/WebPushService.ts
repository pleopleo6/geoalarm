import Env from '@ioc:Adonis/Core/Env'

class WebPushService {

  private publicVapidKey: string;
  private privateVapidKey: string;
  private webpushEmail: string;
  private webPush;

  private booted = false

  public boot() {

    /** Ignore multiple calls to the boot method */
    if (this.booted) {
      return
    }

    this.publicVapidKey = Env.get('PUBLIC_VAPID_KEY')
    this.privateVapidKey = Env.get('PRIVATE_VAPID_KEY')
    this.webpushEmail = Env.get('WEB_PUSH_CONTACT')
    this.booted = true

    this.init().then(() => console.log('> WebPushService ready'));

  }

  public notify(subscription: any, notification: string) {
    const options = {
      /*vapidDetails: {
        subject: `mailto:${this.webpushEmail}`,
        publicKey: this.publicVapidKey,
        privateKey: this.privateVapidKey
      },*/
      urgency: 'normal' // 'very-low'|'low'|'normal'|'high'
    }
    this.webPush?.sendNotification(subscription, notification, options).then(() => console.log('-> notification sent')).catch(error => console.error('!!! failed to send notification, error:', error));
  }

  private async init() {
    this.webPush = await import('web-push')
    this.webPush.setVapidDetails(`mailto:${this.webpushEmail}`, this.publicVapidKey, this.privateVapidKey)
  }

}

export default new WebPushService()
