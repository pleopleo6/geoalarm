import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Sensor from "../../Models/Sensor";
import SensorType from "../../Models/SensorType";
import Barani_MeteoHelix_decoder from "../../Services/decoders/Barani_MeteoHelix_decoder";

// !!! classname à corriger
export default class CAHandlerApiController {

  public async decodeRequest({request, response}: HttpContextContract) {
    const {DevEUI_uplink} = request.all();

    if (DevEUI_uplink && DevEUI_uplink.DevEUI && DevEUI_uplink.payload_hex) {
      //verify that the devEUI is present
      const capteur = await Sensor.findBy('sensorId', DevEUI_uplink.DevEUI);
      if (capteur) {
        let currentTime = new Date().toISOString();

        const capteur_type = await SensorType.findBy('id', capteur.typeId)

        // @ts-ignore
        switch (capteur_type.decoder) {
          case 'Barani_MeteoHelix':
            await Barani_MeteoHelix_decoder.decodeAndSave(DevEUI_uplink.payload_hex, capteur);
            break;
          default:
            break;
        }
        capteur.updatedAt = currentTime;
      } else {
        // Handle the case when the sensor object is not found
        response.status(404).json({message: 'Sensor not found'});
        // todo log it
      }
    }
  }
}
