import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Site from "../../Models/Site";
import AlarmStatus from "../../Models/AlarmStatus";
import DefaultStatus from "../../Models/DefaultStatus";
import Station from "../../Models/Station";
import StationStatus from "../../Models/StationStatus";
import Device from "../../Models/Device";
import DeviceStatus from "../../Models/DeviceStatus";
import SiteAlarmAssociation from "../../Models/SiteAlarmAssociation";
import SiteDefaultAssociation from "../../Models/SiteDefaultAssociation";
import StationDefaultAssociation from "../../Models/StationDefaultAssociation";
import DeviceAlarmAssociation from "../../Models/DeviceAlarmAssociation";
import DeviceDefaultAssociation from "../../Models/DeviceDefaultAssociation";
import DeviceDataAssociation from "../../Models/DeviceDataAssociation";
import DeviceData from "../../Models/DeviceData";
import Command from "../../Models/Command";
import SiteStationAssociation from "../../Models/SiteStationAssociation";
import DeviceStationAssociation from "../../Models/DeviceStationAssociation";
import db_constants from "../../../config/constants";
import CommandPool from "../../Models/CommandPool";
import SiteLog from "../../Models/SiteLog";
import LogActorType from "../../Models/LogActorType";
import CommandState from "../../Models/CommandState";
import Ws from "../../Services/websocket/Ws";
import SignalStatus from "../../Models/SignalStatus";

import SiteSignalAssociation from "../../Models/SiteSignalAssociation";
import SiteStatus from "../../Models/SiteStatus";
import CurrentWs from "../../Models/CurrentWs";
import UserSiteAssociation from "../../Models/UserSiteAssociation";
import User from "../../Models/User";
import LidarData from "../../Models/LidarData";


export default class CAHandlerApiController {

  public async postHandler_v1({request, response}: HttpContextContract) {
    // Store and manage site information //time 1990 : JJ.MM.AAAA HH:MM:SS
    const {uid, timeSite, idSite, dftSite, almSite, statSite, sgnlSite, stations} = request.all();
    const site = await Site.findBy('siteId', idSite);

    // if site exists
    if (site && site.id && site.statusId != db_constants.SITE_STATUS_OUT_OF_SERVICE) {
      if (uid) {
        await this._handleReturnedCommands(uid, site.id, request);
      }
      let currentTime = new Date().toISOString();

      /*
      if (timeSite) {
        const dateParts = timeSite.split(" ");
        const datePartsSplit = dateParts[0].split(".");
        const timeParts = dateParts[1].split(":");

        const year = parseInt(datePartsSplit[2]);
        const month = parseInt(datePartsSplit[1]) - 1; // Month is zero-based
        const day = parseInt(datePartsSplit[0]);
        const hour = parseInt(timeParts[0]);
        const minute = parseInt(timeParts[1]);
        const second = parseInt(timeParts[2]);

        const parsedDate = new Date(year, month, day, hour, minute, second);
        currentTime = parsedDate.toISOString();
      }
      */

      // IF only ID is present => it is a command pool - watchdog
      if (idSite !== undefined && timeSite === undefined && dftSite === undefined && almSite === undefined && statSite === undefined && stations === undefined) {

        // update site last connexions but not 'updated_at'. That information is only when everything is received.
        site.lastConnexion = currentTime;
        await site.save()
        const cmd_array_json = await this._checkForCommands(site.id);
        return response.ok({cmd_array_json});

        // if everything is present, then manage all
      } else {

        // update SITE alarm status
        const alarmObjForSite = await AlarmStatus.findBy('level', almSite);

        // get the last - save only if alarm hasn't changed
        const lastSiteAlarm = await SiteAlarmAssociation.query()
          .where('siteId', site.id)
          .orderBy('timestamp', 'desc')
          .first();

        if (alarmObjForSite && (!lastSiteAlarm || lastSiteAlarm.alarmId !== alarmObjForSite.id)) {

          const siteAlarm = new SiteAlarmAssociation();
          siteAlarm.timestamp = currentTime;
          siteAlarm.siteId = site.id;
          siteAlarm.alarmId = alarmObjForSite.id;
          await siteAlarm.save();

          // create the log
          const logActorSite = await LogActorType.findBy('name', 'site');
          const logAlarm = new SiteLog();
          logAlarm.siteId = site.id;
          logAlarm.logTypeId = db_constants.LOG_TYPE_ALARM;
          logAlarm.actorId = site.id;
          logAlarm.actorTypeId = logActorSite!.id;
          logAlarm.logData = {
            requestObject: 'alarm',
            requestStatus: null,
            requestParams: request.body(),
            requestResult: alarmObjForSite.level.toString(),
          };
          await logAlarm.save();

          // send new log globally
          Ws.emit({change: {object: 'new_log', target_id: site.id, data: logAlarm}});
        }

        // update SITE default status
        const defaultObjForSite = await DefaultStatus.findBy('level', dftSite)

        // get the last - save only if default hasn't changed
        const lastSiteDefault = await SiteDefaultAssociation.query()
          .where('siteId', site.id)
          .orderBy('timestamp', 'desc')
          .first();

        if (defaultObjForSite && (!lastSiteDefault || lastSiteDefault.defaultId !== defaultObjForSite.id)) {

          const siteDefault = new SiteDefaultAssociation();
          siteDefault.siteId = site.id;
          siteDefault.defaultId = defaultObjForSite.id;
          siteDefault.timestamp = currentTime;
          await siteDefault.save()

          // create the log
          const logActorSite = await LogActorType.findBy('name', 'site');
          const logDefault = new SiteLog();
          logDefault.siteId = site.id;
          logDefault.logTypeId = db_constants.LOG_TYPE_DEFAULT;
          logDefault.actorId = site.id;
          logDefault.actorTypeId = logActorSite!.id;
          logDefault.logData = {
            requestObject: 'default',
            requestStatus: null,
            requestParams: request.body(),
            requestResult: defaultObjForSite.level.toString(),
          };
          await logDefault.save();

          // send new log globally
          Ws.emit({change: {object: 'new_log', target_id: site.id, data: logDefault}});
        }

        // udpdate the signalisation level for the site
        if (sgnlSite) {
          const signStatus = await SignalStatus.findBy('levelNum', sgnlSite)

          // get the last - save only if default hasn't changed
          const lastSiteSignal = await SiteSignalAssociation.query()
            .where('siteId', site.id)
            .orderBy('timestamp', 'desc')
            .first();

          if (signStatus && ((lastSiteSignal && lastSiteSignal.signalId != signStatus.id) || !lastSiteSignal)) {
            const siteSignalisation = new SiteSignalAssociation();
            siteSignalisation.siteId = site.id;
            siteSignalisation.signalId = signStatus.id;
            await siteSignalisation.save();
          }
        }

        // update SITE status // => SITE STATUS UPDATE
        const siteStatus = await SiteStatus.findBy('levelNum', statSite);
        if (siteStatus) {
          site.statusId = siteStatus.id;
        }

        // Loop through all stations received
        for (const station_json of stations) {

          // Access the properties of each station
          const {idSt, dftSt, statSt, devices} = station_json;

          let station: Station = new Station();
          const possibleSiteStationLinks = await SiteStationAssociation.query().where('siteId', '=', site.id);
          for (const link of possibleSiteStationLinks) {
            const possible_station = await Station.findBy('id', link.stationId);

            // @ts-ignore
            if (possible_station.stationId == idSt) {
              // @ts-ignore
              station = possible_station;
              break;
            }
          }

          if (station && station.id) {
            // update its default status
            const defaultObjForStation = await DefaultStatus.findBy('level', dftSt)

            // get the last - save only if default hasn't changed
            const lastStationDefault = await StationDefaultAssociation.query()
              .where('stationId', station.id)
              .orderBy('timestamp', 'desc')
              .first();

            if (defaultObjForStation && (!lastStationDefault || lastStationDefault.defaultId !== defaultObjForStation.id)) {
              const stationDefault = new StationDefaultAssociation();
              stationDefault.stationId = station.id;
              stationDefault.defaultId = defaultObjForStation.id;
              stationDefault.timestamp = currentTime;
              await stationDefault.save()
            }

            // update the status
            const stationStatus = await StationStatus.findBy('id', statSt)
            if (stationStatus) {
              station.statusId = stationStatus.id;
            }

            // loop through all the devices of the given station
            for (const device_json of devices) {

              // Access properties of given device
              const {idDvc, almDvc, dftDvc, statDvc, dataDvc} = device_json;

              let device: Device = new Device();
              const possibleDeviceStationLinks = await DeviceStationAssociation.query().where('stationId', '=', station.id);

              for (const link of possibleDeviceStationLinks) {

                const possible_device = await Device.findBy('id', link.deviceId);
                // @ts-ignore
                if (possible_device.deviceId == idDvc) {
                  // @ts-ignore
                  device = possible_device;
                  break;
                }
              }
              if (device && device.id) {

                // update the device alarm status
                const alarmObjForDevice = await AlarmStatus.findBy('level', almDvc)

                // get the last - save only if alarm hasn't changed
                const lastDeviceAlarm = await DeviceAlarmAssociation.query()
                  .where('deviceId', device.id)
                  .orderBy('timestamp', 'desc')
                  .first();


                if (alarmObjForDevice && (!lastDeviceAlarm || lastDeviceAlarm.alarmId !== alarmObjForDevice.id)) {
                  const deviceAlarm = new DeviceAlarmAssociation();
                  deviceAlarm.deviceId = device.id;
                  deviceAlarm.alarmId = alarmObjForDevice.id;
                  deviceAlarm.timestamp = currentTime;
                  await deviceAlarm.save()

                  // create the log // only for alarm devices which have device type 1 or 2
                  if (device.typeId === db_constants.DEVICE_TYPE_ALARM_BOOL || device.typeId === db_constants.DEVICE_TYPE_ALARM_ANALOGIC) {
                    const logActorSite = await LogActorType.findBy('name', 'device');
                    const logAlarm = new SiteLog();
                    logAlarm.siteId = site.id;
                    logAlarm.logTypeId = db_constants.LOG_TYPE_DEVICE_ALARM;
                    logAlarm.actorId = device.id;
                    logAlarm.actorTypeId = logActorSite!.id;
                    logAlarm.logData = {
                      requestObject: 'alarm',
                      requestStatus: null,
                      requestParams: request.body(),
                      requestResult: alarmObjForDevice.level.toString(),
                    };
                    await logAlarm.save();

                    // send new log globally
                    Ws.emit({change: {object: 'new_log', target_id: site.id, data: logAlarm}});
                  }
                }

                // update its default status
                const defaultObjForDevice = await DefaultStatus.findBy('level', dftDvc)

                // get the last - save only if default hasn't changed
                const lastDeviceDefault = await DeviceDefaultAssociation.query()
                  .where('deviceId', device.id)
                  .orderBy('timestamp', 'desc')
                  .first();

                if (defaultObjForDevice && (!lastDeviceDefault || lastDeviceDefault.defaultId !== defaultObjForDevice.id)) {
                  const deviceDefault = new DeviceDefaultAssociation();
                  deviceDefault.deviceId = device.id;
                  deviceDefault.defaultId = defaultObjForDevice.id;
                  deviceDefault.timestamp = currentTime;
                  await deviceDefault.save()


                  // create the log
                  const logActorSite = await LogActorType.findBy('name', 'device');
                  const logDefaultDevice = new SiteLog();
                  logDefaultDevice.siteId = site.id;
                  logDefaultDevice.logTypeId = db_constants.LOG_TYPE_DEVICE_DEFAULT;
                  logDefaultDevice.actorId = device.id;
                  logDefaultDevice.actorTypeId = logActorSite!.id;
                  logDefaultDevice.logData = {
                    requestObject: 'default',
                    requestStatus: null,
                    requestParams: request.body(),
                    requestResult: defaultObjForDevice.level.toString(),
                  };
                  await logDefaultDevice.save();

                  // send new log globally
                  Ws.emit({change: {object: 'new_log', target_id: site.id, data: logDefaultDevice}});
                }

                // update its status
                const deviceStatus = await DeviceStatus.findBy('id', statDvc)
                if (deviceStatus) {
                  device.statusId = deviceStatus.id;
                }

                // Get the device and data type association id
                const deviceDataAssociation = await DeviceDataAssociation.findBy('deviceId', device.id);

                // UPDATE DATA
                if (deviceDataAssociation) {

                  /*
                  // TODO push this ?
                  // Get the last data, save only if not the same
                  const lastDvcData = await DeviceData.query()
                    .where('device_data_association_id', deviceDataAssociation.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  if (!lastDvcData || lastDvcData.data != dataDvc) {

                   */
                  const deviceData = new DeviceData()
                  deviceData.data = dataDvc;
                  deviceData.device_data_association_id = deviceDataAssociation.id;
                  deviceData.timestamp = currentTime;
                  await deviceData.save();

                }

                device.updatedAt = currentTime;
                await device.save();
              }
            }
            station.updatedAt = currentTime;
            await station.save();
          }
        }
        // update site last connexions
        site.updatedAt = currentTime;
        site.lastConnexion = currentTime;

        // save the new information
        await site.save();

        // send new changes through websocket
        await this._manageWs(site.id);

        const cmd_array_json = await this._checkForCommands(site.id);
        return response.ok({cmd_array_json});
      }
    } else {
      return response.notFound();
    }
  }


  // FIXME Temporary request - used to store remote lidar profiles
  // to be replaced soon - called from lidars

  public async save_lidar_data({request}: HttpContextContract) {

    // FIXME temporary
    const lidarNameValid = 'testlidar';

    const {xCoords, yCoords, lidarName} = request.all();

    if (xCoords && yCoords && lidarName) {
      if (lidarName == lidarNameValid) {
        let newData = new LidarData()
        newData.lidar_name = lidarName;
        // Assign data field as a JSON object with xCoords and yCoords
        newData.data = {
          xCoords: xCoords,
          yCoords: yCoords
        };
        await newData.save()
      }
    }
  }


  // Called from a CRON JOB, checks if sites haven't been connected
  // in a while and updates their status
  // Called from a CRON JOB, checks if sites haven't been connected
  // in a while and updates their status
  public async verify_connexion({}: HttpContextContract) {

    const sites = await Site.all();
    const currentTime = new Date();
    for (const site of sites) {
      if (site.statusId !== db_constants.SITE_STATUS_OUT_OF_SERVICE && site.statusId !== db_constants.SITE_STATUS_CONN_LOST) {

        const siteLastCon = new Date(site.lastConnexion);
        // @ts-ignore
        const timeDifferenceInMilliseconds = currentTime - siteLastCon;
        const timeDifferenceInMinutes = Math.floor(timeDifferenceInMilliseconds / (1000 * 60));


        // Minutes to trigger 'lost connexion' site status.
        if (timeDifferenceInMinutes > 360) {
          // update site status
          site.statusId = db_constants.SITE_STATUS_CONN_LOST;
          await site.save();
          await this._manageWs(site.id);
        }
      }
    }

  }


  // Handles received confirmation from the CA, updates the command status and logs it
  private async _handleReturnedCommands(uid: string, site_id: number, request: any) {

    const commandPoolForSite = await CommandPool.query()
      .where('siteId', site_id).andWhere('stateId', '=', db_constants.CMD_STATUS_WAITING);

    const currentTime = new Date(); // Get the current time

    for (const commandForSite of commandPoolForSite) {

      // Check if current request holds any command response
      if (uid && uid === commandForSite.uid) { // Check for truthiness of uid

        // UPDATE THE NEW STATE OF THE CONFIRMED COMMAND - EITHER IN TIME OR TOO LATE
        // timeout or confirmed
        if (commandForSite.timestamp) {
          const commandTimestamp = new Date(commandForSite.timestamp);
          // @ts-ignore
          const timeDifferenceInSeconds = Math.floor((currentTime - commandTimestamp) / 1000);

          if (timeDifferenceInSeconds > 30) {
            commandForSite.stateId = db_constants.CMD_STATUS_TIMEOUT; // Set the status to rejected
          } else {
            commandForSite.stateId = db_constants.CMD_STATUS_CONFIRMED;
          }
          await commandForSite.save();
        }

        // log confirmed command
        const logActorSite = await LogActorType.findBy('name', 'site')
        const commandState = await CommandState.find(commandForSite.stateId)

        const logCmd = new SiteLog();
        logCmd.siteId = site_id;
        logCmd.logTypeId = db_constants.LOG_TYPE_CMD_RECEIVED;
        logCmd.actorId = site_id;
        logCmd.actorTypeId = logActorSite!.id;
        logCmd.logData = {
          requestObject: `${commandForSite.params.cmd} command`,
          requestStatus: commandState?.name,
          requestParams: request.body(),
          requestResult: null,
        };
        await logCmd.save();

        Ws.emit({change: {object: 'new_log', target_id: site_id, data: logCmd}});
      }
    }
  }


  // Checks if there are any commands that need to be sent back to the CA
  private async _checkForCommands(siteId: number): Promise<Array<{ cmd: string }>> {
    const cmd_array_json: any = [];

    const readyCommands = await CommandPool.query()
      .where('siteId', siteId)
      .where('stateId', '=', db_constants.CMD_STATUS_STORED)
      .orderBy('timestamp', 'asc');

    for (const row of readyCommands) {
      const command = await Command.findBy('id', row.commandId);
      if (command) {

        const cmd = row.params;
        if (cmd.param !== undefined) {
          cmd.param = cmd.param.toString();
        }

        cmd.cmd = command.name;
        row.stateId = db_constants.CMD_STATUS_WAITING;
        await row.save();
        cmd_array_json.push(cmd);
      }
    }
    return cmd_array_json;
  }

  private async _manageWs(site_id: number) {
    // WEBSOCKET MANAGEMENT
    const connUsers = await CurrentWs.all();
    if (connUsers) {
      for (const connUser of connUsers) {
        const userToSite = await UserSiteAssociation.query()
          .where('siteId', site_id)
          .where('userId', '=', connUser.userId);
        if (userToSite) {
          const user = await User.query().where('id', connUser.userId).first()
          Ws.emit({change: {object: 'site_change', target_id: site_id}}, user!)
        }
      }
    }
  }
}
