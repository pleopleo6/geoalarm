import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Site from "../../Models/Site";
import SiteCommandAssociation from "../../Models/SiteCommandAssociation";
import SiteStationAssociation from "../../Models/SiteStationAssociation";
import Station from "../../Models/Station";
import User from "../../Models/User";
import StationLog from "../../Models/StationLog";
import SiteDefaultAssociation from "../../Models/SiteDefaultAssociation";
import SiteAlarmAssociation from "../../Models/SiteAlarmAssociation";
import SiteStatus from "../../Models/SiteStatus";
import DefaultStatus from "../../Models/DefaultStatus";
import AlarmStatus from "../../Models/AlarmStatus";
import UserSiteAssociation from "../../Models/UserSiteAssociation";
import Role from "../../Models/Role";
import Command from "../../Models/Command";
import DeviceStationAssociation from "../../Models/DeviceStationAssociation";
import Device from "../../Models/Device";
import DeviceDefaultAssociation from "../../Models/DeviceDefaultAssociation";
import DeviceAlarmAssociation from "../../Models/DeviceAlarmAssociation";
import DeviceType from "../../Models/DeviceType";
import DeviceDataAssociation from "../../Models/DeviceDataAssociation";
import DeviceData from "../../Models/DeviceData";
import DataType from "../../Models/DataType";
import EventType from "../../Models/EventType";
import EventTypeType from "../../Models/EventTypeType";
import CommunicationType from "../../Models/CommunicationType";
import UserSendTypeAssociation from "../../Models/UserSendTypeAssociation";

export default class DesktopApiController {

  /*------------------------
  > GESTION LOGIN / LOGOUT
  -----------------------*/
  public async login({ auth, request, response }: HttpContextContract) {

    const { email, password } = request.only([ 'email', 'password' ])

    // hash gen
    // console.log('hash pass:', await Hash.make(password))

    try {
      const token = await auth.use('api').attempt(email, password, { expiresIn: '1 hour' })
      const user = await auth.user
      await user.load('entreprise')
      const userSiteAssociations = await UserSiteAssociation.query()
        .where('userId', '=', user.id)
        .orderBy('roleId', 'desc');
      const roles = await Role.all();
      const rolesBySite: Record<number, string|undefined> = {};
      userSiteAssociations.forEach((item: UserSiteAssociation) => {
        rolesBySite[item.siteId!] = roles.find((role: Role) => role.id===item.roleId)?.name
      })
      // const administratedSites = Object.values(rolesBySite).filter((value: string) => value==='administrator'||value==='super-administrator')
      // TODO > LIMITATION ACCÈS ADMINS ?
      // ...
      return {user, rolesBySite, token: token}
    } catch {
      return response.unauthorized('Invalid credentials')
    }
  }

  public async logout({ auth }: HttpContextContract) {
    await auth.use('api').revoke()
    return { revoked: true }
  }

  /*------------------------
  > GESTION SITES
  -----------------------*/
  public async getSites ({ auth, response }: HttpContextContract) {
    const user = auth.user;
    const siteObjects: any[] = await this._getUserSitesDetailed(user.id)
    return response.status(200).json(siteObjects)
  }

  public async getSite ({ auth, params, response }: HttpContextContract) {

    const authUser = auth.user;
    const userSiteAssociation = await UserSiteAssociation.query()
      .where('userId', '=', authUser.id)
      .where('siteId', '=', params.id)
      .first();

    if(userSiteAssociation){

      const site = await Site.find(params.id)

      if(site){

        const userSiteAssociations = await UserSiteAssociation .query().where('siteId','=',site.id)
        const siteUsers:User[] = []
        for(let userSiteAssociation of userSiteAssociations){
          const siteUser = await User.find(userSiteAssociation.userId)
          if(siteUser && !siteUsers.map((item:User) => item.id).includes(siteUser.id)) siteUsers.push(siteUser)
        }

        const commands = await Command.query()
          .where('roleId', userSiteAssociation.roleId)
          // .andWhere('isGlobal', false)
          .orderBy('priority','asc')

        const siteState = await this._getSiteState(site.id, site.statusId);

        const eventTypeTypes = await EventTypeType.query().orderBy('id','asc')
        const eventTypes = await EventType.query().orderBy('id','asc')
        const communicationTypes = await CommunicationType.query().orderBy('id','asc')

        const distributionListsByType:Record<string, EventType[]> = {}
        for(let eventType of eventTypes){
          const typeName = eventTypeTypes.find((item:EventTypeType) => item.id===eventType.typeId)?.toJSON().label_name
          if(typeName){
            if(!distributionListsByType[typeName]){
              distributionListsByType[typeName] = []
            }
            distributionListsByType[typeName].push(eventType)
          }
        }

        const distributionData = await this.getSiteDistributionData(params.id)

        const siteObject:any = {}
        siteObject.siteInfos = site.toJSON()
        siteObject.commands = commands.filter((command:Command) => !command.name.includes('QUITTANCE'))
        siteObject.alarm = siteState.alarm
        siteObject.default = siteState.default
        siteObject.siteStatus = siteState.status
        siteObject.distribution_list = {
          users: siteUsers.map((item:any) => ({ ...item.toJSON(), active: item.id===authUser.id })),
          communication_types: communicationTypes,
          lists_by_type: distributionListsByType,
          data: distributionData,
        }

        const siteStations = await SiteStationAssociation.query().where('siteId', '=', site.id)
        siteObject.siteStations = [];

        if (siteStations) {

          for (const siteStation of siteStations) {

            const station= await Station.find(siteStation.stationId)

            if(station){

              const stationObj:any = station.toJSON()
              // stationObj.alarm_devices = []
              // stationObj.sign_devices = []
              stationObj.devices = []

              // Get the devices
              const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', station.id)

              for (const stationDevice of stationDevices) {

                const device = await Device.find(stationDevice.deviceId)

                if(device){

                  const deviceObj:any = device.toJSON();

                  // get the last device default association
                  const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  // get the last device alarm association
                  const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  deviceObj.stationId = siteStation.stationId;
                  deviceObj.type = await DeviceType.find(device.typeId);
                  deviceObj.default = deviceDefaultAssoc? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : await DefaultStatus.find(1)
                  deviceObj.alarm = deviceAlarmAssoc? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : await AlarmStatus.find(4)

                  // get the device datas
                  const deviceDatasObjects: any[] = []
                  const deviceDataAssocs = await DeviceDataAssociation.query()
                    .select('*')
                    .where('deviceId', '=', deviceObj.id)

                  if(deviceDataAssocs) {

                    for (let deviceDataAssoc of deviceDataAssocs) {
                      const deviceDatas = await DeviceData.query().where('device_data_association_id', '=', deviceDataAssoc.id)
                      const dataType = await DataType.find(deviceDataAssoc.dataTypeId)
                      for (let deviceData of deviceDatas) {
                        const deviceDataObj = deviceData.toJSON()
                        deviceDataObj.dataType = dataType?.toJSON()
                        deviceDatasObjects.push(deviceDataObj)
                      }
                    }

                    // battery level
                    const batteryLevelData = deviceDatasObjects.find((item:any) => item.dataType.id===2)
                    deviceObj.batteryLevel = batteryLevelData? batteryLevelData.data + batteryLevelData.dataType.unity : ''

                    // com. level
                    // TODO > détermination com.level
                    // const comLevelData = deviceDatasObjects.filter((item:any) => item.dataType===2)
                    // deviceObj.comLevel = comLevelData? comLevelData.data + batteryLevelData.dataType.unity : ''
                    // TMP
                    deviceObj.comLevel = ''

                    // flag com.
                    // TODO > détermination flag com.
                    deviceObj.comFlag = ''

                    // deviceObj.data = deviceDatasObjects

                  }

                  /*
                  if(db_constants.DEVICE_TYPE_ALARM_IDS.includes(device.typeId)){
                      stationObj.alarm_devices.push(deviceObj);
                    }else if(db_constants.DEVICE_TYPE_SIGN_IDS.includes(device.typeId)){
                      stationObj.sign_devices.push(deviceObj);
                    }/*else{
                      stationObj.other_devices.push((deviceObj));
                    }*/

                  stationObj.devices.push(deviceObj);

                }
              }

              siteObject.siteStations.push(stationObj);

            }

          }
        }

        return response.status(200).json(siteObject);

      }else{
        // Bad request
        response.status(400)
      }
    }else{
      // Missing permissions
      response.status(403)
    }
  }

  public async createSiteDistribution({ request, response }) {
    console.log('create distrib.', request.body())
    const body = request.only(['userId', 'siteId', 'eventIds', 'messageIds'])
    const distributionAssociation = new UserSendTypeAssociation()
    await distributionAssociation.fill({
      userId: body.userId,
      siteId: body.siteId,
      data: { "eventIds":body.eventIds, "messageIds":body.messageIds }
    }).save()
    const distributionData = await this.getSiteDistributionData(body.siteId)
    return response.status(200).json( { data:distributionData } );
  }
  public async updateSiteDistribution({ params, request, response }) {
    const body = request.only(['userId', 'eventIds', 'messageIds'])
    const distributionAssociation = await UserSendTypeAssociation.find(params.distributionId)
    if(distributionAssociation){
      distributionAssociation.userId = body.userId
      distributionAssociation.data = { "eventIds":body.eventIds, "messageIds":body.messageIds }
      await distributionAssociation.save()
      const distributionData = await this.getSiteDistributionData(params.id)
      return response.status(200).json( { data:distributionData } );
    }else{
      // Bad request
      response.status(400)
    }
  }
  public async suppressSiteDistribution({ params, response }) {
    console.log('suppress distrib.', params.distributionId, params.body)
    const distributionAssociation = await UserSendTypeAssociation.find(params.distributionId)
    if(distributionAssociation){
      await distributionAssociation.delete()
      const distributionData = await this.getSiteDistributionData(params.id)
      return response.status(200).json( { data:distributionData } );
    }else{
      // Bad request
      response.status(400)
    }
  }

  public async getSiteDevice ({ auth, params, response }: HttpContextContract) {

    const user = auth.user;
    const userSiteAssociation = await UserSiteAssociation.query()
      .where('userId', '=', user.id)
      .where('siteId', '=', params.id)
      .first();

    if (userSiteAssociation) {

      const site = await Site.find(params.id)
      const deviceAssociatedToSite = await this._checkSiteDeviceAssociation(params.id, params.deviceId)

      if (site && deviceAssociatedToSite) {

        const device = await Device.find(params.deviceId)

        if (device) {

          const deviceObj: any = device.toJSON();
          deviceObj.sites = [];

          // get all stations associated to device
          const siteStations = await SiteStationAssociation.query().where('siteId', '=', site.id)
          if (siteStations) {
            for (const siteStation of siteStations) {
              let deviceFound = false
              const stations:any[] = [];
              const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', siteStation.stationId)
              const deviceStationAssocs = stationDevices.filter((item: DeviceStationAssociation) => item.deviceId == params.deviceId)
              if (deviceStationAssocs.length) {
                for(let deviceStationAssoc of deviceStationAssocs) {
                  if(deviceStationAssoc.deviceId == params.deviceId){
                    const station = await Station.find(deviceStationAssoc.stationId)
                    stations.push(station?.toJSON())
                    deviceFound = true
                  }
                }
                if(deviceFound){
                  const site = await Site.find(siteStation.siteId)
                  if(site){
                    const siteObj = site?.toJSON()
                    siteObj.stations = stations
                    deviceObj.sites.push(siteObj)
                  }
                }
              }
            }
          }

          // get the last device default association
          const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
            .select('*')
            .where('device_id', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();

          // get the last device alarm association
          const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
            .select('*')
            .where('device_id', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();

          // get the device data associations
          const deviceDatasObjects: any[] = []
          const deviceDataAssocs = await DeviceDataAssociation.query()
            .select('*')
            .where('deviceId', '=', deviceObj.id)

          if(deviceDataAssocs) {

            for (let deviceDataAssoc of deviceDataAssocs) {

              const deviceDatas = await DeviceData.query().where('device_data_association_id', '=', deviceDataAssoc.id)
              const dataType = await DataType.find(deviceDataAssoc.dataTypeId)

              for (let deviceData of deviceDatas) {
                const deviceDataObj = deviceData.toJSON()
                deviceDataObj.dataType = dataType?.toJSON()
                deviceDatasObjects.push(deviceDataObj)
              }
            }

            deviceObj.data = deviceDatasObjects
          }

          deviceObj.type = await DeviceType.find(device.typeId);
          deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : await DefaultStatus.find(1)
          deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : await AlarmStatus.find(4)

          return response.status(200).json(deviceObj);

        } else {
          // Bad request
          response.status(400)
        }
      } else {
        // Bad request
        response.status(400)
      }

    } else {
      // Missing permissions
      response.status(403)
    }
  }

  public async callCommand ({ params, response }: HttpContextContract) {

    const commandAssociatedToSite = await SiteCommandAssociation.query()
      .where('siteId', '=', params.id)
      .andWhere('commandId', '=', params.commandId)
      .preload('command')

    if(commandAssociatedToSite){
      // TODO > call command
      console.log('->', commandAssociatedToSite)
      // ...
      return
    }

    response.status(400)
    return

  }

  /*public async getSiteObjects ({ params }: HttpContextContract) {
    return await this._getSiteObjects(params.id)
  }

  public async getSiteStation ({ params }: HttpContextContract) {
    return await this._getStationById(params.id, params.targetId)
  }

  public async getSiteSensor ({ params }: HttpContextContract) {
    const sensorAssoc = await this._getSensorById(params.id, params.targetId)
    return sensorAssoc.sensors
  }

  public async getSiteCamera ({ params }: HttpContextContract) {
    const cameraAssoc = await this._getCameraById(params.id, params.targetId)
    return cameraAssoc.camera
  }*/

  /*------------------------
  > GESTION DEVICES
  -----------------------*/
  public async getDevices({ auth, response }: HttpContextContract) {

    const user = auth.user;

    const deviceObjects: any[] = [];
    const userSiteAssociations = await UserSiteAssociation.query().where('userId', '=', user.id)

    for(let userSiteAssociation of userSiteAssociations) {

      const site = await Site.find(userSiteAssociation.siteId)

      if(site){
        const siteObject:any = site.toJSON();
        siteObject.stations = [];

        const siteStations = await SiteStationAssociation.query().where('siteId', '=', site.id)

        if (siteStations) {

          for (const siteStation of siteStations) {

            const station= await Station.find(siteStation.stationId)

            if(station){

              const stationObj:any = station.toJSON()
              stationObj.alarm_devices = []
              stationObj.sign_devices = []

              // Get the devices
              const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', station.id)

              for (const stationDevice of stationDevices) {

                const device = await Device.find(stationDevice.deviceId)

                if(device){

                  const deviceObj:any = device.toJSON();

                  // get the last device default association
                  const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  // get the last device alarm association
                  const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  deviceObj.stationId = siteStation.stationId;
                  deviceObj.type = await DeviceType.find(device.typeId);
                  deviceObj.default = deviceDefaultAssoc? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : await DefaultStatus.find(1)
                  deviceObj.alarm = deviceAlarmAssoc? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : await AlarmStatus.find(4)

                  // get the device datas
                  const deviceDatasObjects: any[] = []
                  const deviceDataAssocs = await DeviceDataAssociation.query()
                    .select('*')
                    .where('deviceId', '=', deviceObj.id)

                  if(deviceDataAssocs) {

                    for (let deviceDataAssoc of deviceDataAssocs) {
                      const deviceDatas = await DeviceData.query().where('device_data_association_id', '=', deviceDataAssoc.id)
                      const dataType = await DataType.find(deviceDataAssoc.dataTypeId)
                      for (let deviceData of deviceDatas) {
                        const deviceDataObj = deviceData.toJSON()
                        deviceDataObj.dataType = dataType?.toJSON()
                        deviceDatasObjects.push(deviceDataObj)
                      }
                    }

                    // battery level
                    const batteryLevelData = deviceDatasObjects.find((item:any) => item.dataType.id===2)
                    deviceObj.batteryLevel = batteryLevelData? batteryLevelData.data + batteryLevelData.dataType.unity : ''

                    // com. level
                    // TODO > détermination com.level
                    // const comLevelData = deviceDatasObjects.filter((item:any) => item.dataType===2)
                    // deviceObj.comLevel = comLevelData? comLevelData.data + batteryLevelData.dataType.unity : ''
                    // TMP
                    deviceObj.comLevel = ''

                    // flag com.
                    // TODO > détermination flag com.
                    deviceObj.comFlag = ''

                    // deviceObj.data = deviceDatasObjects

                  }

                  deviceObj.siteId = site.id
                  deviceObj.siteName = site.name
                  deviceObj.stationName = station.name
                  deviceObjects.push(deviceObj)

                }
              }

            }

          }
        }
      }

    }

    /*
    // Get all user's entreprise sites
    const sites = await Site.query()
      .where('entrepriseId', '=', user.entrepriseId)
      .orderBy('name', 'asc');

    // Create an array to store the device objects
    const deviceObjects: any[] = [];

    for (let site of sites) {

      const userSiteAssociation = await UserSiteAssociation.query()
        .where('userId', '=', user.id)
        .where('siteId', '=', site.id)
        .first();

      if(userSiteAssociation){

        // all stations by site
        const siteObject:any = site.toJSON();
        siteObject.stations = [];

        const siteStations = await SiteStationAssociation.query().where('siteId', '=', site.id)

        if (siteStations) {

          for (const siteStation of siteStations) {

            const station= await Station.find(siteStation.stationId)

            if(station){

              const stationObj:any = station.toJSON()
              stationObj.alarm_devices = []
              stationObj.sign_devices = []

              // Get the devices
              const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', station.id)

              for (const stationDevice of stationDevices) {

                const device = await Device.find(stationDevice.deviceId)

                if(device){

                  const deviceObj:any = device.toJSON();

                  // get the last device default association
                  const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  // get the last device alarm association
                  const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  deviceObj.stationId = siteStation.stationId;
                  deviceObj.type = await DeviceType.find(device.typeId);
                  deviceObj.default = deviceDefaultAssoc? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : await DefaultStatus.find(1)
                  deviceObj.alarm = deviceAlarmAssoc? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : await AlarmStatus.find(4)

                  // get the device datas
                  const deviceDatasObjects: any[] = []
                  const deviceDataAssocs = await DeviceDataAssociation.query()
                    .select('*')
                    .where('deviceId', '=', deviceObj.id)

                  if(deviceDataAssocs) {

                    for (let deviceDataAssoc of deviceDataAssocs) {
                      const deviceDatas = await DeviceData.query().where('device_data_association_id', '=', deviceDataAssoc.id)
                      const dataType = await DataType.find(deviceDataAssoc.dataTypeId)
                      for (let deviceData of deviceDatas) {
                        const deviceDataObj = deviceData.toJSON()
                        deviceDataObj.dataType = dataType?.toJSON()
                        deviceDatasObjects.push(deviceDataObj)
                      }
                    }

                    // battery level
                    const batteryLevelData = deviceDatasObjects.find((item:any) => item.dataType.id===2)
                    deviceObj.batteryLevel = batteryLevelData? batteryLevelData.data + batteryLevelData.dataType.unity : ''

                    // com. level
                    // TODO > détermination com.level
                    // const comLevelData = deviceDatasObjects.filter((item:any) => item.dataType===2)
                    // deviceObj.comLevel = comLevelData? comLevelData.data + batteryLevelData.dataType.unity : ''
                    // TMP
                    deviceObj.comLevel = ''

                    // flag com.
                    // TODO > détermination flag com.
                    deviceObj.comFlag = ''

                    // deviceObj.data = deviceDatasObjects

                  }

                  deviceObj.siteId = site.id
                  deviceObj.siteName = site.name
                  deviceObj.stationName = station.name
                  deviceObjects.push(deviceObj)

                }
              }

            }

          }
        }

      }

    }*/

    return response.status(200).json(deviceObjects.sort((a:Device,b:Device) => ((a.name > b.name)? 1 : (a.name < b.name ? -1 : 0))));

  }

  public async getDevice({ auth, params, response }: HttpContextContract) {

    const user = auth.user
    let siteStationAssociations:SiteStationAssociation[] = []
    let deviceAssociatedToSite = false
    const deviceSites:any[] = []
    let deviceObj: any

    const deviceStationAssociations = await DeviceStationAssociation.query().where('deviceId','=',params.deviceId)
    for(let deviceStationAssociation of deviceStationAssociations){
      const stationSiteAssociations = await SiteStationAssociation.query().where('stationId','=',deviceStationAssociation.stationId)
      siteStationAssociations = [ ...siteStationAssociations,  ...stationSiteAssociations ]
    }

    for(let siteStationAssociation of siteStationAssociations){

      const userSiteAssociation = await UserSiteAssociation.query()
        .where('userId', '=', user.id)
        .where('siteId', '=', siteStationAssociation.siteId)
        .first();

      if (userSiteAssociation) {
        deviceAssociatedToSite = true
        const site = await Site.find(siteStationAssociation.siteId)
        const siteObj:any = site?.toJSON()
        const station = await Station.find(siteStationAssociation.stationId)
        const stationObj = station?.toJSON()
        if(!deviceSites.includes(siteObj)) {
          siteObj.stations = [stationObj];
          deviceSites.push(siteObj)
        }else {
          deviceSites.find((item: any) => item.id === siteObj.id)?.stations.push(stationObj)
        }
      }

    }

    if (deviceAssociatedToSite) {

      const device = await Device.find(params.deviceId)

      if (device) {

        deviceObj = device.toJSON();
        deviceObj.sites = deviceSites;

        // get the last device default association
        const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
          .select('*')
          .where('device_id', '=', deviceObj.id)
          .orderBy('timestamp', 'desc')
          .first();

        // get the last device alarm association
        const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
          .select('*')
          .where('device_id', '=', deviceObj.id)
          .orderBy('timestamp', 'desc')
          .first();

        // get the device data associations
        const deviceDatasObjects: any[] = []
        const deviceDataAssocs = await DeviceDataAssociation.query()
          .select('*')
          .where('deviceId', '=', deviceObj.id)

        if(deviceDataAssocs) {

          for (let deviceDataAssoc of deviceDataAssocs) {

            const deviceDatas = await DeviceData.query().where('device_data_association_id', '=', deviceDataAssoc.id)
            const dataType = await DataType.find(deviceDataAssoc.dataTypeId)

            for (let deviceData of deviceDatas) {
              const deviceDataObj = deviceData.toJSON()
              deviceDataObj.dataType = dataType?.toJSON()
              deviceDatasObjects.push(deviceDataObj)
            }
          }

          deviceObj.data = deviceDatasObjects
        }

        deviceObj.type = await DeviceType.find(device.typeId);
        deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : await DefaultStatus.find(1)
        deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : await AlarmStatus.find(4)

        return response.status(200).json(deviceObj);

      }else {
        // Bad request
        response.status(400)
      }
    }else {
      // Missing permissions
      response.status(403)
    }

  }

  /*------------------------
  > GESTION OBJETS
  -----------------------*/
  /*public async getObjects ({ auth }: HttpContextContract) {
    return await this._getUserObjects(auth.user);
  }

  public async getStation ({ params }: HttpContextContract) {
    return await Station.find(params.id)
  }

  public async getSensor ({ params }: HttpContextContract) {
    return await Sensor.find(params.id)
  }

  public async getCamera ({ params }: HttpContextContract) {
    return await Camera.find(params.id)
  }

  public async getCameraStream ({ /!*params*!/ }: HttpContextContract) {
    // TODO
  }*/

  /*------------------------
  > GESTION USERS
  -----------------------*/
  public async getUsers ({ auth, response }: HttpContextContract) {
    const user = auth.user
    const users:any[] = [];
    const entrepriseUsers = await User.query().where('entrepriseId', '=', user.entrepriseId).select('id','firstname', 'lastname')
    for(let user of entrepriseUsers) {
      const userObject = user.toJSON()
      userObject.roles_by_site = []
      const userSiteAssociations = await UserSiteAssociation.query()
        .where('userId', '=', user.id)
        .orderBy('roleId', 'asc')
      for(let userSiteAssociation of userSiteAssociations) {
        const site = await Site.query().where('id','=',userSiteAssociation.siteId).select('id','name').first()
        const role = await Role.query().where('id','=',userSiteAssociation.roleId).select('id','name').first()
        userObject.roles_by_site.push( {
          site_id:site?.id,
          site_name:site?.name,
          role_id:role?.id,
          role_name:role?.name
        } )
      }
      users.push(userObject)
    }
    return response.status(200).json(users.sort((a:StationLog, b:StationLog) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0)));
  }

  public async getUser ({ auth, params, response }: HttpContextContract) {

    const authUser = auth.user

    const user = await User.find(params.id)

    if(user){

      const roles = await Role.query().select('id','name').orderBy('id','asc')

      const userSiteAssociations = await UserSiteAssociation .query().where('userId','=',user.id)
        .where('userId', '=', user.id)
        .orderBy('roleId', 'asc')
      const siteUsers:User[] = []
      const userSites:Site[] = []
      for(let userSiteAssociation of userSiteAssociations){
        const siteUser = await User.find(userSiteAssociation.userId)
        if(siteUser && !siteUsers.map((item:User) => item.id).includes(siteUser.id)) siteUsers.push(siteUser)
        const userSite = await Site.find(userSiteAssociation.siteId)
        if(userSite && !userSites.map((item:Site) => item.id).includes(userSite.id)) userSites.push(userSite)
      }

      const userObject = user.toJSON()
      userObject.roles_by_site = []

      for(let userSiteAssociation of userSiteAssociations) {
        const site = await Site.query().where('id','=',userSiteAssociation.siteId).select('id','name').first()
        const role = await Role.query().where('id','=',userSiteAssociation.roleId).select('id','name').first()
        userObject.roles_by_site.push( {
          site_id:site?.id,
          site_name:site?.name,
          role_id:role?.id,
          role_name:role?.name,
        } )
      }

      return response.status(200).json( {
        user:userObject,
        users:siteUsers.map((item:any) => ({ ...item.toJSON(), active: item.id===authUser.id })),
        sites:userSites,
        roles,
      } )

    }else{
      // Bad request
      response.status(400)
    }
  }

  public async createUser ({ params }: HttpContextContract) {
    const user = new User()
    user.merge(params.body)
    return await user.save()
  }

  public async updateUser ({ params, response }: HttpContextContract) {
    const user = await User.find(params.id)
    if (user) {
      user.merge(params.body)
      return await user.save()
    }
    response.status(400)
    return
  }

  public async suppressUser ({ params, response }: HttpContextContract) {
    const user = await User.find(params.id)
    if (user){
      await user.delete()
      response.status(204)
    }else{
      response.status(400)
    }
    return
  }

  /*------------------------
  > GESTION LOGS
  -----------------------*/
  public async getLogs ({ auth }: HttpContextContract) {

    let logs: StationLog[] = []

    const user = auth.user
    const userSites= await this._getUserSites(user.id)

    for (const site of userSites) {
      const siteAssociatedStations= await SiteStationAssociation.query().where('siteId', '=', site.id)
      const stationsIds = siteAssociatedStations.map((assoc: SiteStationAssociation) => assoc.stationId)
      for (const stationsId of stationsIds) {
        const stationLogs = await StationLog.query().where('stationId', '=', stationsId).preload('station').preload('logType')
        logs = [...logs, ...stationLogs]
      }
    }

    return logs.sort((a:StationLog, b:StationLog) => (a.logTimestamp > b.logTimestamp) ? 1 : ((b.logTimestamp > a.logTimestamp) ? -1 : 0))
  }


  /*------------------------
  > FONCTIONS UTILITAIRES
  -----------------------*/
  /*private async _getStationById(site_id: number, target_id: number) {
    const siteStationAssociations = await SiteStationAssociation.query()
      .where('siteId', '=', site_id)
      .andWhere('stationId', '=', target_id)
      .preload('station')
      return siteStationAssociations[0].station
  }

  private async _getSensorById (site_id: number, target_id: number) {
    return await SiteSensorAssociation.query()
      .where('siteId', '=', site_id)
      .andWhere('sensorId', '=', target_id)
      .preload('sensor')[0]
  }

  private async _getCameraById (site_id: number, target_id: number) {
   return await SiteCameraAssociation.query()
     .where('siteId', '=', site_id)
     .andWhere('cameraId', '=', target_id)
     .preload('camera')[0]
  }*/

  private async _getSiteState(siteId:number, siteStatusId:number) {

    // get the last default association
    const defaultLevelAssoc = await SiteDefaultAssociation.query()
      .select('siteId', 'defaultId', 'timestamp')
      .where('siteId', '=', siteId)
      .orderBy('timestamp', 'desc')
      .first();

    // get the last alarm association
    const alarmLevelAssoc = await SiteAlarmAssociation.query()
      .select('*')
      .where('siteId', '=', siteId)
      .orderBy('timestamp', 'desc')
      .first();

    // get the site status
    const siteStatus = await SiteStatus.query()
      .select('*')
      .where('id', '=', siteStatusId)
      .first();

    const siteDefault = await DefaultStatus.findBy(defaultLevelAssoc? 'id':'level',defaultLevelAssoc? defaultLevelAssoc.defaultId : 1)
    const siteAlarm = await AlarmStatus.findBy(alarmLevelAssoc? 'id':'level',alarmLevelAssoc? alarmLevelAssoc.alarmId : 1)

    return { status: siteStatus, alarm:siteAlarm?.toJSON(), default: siteDefault?.toJSON() }

  }

  private async _getUserSites(userId:number){

    const userSiteAssociations = await UserSiteAssociation.query().where('userId', '=', userId)
    const siteObjects: any[] = []

    for(let userSiteAssociation of userSiteAssociations) {

      const site = await Site.find(userSiteAssociation.siteId)

      if(site && !siteObjects.find((item:any) => item.id===site.id)){
        const siteObject:any = site.toJSON()
        siteObjects.push(siteObject)
      }

    }

    return siteObjects
  }

  private async _getUserSitesDetailed(userId:number){

    const userSiteAssociations = await UserSiteAssociation.query().where('userId', '=', userId)
    const siteObjects: any[] = []

    for(let userSiteAssociation of userSiteAssociations) {

      const site = await Site.find(userSiteAssociation.siteId)

      if(site && !siteObjects.find((item:any) => item.id===site.id)){

        const siteState = await this._getSiteState(site.id, site.statusId);
        const siteObject:any = site.toJSON()

        siteObject.alarm = siteState.alarm;
        siteObject.default = siteState.default;
        siteObject.status = siteState.status;

        siteObjects.push(siteObject)

      }

    }

    return siteObjects
  }
  /*private async _getSiteObjects (site_id: number) {
    const siteAssociatedStations= await SiteStationAssociation.query()
      .where('siteId', '=', site_id)
      .preload('station')
    const siteAssociatedSensors= await SiteSensorAssociation.query()
      .where('siteId', '=', site_id)
      .preload('sensor')
    const siteAssociatedCameras= await SiteCameraAssociation.query()
      .where('siteId', '=', site_id)
      .preload('camera')
    return {
      stations: siteAssociatedStations.map((assoc: SiteStationAssociation) => assoc.station),
      sensors: siteAssociatedSensors.map((assoc: SiteSensorAssociation) => assoc.sensor),
      cameras: siteAssociatedCameras.map((assoc: SiteCameraAssociation) => assoc.camera)
    }
  }*/

  /*private async _getUserObjects(user: User) {
    const sites= await Site.query()
      .where('entrepriseId', '=', user.entrepriseId)
      .orderBy('name', 'asc')
    const objects: Record<string, unknown[]> = {};
    for (const site of sites) {
      const siteObjects = await this._getSiteObjects(site.id)
      objects.stations = [...objects.stations?? [], siteObjects.stations]
      objects.sensors = [...objects.sensors?? [], siteObjects.sensors]
      objects.cameras = [...objects.cameras?? [], siteObjects.cameras]
    }
    return objects;
  }*/

  private async getSiteDistributionData(siteId:number){
    const userSendTypeAssociations = await UserSendTypeAssociation.query().where('siteId','=',siteId).orderBy('id')
    return userSendTypeAssociations.map((item:UserSendTypeAssociation) => ({
      id: item.id,
      userId: item.userId,
      eventIds: item.data.eventIds,
      messageIds: item.data.messageIds,
    }))
  }

  private async _checkSiteDeviceAssociation(siteId:number, deviceId:number) {
    const siteStations = await SiteStationAssociation.query().where('siteId', '=', siteId)
    let associationResult = false
    for (const siteStation of siteStations) {
      const station= await Station.find(siteStation.stationId)
      if(station){
        const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', station.id)
        for (const stationDevice of stationDevices) {
          if(stationDevice.deviceId==deviceId){
            associationResult = true
            break
          }
        }
      }
    }
    return associationResult
  }
}
