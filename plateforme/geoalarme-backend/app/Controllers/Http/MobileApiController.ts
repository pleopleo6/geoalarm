import db_constants from "../../../config/constants";
import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import Device from "../../Models/Device";
import DeviceStationAssociation from "../../Models/DeviceStationAssociation";
import Site from "../../Models/Site";
import SiteCommandAssociation from "../../Models/SiteCommandAssociation";
import SiteStationAssociation from "../../Models/SiteStationAssociation";
import SiteCameraAssociation from "../../Models/SiteCameraAssociation";
import SiteSensorAssociation from "../../Models/SiteSensorAssociation";
import StationLog from "../../Models/StationLog";
import UserSiteAssociation from "../../Models/UserSiteAssociation";
import SiteDefaultAssociation from "../../Models/SiteDefaultAssociation";
import SiteAlarmAssociation from "../../Models/SiteAlarmAssociation";
import DefaultStatus from "../../Models/DefaultStatus";
import AlarmStatus from "../../Models/AlarmStatus";
import Command from "../../Models/Command";
import Station from "../../Models/Station";
import SiteStatus from "../../Models/SiteStatus";
import DeviceDefaultAssociation from "../../Models/DeviceDefaultAssociation";
import DeviceAlarmAssociation from "../../Models/DeviceAlarmAssociation";
import Role from "../../Models/Role";
import Camera from "../../Models/Camera";
import Sensor from "../../Models/Sensor";
import SensorType from "../../Models/SensorType";
import DeviceDataAssociation from "../../Models/DeviceDataAssociation";
import DeviceData from "../../Models/DeviceData";
import DataType from "../../Models/DataType";
import SensorDataAssociation from "../../Models/SensorDataAssociation";
import CommandPool from "../../Models/CommandPool";
import CurrentWs from "../../Models/CurrentWs";
import SensorData from "../../Models/SensorData";
import WebPushService from "../../Services/webpush/WebPushService";
import Language from "../../Models/Language";
import Entreprise from "../../Models/Entreprise";
import axios from "axios";
import pino from "pino";
import SiteLog from "../../Models/SiteLog";
import Ws from "../../Services/websocket/Ws";
import LogActorType from "../../Models/LogActorType";
import CommandState from "../../Models/CommandState";
import SiteSignalAssociation from "../../Models/SiteSignalAssociation";
import SignalStatus from "../../Models/SignalStatus";
import User from "../../Models/User";
import CameraType from "../../Models/CameraType";
import StationStatus from "../../Models/StationStatus";
import DeviceStatus from "../../Models/DeviceStatus";

export default class MobileApiController {

  /*------------------------
  > GESTION LOGIN / LOGOUT
  -----------------------*/
  public async login({auth, request, response}: HttpContextContract) {

    const {email, password} = request.only(['email', 'password'])

    // hash gen
    // console.log('hash pass:', await Hash.make(password))
    // const hash = await Hash.make(password)

    try {

      const tokenExpiration = '12 hours'
      const token = await auth.use('api').attempt(email, password, {expiresIn: tokenExpiration})

      const user = await auth.user
      const userObj = user.toJSON()

      const entreprise = await Entreprise.find(user.entrepriseId)
      userObj.entreprise = entreprise?.toJSON()

      const language = await Language.find(user.languageId)
      userObj.language = language?.toJSON()

      const userSiteAssociations = await UserSiteAssociation.query().where('userId', '=', user.id).orderBy('roleId', 'desc')
      const roles = await Role.all()
      const rolesBySite: Record<number, string | undefined> = {}
      userSiteAssociations.forEach((item: UserSiteAssociation) => {
        rolesBySite[item.siteId!] = roles.find((role: Role) => role.id === item.roleId)?.name
      })

      return {user: userObj, rolesBySite, token: token}

    } catch {
      return response.unauthorized('Invalid credentials')
    }
  }

  public async logout({auth, response}: HttpContextContract) {
    const user = auth.user;
    if (user) {
      const currentWs = await CurrentWs.query().where('userId', user.id).first();
      if (currentWs) await currentWs.delete();
      await auth.use('api').revoke()
      return response.status(200).json({revoked: true})
    } else {
      return response.status(200)
    }
  }

  /*---------------------------------
    > GESTION WEB-PUSH SUBSCRIPTION
  --------------------------------*/
  public async notifySubscription({auth, request, response}: HttpContextContract) {

    const {subscription, notification} = request.only(['subscription', 'notification'])

    const user = auth.user
    user.webpushSubscription = JSON.stringify(subscription)
    await user.save()

    WebPushService.notify(subscription, JSON.stringify(notification))

    return response.status(200)
  }

  /*---------------------------------
    > REGISTER THE USER PREFERENCES
  --------------------------------*/
  public async setUserPreferences({request, auth, response}: HttpContextContract) {
    const data = request.body();
    const user = auth.user;
    if (data && data.language) {
      const language = await Language.query().where('code', '=', data.language).first()
      if (language) {
        user.languageId = language.id
        await user.save()
        return response.status(200)
      }
    } else {
      // Missing post data
      return response.status(400)
    }
  }

  /*------------------------
  > GESTION SITE / STATION
  -----------------------*/
  public async getSites({response, auth}: HttpContextContract) {

    const user = auth.user;

    const userSiteAssociations = await UserSiteAssociation.query().where('userId', '=', user.id)


    const siteObjects: any[] = []

    for (let userSiteAssociation of userSiteAssociations) {

      const site = await Site.find(userSiteAssociation.siteId)

      if (site && !siteObjects.find((item: any) => item.id === site.id)) {

        const siteState = await this._getSiteState(site.id, site.statusId);


        // old logic
        /*
        const signalisationStatusOk = await AlarmStatus.query().where('level', '=', 1).first();
        const signalisationStatusAlarms = siteState.signalisationDevicesInAlarm.map((item: any) => item.alarm);
        let signalisationStatus = signalisationStatusOk;
        signalisationStatusAlarms.forEach((item: AlarmStatus) => {
          if (item.level > signalisationStatus!.level) {
            signalisationStatus = item;
          }
        }) */

        // get the last default association
        const signLevelAssoc = await SiteSignalAssociation.query()
          .select('siteId', 'signalId', 'timestamp')
          .where('siteId', '=', site.id)
          .orderBy('timestamp', 'desc')
          .first();

        // old logic updated : mapping signalisationStatus -> alarmStatus
        /*
        let signalisationStatus;
        if (signLevelAssoc) {
          const signStatus = await SignalStatus.findBy('id', signLevelAssoc.signalId)
          let level = signStatus!.levelNum! % 10
          // states from SGNLALM1 to SGNLALM4 and from SGNLALM1 forced to SGNLALM4 forced
          if (level >= 1 && level <= 4) {
            signalisationStatus = await AlarmStatus.query().where('level', '=', level).first()
          } else if (level == 0) {
            signalisationStatus = await AlarmStatus.query().where('level', "=", 4).first()
          } else if (level == 5) {
            signalisationStatus = await AlarmStatus.query().where('level', "=", 1).first()
          }
        } else {
          signalisationStatus = await AlarmStatus.query().where('level', "=", 1).first()
        }*/

        const signalisationStatus = signLevelAssoc
          ? await SignalStatus.findBy('id', signLevelAssoc.signalId)
          : await AlarmStatus.query().where('level', "=", 1).first()

        const siteObject: any = site.toJSON()
        siteObject.alarm = siteState.alarm;
        siteObject.default = siteState.default;
        siteObject.status = siteState.status;
        siteObject.signalisation_status = signalisationStatus;
        siteObjects.push(siteObject)

      }

    }
    const sortOrder = [1, 4, 2, 3];

    // SORT by : Status of the site and then by alarm lvl (descending)
    const customSort = (a, b) => {
      const statusIdA = a.status.id;
      const statusIdB = b.status.id;

      const weightA = sortOrder.indexOf(statusIdA);
      const weightB = sortOrder.indexOf(statusIdB);

      // First, compare by status.id
      if (weightA !== weightB) {
        return weightA - weightB;
      } else {
        // If status.id is equal, compare by alarm.level in descending order
        const levelA = a.alarm ? parseInt(a.alarm.level) : Number.MAX_SAFE_INTEGER;
        const levelB = b.alarm ? parseInt(b.alarm.level) : Number.MAX_SAFE_INTEGER;
        return levelB - levelA;
      }
    };

    // Sort the sites array using the custom sorting function
    siteObjects.sort(customSort);
    return response.status(200).json(siteObjects);

  }

  public async getSite({response, auth, params}: HttpContextContract) {

    if (params.id) {

      // get the user
      const user = auth.user;

      // check the user is connected to the site
      const siteUserRole = await this._getUserRolePerSite(params.id, user.id)

      if (siteUserRole) {

        const site = await Site.findOrFail(params.id)

        if (site) {

          // fetch non-global commands depending on site user role
          const commands = await Command.query()
            .where('roleId', siteUserRole)
            .andWhere('isGlobal', false)
            .orderBy('priority', 'asc')

          const siteSignalStatus = await this._getSiteSignalStatus(site.id);

          // Create a new object that includes the alarm & default
          const siteObj: any = {
            ...site.toJSON(),
            ...{commands: commands},
            // ...{commands: commands.filter((command: Command) => !command.name.includes('QUITTANCE'))},
            sgnl_site: siteSignalStatus
          }

          // get the site status
          siteObj.status = await SiteStatus.find(site.statusId)

          // get the last default association
          const defaultLevelAssoc = await SiteDefaultAssociation.query()
            .select('*')
            .where('siteId', '=', site.id)
            .orderBy('timestamp', 'desc')
            .first()

          // get the last alarm association
          const alarmLevelAssoc = await SiteAlarmAssociation.query()
            .select('*')
            .where('siteId', '=', site.id)
            .orderBy('timestamp', 'desc')
            .first()

          siteObj.default = defaultLevelAssoc ? await DefaultStatus.find(defaultLevelAssoc.defaultId) : null
          siteObj.alarm = alarmLevelAssoc ? await AlarmStatus.find(alarmLevelAssoc.alarmId) : null

          // Next step : loop through all stations to add the devices seperated by the 3 types
          const siteStations = await SiteStationAssociation.query().where('siteId', '=', site.id)

          // 3 types to display in frontend
          siteObj.stations = [];

          if (siteStations) {

            for (const siteStation of siteStations) {

              const station = await Station.find(siteStation.stationId)

              if (station) {

                const stationObj: any = station.toJSON()
                stationObj.alarm_devices = []
                stationObj.sign_devices = []
                // stationObj.other_devices = [];

                // Get the devices
                const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', station.id)

                for (const stationDevice of stationDevices) {

                  const device = await Device.find(stationDevice.deviceId)

                  if (device) {

                    const deviceObj: any = device.toJSON();
                    deviceObj.stationId = siteStation.stationId;

                    // get the last device default association
                    const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
                      .select('*')
                      .where('device_id', '=', deviceObj.id)
                      .orderBy('timestamp', 'desc')
                      .first();

                    // get the last device alarm association
                    const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
                      .select('*')
                      .where('device_id', '=', deviceObj.id)
                      .orderBy('timestamp', 'desc')
                      .first();

                    deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
                    deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null

                    if (db_constants.DEVICE_TYPE_ALARM_IDS.includes(device.typeId)) {
                      stationObj.alarm_devices.push(deviceObj);
                    } else if (db_constants.DEVICE_TYPE_SIGN_IDS.includes(device.typeId)) {
                      stationObj.sign_devices.push(deviceObj);
                    }
                    /*else{
                      stationObj.other_devices.push((deviceObj));
                    }*/

                  }
                }

                siteObj.stations.push(stationObj);

              }

            }
          }
          return response.status(200).json(siteObj);
        }
      } else {
        // User has no rights
        return response.status(403);
      }
    } else {
      // Missing parameters
      return response.status(400)
    }
  }

  public async getSiteLogs({request, params}: HttpContextContract) {

    const {pageIndex, pageLimit} = request.body();
    const logs = await SiteLog.query()
      .where('siteId', '=', params.id)
      // .preload('site')
      .preload('logType')
      .preload('actorType')
      .orderBy('logTimestamp', 'desc')
      .paginate(pageIndex, pageLimit);
    // return logs;
    const qualifiedLogs: any[] = [];
    const paginatedLogs = logs.serialize();
    for (const l of logs) {
      const log = l.toJSON();
      switch (log.actor_type.name) {
        case 'user':
          log.actor = await User.query().select('firstname', 'lastname').where('id', '=', log.actor_id).first();
          break;
        case 'station':
          log.actor = await Station.find(log.actor_id);
          break;
        case 'device':
          log.actor = await Device.find(log.actor_id);
          break;
        case 'site':
          log.actor = await Site.find(log.actor_id);
          break;
      }
      qualifiedLogs.push(log);
    }
    return {data: qualifiedLogs, meta: paginatedLogs.meta};
  }

  public async callSiteCommand({request, auth, params, response}: HttpContextContract) {

    const commandData = request.body();

    //check params
    if (params.id > 0 && params.commandId > 0) {

      // check if user belongs to site
      const user = auth.user
      const userRoleForSiteID = await this._getUserRolePerSite(params.id, user.id)

      if (userRoleForSiteID) {

        const commandAssociatedToSite = await SiteCommandAssociation.query()
          .where('siteId', '=', params.id)
          .andWhere('commandId', '=', params.commandId)
          .first();

        // now get the command
        if (commandAssociatedToSite) {

          const command = await Command.find(commandAssociatedToSite.commandId)

          // check if user's role is enough to call the given command
          const hasRight = await this._checkRoleMatchesOther(userRoleForSiteID, command!.roleId);

          if (hasRight && command) {
            //check if the structure is respected
            if (this._checkJsonHasSameStructure(command.params, commandData)) {

              // pool the command
              const commandPool = new CommandPool();
              commandPool.commandId = command.id;
              commandPool.uid = commandData.uid;
              commandPool.userId = user.id;
              commandPool.stateId = db_constants.CMD_STATUS_STORED;
              commandPool.siteId = params.id;
              commandPool.params = commandData;
              await commandPool.save();

              // create the log
              const logActorUser = await LogActorType.findBy('name', 'user')
              const commandState = await CommandState.find(db_constants.CMD_STATUS_STORED)
              const logCmd = new SiteLog();
              logCmd.siteId = commandAssociatedToSite.siteId;
              logCmd.logTypeId = db_constants.LOG_TYPE_CMD_SENT;
              logCmd.actorId = auth.user.id;
              logCmd.actorTypeId = logActorUser!.id;
              // logCmd.actorType = logActorUser!;
              logCmd.logData = {
                requestObject: `${command.name} command`,
                requestStatus: commandState?.name,
                requestParams: request.body(),
                requestResult: null,
              };

              await logCmd.save();

              const log = {
                ...logCmd.toJSON(),
                actor_type: logActorUser,
                actor: await User.query().select('firstname', 'lastname').where('id', '=', auth.user.id).first()
              };

              // send new log globally
              Ws.emit({change: {object: 'new_log', target_id: params.id, data: log}});

            } else {
              // bad request
              return response.status(400)
            }
          } else {
            response.status(403)
          }
        } else {
          // bad request
          return response.status(400)
        }
      } else {
        // Missing permissions
        return response.status(403);
      }
    } else {
      // Missing parameters - bad request
      return response.status(400)
    }
  }

  public async getSiteAlarmDevices({params, response}: HttpContextContract) {

    // returns site alarm devices only

    const devices: any[] = [];
    const siteStationAssociations = await SiteStationAssociation.query().where('siteId', '=', params.id)

    for (let siteStationAssociation of siteStationAssociations) {

      const devicesStatiomAssocs = await DeviceStationAssociation.query().where('stationId', '=', siteStationAssociation.stationId)

      for (let assoc of devicesStatiomAssocs) {

        const device = await Device.find(assoc.deviceId)

        if (device && (device.typeId === 1 || device.typeId === 2)) {

          const deviceObj: any = device.toJSON()
          deviceObj.stationId = assoc.stationId;

          // get the last device default association
          const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
            .select('*')
            .where('deviceId', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();


          // get the last device alarm association
          const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
            .select('*')
            .where('deviceId', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();

          deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
          deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null
          devices.push(deviceObj)

        }
      }

    }

    return response.status(200).json({devices});
  }

  public async getSiteSignalisationDevices({params, response}: HttpContextContract) {

    // returns site signalisation devices only

    const devices: any[] = [];
    const siteStationAssociations = await SiteStationAssociation.query().where('siteId', '=', params.id)

    for (let siteStationAssociation of siteStationAssociations) {

      const devicesAssocsByStation = await DeviceStationAssociation.query().where('stationId', '=', siteStationAssociation.stationId)

      for (let assoc of devicesAssocsByStation) {

        const device = await Device.find(assoc.deviceId)

        if (device && db_constants.DEVICE_TYPE_SIGN_IDS.includes(device.typeId)) {

          const deviceObj: any = device.toJSON()
          deviceObj.stationId = assoc.stationId;

          // get the last device default association
          const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
            .select('*')
            .where('deviceId', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();

          // get the last device alarm association
          const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
            .select('*')
            .where('deviceId', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();

          deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
          deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null
          devices.push(deviceObj)

        }
      }

    }

    return response.status(200).json({devices});
  }

  public async getSiteDevicesInDefault({params, response}: HttpContextContract) {

    // returns site devices in default only

    const devices: any[] = [];
    const siteStationAssociations = await SiteStationAssociation.query().where('siteId', '=', params.id)

    for (let siteStationAssociation of siteStationAssociations) {

      const devicesAssocsByStation = await DeviceStationAssociation.query().where('stationId', '=', siteStationAssociation.stationId)

      for (let assoc of devicesAssocsByStation) {

        const device = await Device.find(assoc.deviceId)

        if (device
          && db_constants.DEVICE_TYPE_DEFAULT.includes(device.typeId)) {

          const deviceObj: any = device.toJSON()
          deviceObj.stationId = assoc.stationId;

          // get the last device default association
          const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
            .select('*')
            .where('deviceId', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();

          // get the last device alarm association
          const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
            .select('*')
            .where('deviceId', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();

          if (deviceDefaultAssoc || deviceAlarmAssoc) {
            deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null;
            deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null;
            if ((deviceObj.alarm && !deviceObj.default) || (deviceObj.default && deviceObj.default.toJSON().id > 1)) {
              devices.push(deviceObj);
            }
          }

        }
      }

    }

    response.status(200).json({devices});
  }

  public async getStationAlarmDevices({params}: HttpContextContract) {

    // returns station alarm devices only

    const devicesAssocsByStation = await DeviceStationAssociation.query().where('stationId', '=', params.stationId)
    const station = await this._getStationById(params.id, params.stationId)
    const devices: any[] = [];

    for (let assoc of devicesAssocsByStation) {

      const device = await Device.find(assoc.deviceId)

      if (device && device.typeId === 1) {

        const deviceObj: any = device.toJSON()

        // get the last device default association
        const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
          .select('*')
          .where('deviceId', '=', deviceObj.id)
          .orderBy('timestamp', 'desc')
          .first();

        // get the last device alarm association
        const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
          .select('*')
          .where('deviceId', '=', deviceObj.id)
          .orderBy('timestamp', 'desc')
          .first();

        deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
        deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null
        devices.push(deviceObj)

      }
    }

    return {station: station?.toJSON(), devices}
  }

  public async getStationSignalisationDevices({params}: HttpContextContract) {

    // returns station signalisation devices only

    const devicesAssocsByStation = await DeviceStationAssociation.query().where('stationId', '=', params.stationId)
    const station = await this._getStationById(params.id, params.stationId)
    const devices: Device[] = [];

    for (let assoc of devicesAssocsByStation) {
      const device = await Device.find(assoc.deviceId)
      if (device && db_constants.DEVICE_TYPE_SIGN_IDS.includes(device.typeId)) {
        // TODO > AJOUT STATE SIGNALIASAION DEVICE
        // ...
        devices.push(device)
      }
    }
    return {station: station?.toJSON(), devices}
  }

  public async getSiteDevice({response, params}: HttpContextContract) {

    const deviceAssociation = await DeviceStationAssociation.query().where('deviceId', '=', params.deviceId).first()
    const device = await Device.find(deviceAssociation?.deviceId)

    if (device) {

      const deviceObj = device.toJSON()

      // get the last device default association
      const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
        .select('*')
        .where('deviceId', '=', deviceObj.id)
        .orderBy('timestamp', 'desc')
        .first();

      // get the last device alarm association
      const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
        .select('*')
        .where('deviceId', '=', deviceObj.id)
        .orderBy('timestamp', 'desc')
        .first();

      deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
      deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null

      const deviceDatasObjects: any[] = []
      const deviceDataAssocs = await DeviceDataAssociation.query()
        .select('*')
        .where('deviceId', '=', deviceObj.id)

      if (deviceDataAssocs) {

        for (let deviceDataAssoc of deviceDataAssocs) {

          const dataType = await DataType.find(deviceDataAssoc.dataTypeId)
          const deviceData = await DeviceData.query().where('device_data_association_id', '=', deviceDataAssoc.id).orderBy('timestamp', 'desc').first()

          if (deviceData && dataType) {
            const deviceDataObj = deviceData.toJSON()

            // 1/0 => OK/NOK pour Comflag, Câble et MainV - ETATS | data type ids 11,13,16
            if (dataType.id === db_constants.DT_COMFLAG ||
              dataType.id === db_constants.DT_MAINV_STATUS) {
              if (deviceDataObj.data === 1) {
                deviceDataObj.data = 'OK';
              } else if (deviceDataObj.data === 0) {
                deviceDataObj.data = 'NOK';
              }
              // 1/0 => ON / OFF pour Camera | data type id 14
            } else if (dataType.id === db_constants.DT_CAMERA) {
              if (deviceDataObj.data === 1) {
                deviceDataObj.data = 'ON';
              } else if (deviceDataObj.data === 0) {
                deviceDataObj.data = 'OFF';
              }
              // 1/0 => OK/NOK pour Cable | data type ids 13
            } else if (dataType.id === db_constants.DT_CABLE) {
              if (deviceDataObj.data === 1) {
                deviceDataObj.data = 'OK';
              } else if (deviceDataObj.data === 0) {
                deviceDataObj.data = 'NOK';
              }
              // 1/0 => NOK/OK pour LIDAR | data type ids 17
            } else if (dataType.id === db_constants.DT_LIDAR) {
              if (deviceDataObj.data === 0) {
                deviceDataObj.data = 'OK';
              } else if (deviceDataObj.data === 1) {
                deviceDataObj.data = 'NOK';
              }
            } else if (dataType.id === db_constants.DT_CMD_ALM) {
              if (deviceDataObj.data === 0) {
                deviceDataObj.data = 'OFF';
              } else if (deviceDataObj.data === 1) {
                deviceDataObj.data = 'ON';
              }
            }

            deviceDataObj.dataType = dataType?.toJSON();
            deviceDatasObjects.push(deviceDataObj);
          }

          /*const deviceDatas = await DeviceData.query().where('device_data_association_id', '=', deviceDataAssoc.id).orderBy('timestamp', 'desc')for(let deviceData of deviceDatas){
            const deviceDataObj = deviceData.toJSON()
            deviceDataObj.dataType = dataType?.toJSON();
            deviceDatasObjects.push(deviceDataObj);
          }*/

        }

        deviceObj.data = deviceDatasObjects

      }

      return {device: deviceObj}

    } else {
      // Missing parameters
      return response.status(400)
    }
  }

  public async getStationDevice({response, params}: HttpContextContract) {

    const deviceAssociation = await DeviceStationAssociation.query()
      .where('stationId', '=', params.stationId)
      .andWhere('deviceId', '=', params.deviceId).first()

    const device = await Device.find(deviceAssociation?.deviceId)

    const station = await this._getStationById(params.id, params.stationId)

    if (device) {

      const deviceObj = device.toJSON()

      // get the last device default association
      const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
        .select('*')
        .where('deviceId', '=', deviceObj.id)
        .orderBy('timestamp', 'desc')
        .first();

      // get the last device alarm association
      const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
        .select('*')
        .where('deviceId', '=', deviceObj.id)
        .orderBy('timestamp', 'desc')
        .first();

      deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
      deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null

      return {station: station?.toJSON(), device: deviceObj}

    } else {
      // Missing parameters
      return response.status(400)
    }
  }

  public async getStations({response, auth, params}: HttpContextContract) {

    if (params.id) {

      // get the user
      const user = auth.user;

      // check the user is connected to the site
      const siteUserRole = await this._getUserRolePerSite(params.id, user.id)

      if (siteUserRole) {

        const site = await Site.find(params.id);

        if (site) {

          const stations: any[] = [];
          const siteStations = await SiteStationAssociation.query().where('siteId', '=', site.id)

          if (siteStations) {
            for (const siteStation of siteStations) {
              const station = await Station.find(siteStation.stationId)
              const stationStatus = await StationStatus.find(station?.statusId)
              const stationObj: any = station!.toJSON()
              stationObj.alarm_devices = []
              stationObj.sign_devices = []
              stationObj.other_devices = [];
              stationObj.status = stationStatus;

              // Get the devices
              const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', stationObj.id)

              for (const stationDevice of stationDevices) {

                const device = await Device.find(stationDevice.deviceId)

                if (device) {

                  const deviceObj: any = device.toJSON();
                  deviceObj.stationId = siteStation.stationId;

                  // get the last device default association
                  const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  // get the last device alarm association
                  const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
                    .select('*')
                    .where('device_id', '=', deviceObj.id)
                    .orderBy('timestamp', 'desc')
                    .first();

                  deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
                  deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null

                  if (db_constants.DEVICE_TYPE_ALARM_IDS.includes(device.typeId)) {
                    stationObj.alarm_devices.push(deviceObj);
                  } else if (db_constants.DEVICE_TYPE_SIGN_IDS.includes(device.typeId)) {
                    stationObj.sign_devices.push(deviceObj);
                  } else {
                    stationObj.other_devices.push((deviceObj));
                  }

                }
              }

              stations.push(stationObj);
            }
          }
          return response.status(200).json({site, stations});
        }
      } else {
        // User has no rights
        return response.status(403);
      }
    } else {
      // Missing parameters
      return response.status(400)
    }
  }

  public async getStation({params, response}: HttpContextContract) {

    const devices: Device[] = [];
    const devicesByStation = await DeviceStationAssociation.query().where('stationId', '=', params.stationId)

    for (const assoc of devicesByStation) {

      const device = await Device.find(assoc.deviceId)
      // loop through each device
      if (device) {
        const deviceObj: any = device.toJSON();

        // get the last device default association
        const deviceDefaultAssoc = await DeviceDefaultAssociation.query()
          .select('*')
          .where('device_id', '=', deviceObj.id)
          .orderBy('timestamp', 'desc')
          .first();

        // get the last device alarm association
        const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
          .select('*')
          .where('device_id', '=', deviceObj.id)
          .orderBy('timestamp', 'desc')
          .first();

        const deviceStatus = await DeviceStatus.find(device?.statusId)

        deviceObj.default = deviceDefaultAssoc ? await DefaultStatus.find(deviceDefaultAssoc.defaultId) : null
        deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null
        deviceObj.status = deviceStatus

        const deviceDatasObjects: any[] = []
        const deviceDataAssocs = await DeviceDataAssociation.query()
          .select('*')
          .where('deviceId', '=', deviceObj.id)

        if (deviceDataAssocs) {

          // for each device - data type => usually one per device
          for (let deviceDataAssoc of deviceDataAssocs) {

            const dataType = await DataType.find(deviceDataAssoc.dataTypeId)

            // Following replacements must be returned
            // 0/1 => OK/NOK pour Comflag,  et MainV - ETATS | data type ids 11,16
            // 1/0 => ON / OFF pour Camera | data type id 14
            // 1/0 => OK/NOK pour Cable | data type ids 13

            // get last data received from device
            const deviceData = await DeviceData.query().where('device_data_association_id', '=',
              deviceDataAssoc.id).orderBy('timestamp', 'desc').first()

            if (deviceData && dataType) {
              const deviceDataObj = deviceData.toJSON()

              // 1/0 => OK/NOK pour Comflag, Câble et MainV - ETATS | data type ids 11,13,16
              if (dataType.id === db_constants.DT_COMFLAG ||
                dataType.id === db_constants.DT_MAINV_STATUS) {
                if (deviceDataObj.data === 1) {
                  deviceDataObj.data = 'OK';
                } else if (deviceDataObj.data === 0) {
                  deviceDataObj.data = 'NOK';
                }
                // 1/0 => ON / OFF pour Camera | data type id 14
              } else if (dataType.id === db_constants.DT_CAMERA) {
                if (deviceDataObj.data === 1) {
                  deviceDataObj.data = 'ON';
                } else if (deviceDataObj.data === 0) {
                  deviceDataObj.data = 'OFF';
                }
                // 1/0 => OK/NOK pour Cable | data type ids 13
              } else if (dataType.id === db_constants.DT_CABLE) {
                if (deviceDataObj.data === 1) {
                  deviceDataObj.data = 'OK';
                } else if (deviceDataObj.data === 0) {
                  deviceDataObj.data = 'NOK';
                }
                // 1/0 => NOK/OK pour LIDAR | data type ids 17
              } else if (dataType.id === db_constants.DT_LIDAR) {
                if (deviceDataObj.data === 0) {
                  deviceDataObj.data = 'OK';
                } else if (deviceDataObj.data === 1) {
                  deviceDataObj.data = 'NOK';
                }
              } else if (dataType.id === db_constants.DT_CMD_ALM) {
                if (deviceDataObj.data === 0) {
                  deviceDataObj.data = 'OFF';
                } else if (deviceDataObj.data === 1) {
                  deviceDataObj.data = 'ON';
                }
              }

              deviceDataObj.dataType = dataType?.toJSON();
              deviceDatasObjects.push(deviceDataObj);
            }
          }

          deviceObj.data = deviceDatasObjects

        }
        devices.push(deviceObj)

      }
    }

    const station = await this._getStationById(params.id, params.stationId)

    return response.status(200).json({station, devices});

  }

  public async getStationLogs({request, params}: HttpContextContract) {
    const {pageIndex, pageLimit} = request.body();
    return await StationLog.query()
      .where('stationId', '=', params.stationId)
      // .preload('station')
      .preload('logType')
      .orderBy('logTimestamp', 'desc')
      .paginate(pageIndex, pageLimit);
  }

  public async getSiteCameras({params, response}: HttpContextContract) {
    const cameras: Camera[] = []
    const associations = await SiteCameraAssociation.query().where('siteId', '=', params.id).orderBy('cameraId', 'asc')
    for (const assoc of associations) {
      const camera = await Camera.query()
        .select('id', 'name')
        .where('id', '=', assoc.cameraId).first()
      if (camera) cameras.push(camera)
    }
    return response.status(200).json(cameras);
  }

  public async getSiteCamera({params, response}: HttpContextContract) {
    const association = await SiteCameraAssociation.query()
      .where('siteId', '=', params.id)
      .andWhere('cameraId', '=', params.cameraId)
      .first()
    if (association) {
      const camera = await Camera.query()
        .select('id', 'name')
        .where('id', '=', association.cameraId).first()
      if (camera) return response.status(200).json(camera);
      response.status(400)
    }
    response.status(400)
    return
  }

  public async getSiteCameraStream({params, response}: HttpContextContract) {

    const transport = pino.transport({
      target: "pino/file",
      options: {destination: './logs/logs.log', mkdir: true, append: true}
    });
    const logger = pino(transport);

    const association = await SiteCameraAssociation.query()
      .where('siteId', '=', params.id)
      .andWhere('cameraId', '=', params.cameraId)
      .first()

    if (association) {

      const camera = await Camera.query().where('id', '=', association.cameraId).first()

      if (camera) {
        const cameraType = await CameraType.findBy('id', camera.typeId)

        const streamUser = camera.username
        const streamPwd = camera.password
        const streamAuth = Buffer.from(`${streamUser}:${streamPwd}`).toString('base64')
        const streamBase = `http://${camera.url}`
        const streamPort = camera.port
        let streamUrl;

        if (cameraType && cameraType.name == 'Mobotix') {

          const streamPath = '/cgi-bin/faststream.jpg?stream=full'
          streamUrl = `${streamBase}:${streamPort}${streamPath}`
          const streamBoundary = 'MOBOTIX_Fast_Serverpush'

          response.header('Content-Type', `multipart/x-mixed-replace; boundary="${streamBoundary}"`)


        } else if (cameraType && cameraType.name == 'Daheng') {

          const streamPath = '/'
          streamUrl = `${streamBase}:${streamPort}${streamPath}`
          const streamBoundary = 'frame'

          response.header('Content-Type', `multipart/x-mixed-replace; boundary="${streamBoundary}"`)
        }


        response.header('Cache-Control', 'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0')
        response.header('Pragma', 'no-cache')
        response.header('Connection', 'close')
        try {
          const stream = await axios.get(streamUrl, {
            headers: {Authorization: `Basic ${streamAuth}`},
            responseType: 'stream',
          })
          return response.stream(stream.data, (error) => {
            return response.status(400).send({message: 'Failed to stream camera', error})
          });
        } catch (error) {
          logger.error(error, `Failed to get camera stream`);
          return response.status(401).send({message: 'Failed to get camera stream', error})
        }

      }
      return response.status(404).send('camera not found')
    }

    return response.status(404).send('bad request')
  }

  public async getSiteSensors({params}: HttpContextContract) {
    const associations = await SiteSensorAssociation.query().where('siteId', '=', params.id)
    const sensors: any[] = [];
    for (let assoc of associations) {
      const sensor = await Sensor.find(assoc.sensorId)
      if (sensor) {
        const sensorObj: any = sensor.toJSON()
        const sensorType = await SensorType.find(sensor.typeId)
        sensorObj.type = sensorType?.toJSON()
        sensorObj.data = await this._getSensorDatas(sensor.id)
        sensors.push(sensorObj)
      }
    }
    return {sensors}
  }

  public async getSiteSensor({params, response}: HttpContextContract) {

    const association = await SiteSensorAssociation.query()
      .where('siteId', '=', params.id)
      .andWhere('sensorId', '=', params.sensorId).first()

    if (association) {

      const sensor = await Sensor.find(association.sensorId)

      if (sensor) {

        const sensorObj: any = sensor.toJSON()
        const sensorType = await SensorType.find(sensor.typeId)
        sensorObj.type = sensorType?.toJSON()
        sensorObj.data = await this._getSensorDatas(sensor.id)

        return {sensor: sensorObj}

      } else {
        // Missing parameters
        return response.status(400)
      }
    } else {
      // Missing parameters
      return response.status(400)
    }

  }

  public async getStationSensors({params, response}: HttpContextContract) {
    const station = await this._getStationById(params.id, params.id)
    if (station) {
      const sensors: any[] = [];
      const associations = await SiteSensorAssociation.query().where('siteId', '=', params.id)
      for (let assoc of associations) {
        const sensor = await Sensor.find(assoc.sensorId)
        if (sensor) {
          const sensorObj: any = sensor.toJSON()
          const sensorType = await SensorType.find(sensor.typeId)
          sensorObj.type = sensorType?.toJSON()
          sensors.push(sensorObj)
        }
      }
      return {station: station?.toJSON(), sensors}
    } else {
      // Missing parameters
      return response.status(400)
    }
  }

  public async getStationSensor({params, response}: HttpContextContract) {
    const station = await this._getStationById(params.id, params.id)
    const association = await SiteSensorAssociation.query()
      .where('siteId', '=', params.id)
      .andWhere('sensorId', '=', params.sensorId).first()
    if (association) {
      const sensor = await Sensor.find(association.sensorId)
      if (sensor) {
        const sensorObj: any = sensor.toJSON()
        const sensorType = await SensorType.find(sensor.typeId)
        sensorObj.type = sensorType?.toJSON()
        return {station: station?.toJSON(), sensor: sensorObj}
      } else {
        // Missing parameters
        return response.status(400)
      }
    } else {
      // Missing parameters
      return response.status(400)
    }


  }

  /*------------------------
  > FONCTIONS UTILITAIRES
  -----------------------*/

  private async _getSiteState(siteId: number, siteStatusId: number) {

    const siteStations = await SiteStationAssociation.query().where('siteId', '=', siteId)
    const signalisationDevicesInAlarm: any[] = []
    for (const siteStation of siteStations) {
      const stationDevices = await DeviceStationAssociation.query().where('stationId', '=', siteStation.stationId)
      for (const stationDevice of stationDevices) {
        const device = await Device.find(stationDevice.deviceId)
        if (device && db_constants.DEVICE_TYPE_SIGN_IDS.includes(device.typeId)) {
          const deviceObj = device.toJSON()
          const deviceAlarmAssoc = await DeviceAlarmAssociation.query()
            .select('*')
            .where('device_id', '=', deviceObj.id)
            .orderBy('timestamp', 'desc')
            .first();
          if (deviceAlarmAssoc) {
            deviceObj.alarm = deviceAlarmAssoc ? await AlarmStatus.find(deviceAlarmAssoc.alarmId) : null
            signalisationDevicesInAlarm.push(deviceObj)
          }
        }
      }
    }

    // get the last default association
    const defaultLevelAssoc = await SiteDefaultAssociation.query()
      .select('siteId', 'defaultId', 'timestamp')
      .where('siteId', '=', siteId)
      .orderBy('timestamp', 'desc')
      .first();

    // get the last alarm association
    const alarmLevelAssoc = await SiteAlarmAssociation.query()
      .select('*')
      .where('siteId', '=', siteId)
      .orderBy('timestamp', 'desc')
      .first();


    // get the site status
    const siteStatus = await SiteStatus.query()
      .select('*')
      .where('id', '=', siteStatusId)
      .first();


    const siteDefault = await DefaultStatus.findBy(defaultLevelAssoc ? 'id' : 'level', defaultLevelAssoc ? defaultLevelAssoc.defaultId : 1)
    const siteAlarm = await AlarmStatus.findBy(alarmLevelAssoc ? 'id' : 'level', alarmLevelAssoc ? alarmLevelAssoc.alarmId : 1)

    return {
      status: siteStatus,
      alarm: siteAlarm?.toJSON(),
      default: siteDefault?.toJSON(),
      signalisationDevicesInAlarm: signalisationDevicesInAlarm
    }

  }

  private async _getSiteSignalStatus(siteId: number) {

    const siteSignalStatus = await SiteSignalAssociation.query()
      .where('siteId', '=', siteId)
      .orderBy('timestamp', 'desc')
      .first();

    const signalStatus = siteSignalStatus ? await SignalStatus.find(siteSignalStatus?.signalId) : {level: 'SGNLON'};

    return signalStatus?.level;

  }

  private async _getStationById(site_id: number, target_id: number) {
    const siteStationAssociations = await SiteStationAssociation.query()
      .where('siteId', '=', site_id)
      .andWhere('stationId', '=', target_id)
    return await Station.find(siteStationAssociations[0].stationId)
  }

  private async _getSensorDatas(sensorId: number): Promise<any[]> {
    const sensorDatasObjects: any[] = [];
    const sensorDataAssocs = await SensorDataAssociation.query().select('*').where('sensorId', '=', sensorId)
    for (let sensorDataAssoc of sensorDataAssocs) {
      const dataType = await DataType.find(sensorDataAssoc.dataTypeId)
      const dataTypeObj = dataType?.toJSON()
      const sensorData = await SensorData.query().where('sensor_data_association_id', '=', sensorDataAssoc.id).orderBy('timestamp', 'desc').first()
      if (sensorData) {
        const sensorDataObj: any = sensorData.toJSON()
        sensorDataObj.dataType = dataTypeObj
        sensorDatasObjects.push(sensorDataObj)
      }
    }
    return sensorDatasObjects;
  }

  // retrieves the user role for the given site
  private async _getUserRolePerSite(site_id: number, user_id: number) {
    const siteUserAssociation = await UserSiteAssociation.query()
      .where('siteId', '=', site_id)
      .andWhere('userId', '=', user_id)
      .orderBy('roleId', 'asc');
    if (siteUserAssociation[0]) {
      return siteUserAssociation[0].roleId;
    } else {
      return null;
    }
  }

  // checks that a role is superior or same as other
  private async _checkRoleMatchesOther(role_id: number, other_role_id: number) {
    //const firstRole = await Role.query().where('id', '=', role_id);
    //const otherRole = await Role.query().where('id', '=', other_role_id);
    // todo adapt logic
    if (role_id <= other_role_id) {
      return true;
    } else {
      return false;
    }
  }

  private _checkJsonHasSameStructure(obj1, obj2) {
    const keys1 = Object.keys(obj1);
    const keys2 = Object.keys(obj2);
    return keys1.length === keys2.length && keys1.every(key => keys2.includes(key));
  }
}
