import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Env from "@ioc:Adonis/Core/Env";
import Language from "../../Models/Language";
import Wording from "../../Models/Wording";
import WordingCateg from "../../Models/WordingCateg";

export default class SharedApisController {

  public async getWording ({ params }: HttpContextContract){
    const language = await Language.findBy('code',params.langId.toUpperCase())?? await Language.findBy('code', Env.get('APP_LANGUAGE') )
    const context = await WordingCateg.findBy('name',params.contextId.toLowerCase())
    const allWording = await Wording.query()
      .where('contextId', '=', context!.id)
      .orWhere('contextId', '=', 1)
    return allWording?.filter((w: Wording) => w.languageId===language!.id)
  }

}
