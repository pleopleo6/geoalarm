import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {AuthenticationException} from "@adonisjs/auth/build/standalone";

export default class ApiAuth {
  public async handle({ auth }: HttpContextContract, next: () => Promise<void>) {

    if(await auth.use('api').authenticate()){
      return await next()
    }

    throw new AuthenticationException(
      'Unauthorized access',
      'E_UNAUTHORIZED_ACCESS',
    )
  }
}
