import type {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import db_constants from "../../config/constants";


export default class IPWhitelistMiddleware {
  public async handle({request, response}: HttpContextContract, next: () => Promise<void>) {

    const clientIP = request.ip;

    if (clientIP.toString() == db_constants.SERVER_IP) {
      await next();
    } else {
      // If the client's IP is not in the whitelist, deny access with a 403 Forbidden response
      return response.status(403).send('Forbidden');
    }
  }
}
