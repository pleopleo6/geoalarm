import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import {DateTime} from 'luxon'
import DeviceDataAssociation from './DeviceDataAssociation';

export default class DeviceData extends BaseModel {

  static table = 'device_datas';

  @column({isPrimary: true, serializeAs: null})
  public id: number

  @column()
  public data: number

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime

  @column({serializeAs: null})
  public device_data_association_id: number

  @hasOne(() => DeviceDataAssociation, {foreignKey: 'id'})
  public device_data_association: HasOne<typeof DeviceDataAssociation>

}
