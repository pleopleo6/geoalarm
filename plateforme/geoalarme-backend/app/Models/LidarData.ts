import {DateTime} from 'luxon'
import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class LidarData extends BaseModel {

  static table = 'lidar_data';

  @column({isPrimary: true, serializeAs: null})
  public id: number


  @column()
  public lidar_name: string


  @column()
  public data: object

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime

}
