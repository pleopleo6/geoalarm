import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Language from './Language';
import WordingCateg from './WordingCateg';

export default class Wording extends BaseModel {

  @column({ isPrimary: true, serializeAs: null })
  public id: number

  @column()
  public name: string

  @column()
  public value: string

  @column({ serializeAs: null })
  public languageId: number

  @hasOne(() => Language, { foreignKey: 'id' })
  public language: HasOne<typeof Language>

  @column({ serializeAs: null })
  public contextId?: number

  @column({ serializeAs: null })
  @hasOne(() => WordingCateg, { foreignKey: 'id' })
  public context: HasOne<typeof WordingCateg>

}
