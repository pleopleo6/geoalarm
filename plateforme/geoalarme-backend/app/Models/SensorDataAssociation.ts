import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import DataType from "./DataType";
import Sensor from "./Sensor";

export default class SensorDataAssociation extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public sensorId: number

  @hasOne(() => Sensor, { foreignKey: 'id' })
  public sensor: HasOne<typeof Sensor>

  @column({ serializeAs: null })
  public dataTypeId: number

  @hasOne(() => DataType, { foreignKey: 'id' })
  public dataType: HasOne<typeof DataType>

}
