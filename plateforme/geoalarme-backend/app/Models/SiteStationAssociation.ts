import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import Site from "./Site";
import Station from "./Station";

export default class SiteStationAssociation extends BaseModel {

  static table = 'station_site_associations';

  @column({isPrimary: true})
  public id: number

  @column({ serializeAs: null })
  public stationId: number

  @hasOne(() => Station, { foreignKey: 'id' })
  public station: HasOne<typeof Station>

  @column({ serializeAs: null })
  public siteId: number

  @hasOne(() => Site, { foreignKey: 'id' })
  public site: HasOne<typeof Site>

}
