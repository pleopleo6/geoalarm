import {DateTime} from 'luxon'
import {BaseModel, column, HasOne, hasOne, beforeUpdate} from '@ioc:Adonis/Lucid/Orm'
import SiteStatus from "./SiteStatus";
import Entreprise from "./Entreprise";
import Polygon from "polygon";
import UserSiteAssociation from "./UserSiteAssociation";
import UserSendTypeAssociation from "./UserSendTypeAssociation";
import EventType from "./EventType";
import User from "./User";
import EcallMessage from "../Services/ecall_message/EcallMessage";
import Wording from "./Wording";
import Env from "../../env";
import WebPushService from "../Services/webpush/WebPushService";


export default class Site extends BaseModel {

  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public statusId: number

  @hasOne(() => SiteStatus, {foreignKey: 'id'})
  public status: HasOne<typeof SiteStatus>

  @column({serializeAs: null})
  public entrepriseId: number

  @hasOne(() => Entreprise, {foreignKey: 'id'})
  public entreprise: HasOne<typeof Entreprise>

  @column()
  public name: string

  @column()
  public area?: Polygon

  @column()
  public siteId?: string

  @column()
  public latitude?: number

  @column()
  public longitude?: number

  @column()
  public altitude?: number

  @column()
  public city: string

  @column()
  public district: string

  @column()
  public country: string

  @column.dateTime({autoCreate: true, autoUpdate: false})
  public lastConnexion: DateTime

  @column.dateTime({autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({autoCreate: true, autoUpdate: false})
  public updatedAt: DateTime


  // @ts-ignore
  @beforeUpdate()
  public static async handleBeforeUpdate(site: Site) {
    const oldStatusId = site.$original.statusId;
    const newStatusId = site.statusId;

    if (oldStatusId != newStatusId) {
      await Site.manageNotif(site);
      await Site.manageSms(site);
    }
  }

  private static async manageNotif(site: Site) {

    // Get list of users to send the notififcation
    const siteUsers = await UserSiteAssociation.query()
      .where('siteId', site.id)

    // if there are users, prepare the notification
    if (siteUsers) {

      const siteStatus = await SiteStatus.findBy("id", site.statusId)

      //loop through users and send them the notif
      for (const siteUser of siteUsers) {
        const user = await User.findBy('id', siteUser.userId);

        if (user && user.webpushSubscription) {
          const notifBody = await Wording.query()
            .where('languageId', '=', user!.languageId)
            .where('name', '=', 'webpush_notification_site_status').first();
          // @ts-ignore
          const notifStatusChange: string = notifBody.value.replace('%siteStatus%', siteStatus.name);

          // définit le message à envoyer
          const notification = {
            notification: {
              // @ts-ignore
              icon: `https://${Env.HOST}/assets/mobile/icons/icon-512x512.png`,
              title: site.name,
              body: notifStatusChange,
              data: {
                // @ts-ignore
                url: `https://${Env.HOST}/sites/${site.id}`
              }
            }
          }

          // generate the notification
          WebPushService.notify(JSON.parse(user.webpushSubscription), JSON.stringify(notification))
        }
      }
    }
  }

  private static async manageSms(site: Site) {
    const fs = require('fs');
    // get list of all users from site
    const siteUsers = await UserSiteAssociation.query()
      .where('siteId', site.id)

    if (siteUsers) {

      const siteStatus = await SiteStatus.findBy("id", site.statusId)

      // list that will contain all phone numbers to send sms
      let phoneNumbers = [];

      // loop through all users from site
      for (const siteUser of siteUsers) {

        // get the object containing notifying information
        const user_send_types = await UserSendTypeAssociation.query()
          .where('siteId', siteUser.siteId)
          .andWhere('userId', siteUser.userId)
          .first();

        if (user_send_types) {

          // user_send_types.data is a variable representing  JSON object
          const dataObject = user_send_types.data;

          // Check the type of dataObject
          if (typeof dataObject === 'object' && dataObject !== null) {

            // Accessing eventIds array and getting the first element
            const eventIds = dataObject.eventIds || [];

            const event = await EventType.query()
              .where('level', siteStatus.levelNum)
              .andWhere('typeId', 3)
              .first();

            // check if the array of notifications for the given user and site contains the event
            if (event && eventIds.includes(event.id)) {
              const user = await User.findBy('id', siteUser.userId);
              phoneNumbers.push(user.mobilePhoneNumber);
            }
          }
        }
      }

      if (phoneNumbers.length != 0) {
        const smsBody = await Wording.query()
          .where('languageId', '=', 1)
          .where('name', '=', 'sms_notification_site_status').first();
        // @ts-ignore
        const smsTxt: string = smsBody.value.replace('%siteStatus%', siteStatus.name).replace('%siteName%', site.name);

        await EcallMessage.sendMessage(phoneNumbers, smsTxt);
      }
    }
  }
}
