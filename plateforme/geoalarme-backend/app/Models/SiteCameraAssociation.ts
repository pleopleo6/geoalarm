import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Camera from "./Camera";
import Site from "./Site";

export default class SiteCameraAssociation extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public siteId: number

  @hasOne(() => Site, { foreignKey: 'id' })
  public site: HasOne<typeof Site>

  @column({ serializeAs: null })
  public cameraId: number

  @hasOne(() => Camera, { foreignKey: 'id' })
  public camera: HasOne<typeof Camera>

}
