import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import Command from "./Command";
import Site from "./Site";

export default class SiteCommandAssociation extends BaseModel {

  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public siteId: number

  @hasOne(() => Site, {foreignKey: 'id'})
  public site: HasOne<typeof Site>

  @column({serializeAs: null})
  public commandId: number

  @hasOne(() => Command, {foreignKey: 'id'})
  public command: HasOne<typeof Command>
}
