import {DateTime} from 'luxon'
import {BaseModel, column, HasOne, hasOne, afterCreate} from '@ioc:Adonis/Lucid/Orm'
import Site from "./Site"
import AlarmStatus from "./AlarmStatus"
import Ws from "../Services/websocket/Ws";
import User from "./User";
import CurrentWs from "./CurrentWs";
import UserSiteAssociation from "./UserSiteAssociation";
import WebPushService from "../Services/webpush/WebPushService";
import Env from "../../env";
import Wording from "./Wording";

import EcallMessage from "../Services/ecall_message/EcallMessage";
import EventType from "./EventType";
import db_constants from "../../config/constants";
import UserSendTypeAssociation from "./UserSendTypeAssociation";


export default class SiteAlarmAssociation extends BaseModel {

  @column({isPrimary: true})
  public id: number;

  @column({serializeAs: null})
  public siteId: number

  @hasOne(() => Site, {foreignKey: 'id'})
  public site: HasOne<typeof Site>

  @column({serializeAs: null})
  public alarmId: number

  @hasOne(() => AlarmStatus, {foreignKey: 'id'})
  public alarm: HasOne<typeof AlarmStatus>

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime

  // @ts-ignore
  @afterCreate()
  public static async handleAfterCreate(siteAlarmAssociation: SiteAlarmAssociation) {
    //await SiteAlarmAssociation.manageWs(siteAlarmAssociation);
    await SiteAlarmAssociation.manageNotif(siteAlarmAssociation);
    await SiteAlarmAssociation.manageSms(siteAlarmAssociation);
  }

  private static async manageNotif(siteAlarmAssociation: SiteAlarmAssociation) {
    // WEBPUSH NOTIF MANAGEMENT
    const alarm = await AlarmStatus.findBy("id", siteAlarmAssociation.alarmId)
    const site = await Site.findBy("id", siteAlarmAssociation.siteId)
    if (alarm && site) {

      // Get list of users to send the notififcation
      const siteUsers = await UserSiteAssociation.query()
        .where('siteId', siteAlarmAssociation.siteId)

      // if there are users, prepare the notification
      if (siteUsers) {
        //loop through users and send them the notif
        for (const siteUser of siteUsers) {
          const user = await User.findBy('id', siteUser.userId);
          if (user && user.webpushSubscription) {

            const notifBody = await Wording.query()
              .where('languageId', '=', user!.languageId)
              .where('name', '=', 'webpush_notification_site_alarm').first();
            // @ts-ignore
            const notifAlarmTxt: string = notifBody.value.replace('%alarmLevel%', alarm.level).replace('%alarmName%', alarm.description);

            // définit le message à envoyer
            const notification = {
              notification: {
                // @ts-ignore
                icon: `https://${Env.HOST}/assets/mobile/icons/icon-512x512.png`,
                title: site.name,
                body: notifAlarmTxt,
                data: {
                  // @ts-ignore
                  url: `https://${Env.HOST}/sites/${site.id}`
                }
              }
            }

            // generate the notification
            WebPushService.notify(JSON.parse(user.webpushSubscription), JSON.stringify(notification))
          }
        }
      }

    }
  }


  // FIXME : DEPRECATED, because _manageWs is called upon site change in CAHandler anyways
  private static async manageWs(siteAlarmAssociation: SiteAlarmAssociation) {
    // WEBSOCKET MANAGEMENT
    const connUsers = await CurrentWs.all();
    if (connUsers) {
      for (const connUser of connUsers) {
        const userToSite = await UserSiteAssociation.query()
          .where('siteId', siteAlarmAssociation.siteId)
          .where('userId', '=', connUser.userId);
        if (userToSite) {
          const user = await User.query().where('id', connUser.userId).first()
          Ws.emit({change: {object: 'site_change', target_id: siteAlarmAssociation.siteId}}, user!)
        }
      }
    }
  }

  private static async manageSms(siteAlarmAssociation: SiteAlarmAssociation) {

    // For now, only alarms lvl 4 send SMS
    const alarm = await AlarmStatus.findBy("id", siteAlarmAssociation.alarmId);
    if (alarm.level == db_constants.ALARM_EVENT_IN_PROGRESS_LVL) {

      // get list of all users from site
      const siteUsers = await UserSiteAssociation.query()
        .where('siteId', siteAlarmAssociation.siteId)

      if (siteUsers) {
        // list that will contain all phone numbers to send sms
        let phoneNumbers = [];

        // loop through all users from site
        for (const siteUser of siteUsers) {

          // get the object containing notifying information
          const user_send_types = await UserSendTypeAssociation.query()
            .where('siteId', siteUser.siteId)
            .andWhere('userId', siteUser.userId)
            .first();

          if (user_send_types) {
            // Assuming user_send_types.data is a variable representing your JSON object
            const dataObject = user_send_types.data;

            // Check the type of dataObject
            if (typeof dataObject === 'object' && dataObject !== null) {
              // Accessing eventIds array and getting the first element
              const eventIds = dataObject.eventIds || [];

              const event = await EventType.query()
                .where('level', alarm.level)
                .andWhere('typeId', 1)
                .first();

              if (event && eventIds.includes(event.id)) {
                const user = await User.findBy('id', siteUser.userId);
                phoneNumbers.push(user.mobilePhoneNumber);
              }
            }
          }
        }
        if (phoneNumbers.length != 0) {
          const site = await Site.findBy("id", siteAlarmAssociation.siteId);

          const smsBody = await Wording.query()
            .where('languageId', '=', 1)
            .where('name', '=', 'sms_notification_site_alarm').first();
          // @ts-ignore
          let smsTxt: string = smsBody.value.replace('%alarmLevel%', alarm.level).replace('%alarmName%', alarm.description).replace('%siteName%', site.name);
          if (site.id === 11) {
            smsTxt = "Site du Comblonard en alarme niveau 4. Suivre svp la procédure prévue";

          }
          await EcallMessage.sendMessage(phoneNumbers, smsTxt);
        }
      }
    }
  }
}
