import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import SensorType from "./SensorType";

export default class Sensor extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public typeId: number

  @hasOne(() => SensorType, { foreignKey: 'id' })
  public type: HasOne<typeof SensorType>

  @column()
  public location?: number

  @column()
  public name: string

  @column()
  public sensorId: string

  @column()
  public latitude: number

  @column()
  public longitude: number

  @column()
  public altitude: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

}
