import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import EventTypeType from "./EventTypeType";

export default class EventType extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column(/*{ serializeAs: null }*/)
  public typeId: number

  @hasOne(() => EventTypeType, { foreignKey: 'id' })
  public type: HasOne<typeof EventTypeType>

  @column()
  public level?: number

  @column()
  public labelName: string

}
