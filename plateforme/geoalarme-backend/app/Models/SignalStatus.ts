import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class SignalStatus extends BaseModel {

  static table = 'signal_status';

  @column({isPrimary: true})
  public id: number

  @column()
  public level: string

  @column()
  public description?: string

  @column()
  public levelNum?: number

}
