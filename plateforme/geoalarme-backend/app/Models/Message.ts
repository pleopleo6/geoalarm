import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Language from "./Language";
import Wording from "./Wording";

export default class Message extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column({ serializeAs: null })
  public languageId: number

  @hasOne(() => Language, { foreignKey: 'id' })
  public language: HasOne<typeof Language>

  @column({ serializeAs: null })
  public contentId: number

  @hasOne(() => Wording, { foreignKey: 'id' })
  public content: HasOne<typeof Wording>

}
