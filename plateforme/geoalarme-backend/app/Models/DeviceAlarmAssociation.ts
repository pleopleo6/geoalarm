import {DateTime} from 'luxon'
import {afterCreate, BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import Device from "./Device";
import AlarmStatus from "./AlarmStatus";
import Ws from "../Services/websocket/Ws";
import CurrentWs from "./CurrentWs";
import SiteStationAssociation from "./SiteStationAssociation";
import UserSiteAssociation from "./UserSiteAssociation";
import User from "./User";
import DeviceStationAssociation from "./DeviceStationAssociation";


export default class DeviceAlarmAssociation extends BaseModel {

  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public deviceId: number

  @hasOne(() => Device, {foreignKey: 'id'})
  public device: HasOne<typeof Device>

  @column({serializeAs: null})
  public alarmId: number

  @hasOne(() => AlarmStatus, {foreignKey: 'id'})
  public alarm: HasOne<typeof AlarmStatus>

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime

  @afterCreate()
  public static async handleAfterCreate(deviceAlarmAssociation: DeviceAlarmAssociation) {

    const deviceStations = await DeviceStationAssociation.query()
      .where('deviceId', deviceAlarmAssociation.deviceId)

    if (deviceStations) {
      for (const deviceStation of deviceStations) {
        const siteStations = await SiteStationAssociation.query()
          .where('stationId', deviceStation.stationId)

        if (siteStations) {
          for (const siteStation of siteStations) {

            // HANDLE WEBSOCKET
            const connUsers = await CurrentWs.all();
            if (connUsers) {
              for (const connUser of connUsers) {
                const userToSite = await UserSiteAssociation.query()
                  .where('siteId', siteStation.siteId)
                  .where('userId', '=', connUser.userId);

                if (userToSite) {
                  const user = await User.query().where('id', connUser.userId).first()
                  if (user) {
                    //Ws.emit({change: {object: 'site_change', target_id: siteStation.siteId}}, user)
                    Ws.emit({change: {object: 'device_change', target_id: deviceAlarmAssociation.deviceId}}, user)
                  }
                }
              }
            }
            /*
                        // WEBPUSH NOTIF MANAGEMENT
                        const alarm = await AlarmStatus.findBy("id", deviceAlarmAssociation.alarmId)
                        const device = await Device.findBy("id", deviceAlarmAssociation.deviceId)
                        if (alarm && device) {
                          if (alarm.level == db_constants.ALARM_EVENT_IN_PROGRESS_LVL || alarm.level == db_constants.ALARM_IMMINENT_DANGER_LVL) {

                            // Get list of users to send the notififcation
                            const siteUsers = await UserSiteAssociation.query()
                              .where('siteId', siteStation.siteId)

                            // if there are users, prepare the notification
                            if (siteUsers) {
                              //loop through users and send them the notif
                              for (const siteUser of siteUsers) {
                                const user = await User.findBy('id', siteUser.userId);
                                if (user && user.webpushSubscription) {

                                  const notifBody= await Wording.query()
                                    .where('languageId', '=', user!.languageId)
                                    .where('name', '=', 'webpush_notification_device_alarm').first();
                                  // @ts-ignore
                                  const notifBodyTxt: string = notifBody.value.replace('%alarmLevel%', alarm.level).replace('%targetName%', device.name);

                                  const notifTitleObj = await Wording.query()
                                    .where('languageId', '=', user!.languageId)
                                    .where('name', '=', 'webpush_notification_title').first();
                                  // @ts-ignore
                                  const notifTitle: string = notifTitleObj.value;

                                  // définit le message à envoyer
                                  const notification = {
                                    notification: {
                                      icon: `https://${Env.HOST}/assets/mobile/icons/icon-512x512.png`,
                                      title: notifTitle,
                                      body: notifBodyTxt,
                                      data: {
                                        url: `https://${Env.HOST}/sites/${siteStation.siteId}/stations/${siteStation.stationId}/${device.id}`
                                      }
                                    }
                                  }

                                  // generate the notification
                                  if (typeof user.webpushSubscription === "string") {
                                    WebPushService.notify(JSON.parse(user.webpushSubscription), JSON.stringify(notification))
                                  }
                                }
                              }
                            }
                          }
                        }
                        */
          }
        }
      }
    }
  }
}
