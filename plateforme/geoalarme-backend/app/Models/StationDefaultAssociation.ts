import {afterCreate, BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import {DateTime} from 'luxon'
import DefaultStatus from "./DefaultStatus";
import Station from "./Station";
import Ws from "../Services/websocket/Ws";
import CurrentWs from "./CurrentWs";
import UserSiteAssociation from "./UserSiteAssociation";
import User from "./User";
import SiteStationAssociation from "./SiteStationAssociation";

export default class StationDefaultAssociation extends BaseModel {

  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public stationId: number

  @hasOne(() => Station, {foreignKey: 'id'})
  public station: HasOne<typeof Station>

  @column({serializeAs: null})
  public defaultId: number

  @hasOne(() => DefaultStatus, {foreignKey: 'id'})
  public default: HasOne<typeof DefaultStatus>

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime

  @afterCreate()
  public static async handleAfterCreate(stationDefaultAssociation: StationDefaultAssociation) {

    const siteStations = await SiteStationAssociation.query()
      .where('stationId', stationDefaultAssociation.stationId)

    if (siteStations) {
      for (const siteStation of siteStations) {
        // update websocket
        const connUsers = await CurrentWs.all();
        if (connUsers) {
          for (const connUser of connUsers) {
            const userToSite = await UserSiteAssociation.query()
              .where('siteId', siteStation.siteId)
              .where('userId', '=', connUser.userId);

            if (userToSite) {

              const user = await User.query().where('id', connUser.userId).first()
              Ws.emit({change: {object: 'station_change', target_id: stationDefaultAssociation.stationId}}, user!)
              console.log("sent")
              break;
            }
          }
        }

        /*
        // WEB PUSH NOTIF
        const defaultObj = await DefaultStatus.findBy('id', stationDefaultAssociation.defaultId)
        const station = await Station.findBy("id", stationDefaultAssociation.stationId)
        if (defaultObj && station) {
          if (defaultObj.level == db_constants.DEFAULT_CRITICAL_3 || defaultObj.level == db_constants.DEFAULT_STATION_4) {

            // Get list of users to send the notififcation
            const siteUsers = await UserSiteAssociation.query()
              .where('siteId', siteStation.siteId)

            // if there are users, prepare the notification
            if (siteUsers) {

              //loop through users and send them the notif
              for (const siteUser of siteUsers) {
                const user = await User.findBy('id', siteUser.userId);
                if (user && user.webpushSubscription) {

                  const notifBody = await Wording.query()
                    .where('languageId', '=', user!.languageId)
                    .where('name', '=', 'webpush_notification_station_default').first();
                  // @ts-ignore
                  const notifBodyTxt: string = notifBody.value.replace('%defaultLevel%', defaultObj.level).replace('%targetName%', station.name);

                  const notifTitleObj = await Wording.query()
                    .where('languageId', '=', user!.languageId)
                    .where('name', '=', 'webpush_notification_title').first();
                  // @ts-ignore
                  const notifTitle: string = notifTitleObj.value;

                  // définit le message à envoyer
                  const notification = {
                    notification: {
                      icon: `https://${Env.HOST}/assets/mobile/icons/icon-512x512.png`,
                      title: notifTitle,
                      body: notifBodyTxt,
                      data: {
                        url: `https://${Env.HOST}/sites/${siteStation.siteId}/stations/${siteStation.stationId}`
                      }
                    }
                  }

                  // generate the notification
                  if (typeof user.webpushSubscription === "string") {
                    WebPushService.notify(JSON.parse(user.webpushSubscription), JSON.stringify(notification))
                  }
                }
              }
            }
          }
        }
*/
      }
    }

  }
}

