import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class StationStatus extends BaseModel {
  static table = 'station_status';

  @column({isPrimary: true})
  public id: number

  @column()
  public name: string

  @column()
  public description?: string

}
