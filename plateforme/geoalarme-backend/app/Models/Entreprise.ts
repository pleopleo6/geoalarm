import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Entreprise extends BaseModel {

  @column({ isPrimary: true, serializeAs: null })
  public id: number

  @column()
  public name: string

  @column()
  public adresse: string

  @column()
  public adresse_2: string

  @column()
  public npa: number

  @column()
  public city: string

  @column()
  public country: string

  @column()
  public phone: string

  @column()
  public email: string

  @column.dateTime({ autoCreate: true, serializeAs: null })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: null })
  public updatedAt: DateTime

}
