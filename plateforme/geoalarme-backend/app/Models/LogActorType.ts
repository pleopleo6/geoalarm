import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class LogActorType extends BaseModel {

  @column({ isPrimary: true, serializeAs: null })
  public id: number

  @column()
  public name: string

  @column()
  public description: string

}
