import { DateTime } from 'luxon'
import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import SensorDataAssociation from "./SensorDataAssociation";

export default class SensorData extends BaseModel {

  static table = 'sensor_datas';

  @column({ isPrimary: true, serializeAs: null })
  public id: number

  @column()
  public data: number

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public timestamp: DateTime

  @column({ serializeAs: null })
  public sensor_data_association_id: number

  @hasOne(() => SensorDataAssociation, { foreignKey: 'id' })
  public sensor_data_association: HasOne<typeof SensorDataAssociation>
}
