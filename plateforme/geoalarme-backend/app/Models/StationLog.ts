import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import LogType from "./LogType";
import Station from "./Station";

export default class StationLog extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public stationId: number

  @belongsTo(() => Station)
  public station: BelongsTo<typeof Station>

  @column({ serializeAs: null })
  public logTypeId: number

  @belongsTo(() => LogType,{ serializeAs: 'log_type' })
  public logType: BelongsTo<typeof LogType>

  @column()
  public logData: string

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public logTimestamp: DateTime

}
