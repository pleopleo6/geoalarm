import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import DataType from "./DataType";
import Device from "./Device";

export default class DeviceDataAssociation extends BaseModel {

  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public deviceId: number

  @hasOne(() => Device, {foreignKey: 'id'})
  public device: HasOne<typeof Device>

  @column({serializeAs: null})
  public dataTypeId: number

  @hasOne(() => DataType, {foreignKey: 'id'})
  public dataType: HasOne<typeof DataType>

}
