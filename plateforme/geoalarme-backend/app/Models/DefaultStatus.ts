import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class DefaultStatus extends BaseModel {
  static table = 'default_status';
  @column({isPrimary: true})
  public id: number

  @column()
  public level: number

  @column()
  public description?: string

}
