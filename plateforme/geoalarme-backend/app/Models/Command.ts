import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Role from "./Role";
import CommandValidationLevel from "./CommandValidationLevel";

export default class Command extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public innerId: string

  @column()
  public name: string

  @column()
  public labelId: string

  @column()
  public descriptionId: string

  @column()
  public command: string

  @column()
  public params?: any

  @column({ serializeAs: null })
  public roleId: number

  @hasOne(() => Role, { foreignKey: 'id' })
  public role: HasOne<typeof Role>

  @column({ serializeAs: null })
  isGlobal: boolean = false

  @column()
  public priority: number

  @column()
  public validationLevelId: number

  @hasOne(() => CommandValidationLevel, { foreignKey: 'id' })
  public validationLevel: HasOne<typeof CommandValidationLevel>

}
