import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class DeviceStatus extends BaseModel {
  static table = 'device_status';

  @column({isPrimary: true})
  public id: number

  @column()
  public name: string

  @column()
  public description?: string

}
