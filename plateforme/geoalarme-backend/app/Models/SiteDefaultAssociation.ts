import {DateTime} from 'luxon'
import {afterCreate, BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import DefaultStatus from "./DefaultStatus";
import Site from "./Site";
import UserSiteAssociation from "./UserSiteAssociation";
import User from "./User";
import WebPushService from "../Services/webpush/WebPushService";
import Env from "../../env";
import Wording from "./Wording";

export default class SiteDefaultAssociation extends BaseModel {

  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public siteId: number

  @hasOne(() => Site, {foreignKey: 'id'})
  public site: HasOne<typeof Site>

  @column({serializeAs: null})
  public defaultId: number

  @hasOne(() => DefaultStatus, {foreignKey: 'id'})
  public default: HasOne<typeof DefaultStatus>

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime


  @afterCreate()
  public static async handleAfterCreate(siteDefaultAssociation: SiteDefaultAssociation) {
    const MIN_LEVEL_FOR_NOTIF = 3;
    /*
    const connUsers = await CurrentWs.all();

    if (connUsers) {
      for (const connUser of connUsers) {
        const userToSite = await UserSiteAssociation.query()
          .where('siteId', siteDefaultAssociation.siteId)
          .where('userId', '=', connUser.userId);

        if (userToSite) {
          const user = await User.query().where('id', connUser.userId).first()
          Ws.emit({change: {object: 'site_change', target_id: siteDefaultAssociation.siteId}}, user!)
        }
      }
    }
*/
    const defaultObj = await DefaultStatus.findBy('id', siteDefaultAssociation.defaultId)
    const site = await Site.findBy("id", siteDefaultAssociation.siteId)


    if (defaultObj && site) {
      if (defaultObj.level >= MIN_LEVEL_FOR_NOTIF) {

        // Get list of users to send the notififcation
        const siteUsers = await UserSiteAssociation.query()
          .where('siteId', siteDefaultAssociation.siteId)

        // if there are users, prepare the notification
        if (siteUsers) {
          //loop through users and send them the notif
          for (const siteUser of siteUsers) {
            const user = await User.findBy('id', siteUser.userId);

            if (user && user.webpushSubscription) {

              const notifBody = await Wording.query()
                .where('languageId', '=', user!.languageId)
                .where('name', '=', 'webpush_notification_site_default').first();
              // @ts-ignore
              const notifBodyTxt: string = notifBody.value.replace('%defaultLevel%', defaultObj.level).replace('%defaultName%', defaultObj.description);


              // définit le message à envoyer
              const notification = {
                notification: {
                  icon: `https://${Env.HOST}/assets/mobile/icons/icon-512x512.png`,
                  title: site.name,
                  body: notifBodyTxt,
                  data: {
                    // @ts-ignore
                    url: `https://${Env.HOST}/sites/${site.id}`
                  }
                }
              }
              // generate the notification
              if (typeof user.webpushSubscription === "string") {
                WebPushService.notify(JSON.parse(user.webpushSubscription), JSON.stringify(notification))
              }
            }
          }
        }
      }
    }
  }
}
