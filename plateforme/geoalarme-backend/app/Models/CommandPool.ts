import {DateTime} from 'luxon'
import {afterUpdate, BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import Site from "./Site";
import Command from "./Command";
import CommandState from "./CommandState";
import User from './User';
import Ws from "../Services/websocket/Ws";
import db_constants from "../../config/constants";


export default class CommandPool extends BaseModel {
  static table = 'command_pools';

  @column({isPrimary: true})
  public id: number

  @column()
  public uid: string

  @column({serializeAs: null})
  public siteId: number

  @hasOne(() => Site, {foreignKey: 'id'})
  public site: HasOne<typeof Site>

  @column({serializeAs: null})
  public userId: number

  @hasOne(() => User, {foreignKey: 'id'})
  public user: HasOne<typeof User>

  @column({serializeAs: null})
  public stateId: number

  @hasOne(() => CommandState, {foreignKey: 'id'})
  public commandState: HasOne<typeof CommandState>

  @column({serializeAs: null})
  public commandId: number

  @hasOne(() => Command, {foreignKey: 'id'})
  public command: HasOne<typeof Command>

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime

  @column()
  public params?: any

  @afterUpdate()
  public static async handleAfterUpdate(commandPool: CommandPool) {
    // send change through websocket if there are users concerned
    if (commandPool.stateId == db_constants.CMD_STATUS_CONFIRMED || commandPool.stateId == db_constants.CMD_STATUS_REJECTED) {
      const user = await User.query().where('id', commandPool.userId).first()
      Ws.emitCommand({change: {object: 'command_change', target: {state_id: commandPool.stateId}}}, user!)
      //Ws.emit({change: {object: 'site_change', target_id: 2}}, user)
    }
  }
}
