import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import Sensor from "./Sensor";
import Site from "./Site";

export default class SiteSensorAssociation extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public siteId: number

  @hasOne(() => Site, { foreignKey: 'id' })
  public site: HasOne<typeof Site>

  @column({ serializeAs: null })
  public sensorId: number

  @hasOne(() => Sensor, { foreignKey: 'id' })
  public sensor: HasOne<typeof Sensor>

}
