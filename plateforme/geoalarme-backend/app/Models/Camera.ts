import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import CameraType from "./CameraType";

export default class Camera extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public typeId: number

  @hasOne(() => CameraType, { foreignKey: 'id' })
  public type: HasOne<typeof CameraType>

  @column()
  public name: string

  @column()
  public location?: number

  @column()
  public url: string

  @column()
  public port: number

  @column()
  public url_ftp: string

  @column()
  public latitude: number

  @column()
  public longitude: number

  @column()
  public altitude: number

  @column()
  public username: string

  @column()
  public password: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

}
