import {DateTime} from 'luxon'
import {afterCreate, BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import Site from "./Site";
import SignalStatus from "./SignalStatus";
import CurrentWs from "./CurrentWs";
import UserSiteAssociation from "./UserSiteAssociation";
import User from "./User";
import Ws from "../Services/websocket/Ws";


export default class SiteSignalAssociation extends BaseModel {
  static table = 'site_signal_associations'

  @column({isPrimary: true})
  public id: number;

  @column({serializeAs: null})
  public siteId: number

  @hasOne(() => Site, {foreignKey: 'id'})
  public site: HasOne<typeof Site>

  @column({serializeAs: null})
  public signalId: number

  @hasOne(() => SignalStatus, {foreignKey: 'id'})
  public signal: HasOne<typeof SignalStatus>

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public timestamp: DateTime

  // @ts-ignore
  @afterCreate()
  public static async handleAfterCreate(siteSignalAssociation: SiteSignalAssociation) {
    //await SiteSignalAssociation.manageWs(siteSignalAssociation);
  }

// FIXME : DEPRECATED, because _manageWs is called upon site change in CAHandler anyways
  private static async manageWs(siteSignalAssociation: SiteSignalAssociation) {
    // WEBSOCKET MANAGEMENT
    const connUsers = await CurrentWs.all();
    if (connUsers) {
      for (const connUser of connUsers) {
        const userToSite = await UserSiteAssociation.query()
          .where('siteId', siteSignalAssociation.siteId)
          .where('userId', '=', connUser.userId);
        if (userToSite) {
          const user = await User.query().where('id', connUser.userId).first()
          Ws.emit({change: {object: 'site_change', target_id: siteSignalAssociation.siteId}}, user!)
        }
      }
    }
  }
}

