import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import LogType from "./LogType";

export default class GlobalLog extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public logTypeId: number

  @hasOne(() => LogType, { foreignKey: 'id' })
  public logType: HasOne<typeof LogType>

  @column()
  public logData: string

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public logTimestamp: DateTime

  @column()
  public actor: string

}
