import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class SiteStatus extends BaseModel {
  static table = 'site_status';

  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public description?: string

  @column()
  public levelNum?: number

}
