import {DateTime} from 'luxon'
import {BaseModel, column, belongsTo, BelongsTo} from '@ioc:Adonis/Lucid/Orm'
import LogType from "./LogType";
import LogActorType from "./LogActorType";
import Site from "./Site";

export default class SiteLog extends BaseModel {

  @column({isPrimary: true})
  public id: number

  @column({serializeAs: null})
  public siteId: number

  @belongsTo(() => Site)
  public site: BelongsTo<typeof Site>

  @column({serializeAs: null})
  public logTypeId: number

  @belongsTo(() => LogType,{ serializeAs: 'log_type' })
  public logType: BelongsTo<typeof LogType>

  @column()
  public logData?: any

  @column()
  public actorId: number

  @column({serializeAs: null})
  public actorTypeId: number

  @belongsTo(() => LogActorType,{ foreignKey: 'actorTypeId', serializeAs: 'actor_type' })
  public actorType: BelongsTo<typeof LogActorType>

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public logTimestamp: DateTime

}
