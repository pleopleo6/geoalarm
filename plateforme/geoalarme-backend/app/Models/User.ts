import { column, beforeSave, BaseModel, hasOne, HasOne } from '@ioc:Adonis/Lucid/Orm'
import Hash from '@ioc:Adonis/Core/Hash'
import { DateTime } from 'luxon'
import Language from "./Language";
import Entreprise from "./Entreprise";

export default class User extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public entrepriseId: number

  @hasOne(() => Entreprise, { foreignKey: 'id' })
  public entreprise: HasOne<typeof Entreprise>

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column( { serializeAs: null } )
  public rememberMeToken: string | null

  @column()
  public firstname: string

  @column()
  public lastname: string

  @column()
  public mobilePhoneNumber: string

  @column()
  public phoneNumber?: string

  @column()
  public idThreema?: string

  @column({ serializeAs: null })
  public languageId: number

  @hasOne(() => Language, { foreignKey: 'id' })
  public language: HasOne<typeof Language>

  @column({ serializeAs: null })
  public webpushSubscription: string | null

  @column.dateTime({ autoCreate: true, serializeAs: null })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: null })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }
}
