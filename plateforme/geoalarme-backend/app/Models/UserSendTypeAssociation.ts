import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Site from "./Site";
import User from "./User";

export default class UserSendTypeAssociation extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public siteId: number

  @hasOne(() => Site, { foreignKey: 'id' })
  public site: HasOne<typeof Site>

  @column()
  public userId: number

  @hasOne(() => User, { foreignKey: 'id' })
  public user: HasOne<typeof User>

  @column()
  public data: any

}
