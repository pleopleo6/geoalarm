import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import Device from "./Device";
import Station from "./Station";

export default class DeviceStationAssociation extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public deviceId: number

  @hasOne(() => Device, { foreignKey: 'id' })
  public device: HasOne<typeof Device>

  @column({ serializeAs: null })
  public stationId: number

  @hasOne(() => Station, { foreignKey: 'id' })
  public station: HasOne<typeof Station>

}
