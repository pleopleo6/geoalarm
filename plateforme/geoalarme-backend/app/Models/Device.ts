import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import DeviceStatus from "./DeviceStatus";
import DeviceType from "./DeviceType";
import DeviceData from "./DeviceData";
import AlarmStatus from "./AlarmStatus";
import DefaultStatus from "./DefaultStatus";

export default class Device extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public typeId: number

  @hasOne(() => DeviceType, { foreignKey: 'id' })
  public type: HasOne<typeof DeviceType>

  @column({ serializeAs: null })
  public statusId: number

  @hasOne(() => DeviceStatus, { foreignKey: 'id' })
  public status: HasOne<typeof DeviceStatus>

  @column({ serializeAs: null })
  public dataId: number

  @hasOne(() => DeviceData, { foreignKey: 'id' })
  public data: HasOne<typeof DeviceData>

  @column({ serializeAs: null })
  public alarmStatusId: number

  @hasOne(() => AlarmStatus, { foreignKey: 'id' })
  public alarmStatus: HasOne<typeof AlarmStatus>

  @column({ serializeAs: null })
  public defaultStatusId: number

  @hasOne(() => DefaultStatus, { foreignKey: 'id' })
  public defaultStatus: HasOne<typeof DefaultStatus>

  @column()
  public name: string

  @column()
  public deviceId: string

  @column()
  public position?: number[]

  @column()
  public latitude: number

  @column()
  public longitude: number

  @column()
  public altitude: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

}
