import { DateTime } from 'luxon'
import {BaseModel, column, HasOne, hasOne} from '@ioc:Adonis/Lucid/Orm'
import StationStatus from "./StationStatus";
import StationType from "./StationType";

export default class Station extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public typeId: number

  @hasOne(() => StationType, { foreignKey: 'id' })
  public type: HasOne<typeof StationType>

  @column({ serializeAs: null })
  public statusId: number

  @hasOne(() => StationStatus, { foreignKey: 'id' })
  public status: HasOne<typeof StationStatus>

  @column()
  public name: string

  @column()
  public stationId: string

  @column()
  public location?: number

  @column()
  public latitude: number

  @column()
  public longitude: number

  @column()
  public altitude: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
