import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class CurrentWs extends BaseModel {
  static table = 'current_ws';
  @column({isPrimary: true})
  public id: number

  @column()
  public userId: number
}
