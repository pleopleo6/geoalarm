import { BaseModel, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Site from "./Site";
import User from "./User";
import Role from "./Role";

export default class UserSiteAssociation extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column({ serializeAs: null })
  public userId: number

  @hasOne(() => User, { foreignKey: 'id' })
  public user: HasOne<typeof User>

  @column({ serializeAs: null })
  public siteId: number

  @hasOne(() => Site, { foreignKey: 'id' })
  public site: HasOne<typeof Site>

  @column({ serializeAs: null })
  public roleId: number

  @hasOne(() => Role, { foreignKey: 'id' })
  public role: HasOne<typeof Role>

}
