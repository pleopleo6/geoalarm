
# Projet Geoalarme

## Architecture
Le projet est structuré sous forme d'un mono-repository regroupant plusieurs sous-projet :
- plateforme :
    - geoalarme-backend : back-end Adonis

  
- ui :
    - geoalarme-back-office : application Angular 15 associée au back-office
    - geoalarme-desktop : application Angular 15 associée au client desktop
    - geoalarme-mobile : application Angular 15 associée au client mobile

## Mise en place

#### Cloner le dépos
```http
  git clone https://gitlab.com/pleopleo6/geoalarm.git
```

#### Ré-installer globalement l'ensemble des dépendances depuis la racine du projet cloné
```http
  pnpm install
```
#### Mettre à jour les paramètres se rapportant à postgreSQL dans le fichier .env du stack Adonis (plateforme/geoalarm-backend)
```http
  PG_USER={ local-postgre-user-name }
  PG_PASSWORD={ local-postgre-user-password }
  PG_DB_NAME={ local-postgre-db-name }
```

## Utilisation

Chaque cible peut être solicitée via une commande spécifique, toute commandes étant à lancer depuis la racine du projet lorsque l'attribut --filter est utlisé, ou depuis le sous-dossier concerné sans attribut --filter.

### Commandes associées au back-end

#### Démarrer le back-end en mode développement 
```http
  pnpm --filter @geoalarme/geoalarme-backend run dev
```

#### Construire le back-end 
```http
  pnpm --filter @geoalarme/geoalarme-backend run build
```

#### Lancer les tests back-end
```http
  pnpm --filter @geoalarme/geoalarme-backend run test
```

#### Démarrer le back-end en mode production 
```http
  pnpm --filter @geoalarme/geoalarme-backend run start
```


### Commandes associées à l'application desktop

#### Démarrer l'application desktop en mode développement 
```http
  pnpm --filter @geoalarme/geoalarme-desktop run dev
```

#### Construire l'application desktop
```http
  pnpm --filter @geoalarme/geoalarme-desktop run build
```

#### Lancer les tests sur l'application desktop 
```http
  pnpm --filter @geoalarme/geoalarme-desktop run test
```

#### Démarrer l'application desktop en mode production 
```http
  pnpm --filter @geoalarme/geoalarme-desktop run start
```

### Commandes associées à l'application mobile

#### Démarrer l'application mobile en mode développement 
```http
  pnpm --filter @geoalarme/geoalarme-mobile run dev
```

#### Construire l'application mobile
```http
  pnpm --filter @geoalarme/geoalarme-mobile run build
```

#### Lancer les tests sur l'application mobile
```http
  pnpm --filter @geoalarme/geoalarme-mobile run test
```

#### Démarrer l'application mobile en mode production 
```http
  pnpm --filter @geoalarme/geoalarme-mobile run start
```

### Commandes associées au back-office

#### Démarrer le back-office en mode développement 
```http
  pnpm --filter @geoalarme/geoalarme-back-office run dev
```

#### Construire le stack du back-office 
```http
  pnpm --filter @geoalarme/geoalarme-back-office run build
```

#### Lancer les tests sur le back-office 
```http
  pnpm --filter @geoalarme/geoalarme-back-office run test
```

#### Démarrer le back-office en mode production 
```http
  pnpm --filter @geoalarme/geoalarme-back-office run start
```
