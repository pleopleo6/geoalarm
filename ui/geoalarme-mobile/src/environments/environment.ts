import ngswConfig from '../../ngsw-config.json';
export const environment = {
  production: false,
  prod_config: 'localhost',
  location: window.location,
  version: ngswConfig.appData.version,
};
