import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { MatAccordion, MatExpansionPanel, MatExpansionPanelHeader, MatExpansionPanelTitle } from "@angular/material/expansion";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatListModule } from "@angular/material/list";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTabsModule } from "@angular/material/tabs";
import { MatRippleModule } from "@angular/material/core";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { ServiceWorkerModule } from '@angular/service-worker';

import { InfiniteScrollModule } from "ngx-infinite-scroll";

import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { AppDatasResolver } from "./app.datas.resolver";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SitesViewComponent } from './view/sites-view/sites-view.component';
import { SiteViewComponent } from './view/site-view/site-view.component';
import { StationsViewComponent } from "./view/stations-view/stations-view.component";
import { StationViewComponent } from './view/station-view/station-view.component';
import { SignalisationsViewComponent } from './view/signalisations-view/signalisations-view.component';
import { SignalisationViewComponent } from './view/signalisation-view/signalisation-view.component';
import { CamerasViewComponent } from './view/cameras-view/cameras-view.component';
import { CameraViewComponent } from "./view/camera-view/camera-view.component";
import { ContactViewComponent } from "./view/contact-view/contact-view.component";
import { SensorsViewComponent } from './view/sensors-view/sensors-view.component';
import { SensorViewComponent } from './view/sensor-view/sensor-view.component';
import { ProfileViewComponent } from './view/profile-view/profile-view.component';
import { LogsViewComponent } from './view/logs-view/logs-view.component';
import { LoginViewComponent } from './view/login-view/login-view.component';
import { WordingResolverService } from './resolver/WordingResolverService';
import { HeaderViewComponent } from "./view/header-view/header-view.component";
import { StatusPictoComponent } from './component/status-picto/status-picto.component';
import { PageHeaderComponent } from './component/page-header/page-header.component';
import { DevicesViewComponent } from "./view/devices-view/devices-view.component";
import { DeviceViewComponent } from "./view/device-view/device-view.component";
import { LoadingSpinnerComponent } from "./component/loading-spinner/loading-spinner.component";
import { FirstLetterUppercasePipe } from "./util/first-letter-uppercase.pipe";
import { AlertViewComponent } from './component/alert-view/alert-view.component';
import { ContactFormComponent } from './component/contact-form/contact-form.component';
import { UpdateDialogComponent } from './component/update-dialog/update-dialog.component';
// import { IosWsDialogComponent } from "./component/ios-ws-dialog/ios-ws-dialog.component";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/mobile/i18n/');
}

@NgModule({
  declarations: [
    AppComponent,
    CamerasViewComponent,
    CameraViewComponent,
    ContactViewComponent,
    SitesViewComponent,
    SiteViewComponent,
    DevicesViewComponent,
    DeviceViewComponent,
    LogsViewComponent,
    LoginViewComponent,
    HeaderViewComponent,
    LoadingSpinnerComponent,
    ProfileViewComponent,
    SensorsViewComponent,
    SensorViewComponent,
    SignalisationsViewComponent,
    SignalisationViewComponent,
    StationsViewComponent,
    StationViewComponent,
    StatusPictoComponent,
    PageHeaderComponent,
    FirstLetterUppercasePipe,
    AlertViewComponent,
    ContactFormComponent,
    UpdateDialogComponent,
    // IosWsDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatTabsModule,
    MatSidenavModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    MatDialogModule,
    MatAccordion,
    MatExpansionPanel,
    MatExpansionPanelHeader,
    MatExpansionPanelTitle,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'fr'
    }),
    ServiceWorkerModule.register('ngsw-mobile-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [
    AppDatasResolver,
    WordingResolverService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
