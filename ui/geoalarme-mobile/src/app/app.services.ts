import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { BehaviorSubject, Observable, Subject}  from 'rxjs';
import { io } from "socket.io-client";
import { Socket } from "socket.io-client/build/esm/socket";
import { API_BASE, DEFAULT_LANGUAGE } from "./app.config";
import { GenericLogResponse } from "./model/generic-log-response";
import { Wording } from "./model/wording";
import { LogTarget } from "./model/log-target";

@Injectable({
  providedIn: 'root'
})
export class HTTPService {

  constructor(private http: HttpClient) { }

  request(api_url: string, query_obj?:Object, header_obj?:Object):Observable<Record<string, any>> {
    return this.http.request( api_url + this.getQueryString(query_obj), (header_obj? this.getHeader(header_obj) : null) );
  }

  get(api_url: string, query_obj?:Object, header_obj?:Object):Observable<Record<string, any>> {
    return this.http.get( api_url + this.getQueryString(query_obj), header_obj? this.getHeader(header_obj) : null );
  }

  post(api_url: string, query_obj?:Object, header_obj?:Object, data?:Object):Observable<Record<string, any>> {
    return this.http.post( api_url + this.getQueryString(query_obj), data, (header_obj? this.getHeader(header_obj) : null) );
  }

  put(api_url: string, query_obj?:Object, header_obj?:Object, data?:Object):Observable<Record<string, any>> {
    return this.http.put( api_url + this.getQueryString(query_obj), data, (header_obj? this.getHeader(header_obj) : null) );
  }

  patch(api_url: string, query_obj?:Object, header_obj?:Object, data?:Object):Observable<Record<string, any>> {
    return this.http.patch( api_url + this.getQueryString(query_obj), data, (header_obj? this.getHeader(header_obj) : null) );
  }

  delete(api_url: string, query_obj?:Object, header_obj?:Object):Observable<Record<string, any>> {
    return this.http.delete( api_url + this.getQueryString(query_obj), (header_obj? this.getHeader(header_obj) : null) );
  }

  private getQueryString(query_obj:any):string{
    let k, p: string = '';
    for (k in query_obj){ p+= ('&' + k + '=' + query_obj[k]); };
    return ((p!=='')? ('?' + p) : '');
  }
  private getHeader(header_obj:any): any {
    return { headers: new HttpHeaders(header_obj) };
  }

}

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  loading$: BehaviorSubject<boolean>;
  private currentState: boolean;
  constructor(){
    this.currentState = false;
    this.loading$ = new BehaviorSubject<boolean>(this.currentState);
  }
  toggleLoading() {
    this.currentState = true;
    this.loading$.next(this.currentState);
  }
  toggleLoaded() {
    this.currentState = false;
    this.loading$.next(this.currentState);
  }
}

@Injectable({
  providedIn: 'root'
})
export class SharedService {
    private emitChangeSource = new Subject<any>();
    changeEmitted$ = this.emitChangeSource.asObservable();
    emitChange(event_name:string, data?:any) { this.emitChangeSource.next({ name:event_name, data:data }); }
}

@Injectable({
  providedIn: 'root'
})
export class WordingService {
  private wording: Record<string, string> = {};
  map$: BehaviorSubject<Record<string, string>>;
  constructor(private readonly http: HttpClient){
    this.map$ = new BehaviorSubject<Record<string, string>>({});
  }
  load(): Observable<Object> {
    const favoriteLanguage = localStorage.getItem('geoalarme-mobile-favorite-language');
    return this.http.get(`${API_BASE}/api/v1/wording/mobile/${favoriteLanguage?? DEFAULT_LANGUAGE}`)
  }
  register(wording: Wording[]) {
    wording.forEach((item: Wording) => this.wording[item.name] = item.value )
    this.map$.next(this.wording);
  }
  reset() {
    Object.keys(this.wording).forEach(key => delete this.wording[key]);
  }
  getWording(): Record<string, string>{
    return this.wording;
  }
}

@Injectable({
  providedIn: 'root'
})
export class PageHeaderService {
  label$: BehaviorSubject<string>;
  private currentLabel: string = '';
  constructor(){
    this.label$ = new BehaviorSubject<string>(this.currentLabel);
  }
  update(label: string) {
    this.currentLabel = label;
    this.label$.next(this.currentLabel);
  }
}

@Injectable({
  providedIn: 'root'
})
export class LogsService {
  logs$: BehaviorSubject<GenericLogResponse>;
  logsTarget$: BehaviorSubject<LogTarget>;
  constructor(private readonly http: HttpClient){
    this.logs$ = new BehaviorSubject<GenericLogResponse>({});
    this.logsTarget$ = new BehaviorSubject<LogTarget>(LogTarget.SITE);
  }
  load(url: string, pageIndex: number = 1, pageLimit = 10): Observable<Object> {
    const token = localStorage.getItem('geoalarme-mobile-token');
    const headers: any = { Authorization: 'Bearer ' + JSON.parse(token!).token };
    return this.http.post(url, { pageIndex, pageLimit },{ headers: new HttpHeaders(headers) });
  }
  tap(logs: GenericLogResponse) {
    this.logs$.next(logs);
  }
  reset() {
    this.logs$.next({ data:[] });
  }
  setLogTarget(target: LogTarget) {
    this.logsTarget$.next(target);
  }
}

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private socket: Socket;
  message$: BehaviorSubject<Record<string, Record<string, string>> | null>;
  constructor(private readonly  router: Router){
    const currentUserInfo = localStorage.getItem('geoalarme-mobile-user');
    let currentUser: any;
    if(currentUserInfo){
      currentUser = JSON.parse(currentUserInfo);
    }else{
      this.router.navigate( ['/login'] );
    }
    console.log('> init websocket service on ' + API_BASE);
    console.log('> websocket service listening ' + ['geoalarme-ws-connection','geoalarme-ws-global',`geoalarme-ws-user-${currentUser.id}`]);
    this.message$ = new BehaviorSubject<Record<string, Record<string, string>> | null>(null);
    this.socket = io(API_BASE, { query: { userId: currentUser.id } });
    this.socket.on('geoalarme-ws-connection', (data) => this.handleMessageReceived('geoalarme-ws-connection', data));
    this.socket.on('geoalarme-ws-global', (data) => this.handleMessageReceived('geoalarme-ws-global', data));
    this.socket.on(`geoalarme-ws-user-${currentUser.id}`, (data) => this.handleMessageReceived(`geoalarme-ws-user-${currentUser.id}`, data));
  }
  private handleMessageReceived(eventName:string, messageData:Record<string, Record<string, string>>): void {
    console.log(`... websocket event on ${eventName}, data:`, messageData);
    this.message$.next(messageData);
  }
}

@Injectable({
  providedIn: 'root'
})
export class AutoLogoutService {
  private maxTimeout: number = 20 * 60 * 1000;  // 20 minutes
  // private maxTimeout: number = 3000; // debug 3 secondes
  private timeout: number = 0;
  private timeoutItv: number = 1000;
  private timeoutCall: number | undefined;
  private sessionEnded: boolean = false;
  sessionTimeout$: BehaviorSubject<boolean>;
  constructor(){
    this.sessionTimeout$ = new BehaviorSubject<boolean>(this.sessionEnded);
  }
  start() {
    this.timeoutCall = setInterval(() => this.updateTimeout(), this.timeoutItv)
  }
  stop() {
    clearInterval(this.timeoutCall);
  }
  reset() {
    this.timeout = 0;
    this.sessionEnded = false;
    this.sessionTimeout$.next(this.sessionEnded);
  }
  private updateTimeout(): void {
    this.timeout+=this.timeoutItv;
    if(this.timeout >= this.maxTimeout)
      this.sessionEnded = true;
      this.sessionTimeout$.next(this.sessionEnded);
  }
}
