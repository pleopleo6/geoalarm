import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppDatasResolver } from "./app.datas.resolver";
import { AuthGuard } from "./auth/auth.guard";
import { CamerasViewComponent } from "./view/cameras-view/cameras-view.component";
import { CameraViewComponent } from "./view/camera-view/camera-view.component";
import { DevicesViewComponent } from "./view/devices-view/devices-view.component";
import { DeviceViewComponent } from "./view/device-view/device-view.component";
import { LoginViewComponent } from "./view/login-view/login-view.component";
import { SensorsViewComponent } from "./view/sensors-view/sensors-view.component";
import { SitesViewComponent } from "./view/sites-view/sites-view.component";
import { SiteViewComponent } from "./view/site-view/site-view.component";
import { StationsViewComponent } from "./view/stations-view/stations-view.component";
import { StationViewComponent } from "./view/station-view/station-view.component";
import { WordingResolverService } from "./resolver/WordingResolverService";

const routes: Routes = [

  {
    path: 'login',
    component: LoginViewComponent
  },

  {
    path: 'sites',
    data: { api_path: 'sites', api_path_replacements: [] },
    canActivate: [ AuthGuard ],
    resolve: { wording: WordingResolverService, data: AppDatasResolver },
    component: SitesViewComponent,
    children: [
      {
        path: ':siteId',
        data: { api_path: 'site/siteId', api_path_replacements: ['siteId'] },
        canActivate: [ AuthGuard ],
        resolve: { wording: WordingResolverService, data: AppDatasResolver },
        component: SiteViewComponent,
        children: [
          {
            path: 'stations',
            data: { api_path: 'site/siteId/stations', api_path_replacements: ['siteId'] },
            canActivate: [ AuthGuard ],
            resolve: { wording: WordingResolverService, data: AppDatasResolver },
            component: StationsViewComponent,
            children: [
              {
                path: ':stationId/alarm-devices',
                data: { api_path: 'site/siteId/devices/alarm', api_path_replacements: ['siteId'] },
                canActivate: [ AuthGuard ],
                resolve: { wording: WordingResolverService, data: AppDatasResolver },
                component: DevicesViewComponent,
                children: [
                  {
                    path: ':deviceId',
                    data: { api_path: 'site/siteId/device/deviceId', api_path_replacements: ['siteId', 'deviceId'] },
                    canActivate: [AuthGuard],
                    resolve: {wording: WordingResolverService, data: AppDatasResolver},
                    component: DeviceViewComponent
                  }
                ]
              },
              {
                path: ':stationId/signalisation-devices',
                data: { api_path: 'site/siteId/devices/signalisation', api_path_replacements: ['siteId'] },
                canActivate: [ AuthGuard ],
                resolve: { wording: WordingResolverService, data: AppDatasResolver },
                component: DevicesViewComponent,
                children: [
                  {
                    path: ':deviceId',
                    data: { api_path: 'site/siteId/device/deviceId', api_path_replacements: ['siteId', 'deviceId'] },
                    canActivate: [AuthGuard],
                    resolve: {wording: WordingResolverService, data: AppDatasResolver},
                    component: DeviceViewComponent
                  }
                ]
              },
              {
                path: ':stationId/devices-in-default',
                data: { api_path: 'site/siteId/devices/defaults', api_path_replacements: ['siteId'] },
                canActivate: [ AuthGuard ],
                resolve: { wording: WordingResolverService, data: AppDatasResolver },
                component: DevicesViewComponent,
                children: [
                  {
                    path: ':deviceId',
                    data: { api_path: 'site/siteId/device/deviceId', api_path_replacements: ['siteId', 'deviceId'] },
                    canActivate: [AuthGuard],
                    resolve: {wording: WordingResolverService, data: AppDatasResolver},
                    component: DeviceViewComponent
                  }
                ]
              },
              {
                path: ':stationId/cameras',
                data: { api_path: 'site/siteId/cameras', api_path_replacements: ['siteId'] },
                canActivate: [ AuthGuard ],
                resolve: { wording: WordingResolverService, data: AppDatasResolver },
                component: CamerasViewComponent
              },
              {
                path: ':stationId/sensors',
                canActivate: [ AuthGuard ],
                data: { api_path: 'site/siteId/sensors', api_path_replacements: ['siteId'] },
                resolve: { wording: WordingResolverService, data: AppDatasResolver },
                component: SensorsViewComponent,
                /*children: [
                  {
                    path: ':sensorsId',
                    canActivate: [ AuthGuard ],
                    data: { api_path: 'site/siteId/sensor/sensorsId', api_path_replacements: ['siteId','sensorsId'] },
                    resolve: { wording: WordingResolverService, data: AppDatasResolver },
                    component: SensorViewComponent
                  },
                ]*/
              },
              {
                path: ':stationId',
                data: { api_path: 'site/siteId/station/stationId', api_path_replacements: ['siteId','stationId'] },
                canActivate: [ AuthGuard ],
                resolve: { wording: WordingResolverService, data: AppDatasResolver },
                component: StationViewComponent,
                children: [
                  {
                    path: ':deviceId',
                    data: { api_path: 'site/siteId/device/deviceId', api_path_replacements: ['siteId', 'deviceId'] },
                    canActivate: [AuthGuard],
                    resolve: {wording: WordingResolverService, data: AppDatasResolver},
                    component: DeviceViewComponent
                  }
                ]
              },
            ]
          },
          {
            path: 'cameras',
            data: { api_path: 'site/siteId/cameras', api_path_replacements: ['siteId'] },
            canActivate: [ AuthGuard ],
            resolve: { wording: WordingResolverService, data: AppDatasResolver },
            component: CamerasViewComponent,
            children: [
              {
                path: ':cameraId',
                data: {api_path: 'site/siteId/camera/cameraId', api_path_replacements: ['siteId', 'cameraId']},
                canActivate: [AuthGuard],
                resolve: {wording: WordingResolverService, data: AppDatasResolver},
                component: CameraViewComponent,
              }
            ]
          },
        ]
      }
    ]
  },

  { path: '', redirectTo: '/sites', pathMatch: 'full' },
  { path: '**', redirectTo: '/sites', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
