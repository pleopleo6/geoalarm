import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { API_URL } from './app.config';
import { AutoLogoutService, LoadingService } from './app.services';

@Injectable()
export class AppDatasResolver implements Resolve<any> {

  constructor( private readonly router: Router,
               private readonly http: HttpClient,
               private readonly auto_logout_service: AutoLogoutService,
               private readonly loading_service: LoadingService) {}

  resolve(route: ActivatedRouteSnapshot):Observable<any> | Promise<any> | any {

    const token = localStorage.getItem('geoalarme-mobile-token');

    if(!token){
      this.router.navigate( ['/login'] );
    }

    const headers: any = { Authorization: 'Bearer ' + JSON.parse(token!).token };

    let request = API_URL + route.data['api_path'];
    // console.log(route.data['api_path_replacements'])
    if(route.data['api_path_replacements']){
      route.data['api_path_replacements'].forEach((id:string, index:number) => {
        const apiPathValue = this.findApiPathValue(id, route)
        // console.log('...', id, apiPathValue)
        if(apiPathValue){
          request = request.replace(id, apiPathValue);
        }
      });
    }

    // console.log('AppDatasResolver request:',request);

    if(request!==API_URL){
      this.loading_service.toggleLoading();
      this.auto_logout_service.reset();
    }

    return (request!==API_URL)? this.http.get(request, { headers: new HttpHeaders(headers) })
        .toPromise()
        .then((response:any|undefined) => {
          this.loading_service.toggleLoaded();
          if(response){ return response; }
          else{ this.router.navigate( ['/'] ); }
        } )
        .catch((err: HttpErrorResponse) => {
          // console.log(('failed to get '+request), err.statusText);
          this.loading_service.toggleLoaded();
          this.router.navigate( ['/login'] );
        })

      : null;

  }

  private findApiPathValue(id:string, route:ActivatedRouteSnapshot): string|null{
    let value = route.paramMap.get(id);
    if(!value && route.parent) value = this.findApiPathValue(id, route.parent);
    return value;
  }

}
