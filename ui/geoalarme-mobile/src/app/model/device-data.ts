import {DataType} from "./data-type";

export interface DeviceData {
  dataType:       DataType,
  data:           number,
  timestamp:      Date,
}
