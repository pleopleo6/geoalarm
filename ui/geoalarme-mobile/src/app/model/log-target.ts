export enum LogTarget {
  NONE = 0,
  SITE = 1,
  STATION = 2
}
