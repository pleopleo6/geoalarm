export enum PageContentType {
  sites,
  site,
  station,
  cameras,
  sensors,
  sensor,
}
