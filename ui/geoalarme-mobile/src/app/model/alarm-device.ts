import { Position } from "./position";
import { Alarm } from "./alarm";
import { AlarmStatus } from "./alarm-status";
import { DefaultStatus } from "./default-status";
import { DeviceType } from "./device-type";
import { Default } from "./default";
import { DeviceStatus } from "./device-status";

export interface AlarmDevice {
  id:             number,
  stationId:      number,
  type:           DeviceType,
  status:         DeviceStatus,
  data:           any,
  alarmStatus:    AlarmStatus,
  alarm:          Alarm,
  defaultStatus:  DefaultStatus,
  default:        Default,
  name:           string,
  device_id:      string
  latitude:       number,
  longitude:      number,
  altitude:       number,
  position:       Position,
  created_at:     Date,
  updated_at:     Date,
}
