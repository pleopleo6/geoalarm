export interface Default {
  id:           number,
  description:  string,
  level:        string,
}
