import { SensorType } from "./sensor-type";
import {SensorData} from "./sensor-data";

export interface Sensor {
  id:             number,
  type:           SensorType,
  sensor_id:      string,
  app_eui:        string,
  name:           string,
  latitude:       number,
  longitude:      number,
  altitude:       number,
  data?:          SensorData[],
  created_at:     Date,
  updated_at:     Date,
}
