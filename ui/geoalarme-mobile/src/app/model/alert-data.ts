export interface AlertData {
  type:               string;
  messageId:          string;
  timeout?:           number;
  auto_close?:        boolean;
  closable?:          boolean;
}
