export interface ActorType {
  id:             number,
  name:           string,
  description:    string,
}
