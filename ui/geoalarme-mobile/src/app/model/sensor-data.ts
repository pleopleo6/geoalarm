import {DataType} from "./data-type";

export interface SensorData {
  dataType:       DataType,
  data:           number,
  timestamp:      Date,
}
