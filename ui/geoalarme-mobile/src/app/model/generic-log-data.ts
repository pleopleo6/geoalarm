export interface GenericLogData {
  requestObject: string,
  requestStatus: string,
  requestParams?: any,
  requestResult?: string|boolean|any|null,
}
