export interface DataType {
  id:             number,
  name:           string,
  abbr?:          string,
  unity:          string,
}
