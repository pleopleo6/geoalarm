export enum Languages {
  fr = 'Français',
  en = 'English',
  de = 'Deutsch',
  it = 'Italiano'
}
