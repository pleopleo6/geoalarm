import { GenericLog } from "./generic-log";
import { GenericLogMeta } from "./generic-log-meta";

export interface GenericLogResponse {
  data?: GenericLog[],
  meta?: GenericLogMeta,
}
