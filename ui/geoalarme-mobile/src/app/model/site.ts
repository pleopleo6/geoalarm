import Polygon from 'polygon';
import { AlarmStatus } from './alarm-status';
import { DefaultStatus } from './default-status';
import { SiteStatus } from "./site-status";
import {SignalisationStatus} from "./signalisation-status";

export interface Site {
  id: number,
  status: SiteStatus,
  alarm: AlarmStatus,
  default: DefaultStatus,
  signalisation_status: SignalisationStatus,
  name: string,
  area?: Polygon,
  site_id?: string,
  latitude?: number,
  longitude?: number,
  altitude?: number,
  city: string,
  district: string,
  country: string,
  last_connexion: Date,
  created_at: Date,
  updated_at: Date,
}
