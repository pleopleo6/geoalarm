import { Position } from "./position";
import { Alarm } from "./alarm";
import { AlarmStatus } from "./alarm-status";
import { DefaultStatus } from "./default-status";
import { DeviceType } from "./device-type";
import { Default } from "./default";
import { DeviceData } from "./device-data";
import { DeviceStatus } from "./device-status";
import { SignalisationState } from "./signalisation-state";

export interface Device {
  id:             number,
  stationId:      number,
  type_id?:       number,
  type:           DeviceType,
  status:         DeviceStatus,
  data?:          DeviceData[],
  alarmStatus:    AlarmStatus,
  alarm:          Alarm,
  defaultStatus:  DefaultStatus,
  default:        Default,
  sign_state:     SignalisationState,
  name:           string,
  device_id:      string
  latitude:       number,
  longitude:      number,
  altitude:       number,
  position:       Position,
  created_at:     Date,
  updated_at:     Date,
}
