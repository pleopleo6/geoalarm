export interface Wording {
  name:   string;
  value:  string;
}
