import { AlarmDevice } from "./alarm-device";
import { OtherDevice } from "./other-device";
import { Device } from "./device";
import { SignalisationDevice } from "./signalisation-device";
import { Position } from "./position";
import { StationStatus } from "./station-status";

export interface Station {
  id: number,
  station_id: string,
  status: StationStatus,
  name: string,
  location: Position,
  latitude: number,
  longitude: number,
  altitude: number,
  devices?: Device[],
  alarm_devices: AlarmDevice[],
  sign_devices: SignalisationDevice[],
  other_devices: OtherDevice[]
  last_log_date?: Date;
  created_at: Date,
  updated_at: Date,
}
