export interface StationStatus {
  id:             number,
  name:           string,
  description?:   string,
}
