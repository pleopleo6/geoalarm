export interface CommandPool {
  id:                 number;
  uid:                string;
  command_id:         number;
  site_id:            number;
  user_id:            number;
  state_id:           number;
  params?:            Record<string, number|string|boolean|null>;
  timestamp:          Date;
}
