import { GenericLogData } from "./generic-log-data";
import { ActorType } from "./log-actor-type";
import { LogType } from "./log-type";

export interface GenericLog {
  id: number,
  actor_id: number,
  actor?: any,
  actor_type: ActorType,
  log_type: LogType,
  log_data: GenericLogData,
  log_timestamp: Date,
}
