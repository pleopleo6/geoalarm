import {CommandValidationLevel} from "./command-validation-level";

export interface Command {
  id:                 number;
  uid?:               string;
  inner_id:           string;
  name:               string;
  label_id:           string;
  description_id:     string;
  command:            string;
  params?:            Record<string, number|string|boolean|null>;
  priority:           number;
  validationLevelId:  CommandValidationLevel;
}
