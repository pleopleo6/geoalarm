import { Position } from "./position";
import {Alarm} from "./alarm";
import {Default} from "./default";
import {SignalisationState} from "./signalisation-state";

export interface SignalisationDevice {
  id:             number,
  stationId:      number,
  name:           string,
  device_id:      string,
  alarm:          Alarm,
  default:        Default,
  sign_state:     SignalisationState,
  latitude:       number,
  longitude:      number,
  altitude:       number,
  position:       Position,
  type_id?:       number,
  created_at:     Date,
  updated_at:     Date,
}
