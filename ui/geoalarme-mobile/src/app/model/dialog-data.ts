export interface DialogData {
  title: 		string;
  icon: 		string;
  message: 	string;
  confirm: 	any;
  cancel: 	any;
}
