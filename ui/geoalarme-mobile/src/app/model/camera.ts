import { Position } from "./position";
import { Alarm } from "./alarm";
import { AlarmStatus } from "./alarm-status";
import { DefaultStatus } from "./default-status";
import { DeviceType } from "./device-type";
import { Default } from "./default";
import { DeviceStatus } from "./device-status";
import {CameraType} from "./camera-type";

export interface Camera {
  id:             number,
  type?:           CameraType,
  url?:            string,
  url_ftp?:        string
  name:            string
  latitude?:       number,
  longitude?:      number,
  altitude?:       number,
  position?:       Position,
  created_at?:     Date,
  updated_at?:     Date,
}
