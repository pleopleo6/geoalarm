import {Device} from "./device";
import {DataType} from "./data-type";

export interface DeviceDataAssociation {
  id:             number,
  device:         Device,
  dataType:       DataType,
}
