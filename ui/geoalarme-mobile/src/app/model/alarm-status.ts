export interface AlarmStatus {
  id: number,
  description: string,
  level: string
}
