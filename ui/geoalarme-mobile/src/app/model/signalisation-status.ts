export interface SignalisationStatus {
  id: number,
  description: string,
  level: string
  level_num?: number
}
