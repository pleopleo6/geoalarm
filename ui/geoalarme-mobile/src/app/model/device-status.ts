export interface DeviceStatus {
  id:             number,
  name:           string,
  description?:   string,
}
