import { Injectable } from '@angular/core';

import { API_URL } from '../app.config';
import { HTTPService } from "../app.services";
import { UserCredentials } from "../model/user-credentials";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http_service:HTTPService) { }

  login(credentials:UserCredentials) {
    return this.http_service.post(`${API_URL}login`,{},{'Content-Type':'application/json'}, credentials);
  }

  logout() {
    return this.http_service.post(`${API_URL}logout`,{},{'Content-Type':'application/json'});
  }

  isLogged() {
    const mobileToken = localStorage.getItem('geoalarme-mobile-token');
    const storedData:any = !!mobileToken? JSON.parse(mobileToken) : null;
    return !!(storedData && storedData.token);
  }

}
