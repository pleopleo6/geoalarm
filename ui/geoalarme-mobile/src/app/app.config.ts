import { environment } from '../environments/environment';

export const DEFAULT_LANGUAGE = 'fr';
export const API_BASE: string = (environment.location.hostname === 'localhost') ? 'http://localhost:3333' : window.location.origin;
export const API_URL: string = API_BASE + '/api/v1/mobile/';
export const PUBLIC_VAPID_KEY: string = 'BIRq0lO-VJ4hay4t87gi4BmzQpk47LjUeJz698x6v_5NcUkEB1tmmKdrVqEK2pWN6qFy-Yp1yxHn-rXcpsNf7Hg';
