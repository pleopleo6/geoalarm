import { Component, Input, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { PageHeaderService, SharedService } from "../../app.services";

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrl: './page-header.component.scss'
})
export class PageHeaderComponent implements OnDestroy{

  @Input() label = '';
  @Input() level = 0;
  @Input() singleSite = false;

  siteId: string | undefined;
  isAdmin = false;
  commandInProgress = false;

  parentUrl: string | undefined;

  sharedServiceSbr$: Subscription | undefined;
  routerSbr$: Subscription | undefined;

  constructor(private readonly router: Router,
              private readonly page_header_service: PageHeaderService,
              private readonly shared_service: SharedService) {

    this.page_header_service.label$.subscribe((label: string) => this.label = label);

    this.sharedServiceSbr$ = this.shared_service.changeEmitted$.subscribe(data=>{ this.handleSharedAction(data); });

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const userRolesBySite: Record<string, string> = JSON.parse(localStorage.getItem('geoalarme-mobile-roles')!);
        const url = this.router.url;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.isAdmin = userRolesBySite[this.siteId]==='administrator' || localStorage.getItem('geoalarme-mobile-role')==='super-administrator';
        this.parentUrl = this.fixParentUrl(url);
        // console.log(this.parentUrl, '>', url)

      }
    });
  }

  ngOnDestroy(){
    this.sharedServiceSbr$?.unsubscribe();
    this.routerSbr$?.unsubscribe();
  }

  handleAction(id:string): void{
    switch(id){
      case 'back':
        // this.location.back();
        if(this.parentUrl){
          this.router.navigate( [this.parentUrl] );
        }
        break;
      case 'refresh-site':
      case 'open-site-settings':
        this.shared_service.emitChange(id);
        break;

    }
  }
  handleSharedAction(data:any){
    switch(data.name) {
      case 'refresh-site-requested':
        this.commandInProgress = true;
        break;
      case 'refresh-site-completed':
        this.commandInProgress = false;
        break;
      case 'refresh-site-rejected':
        this.commandInProgress = false;
        break;
    }
  }

  private fixParentUrl(url: string): string {
    const urlParams = url.split('/').filter((item: string) => item !== '');
    if(url.includes('stations/all/alarm-devices') || url.includes('stations/all/signalisation-devices') || url.includes('stations/all/devices-in-default')){
      urlParams.splice(urlParams.indexOf('stations'))
    } else if(url.includes('alarm-devices/') || url.includes('signalisation-devices/') || url.includes('devices-in-default/')){
      urlParams.pop();
      urlParams[3] = 'all';
    } else if(url.includes('stations/all/sensors/')) {
      urlParams.pop();
    } else if(url.includes('stations/all/sensors')) {
      urlParams.splice(urlParams.indexOf('stations'))
    } else if(url.includes('stations/all/devices-in-default')) {
      urlParams.splice(urlParams.indexOf('devices-in-default'))
    } else {
      urlParams.pop();
    }
    return urlParams.join('/');
  }
}
