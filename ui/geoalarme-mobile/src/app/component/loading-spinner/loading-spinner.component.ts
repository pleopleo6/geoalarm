import { Component } from '@angular/core';
import { Subject, takeUntil } from "rxjs";
import { LoadingService } from "../../app.services";

@Component({
  selector: 'loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrl: './loading-spinner.component.scss'
})
export class LoadingSpinnerComponent {

  loading= true;
  destroy$ = new Subject<boolean>();

  constructor(protected loadingService: LoadingService) {
    this.loadingService.loading$
      .pipe(takeUntil(this.destroy$))
      .subscribe((loading) => {
        this.loading = loading;
      });
  }

}
