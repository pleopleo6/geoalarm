import { Component, Input } from '@angular/core';
import { StatuePictoType } from "../../model/status-picto-type";

@Component({
  selector: 'status-picto',
  templateUrl: './status-picto.component.html',
  styleUrl: './status-picto.component.scss'
})
export class StatusPictoComponent {
  @Input() type: StatuePictoType = StatuePictoType.alarm;
  @Input() label: string | undefined;
  @Input() level: string | undefined;
  @Input() forced: boolean | undefined;
  @Input() unactive: boolean | undefined;
  protected readonly StatuePictoType = StatuePictoType;
}
