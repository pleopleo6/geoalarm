import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusPictoComponent } from './status-picto.component';

describe('StatusPictoComponent', () => {
  let component: StatusPictoComponent;
  let fixture: ComponentFixture<StatusPictoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatusPictoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StatusPictoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
