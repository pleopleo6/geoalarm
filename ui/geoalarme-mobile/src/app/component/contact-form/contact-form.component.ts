import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { WordingService } from "../../app.services";

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrl: './contact-form.component.scss'
})
export class ContactFormComponent {

  contactForm:         FormGroup;
  wording: Record<string, string> = {};

  constructor(private readonly wording_service: WordingService) {

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
      }
    });

    this.contactForm = new FormGroup({
      object: new FormControl('',[Validators.required]),
      message: new FormControl('', [Validators.required]),
    });

  }


}
