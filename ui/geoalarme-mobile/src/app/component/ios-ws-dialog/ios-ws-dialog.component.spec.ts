import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IosWsDialogComponent } from './ios-ws-dialog.component';

describe('IosWsDialogComponent', () => {
  let component: IosWsDialogComponent;
  let fixture: ComponentFixture<IosWsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IosWsDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IosWsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
