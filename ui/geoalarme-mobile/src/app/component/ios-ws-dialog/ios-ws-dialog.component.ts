import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { DialogData } from "../../model/dialog-data";

@Component({
  selector: 'ios-ws-dialog',
  templateUrl: './ios-ws-dialog.component.html',
  styleUrl: './ios-ws-dialog.component.scss'
})
export class IosWsDialogComponent {
  constructor( public dialogRef: MatDialogRef<IosWsDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData ) { }
}
