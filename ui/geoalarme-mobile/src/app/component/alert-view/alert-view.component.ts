import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { AlertData } from "../../model/alert-data";
import { WordingService}  from "../../app.services";

@Component({
  selector: 'alert-view',
  templateUrl: './alert-view.component.html',
  styleUrl: './alert-view.component.scss'
})
export class AlertViewComponent implements  OnChanges, OnInit{

  @Input() data: AlertData | undefined | null;
  @ViewChild('alertContainer', {static: true}) alertContainer: ElementRef | undefined;
  @ViewChild('progressBar', {static: false}) progressBar: ElementRef | undefined;

  wording: Record<string, string> = {};

  alertType: string | undefined;
  alertClosable: boolean | undefined;
  alertTimeout: number | undefined;

  autoCloseTm: number | undefined | null;

  constructor(private readonly wording_service: WordingService) {
    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if (Object.keys(wording).length) this.wording = wording;
    });
  }

  ngOnInit(){
    this.alertType = this.data?.type?? 'success';
    this.alertClosable = this.data?.closable?? true;
    this.alertTimeout = this.data?.timeout?? 3000;
  }

  ngOnChanges(){
    if(this.data){

      this.alertType = this.data?.type?? 'success';
      this.alertTimeout = this.data?.timeout?? 3000;

      this.alertContainer?.nativeElement.classList.add('opened');

      setTimeout(() => {
        this.progressBar?.nativeElement.classList.add('animated');
      }, 350);

      setTimeout(() => {
        this.progressBar?.nativeElement.classList.add('progress');
      }, 350);

      this.autoCloseTm = setTimeout(() => {
        this.close();
      }, this.alertTimeout! + 350);

    }
  }

  private close(): void {
    if(this.autoCloseTm){
      clearTimeout(this.autoCloseTm);
      this.autoCloseTm = null;
    }
    this.alertContainer?.nativeElement.classList.remove('opened');
    this.progressBar?.nativeElement.classList.remove('animated');
    this.progressBar?.nativeElement.classList.remove('progress');
    setTimeout(() => this.data = null, 350);
  }
  handleAction(e: any): void{
    switch(e.name) {
      case 'close':
        this.close();
        break;
    }
  }

}
