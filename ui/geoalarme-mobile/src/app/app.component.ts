import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {

  constructor() {
    window.addEventListener("visibilitychange",  ()=> {
      // reload the current page on app resumed (refetch datas)
      if (document.visibilityState === "visible") window.location.reload();
    });
  }
}
