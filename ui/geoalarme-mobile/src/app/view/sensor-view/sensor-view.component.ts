import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from "rxjs";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { HTTPService, PageHeaderService, WebsocketService, WordingService } from "../../app.services";
import { Sensor } from "../../model/sensor";
import { API_URL } from "../../app.config";

@Component({
  selector: 'sensor-view',
  templateUrl: './sensor-view.component.html',
  styleUrl: './sensor-view.component.scss'
})
export class SensorViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;

  sensor: Sensor | undefined;
  wording: Record<string, string> = {};

  siteId: string;

  contentDeep: number = 4;
  contentOffset: number = 0;

  routerSbr$: Subscription | undefined;
  wsSbr$: Subscription | undefined;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly http_service: HTTPService,
              private readonly wording_service: WordingService,
              private readonly ws_service: WebsocketService,
              private readonly page_header_service: PageHeaderService) {

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        this.page_header_service.update(this.wording['sensor'].charAt(0).toUpperCase() + this.wording['sensor'].slice(1));
      }
    });

    this.route.data.subscribe( (response: any) => {
      this.initData(response);
      this.page_header_service.update(this.wording['sensor'].charAt(0).toUpperCase() + this.wording['sensor'].slice(1));
    });

    this.wsSbr$ = this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

    const url = this.router.url;
    const extralevel = (url.includes('alarm-devices')
      || url.includes('signalisation-devices')
      || url.includes('cameras')
      || url.includes('sensors')
    )? 1:0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    this.siteId = urlParams[0];
    this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const url = this.router.url;
        const extralevel = (url.includes('alarm-devices')
          || url.includes('signalisation-devices')
          || url.includes('cameras')
          || url.includes('sensors')
        )? 1:0;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-'+this.contentOffset), 350);

        this.page_header_service.update(this.wording['sensor'].charAt(0).toUpperCase() + this.wording['sensor'].slice(1));

      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
    this.wsSbr$?.unsubscribe();
  }

  private fetchData(): void {
    const token = localStorage.getItem('geoalarme-mobile-token');
    if(!token)this.router.navigate( ['/login'] );
    this.http_service.get(`${API_URL}site/${this.siteId}/sensor/${this.sensor?.id}`, {}, { Authorization: 'Bearer ' + JSON.parse(token!).token })
      .subscribe((response:any) => {
        this.initData(response)
      }, (error) => {
        // console.log(error)
      })
  }
  private initData(response:any): void {
    this.sensor = (response.data??response).sensor;
  }
  private handleWsMessage(data:any|null): void {
    if(data && data.change && data.change.object==='sensor_change'){
      if(this.sensor?.id===data.change.target_id){
        this.fetchData()
      }
    }
  }

}
