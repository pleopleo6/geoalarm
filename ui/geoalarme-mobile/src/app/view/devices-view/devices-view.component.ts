import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { HTTPService, PageHeaderService, WebsocketService, WordingService } from "../../app.services";
import { Device } from "../../model/device";
import { Station } from "../../model/station";
import { StatuePictoType } from "../../model/status-picto-type";
import { API_URL } from "../../app.config";

@Component({
  selector: 'devices-view',
  templateUrl: './devices-view.component.html',
  styleUrl: './devices-view.component.scss'
})
export class DevicesViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;

  station: Station | undefined;
  devices: Device[] = [];
  wording: Record<string, string> = {};

  siteId: string;

  contentDeep: number = 3;
  contentOffset: number = 0;

  isAlarmView: boolean;
  isSignalisationView: boolean;
  isDeviceInDefaultView: boolean;

  routerSbr$: Subscription | undefined;
  wsSbr$: Subscription | undefined;

  protected readonly StatuePictoType = StatuePictoType;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly http_service: HTTPService,
              private readonly wording_service: WordingService,
              private readonly ws_service: WebsocketService,
              private readonly page_header_service: PageHeaderService) {

    const url = this.router.url;
    const extralevel = (url.includes('alarm-devices')
                           || url.includes('signalisation-devices')
                           || url.includes('devices-in-default')
                           || url.includes('cameras')
                           || url.includes('sensors'))? 1:0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    this.siteId = urlParams[0];
    this.contentOffset = urlParams.length - (extralevel + this.contentDeep);
    this.isAlarmView = url.includes('alarm-devices');
    this.isSignalisationView = url.includes('signalisation-devices');
    this.isDeviceInDefaultView = url.includes('devices-in-default');

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        if(this.station){
          this.initHeader();
        }
      }
    });

    this.route.data.subscribe( (response: any) => {
      this.initData(response);
      this.initHeader();
    });

    this.wsSbr$ = this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const url = this.router.url;
        const extralevel = (url.includes('alarm-devices')
                               || url.includes('signalisation-devices')
                               || url.includes('devices-in-default')
                               || url.includes('cameras')
                               || url.includes('sensors'))? 1:0;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.contentOffset = urlParams.length - (extralevel + this.contentDeep);
        this.isAlarmView = url.includes('alarm-devices');
        this.isSignalisationView = url.includes('signalisation-devices');
        this.isDeviceInDefaultView = url.includes('devices-in-default');

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-'+this.contentOffset), 350);

        if(urlParams.length===4){
          this.initHeader();
        }

      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
    this.wsSbr$?.unsubscribe();
  }

  fixDeviceUrl(stationId: number): string {
    return this.router.url.replace('all', stationId.toString());
  }

  isSignalisationDevice(device:Device): boolean {
    return device.type_id===3
        || device.type_id===4;
  }

  isDefaultDevice(device:Device): boolean {
    return device.type_id===5
        || device.type_id===6
        || device.type_id===7
        || device.type_id===8;
  }

  getPictoLevel(device: Device): string {
    switch (device.type_id) {
      // alarme device
      case 1:
      case 2:
        return device.alarm?.level?? '1';
      // signalisation device
      case 3:
      case 4:
        return device.alarm?.level?? '1';
      // default device
      case 5:
      case 6:
      case 7:
      case 8:
        return device.default?.level?? '1';
      default:
        return '1';
    }
  }

  private fetchData(): void {
    const token = localStorage.getItem('geoalarme-mobile-token');
    if(!token)this.router.navigate( ['/login'] );
    this.http_service.get(`${API_URL}site/${this.siteId}/devices/${this.isAlarmView?'alarm':'signalisation'}`, {}, { Authorization: 'Bearer ' + JSON.parse(token!).token })
      .subscribe((response:any) => {
        this.initData(response)
      }, (error) => {
        // console.log(error)
      })
  }

  private initData(response:any): void {
    this.station = (response.data??response).station;
    this.devices = (response.data??response).devices;
  }

  private handleWsMessage(data:any|null): void {
    if(data && data.change && data.change.object==='device_change'){
      if(this.devices?.find((item:Device) => item.id===data.change.target_id)){
        this.fetchData()
      }
    }
  }

  private initHeader(): void {
    const titleId: string = this.isAlarmView? 'alarm_devices' : (this.isSignalisationView? 'sign_devices' : (this.isDeviceInDefaultView? 'devices_in_default' : 'station') );
    this.page_header_service.update((this.wording[titleId].charAt(0).toUpperCase() + this.wording[titleId].slice(1)));
  }

}
