import { Component, Input } from '@angular/core';
import { Router } from "@angular/router";
import { LogsService, WebsocketService, WordingService } from "../../app.services";
import { LogTarget } from "../../model/log-target";
import { GenericLog } from "../../model/generic-log";
import { GenericLogResponse } from "../../model/generic-log-response";
import { API_URL } from "../../app.config";

@Component({
  selector: 'logs-view',
  templateUrl: './logs-view.component.html',
  styleUrl: './logs-view.component.scss'
})
export class LogsViewComponent {

  @Input() lastInput: Date | undefined;

  protected readonly LogTarget = LogTarget;

  private logConfig : Record<string, { url: string, mode: LogTarget }> = {
    site: {
      url: 'site/:id/logs',
      mode: LogTarget.SITE
    },
    stations: {
      url:  'site/:id/logs',
      mode: LogTarget.SITE
    },
    station:{
      // url:  'site/:id/station/:stationId/logs',
      url:  'site/:id/logs',
      mode: LogTarget.STATION
    },
    devices:{
      url:  'site/:id/logs',
      mode: LogTarget.SITE
    },
    device:{
      url:  'site/:id/logs',
      mode: LogTarget.SITE
    },
  };

  siteId: string | undefined;
  stationId: string | undefined | null;

  data: GenericLog[] = [];
  logTarget: LogTarget | undefined;
  wording: Record<string, string> = {};

  hidden = true;
  opened = false;
  loading = false;

  pageIndex = 1;
  pageMaxIndex: number|undefined;
  pageLimit = 50;

  infiniteScrollThrottle = 300;
  infiniteScrollDistance = 1;

  constructor(private readonly router: Router,
              private readonly wording_service: WordingService,
              private readonly logs_service: LogsService,
              private readonly ws_service: WebsocketService) {

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if (Object.keys(wording).length) this.wording = wording;
    });

    this.logs_service.logsTarget$.subscribe((logTarget: LogTarget) => {
      if(this.logTarget!==logTarget) {

        this.pageIndex = 1;
        this.opened = false;
        this.loading = false;
        this.data = [];
        this.logTarget = logTarget;

        this.hidden = this.logTarget===LogTarget.NONE;

        if(this.logTarget!==LogTarget.NONE) {
          this.fetchLogsPage();
        }

      }
    });

    this.logs_service.logs$.subscribe((result: GenericLogResponse) => {
      if(result.meta) {
        // logs updated
        if(!this.pageMaxIndex || this.pageMaxIndex!==result.meta.total) {
          this.pageMaxIndex = result.meta.total;
        }
        this.data = [...this.data, ...result.data!];
      } else if(result.data) {
        // logs reseted
        this.pageIndex = 1;
        this.data = result.data;
      }
      // debug
      // ---
      // setTimeout(() => { this.loading = false }, 1000)
      // ---
      this.loading = false;
    }, (error) => {
      console.log('!!! failed to fetch logs ',error);
    });

    this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

  }

  getDefaultLevel(log: GenericLog): number {
    let levels: number[] = [];
    let level = 0;
    if(log.actor_type.name==='device') {
      levels = log.log_data.requestParams.stations
        .map((station: any) => station.devices.filter((device: any)=> device.idDvc==='P1' || device.idDvc==='P2' || device.idDvc==='P3' || device.idDvc==='P4')).flat()
        .map((device: any) => device.dftDvc);
      level = Math.max(...levels);
    } else if (log.actor_type.name==='station') {
      levels = log.log_data.requestParams.stations.map((station: any)=> station.dftSt);
      level = Math.max(...levels);
    } else if (log.actor_type.name==='site') {
      level = log.log_data.requestParams.dftSite;
    }
    return level;
  }
  getAlarmLevel(log: GenericLog): number {
    const almDvcs = log.log_data.requestParams.stations
      .map((station: any) => station.devices).flat()
      .map((device: any) => device.almDvc);
    return Math.max(...almDvcs);
  }

  handleLoadNext(): void {
    this.loadNextLogs();
    // test datas
    // this.generateFakeDatas(10);
  }

  handleToggleOpened(): void {
    this.opened = !this.opened;
  }

  private loadNextLogs(): void{
    if(this.logTarget!==LogTarget.NONE && !this.loading && (!this.pageMaxIndex || this.pageIndex < this.pageMaxIndex-1)){
      this.pageIndex++;
      this.fetchLogsPage();
    }
  }
  private fetchLogsPage(): void {
    const logUrl = this.findLogUrl();
    if(logUrl) {
      this.loading = true;
      // console.log('> logs url:', logUrl);
      this.logs_service.load(API_URL + logUrl, this.pageIndex, this.pageLimit).subscribe(
        (response: any) => {
          this.logs_service.tap(response);
        },
        ()=> {
          this.loading = false;
        }
      );
    }
  }

  private handleWsMessage(data:any|null): void {
    // on récupère les logs (site_log ou station_log)
    if(data && data['change'] && data['change']['object']==='new_log'
      && (this.logTarget===LogTarget.SITE && this.siteId===data.change.target_id.toString() ||
          this.logTarget===LogTarget.STATION && this.stationId===data.change.target_id.toString())){
      this.data = [...[data['change']['data']], ...this.data];
    }
  }

  private findLogUrl(): string|null{

    const url = this.router.routerState.snapshot.url;
    const urlParams = url.split('/');
    urlParams.shift();

    this.siteId = urlParams[1];
    this.stationId = url.includes('alarm-devices/') || url.includes('signalisation-devices/') || url.includes('stations/')? urlParams[3] : null;

    if(url.includes('alarm-devices/') || url.includes('signalisation-devices/')){

      this.logTarget = this.logConfig['device'].mode;
      return this.logConfig['device'].url.replace(':id', this.siteId).replace(':stationId', this.stationId!);

    } else if(url.includes('alarm-devices')){

      this.logTarget = this.logConfig['devices'].mode;
      return this.logConfig['devices'].url.replace(':id', this.siteId);

    } else if(url.includes('signalisation-devices')){

      this.logTarget = this.logConfig['devices'].mode;
      return this.logConfig['devices'].url.replace(':id', this.siteId);

    } else if(url.includes('stations/')){

      this.logTarget = this.logConfig['station'].mode;
      // return this.logConfig['station'].url.replace(':id', this.siteId).replace(':stationId', this.stationId!);
      return this.logConfig['stations'].url.replace(':id', this.siteId);

    } else if(url.includes('stations')){

      this.logTarget = this.logConfig['stations'].mode;
      return this.logConfig['stations'].url.replace(':id', this.siteId);

    } else if(url.includes('sites/')){

      this.logTarget = this.logConfig['site'].mode;
      return this.logConfig['site'].url.replace(':id', this.siteId);

    } else{
      return null
    }
  }

}
