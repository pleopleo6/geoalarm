import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalisationsViewComponent } from './signalisations-view.component';

describe('SignalisationsViewComponent', () => {
  let component: SignalisationsViewComponent;
  let fixture: ComponentFixture<SignalisationsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignalisationsViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignalisationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
