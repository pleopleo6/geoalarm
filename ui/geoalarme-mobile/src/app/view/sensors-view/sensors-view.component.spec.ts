import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorsViewComponent } from './sensors-view.component';

describe('SensorsViewComponent', () => {
  let component: SensorsViewComponent;
  let fixture: ComponentFixture<SensorsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SensorsViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SensorsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
