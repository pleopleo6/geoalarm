import { Component, EventEmitter, Input, Output } from '@angular/core';
import { WordingService } from "../../app.services";

@Component({
  selector: 'header-view',
  templateUrl: './header-view.component.html',
  styleUrl: './header-view.component.scss'
})
export class HeaderViewComponent {

  @Input() singleSite = false;

  @Output() action: EventEmitter<any> = new EventEmitter();
  wording: Record<string, string> = {};

  constructor(private readonly wordingService: WordingService) {
    this.wordingService.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) this.wording = wording;
    });
  }

  handleAction(e: any): void{
    this.action.emit(e);
  }

}
