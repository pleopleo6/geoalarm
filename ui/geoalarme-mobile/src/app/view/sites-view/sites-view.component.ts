import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SwPush, SwUpdate, VersionReadyEvent } from "@angular/service-worker";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { MatSidenav } from "@angular/material/sidenav";
import { TranslateService } from "@ngx-translate/core";
import { filter, Subscription } from "rxjs";
import { AutoLogoutService, HTTPService, LogsService, PageHeaderService, SharedService, WebsocketService, WordingService } from "../../app.services";
import { AlertData } from "../../model/alert-data";
import { PageContentType } from "../../model/page-content-type";
import { Site } from "../../model/site";
import { StatuePictoType } from "../../model/status-picto-type";
import { MatDialog , MatDialogConfig, MatDialogRef} from "@angular/material/dialog";
import { UpdateDialogComponent } from "../../component/update-dialog/update-dialog.component";
import { LogTarget } from "../../model/log-target";
import { API_BASE, API_URL, DEFAULT_LANGUAGE, PUBLIC_VAPID_KEY } from "../../app.config";
import { environment } from "../../../environments/environment.prod";
import {AlarmStatus} from "../../model/alarm-status";
import {Alarm} from "../../model/alarm";
import {SignalisationStatus} from "../../model/signalisation-status";

@Component({
  selector: 'sites-view',
  templateUrl: './sites-view.component.html',
  styleUrl: './sites-view.component.scss'
})
export class SitesViewComponent implements OnDestroy, OnInit {

  @ViewChild('menuSidenav', {static: true}) menuSidenav: MatSidenav | undefined;
  @ViewChild('chatSidenav', {static: true}) chatSidenav: MatSidenav | undefined;
  @ViewChild('updateSidenav', {static: true}) updateSidenav: MatSidenav | undefined;
  @ViewChild('contactSidenav', {static: true}) contactSidenav: MatSidenav | undefined;
  @ViewChild('contentContainer', {static: true}) contentContainer: ElementRef | undefined;

  data: Site[] | undefined;
  alertData: AlertData | undefined | null;
  wording: Record<string, string> = {};
  favoriteLanguage: string | null;

  containerScale: number = 1;
  contentOffset: number = 0;
  contentType: PageContentType = PageContentType.sites;

  singleSite = false;

  logHidden = true;

  appVersion = environment.version;
  versionChanged = false;

  updateDialogRef: MatDialogRef<UpdateDialogComponent> | undefined;

  swUpdateEnabled: boolean | undefined;

  routerSbr$: Subscription | undefined;
  updateServiceSbr$: Subscription | undefined;
  autoLogoutServiceSbr$: Subscription | undefined;
  sharedServiceSbr$: Subscription | undefined;
  wsSbr$: Subscription | undefined;

  private userPreferenfesUrl= `${API_URL}preferences`;
  private notifySubscriptionUrl= `${API_URL}notify-subscription`;
  private notificationsSubscription: any;

  protected readonly StatuePictoType = StatuePictoType;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly dialog: MatDialog,
              private readonly sw_update: SwUpdate,
              private readonly sw_push: SwPush,
              private readonly http_service: HTTPService,
              private readonly translate: TranslateService,
              private readonly wording_service: WordingService,
              private readonly auto_logout_service: AutoLogoutService,
              private readonly page_header_service: PageHeaderService,
              private readonly ws_service: WebsocketService,
              private readonly logs_service: LogsService,
              private readonly shared_service: SharedService){

    const currentUserInfo = JSON.parse(localStorage.getItem('geoalarme-mobile-user')!);
    this.favoriteLanguage = localStorage.getItem('geoalarme-mobile-favorite-language');
    this.favoriteLanguage = this.favoriteLanguage?? DEFAULT_LANGUAGE;
    this.translate.setDefaultLang(this.favoriteLanguage);
    this.translate.use(this.favoriteLanguage);

    if(currentUserInfo.language.code.toLowerCase()!==this.favoriteLanguage){
      // update the user preferred language
      this.registerPreferredLanguage();
    }

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.favoriteLanguage = localStorage.getItem('geoalarme-mobile-favorite-language');
        this.wording = wording;
        this.initHeader();
      }
    });

    const url = this.router.url;
    const extralevel = (url.includes('alarm-devices')
                           || url.includes('signalisation-devices')
                           || url.includes('devices-in-default')
                           // || url.includes('cameras')
                           || url.includes('sensors'))? 1:0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    this.contentType = this.setContentType(url);
    this.containerScale = urlParams.length - extralevel  + 1;
    this.contentOffset = urlParams.length - extralevel;
    this.swUpdateEnabled = this.sw_update.isEnabled;

    this.route.data.subscribe( (response: any) => this.initData(response));

    this.wsSbr$ = this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

    this.auto_logout_service.reset();
    this.autoLogoutServiceSbr$ = this.auto_logout_service.sessionTimeout$.subscribe( (sessionEnded: boolean) => {
      if(sessionEnded){
        this.auto_logout_service.stop();
        this.router.navigate( ['/login'] );
      }
    });
    this.auto_logout_service.start();

    this.sharedServiceSbr$ = this.shared_service.changeEmitted$.subscribe(data=>{ this.handleSharedAction(data); });

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {
        const url = this.router.url;
        const extralevel = (url.includes('alarm-devices')
                               || url.includes('signalisation-devices')
                               || url.includes('devices-in-default')
                               // || url.includes('cameras')
                               || url.includes('sensors'))? 1:0;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.contentType = this.setContentType(url);
        this.containerScale = urlParams.length - extralevel + 1;
        this.contentOffset = urlParams.length - extralevel;

        // console.log('containerScale:',this.containerScale, '| contentOffset:',this.contentOffset)

        this.contentContainer?.nativeElement.classList.add('offset-'+this.contentOffset);
        setTimeout(() => this.contentContainer?.nativeElement.classList.remove('offset-' + this.contentOffset), 350);

        if(this.contentOffset===0){
          this.initHeader();
          this.logs_service.setLogTarget(LogTarget.NONE);
        }

      }
    });

  }

  ngOnInit(){
    this.initVersionChecker();
    this.initWebpush();
  }
  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
    this.sharedServiceSbr$?.unsubscribe();
    this.updateServiceSbr$?.unsubscribe();
    this.autoLogoutServiceSbr$?.unsubscribe();
    this.wsSbr$?.unsubscribe();
  }

  isStatusForced(data: SignalisationStatus): boolean {
    const ref_level = data.level_num ?? parseInt(data.level);
    return ref_level >= 10;
  }

  limitLevel(data:SignalisationStatus): string {
    return ((data.level_num ?? parseInt(data.level)) % 10).toString();
  }

  handleAction(e: any): void{
    switch(e.name){
      case 'open-menu':
        this.menuSidenav?.open();
        break;
      case 'close-menu':
        this.menuSidenav?.close();
        break;
      case 'open-chat':
        this.chatSidenav?.open();
        break;
      case 'close-chat':
        this.chatSidenav?.close();
        break;
      case 'open-contact':
        this.menuSidenav?.close();
        this.contactSidenav?.open();
        break;
      case 'close-contact':
        this.contactSidenav?.close();
        break;
      case 'set-language':
        this.favoriteLanguage = e.data;
        this.translate.use(e.data);
        localStorage.setItem('geoalarme-mobile-favorite-language', e.data);
        this.wording_service.load().subscribe((response:any) => {
          if(response.length){
            this.wording_service.register(response);
            this.menuSidenav?.close();
          }
          return true;
        });
        break;

      case 'infirm-update':
        this.versionChanged = false;
        this.updateSidenav?.close();
        break;
      case 'confirm-update':
        this.versionChanged = true;
        window.location.reload();
        break;
      case 'close-update-overlay':
        this.updateSidenav?.close();
        break;
      case 'check-version':
          this.checkVersion();
          break;
    }
  }

  handleSharedAction(data:any){
    switch (data.name) {
      case 'generate-alert-view':
        this.alertData = data.data;
        break;
    }
  }

  private fetchData(): void {
    const token = localStorage.getItem('geoalarme-mobile-token');
    if(!token)this.router.navigate( ['/login'] );
    this.http_service.get(`${API_URL}sites`, {}, { Authorization: 'Bearer ' + JSON.parse(token!).token })
      .subscribe((response:any) => {
        this.initData(response)
      }, (error) => {
        console.log(error)
      })
  }
  private initData(response:any): void {
    this.data = response.data?? response;
    if(this.data && this.data.length === 1) {
      this.singleSite = true;
      if(this.contentOffset===0) {
        this.router.navigate(['sites', this.data[0].id], { replaceUrl: true });
      }
    }
  }

  private initVersionChecker(): void {
    // @ts-ignore
    this.sw_update.versionUpdates.pipe(filter((evt): evt is VersionReadyEvent => evt.type === 'VERSION_READY')).subscribe(e => this.generateUpdateDialog());
    this.sw_update.unrecoverable.subscribe(e => { console.log('!!! service worker unrecoverable state', e.reason); });
    // debug dialog
    // setTimeout(() => this.generateUpdateDialog(), 5000);
  }
  private checkVersion(): void {
    this.sw_update.checkForUpdate().then((result) => {
      if(result){
        this.generateUpdateDialog();
      }else{
        this.shared_service.emitChange('generate-alert-view', {
          type:'info',
          messageId:'no_update_available',
          timeout:3000,
        });
      }
    })
  }
  private generateUpdateDialog(): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.width = '75%';
    dialogConfig.maxWidth = '650px';
    dialogConfig.disableClose = false;
    dialogConfig.hasBackdrop = true;
    dialogConfig.backdropClass = 'dialog-backdrop';
    dialogConfig.data = {
      "title":  		"",
      "icon":	  		"update",
      "message":  	this.wording['update_available'].charAt(0).toUpperCase() + this.wording['update_available'].slice(1),
      "confirm": 	 	{ label: this.wording['confirmation_dialog_confirm_label'].charAt(0).toUpperCase() + this.wording['confirmation_dialog_confirm_label'].slice(1) },
      "cancel": 	 	{ label: this.wording['confirmation_dialog_infirm_label'].charAt(0).toUpperCase() + this.wording['confirmation_dialog_infirm_label'].slice(1) },
    };

    this.updateDialogRef = this.dialog.open(UpdateDialogComponent, dialogConfig);
    this.updateDialogRef.afterClosed().subscribe((result:any) => {
      if (result && result==='confirm'){
        document.location.reload();
      }
    });

  }

  private initWebpush(): void {
    const sw_sub = localStorage.getItem('geoalarme-mobile-sw-subscription');
    this.initNotificationandler();
    if(!sw_sub){
      this.requestSubscription();
    }else{
      this.notificationsSubscription = JSON.parse(sw_sub);
      console.log("> subscriptions already enabled");
    }
  }
  private requestSubscription(): void {

    if (!this.sw_push.isEnabled) {
      console.log("!!! notifications not available !!!");
      return;
    }

    console.log("> notifications available -> request notifications subscription");

    this.sw_push.requestSubscription({ serverPublicKey: PUBLIC_VAPID_KEY } )
      .then((subscription: PushSubscription) => {
        console.log("... notifications subscribed");
        this.notificationsSubscription = subscription;
        localStorage.setItem('geoalarme-mobile-sw-subscription', JSON.stringify(subscription));
        const notification = {
          notification: {
            title: this.wording['webpush_notification_title'],
            body: this.wording['webpush_notifications_activated'],
            icon: `${API_BASE}/assets/mobile/icons/icon-512x512.png`,
            data: {}
          }
        }
        this.registerNotificationsSubscription(this.notifySubscriptionUrl, subscription, notification);
      })
      .catch(err => console.error('!!! failed to subscribe to notifications', err));

  }
  private registerNotificationsSubscription(notifyUrl: string, subscription: PushSubscription|null, notification: any) {
    console.log("... register notifications subscription");
    const token = localStorage.getItem('geoalarme-mobile-token');
    const headers: any = { Authorization: 'Bearer ' + JSON.parse(token!).token };
    this.http_service.post(notifyUrl, {}, headers,subscription? { subscription, notification } : { notification }).subscribe(
      () => { /*console.log("... notifications subscription registered")*/ },
      (err) => { /*console.error('!!! failed to register notifications subscription', err)*/ }
    );
  }
  private initNotificationandler(): void{
    // console.log('> initNotificationandler');
    /*this.sw_push.messages.subscribe((message) => {
      console.log('... notification received', message)
    })*/
    this.sw_push.notificationClicks.subscribe( e => {
      // console.log('... notification clicked: ', e);
      if(e.notification.data.url){
        // window.open(e.notification.data.url, '_blank');
        this.router.navigate( [ e.notification.data.url ] );
      }
    });
  }

  private setContentType(url: string): PageContentType {
    if(url.includes('sites/')){
      return PageContentType.site;
    }else if(url.includes('sites')){
      return PageContentType.sites;
    }else if(url.includes('station/')){
      return PageContentType.station;
    }else if(url.includes('cameras')){
      return PageContentType.cameras;
    }else if(url.includes('sensors/')){
      return PageContentType.sensor;
    }else if(url.includes('sensors')){
      return PageContentType.sensors;
    }
    return PageContentType.sites;
  }

  private registerPreferredLanguage(): void{
    const token = localStorage.getItem('geoalarme-mobile-token');
    const headers: any = { Authorization: 'Bearer ' + JSON.parse(token!).token };
    this.http_service.post(this.userPreferenfesUrl, {}, headers,{ language:this.favoriteLanguage!.toUpperCase() }).subscribe(
      () => {},
      (err) => { /*console.error('!!! failed to register preferred language', err)*/ }
    );
  }

  private handleWsMessage(data:any|null): void {
    if(data && data.state && data.state==='ws-connected'){
      console.log('> websocket connected')
    }else if(data && data.change && data.change.object==='site_change'){
      if(this.data?.find((item:Site) => item.id===data.change.target_id)){
        this.fetchData()
      }
    }
  }

  private initHeader(): void {
    this.page_header_service.update(this.wording['sites'].charAt(0).toUpperCase() + this.wording['sites'].slice(1));
  }

}
