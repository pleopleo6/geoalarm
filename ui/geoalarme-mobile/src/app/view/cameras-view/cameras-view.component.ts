import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from "rxjs";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { PageHeaderService, WordingService } from "../../app.services";
import { Camera } from "../../model/camera";
import { API_URL } from "../../app.config";

@Component({
  selector: 'cameras-view',
  templateUrl: './cameras-view.component.html',
  styleUrl: './cameras-view.component.scss'
})
export class CamerasViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;

  cameras: Camera[] = [];
  wording: Record<string, string> = {};

  siteId: string;

  contentDeep: number = 2;
  contentOffset: number = 0;

  camerasDataSet: Record<string, { camId:string, path:string, streamReady:boolean, streamFailed:boolean }> = {};

  routerSbr$: Subscription | undefined;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly wording_service: WordingService,
              private readonly page_header_service: PageHeaderService) {

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        this.page_header_service.update(this.wording['cameras'].charAt(0).toUpperCase() + this.wording['cameras'].slice(1)/* + ' ' + this.station?.name*/);
      }
    });

    this.route.data.subscribe( (response: any) => {
      this.initData(response);
      this.page_header_service.update(this.wording['cameras'].charAt(0).toUpperCase() + this.wording['cameras'].slice(1)/* + ' ' + this.station?.name*/);
    });

    const url = this.router.url;
    const extralevel= url.includes('camera/')? 1:0;
    const urlParams= url.split('/').filter((item: string) => item !== '');
    urlParams.shift();

    this.siteId = urlParams[0];
    this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

    this.initCamerasDataSet();

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const url = this.router.url;
        const extralevel= url.includes('camera/')? 1:0;
        const urlParams= url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-'+this.contentOffset), 350);

        if(urlParams.length===2){
          this.page_header_service.update(this.wording['cameras'].charAt(0).toUpperCase() + this.wording['cameras'].slice(1));
        }
      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
  }

  handleImgLoaded(camId: string): void{
    this.camerasDataSet[camId].streamReady = true;
  }
  handleImgFailed(camId: string, error: any): void{
    // console.log(`!!! stream error on camera ${camId} !!!`, error)
    this.camerasDataSet[camId].streamFailed = true;
  }

  private initData(response:any): void {
    this.cameras = response.data??response;
  }

  private initCamerasDataSet(): void {
    let ts = Date.now();
    let camId: string;
    this.cameras?.forEach((camera: Camera) => {
      camId = `cam-${camera.id}`;
      this.camerasDataSet[camId] = {
        camId,
        path: `${API_URL}site/${this.siteId}/camera/${camera.id}/stream.jpg?ts=${ts.toString(16)}&ngsw-bypass=true`,
        streamReady: false,
        streamFailed: false
      };
      ts++;
    })
  }
}
