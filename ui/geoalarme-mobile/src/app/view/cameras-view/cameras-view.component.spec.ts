import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CamerasViewComponent } from './cameras-view.component';

describe('CamerasViewComponent', () => {
  let component: CamerasViewComponent;
  let fixture: ComponentFixture<CamerasViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CamerasViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CamerasViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
