import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from "rxjs";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { PageHeaderService, WordingService } from "../../app.services";
import { Camera } from "../../model/camera";
import { API_URL } from "../../app.config";
// import { MatDialog, MatDialogConfig, MatDialogRef } from "@angular/material/dialog";
// import {IosWsDialogComponent} from "../../component/ios-ws-dialog/ios-ws-dialog.component";

@Component({
  selector: 'camera-view',
  templateUrl: './camera-view.component.html',
  styleUrl: './camera-view.component.scss'
})
export class CameraViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;

  camera: Camera | undefined;
  wording: Record<string, string> = {};

  siteId: string;
  cameraId: string;
  streamPath: string;

  contentDeep: number = 3;
  contentOffset: number = 0;

  streamImageReady = false;
  streamImageFailed = false;

  // ios_dialog_ref: MatDialogRef<UpdateDialogComponent> | undefined;

  routerSbr$: Subscription | undefined;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              // private readonly dialog: MatDialog,
              private readonly wording_service: WordingService,
              private readonly page_header_service: PageHeaderService) {

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        this.page_header_service.update(this.wording['cameras'].charAt(0).toUpperCase() + this.wording['cameras'].slice(1)/* + ' ' + this.station?.name*/);
      }
    });

    this.route.data.subscribe( (response: any) => {
      this.initData(response);
      this.page_header_service.update(this.wording['cameras'].charAt(0).toUpperCase() + this.wording['cameras'].slice(1)/* + ' ' + this.station?.name*/);
    });

    const ts = Date.now().toString(16);
    const url = this.router.url;
    const extralevel= url.includes('camera/')? 1:0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    this.siteId = urlParams[0];
    this.cameraId = urlParams[2];
    this.streamPath = `${API_URL}site/${this.siteId}/camera/${this.cameraId}/stream.jpg?ts=${ts}&ngsw-bypass=true`;
    this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const ts = Date.now().toString(16);
        const url = this.router.url;
        const extralevel= url.includes('camera/')? 1:0;
        const urlParams= url.split('/').filter((item: string) => item!=='');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.cameraId = urlParams[2];
        this.streamPath = `${API_URL}site/${this.siteId}/camera/${this.cameraId}/stream.jpg?ts=${ts}&ngsw-bypass=true`;
        this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-'+this.contentOffset), 350);

        if(urlParams.length===2){
          this.page_header_service.update(this.wording['cameras'].charAt(0).toUpperCase() + this.wording['cameras'].slice(1));
        }
      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
  }

  handleImgFailed(error: any): void{
    // console.log('!!! stream error !!!',error)
    this.streamImageFailed = true;
    /*if(this.isIOS()) {
      this.generateIosWsDialog();
    }*/
  }
  handleImgLoaded(): void{
    this.streamImageReady = true;
  }

  private initData(response:any): void {
    this.camera = response.data??response;
  }

  /*private generateIosWsDialog(): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.width = '75%';
    dialogConfig.maxWidth = '650px';
    dialogConfig.disableClose = false;
    dialogConfig.hasBackdrop = true;
    dialogConfig.backdropClass = 'dialog-backdrop';
    dialogConfig.data = {
      "title":  		"",
      "icon":	  		"update",
      "message":  	this.wording['ios_ws_message'].charAt(0).toUpperCase() + this.wording['ios_ws_message'].slice(1),
      "cancel": 	 	{ label: this.wording['ok'].charAt(0).toUpperCase() + this.wording['ok'].slice(1) },
    };

    this.ios_dialog_ref = this.dialog.open(IosWsDialogComponent, dialogConfig);

  }
  private isIOS(): boolean {
    return [
        'iPad',
        'iPhone',
        'iPod',
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
      ].includes(navigator.platform)
      // iPad on iOS 13 detection
      || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
  }*/

}
