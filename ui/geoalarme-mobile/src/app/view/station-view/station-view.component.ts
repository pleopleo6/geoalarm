import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Station} from "../../model/station";
import { Subscription } from "rxjs";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { HTTPService, PageHeaderService, WebsocketService, WordingService } from "../../app.services";
import { Device } from "../../model/device";
import { StatuePictoType } from "../../model/status-picto-type";
import { API_URL } from "../../app.config";

@Component({
  selector: 'station-view',
  templateUrl: './station-view.component.html',
  styleUrl: './station-view.component.scss'
})
export class StationViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;

  station: Station | undefined;
  devices: Device[] = [];
  wording: Record<string, string> = {};

  siteId: string;

  contentDeep: number = 3;
  contentOffset: number = 0;

  routerSbr$: Subscription | undefined;
  wsSbr$: Subscription | undefined;

  protected readonly StatuePictoType = StatuePictoType;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly http_service: HTTPService,
              private readonly wording_service: WordingService,
              private readonly ws_service: WebsocketService,
              private readonly page_header_service: PageHeaderService) {

    const url = this.router.url;
    const extralevel = (url.includes('alarm-devices')
                           || url.includes('signalisation-devices')
                           || url.includes('cameras')
                           || url.includes('sensors'))? 1:0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    this.siteId = urlParams[0];
    this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        if(this.station){
          this.page_header_service.update(this.wording['station'].charAt(0).toUpperCase() + this.wording['station'].slice(1) + ' ' + this.station?.name.charAt(0).toUpperCase() + this.station?.name.slice(1));
        }
      }
    });

    this.route.data.subscribe( (response: any) => {
      this.initData(response);
      this.page_header_service.update(this.wording['station'].charAt(0).toUpperCase() + this.wording['station'].slice(1) + ' ' + this.station?.name.charAt(0).toUpperCase() + this.station?.name.slice(1));
    });

    this.wsSbr$ = this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const url = this.router.url;
        const extralevel = (url.includes('alarm-devices')
                               || url.includes('signalisation-devices')
                               || url.includes('cameras')
                               || url.includes('sensors'))? 1:0;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-'+this.contentOffset), 350);

        // if(urlParams.length===4){
          this.page_header_service.update(this.wording['station'].charAt(0).toUpperCase() + this.wording['station'].slice(1) + ' ' + this.station?.name.charAt(0).toUpperCase() + this.station?.name.slice(1));
        // }
      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
    this.wsSbr$?.unsubscribe();
  }

  isSignalisationDevice(device:Device): boolean {
    return device.type_id===3
        || device.type_id===4;
  }

  isDefaultDevice(device:Device): boolean {
    return device.type_id===5
        || device.type_id===6
        || device.type_id===7
        || device.type_id===8;
  }

  getPictoLevel(device: Device): string {
    switch (device.type_id) {
      // alarme device
      case 1:
      case 2:
        return device.alarm?.level?? '1';
      // signalisation device
      case 3:
      case 4:
        return device.alarm?.level?? '1';
      // default device
      case 5:
      case 6:
      case 7:
      case 8:
        return device.default?.level?? '1';
      default:
        return '1';
    }
  }

  private fetchData(): void {
    const token = localStorage.getItem('geoalarme-mobile-token');
    if(!token)this.router.navigate( ['/login'] );
    this.http_service.get(`${API_URL}site/${this.siteId}/station/${this.station?.id}`, {}, { Authorization: 'Bearer ' + JSON.parse(token!).token })
      .subscribe((response:any) => {
        this.initData(response)
      }, (error) => {
        // console.log(error)
      })
  }
  private initData(response:any): void {
    this.station = (response.data??response).station;
    this.devices = (response.data??response).devices;
  }
  private handleWsMessage(data:any|null): void {
    if(data && data.change && data.change.object==='station_change'){
      if(this.station?.id===data.change.target_id.toString()){
        this.fetchData()
      }
    }
  }

}
