import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { HTTPService, PageHeaderService, WebsocketService, WordingService } from "../../app.services";
import { AlarmDevice } from "../../model/alarm-device";
import { Site } from "../../model/site";
import { Station } from "../../model/station";
import { SignalisationDevice } from "../../model/signalisation-device";
import { StatuePictoType } from "../../model/status-picto-type";
import { API_URL } from "../../app.config";
import {Device} from "../../model/device";

@Component({
  selector: 'stations-view',
  templateUrl: './stations-view.component.html',
  styleUrl: './stations-view.component.scss'
})
export class StationsViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;

  site: Site | undefined;
  stations: Station[] | undefined;
  wording: Record<string, string> = {};

  max_alarm_devices_alarm_levels: Record<string, string> = {};
  max_alarm_devices_default_levels: Record<string, string> = {};
  max_signalisation_devices_alarm_levels: Record<string, string> = {};
  max_signalisation_devices_default_levels: Record<string, string> = {};
  max_devices_alarm_levels: Record<string, string> = {};
  max_devices_default_levels: Record<string, string> = {};

  contentDeep: number = 2;
  contentOffset: number = 0;

  routerSbr$: Subscription | undefined;
  wsSbr$: Subscription | undefined;

  protected readonly StatuePictoType = StatuePictoType;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly http_service: HTTPService,
              private readonly wording_service: WordingService,
              private readonly ws_service: WebsocketService,
              private readonly page_header_service: PageHeaderService) {

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        if(this.site){
          this.page_header_service.update(
            this.wording['stations'].charAt(0).toUpperCase() + this.wording['stations'].slice(1) + ' ' + this.site?.name.charAt(0).toUpperCase() + this.site?.name.slice(1)
          );
        }
      }
    });

    this.route.data.subscribe( (response: any) => {

      this.initData(response);

      this.page_header_service.update(
        this.wording['stations'].charAt(0).toUpperCase() + this.wording['stations'].slice(1) + ' ' + this.site?.name.charAt(0).toUpperCase() + this.site?.name.slice(1)
      );

    });

    this.wsSbr$ = this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

    const url = this.router.url;
    const extralevel = (url.includes('alarm-devices')
                           || url.includes('signalisation-devices')
                           // || url.includes('cameras')
                           || url.includes('sensors')
                           )? 1:0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const url = this.router.url;
        const extralevel = (url.includes('alarm-devices')
                               || url.includes('signalisation-devices')
                               // || url.includes('cameras')
                               || url.includes('sensors'))? 1:0;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.contentOffset = urlParams.length - (extralevel + this.contentDeep);

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-'+this.contentOffset), 350);

        if(urlParams.length===2){
          this.page_header_service.update(
            this.wording['stations'].charAt(0).toUpperCase() + this.wording['stations'].slice(1)
            + ' ' + this.site?.name.charAt(0).toUpperCase() + this.site?.name.slice(1)
          );
        }
      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
    this.wsSbr$?.unsubscribe();
  }

  hasAlarmDevices(station:Station): boolean {
    return !!station.alarm_devices.length;
  }
  /*getAlarmeDevice(station:Station, device:AlarmDevice): boolean {
    return !!station.alarm_devices.find((d:AlarmDevice) => d.id===device.id);
  }*/

  private fetchData(): void {
    const token = localStorage.getItem('geoalarme-mobile-token');
    if(!token)this.router.navigate( ['/login'] );
    this.http_service.get(`${API_URL}site/${this.site?.id}/stations`, {}, { Authorization: 'Bearer ' + JSON.parse(token!).token })
      .subscribe((response:any) => {
        this.initData(response)
      }, (error) => {
        // console.log(error)
      })
  }
  private initData(response:any): void {

    this.site = (response.data??response).site;
    this.stations = (response.data??response).stations;

    this.stations?.forEach((station:Station) => {

      const max_alarm_devices_alarm_level = Math.max( ...[...[1], ...station.alarm_devices.map((item:AlarmDevice) => item.alarm? parseInt(item.alarm.level) : 1)]);
      const max_alarm_devices_default_level = Math.max( ...[...[1], ...station.alarm_devices.map((item:AlarmDevice) => item.default? parseInt(item.default.level) : 1)]);
      const max_signalisation_devices_alarm_level = Math.max( ...[...[1], ...station.sign_devices.map((item:SignalisationDevice) => item.alarm? parseInt(item.alarm.level) : 1)]);
      const max_signalisation_devices_default_level = Math.max( ...[...[1], ...station.sign_devices.map((item:SignalisationDevice) => item.default? parseInt(item.default.level) : 1)]);
      const max_signalisation_devices_sign_level = Math.max( ...[...[0], ...station.sign_devices.map((item:SignalisationDevice) => this.isSignalisationDevice(item) ? (item.alarm? parseInt(item.alarm.level):1) : 0)]);

      const max_other_devices_alarm_level = Math.max( ...[...[1], ...station.other_devices.map((item:any) => item.alarm? parseInt(item.alarm.level) : 1)]);
      const max_other_devices_default_level = Math.max( ...[...[1], ...station.other_devices.map((item:any) => item.default? parseInt(item.default.level) : 1)]);

      this.max_alarm_devices_alarm_levels[station.name] = max_alarm_devices_alarm_level.toString();
      this.max_alarm_devices_default_levels[station.name] = max_alarm_devices_default_level.toString();
      this.max_signalisation_devices_default_levels[station.name] = max_signalisation_devices_default_level.toString();
      // this.max_devices_alarm_levels[station.name] = Math.max(...[max_alarm_devices_alarm_level, max_signalisation_devices_alarm_level]).toString();
      // this.max_devices_default_levels[station.name] = Math.max(...[max_alarm_devices_default_level, max_signalisation_devices_default_level]).toString();
      this.max_devices_alarm_levels[station.name] = Math.max(...[max_alarm_devices_alarm_level, max_signalisation_devices_alarm_level, max_other_devices_alarm_level]).toString();
      this.max_devices_default_levels[station.name] = Math.max(...[max_alarm_devices_default_level, max_signalisation_devices_default_level, max_other_devices_default_level]).toString();

      if(max_signalisation_devices_sign_level>0) this.max_signalisation_devices_alarm_levels[station.name] = max_signalisation_devices_sign_level.toString();

    });
  }
  private handleWsMessage(data:any|null): void {
    if(data && data.change && data.change.object==='station_change'){
      if(this.stations?.find((item:Station) => item.id===data.change.target_id)){
        this.fetchData()
      }
    }
  }

  private isSignalisationDevice(device:SignalisationDevice): boolean {
    return device.type_id===3 || device.type_id===4;
  }

}
