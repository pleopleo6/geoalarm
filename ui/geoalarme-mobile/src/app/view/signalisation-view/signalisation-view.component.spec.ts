import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalisationViewComponent } from './signalisation-view.component';

describe('SignalisationViewComponent', () => {
  let component: SignalisationViewComponent;
  let fixture: ComponentFixture<SignalisationViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignalisationViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignalisationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
