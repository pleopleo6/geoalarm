import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { MatSidenav } from "@angular/material/sidenav";
import { HTTPService, LogsService, PageHeaderService, SharedService, WebsocketService, WordingService } from "../../app.services";
import { Installation } from "../../model/installation";
import { StatuePictoType } from "../../model/status-picto-type";
import { Command } from "../../model/command";
import { AlarmDevice } from "../../model/alarm-device";
import { Station } from "../../model/station";
import { Subscription } from "rxjs";
import { SignalisationDevice } from "../../model/signalisation-device";
import { API_URL } from "../../app.config";
import { LogTarget } from "../../model/log-target";
import {AlarmStatus} from "../../model/alarm-status";
import {Alarm} from "../../model/alarm";

export enum stateMap { OFF = 0, ON = 1 }
export enum commandContext { ADMIN = 0, OPERATOR = 1, ANY = 3 }

@Component({
  selector: 'site-view',
  templateUrl: './site-view.component.html',
  styleUrl: './site-view.component.scss'
})
export class SiteViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;
  @ViewChild('settingsSidenav', {static: true}) settingsSidenav: MatSidenav | undefined;
  @ViewChild('commandsSidenav', {static: true}) commandsSidenav: MatSidenav | undefined;

  commandParams: Record<string, { picto: string, class: string, context: commandContext }> = {
      SITE_REFRESH:       { picto: 'sync',    class: 'blue',    context: commandContext.ADMIN },
      SITE_STATUS:        { picto: 'sync',    class: 'blue',    context: commandContext.ADMIN },
      SITE_INIT:          { picto: 'sync',    class: 'blue',    context: commandContext.ADMIN },
      RESET:              { picto: 'sync',    class: 'blue',    context: commandContext.OPERATOR },
      ORANGE_STATUS:      { picto: 'pause',   class: 'orange',  context: commandContext.OPERATOR },
      ALARME:             { picto: 'warning', class: 'red',     context: commandContext.OPERATOR },
      SITE_SGNLLVL:       { picto: 'traffic',  class: 'orange',  context: commandContext.ADMIN },
      SITE_ALMLVL:        { picto: 'warning', class: 'red',     context: commandContext.ADMIN },
  };
  alarmLevels: Record<string, { id: number|string, label?: string, level?: string, class:string }> = {
    LEVEL_0: { id:'auto',   label:'AUTO',      level:'auto',   class:'level-alm-auto' },
    LEVEL_1: { id:11,        label:'1',         level:'1',      class:'level-alm-1' },
    LEVEL_2: { id:12,        label:'2',         level:'2',      class:'level-alm-2' },
    LEVEL_3: { id:13,        label:'3',         level:'3',      class:'level-alm-3' },
    LEVEL_4: { id:14,        label:'4',         level:'4',      class:'level-alm-4' },
  };
  signalisationLevels: Record<string, { id: number|string, label?: string, level?: string, class:string }> = {
    LEVEL_0: { id:'auto',   label:'AUTO',      level:'SGNLAUTO',      class:'level-sign-auto' },
    LEVEL_1: { id:10,        label:'OFF',       level:'SGNLOFF',       class:'level-sign-off' },
    LEVEL_2: { id:11,        label:'1',         level:'SGNLALM1',      class:'level-sign-1' },
    LEVEL_3: { id:12,        label:'2',         level:'SGNLALM2',      class:'level-sign-2' },
    LEVEL_4: { id:13,        label:'3',         level:'SGNLALM3',      class:'level-sign-3' },
    LEVEL_5: { id:14,        label:'4',         level:'SGNLALM4',      class:'level-sign-4' },
    LEVEL_6: { id:15 ,       label:'PAUSE',     level:'SGNLPAUSE',     class:'level-sign-pause' },
  };

  data: Installation | undefined;
  all_alarm_devices: AlarmDevice[] = [];
  all_signalisation_devices: SignalisationDevice[] = [];
  wording: Record<string, string> = {};

  max_stations_alarm_level: string | undefined;
  max_stations_default_level: string | undefined;
  max_alarm_devices_alarm_level: string | undefined;
  max_alarm_devices_default_level: string | undefined;
  max_signalisation_devices_alarm_level: string | undefined;
  max_signalisation_devices_default_level: string | undefined;

  siteId: string;
  isAdmin = false;

  contentDeep: number = 1;
  contentOffset: number = 0;

  allCommands: Command[] = [];
  basicCommands: Command[] = [];
  adminCommands: Command[] = [];
  currentCommand: Command | undefined | null;
  currentCommandUid: string | undefined | null;
  commandInProgress: boolean = false;
  logHidden: boolean | undefined;

  routerSbr$: Subscription | undefined;
  wsSbr$: Subscription | undefined;
  sharedServiceSbr$: Subscription | undefined;
  logTargetSrb$: Subscription | undefined;

  protected readonly StatuePictoType = StatuePictoType;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly wording_service: WordingService,
              private readonly page_header_service: PageHeaderService,
              private readonly http_service: HTTPService,
              private readonly logs_service: LogsService,
              private readonly ws_service: WebsocketService,
              private readonly shared_service: SharedService) {

    this.route.data.subscribe( (response: any) => this.initData(response));

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        this.initHeader();
      }
    });

    const url = this.router.url;
    const extralevel = (url.includes('alarm-devices')
                           || url.includes('signalisation-devices')
                           || url.includes('devices-in-default')
                           || url.includes('sensors'))? 1 : 0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    const userRolesBySite: Record<string, string> = JSON.parse(localStorage.getItem('geoalarme-mobile-roles')!);

    this.siteId = urlParams[0];
    this.contentOffset = urlParams.length - (extralevel +  this.contentDeep);

    this.isAdmin = userRolesBySite[this.siteId]==='administrator' || userRolesBySite[this.siteId]==='super-administrator';

    this.wsSbr$ = this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

    this.sharedServiceSbr$ = this.shared_service.changeEmitted$.subscribe(data=>{ this.handleSharedAction(data); });

    this.logTargetSrb$ = this.logs_service.logsTarget$.subscribe((logTarget: LogTarget) => {
      this.logHidden = logTarget===LogTarget.NONE;
    });

    this.logs_service.setLogTarget(url.includes('sensors') || url.includes('cameras')? LogTarget.NONE : LogTarget.SITE);

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const url = this.router.url;
        const extraLevel = (url.includes('alarm-devices')
                               || url.includes('signalisation-devices')
                               || url.includes('devices-in-default')
                               || url.includes('sensors'))? 1 : 0;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.contentOffset = urlParams.length - (extraLevel + this.contentDeep);

        this.isAdmin = userRolesBySite[this.siteId]==='administrator' || userRolesBySite[this.siteId]==='super-administrator';

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-' + this.contentOffset), 350);

        if(urlParams.length===1){
          this.initHeader();
        }

        this.logs_service.setLogTarget(url.includes('sensors') || url.includes('cameras')? LogTarget.NONE : LogTarget.SITE);

      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
    this.wsSbr$?.unsubscribe();
    this.sharedServiceSbr$?.unsubscribe();
    this.logTargetSrb$?.unsubscribe();
  }

  getCommandState(command: Command): string {
    const isAdminParametricCommand: boolean = this.isAdmin && (
         command.name==='SITE_STATUS'
      || command.name==='SITE_ALMLVL'
      || command.name==='SITE_SGNLLVL');
    switch(command.name) {
      case 'ORANGE_STATUS':
        return (this.data?.status?.id===2? 'orange-on':'orange-off') + (isAdminParametricCommand? ' parametric': '');
      case 'ALARME':
        return (this.data?.alarm? `alarme-${this.data?.alarm.level}`:'alarme-off') + (isAdminParametricCommand? ' parametric': '');
      case 'SITE_SGNLLVL':
      case 'SITE_ALMLVL':
        return 'parametric';
      /*case 'SITE_SGNLLVL':
        const isSignalForced = this.isSignForced(this.data?.sgnl_site);
        if(!isSignalForced) {
          // mode sign. auto - par de marquage de l'état de la signalisation
          return 'parametric';
        } else {
          // mode sign. forcée - marquage de l'état de la signalisation
          switch (this.data?.sgnl_site) {
            case 'SGNLOFF':
              return 'parametric sign_off';
            case 'SGNLALM1':
              return 'parametric sign_alm1';
            case 'SGNLALM2':
              return 'parametric sign_alm2';
            case 'SGNLALM3':
              return 'parametric sign_alm3';
            case 'SGNLALM4':
              return 'parametric sign_alm4';
            case 'SGNLPAUSE':
              return 'parametric sign_pause';
          }
        }
        return (this.data?.status?.id===2? 'orange-on':'orange-off') + (isAdminParametricCommand? ' parametric': '');
      case 'SITE_ALMLVL':
        const isAlarmForced = this.isAlarmForced(this.data?.alarm);
        return (this.data?.alarm && isAlarmForced? `alarme-${this.data?.alarm.level}`:'alarme-off') + (isAdminParametricCommand? ' parametric': '');*/
      default:
        return this.commandParams[command.name].class;
    }
  }

  isAlarmForced(alarm: Alarm|undefined): boolean {
    // return true;   // debug
    return alarm? parseInt(alarm.level) > 10 : false;
  }

  isSignForced(sgnlSite: string|undefined): boolean {
    // return true;   // debug
    return sgnlSite? sgnlSite.includes('FORCE') : false;
  }

  limitLevel(data:any): string {
    const level = data? parseInt(data.level) : 1;
    return level>9? level.toString().substring(1) : level.toString();
  }

  isCommandParametricButtonChecked(level: string, signalisationMode:boolean): boolean {
    return signalisationMode? (level===this.data?.sgnl_site || level===this.data?.sgnl_site?.replace('FORCE','')) : (level===this.limitLevel(this.data?.alarm));
  }

  handleCommand(command: Command|any, commandArgs?: number|string|undefined): void {

    const token = localStorage.getItem('geoalarme-mobile-token');
    const headers: any = { Authorization: 'Bearer ' + JSON.parse(token!).token };
    let postDatas: { uid:string, station?:number, device?:number, param?:number|string } = { uid: Date.now().toString(16) };

    switch (command.name) {

      case 'SITE_STATUS':
        postDatas.param = (this.data!.status.level_num===1) ? stateMap.OFF : stateMap.ON;
        break;

      case 'ORANGE_STATUS':
        postDatas.param = (this.data!.sgnl_site==='SGNLPAUSE' || this.data!.sgnl_site==='SGNLOFF') ? stateMap.ON : stateMap.OFF;
        break;

      case 'ALARME':
        postDatas.param = !!commandArgs? (commandArgs==='auto'? 0 : commandArgs as number) : 4;
        break;

      case 'SITE_ALMLVL':
      case 'SITE_SGNLLVL':
        // const isForced = command.name==='SITE_SGNLLVL'? this.isSignForced(this.data?.sgnl_site) : this.isAlarmForced(this.data?.alarm);
        // postDatas.param = isForced? `1${commandArgs}` : commandArgs;
        postDatas.param = commandArgs;
        break;

      case 'ALM_QUITTANCE':
        command = this.allCommands.find((command: Command) => command.name==='ALM_QUITTANCE');
        break;

      default:
        break;
    }

    this.currentCommand = command;
    this.currentCommandUid = command.uid;
    this.commandInProgress = true;

    // console.log('command:',command, '| commandArgs:',commandArgs, '| postDatas:',postDatas)

    this.http_service.post(`${API_URL}site/${this.siteId}/command/${command.id}`, {}, headers, postDatas).subscribe(() => {
      this.shared_service.emitChange('generate-alert-view', {
        type:'info',
        messageId:'command_registered',
        timeout:3000,
      });
      if(this.currentCommand?.name==='SITE_REFRESH') {
        this.shared_service.emitChange('refresh-site-requested');
      }
    }, (err: any) => {
      // console.log(err);
      this.shared_service.emitChange('generate-alert-view', {
        type:'error',
        messageId:'command_rejected',
        timeout:3000,
      });
      if(this.currentCommand?.name==='SITE_REFRESH') {
        this.shared_service.emitChange('refresh-site-rejected');
      }
      this.currentCommand = null;
      this.currentCommandUid = null;
      this.commandInProgress = false;
    });

  }

  handleAction(e: any): void{
    switch(e.name) {
      case 'open-settings-overlay':
        this.settingsSidenav?.open();
        break;
      case 'close-settings-overlay':
        this.settingsSidenav?.close();
        break;
      case 'open-commands-overlay':
        this.commandsSidenav?.open();
        break;
      case 'close-commands-overlay':
        this.commandsSidenav?.close();
        break;
    }
  }

  handleSharedAction(data:any){
    switch (data.name) {
      case 'refresh-site':
        const cmd = this.allCommands.find((command: Command) => command.name==='SITE_REFRESH');
        if(cmd) { this.handleCommand(cmd); }
        break;
      case 'refresh-site-completed':
        this.fetchData();
        break;
      case 'open-site-settings':
        this.settingsSidenav?.open();
        break;
    }
  }

  private fetchData(): void {
    const token = localStorage.getItem('geoalarme-mobile-token');
    if(!token)this.router.navigate( ['/login'] );
    this.http_service.get(`${API_URL}site/${this.siteId}`, {}, { Authorization: 'Bearer ' + JSON.parse(token!).token })
      .subscribe((response:any) => {
        this.initData(response)
      }, (error) => {
        // console.log(error)
      })
  }

  private initData(response:any): void {

    let ts = Date.now();

    this.all_alarm_devices.splice(0);
    this.all_signalisation_devices.splice(0);

    this.data = response.data?? response;
    const basicCommands: Command[] = (response.data??response).commands.filter((command: Command) => this.commandParams[command.name] && (this.commandParams[command.name].context===commandContext.OPERATOR || this.commandParams[command.name].context===commandContext.ANY))
      .map((command: Command) => { const c = {...command, uid: ts.toString(16)}; ts++; return c });
    const adminCommands: Command[] = (response.data??response).commands.filter((command: Command) => this.commandParams[command.name] && (this.commandParams[command.name].context===commandContext.ADMIN || this.commandParams[command.name].context===commandContext.ANY)).map((command: Command) => ({...command, uid: Date.now().toString(16) }))
      .map((command: Command) => { const c = {...command, uid: ts.toString(16)}; ts++; return c });
    const quittancesCommands: Command[] = (response.data??response).commands.filter((command: Command) => command.name.includes('QUITTANCE'))
      .map((command: Command) => { const c = {...command, uid: ts.toString(16)}; ts++; return c });
    this.allCommands = [...basicCommands, ...adminCommands, ...quittancesCommands];
    this.basicCommands = basicCommands.filter((command: Command) => command.name!=='SITE_REFRESH');
    this.adminCommands = adminCommands.filter((command: Command) => command.name!=='SITE_REFRESH');
    (response.data??response).stations.forEach((station: Station) => {
      this.all_alarm_devices = [...this.all_alarm_devices, ...station.alarm_devices];
      this.all_signalisation_devices = [...this.all_signalisation_devices, ...station.sign_devices];
    })

    const max_alarm_devices_alarm_level = Math.max( ...[...[1], ...this.all_alarm_devices.map((item:AlarmDevice) => item.alarm? parseInt(item.alarm.level) : 1)] );
    const max_alarm_devices_default_level = Math.max( ...[...[1], ...this.all_alarm_devices.map((item:AlarmDevice) => item.default? parseInt(item.default.level) : 1)] );
    const max_signalisation_devices_alarm_level = Math.max( ...[...[1], ...this.all_signalisation_devices.map((item:SignalisationDevice) => item.alarm? parseInt(item.alarm.level) : 1)] );
    const max_signalisation_devices_default_level = Math.max( ...[...[1], ...this.all_signalisation_devices.map((item:SignalisationDevice) => item.default? parseInt(item.default.level) : 1)] );
    this.max_alarm_devices_alarm_level = max_alarm_devices_alarm_level.toString();
    this.max_alarm_devices_default_level = max_alarm_devices_default_level.toString();
    this.max_signalisation_devices_alarm_level = max_signalisation_devices_alarm_level.toString();
    this.max_signalisation_devices_default_level = max_signalisation_devices_default_level.toString();
    this.max_stations_alarm_level = Math.max(...[max_alarm_devices_alarm_level, max_signalisation_devices_alarm_level]).toString();
    this.max_stations_default_level = Math.max(...[max_alarm_devices_default_level, max_signalisation_devices_default_level]).toString();

    /*console.log('all_alarm_devices:',this.all_alarm_devices, this.all_alarm_devices.map(item => console.log(item.alarm?.level)),
      '\nall_signalisation_devices:',this.all_signalisation_devices, this.all_signalisation_devices.map(item => console.log(item.alarm?.level)),
      '\nmax_alarm_devices_alarm_level:',max_alarm_devices_alarm_level,
      '\nmax_signalisation_devices_alarm_level:',max_signalisation_devices_alarm_level,
      '\n-> max_stations_alarm_level:',this.max_stations_alarm_level);*/

    // console.log('allCommands:',this.allCommands, '| adminCommands:',adminCommands, '| basicCommands:',basicCommands);

  }

  private handleWsMessage(data:any|null): void {
    if(data && data['change'] && data['change']['object']==='site_change'){
      if(this.siteId===data.change.target_id.toString()){
        this.fetchData();
      }
    }else if(data && data['change'] && data['change']['object']==='command_change'){
      this.handleCommandResult(data['change']);
    }
  }

  private handleCommandResult(data:any): void {

    switch(data.target.state_id){
      case 3:
        // commande confirmed
        this.shared_service.emitChange('generate-alert-view', {
          type:'success',
          messageId:'command_confirmed',
          timeout:3000,
        });
        this.shared_service.emitChange('refresh-site-completed');
        break;
      case 4:
        // commande rejected
        this.shared_service.emitChange('generate-alert-view', {
          type:'error',
          messageId:'command_rejected',
          timeout:3000,
        });
        this.shared_service.emitChange('refresh-site-rejected');
        break;
    }

    this.commandInProgress = false;
    this.currentCommandUid = null;
    this.currentCommand = null;

    this.commandsSidenav?.close();

  }

  private initHeader(): void {
    this.page_header_service.update((this.data?.name.toLowerCase().indexOf('site')!==0?( this.wording['site'].charAt(0).toUpperCase() + this.wording['site'].slice(1)) : '') + ' ' + this.data?.name.charAt(0).toUpperCase() + this.data?.name.slice(1));
  }


}
