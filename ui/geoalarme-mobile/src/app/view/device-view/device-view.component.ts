import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { HTTPService, PageHeaderService, WebsocketService, WordingService } from "../../app.services";
import { Subscription } from "rxjs";
import { StatuePictoType } from "../../model/status-picto-type";
import { Device } from "../../model/device";
import { API_URL } from "../../app.config";

@Component({
  selector: 'device-view',
  templateUrl: './device-view.component.html',
  styleUrl: './device-view.component.scss'
})
export class DeviceViewComponent implements OnDestroy {

  @ViewChild('body', {static: true}) body: ElementRef | undefined;

  device: Device | undefined;
  wording: Record<string, string> = {};

  siteId: string;

  contentDeep: number = 4;
  contentOffset: number = 0;

  isSignalisationView: boolean;
  isDeviceInDefaultView: boolean;

  routerSbr$: Subscription | undefined;
  wsSbr$: Subscription | undefined;

  protected readonly StatuePictoType = StatuePictoType;

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly http_service: HTTPService,
              private readonly wording_service: WordingService,
              private readonly ws_service: WebsocketService,
              private readonly page_header_service: PageHeaderService) {

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        if(this.device){
          this.initHeader();
        }
      }
    });

    this.route.data.subscribe( (response: any) => {
      this.initData(response);
      this.initHeader();
    });

    this.wsSbr$ = this.ws_service.message$.subscribe( (data:any|null) => this.handleWsMessage(data));

    const url = this.router.url;
    const extralevel = (url.includes('alarm-devices')
                           || url.includes('signalisation-devices')
                           || url.includes('devices-in-default')
                           || url.includes('cameras')
                           || url.includes('sensors'))? 1:0;
    const urlParams= url.split('/').filter((item: string) => item!=='');
    urlParams.shift();

    this.siteId = urlParams[0];
    this.contentOffset = urlParams.length - (extralevel + this.contentDeep);
    this.isSignalisationView = url.includes('signalisation-devices');
    this.isDeviceInDefaultView = url.includes('devices-in-default');

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {

        const url = this.router.url;
        const extralevel = (url.includes('alarm-devices')
                               || url.includes('signalisation-devices')
                               || url.includes('devices-in-default')
                               || url.includes('cameras')
                               || url.includes('sensors'))? 1:0;
        const urlParams = url.split('/').filter((item: string) => item !== '');
        urlParams.shift();

        this.siteId = urlParams[0];
        this.contentOffset = urlParams.length - (extralevel + this.contentDeep);
        this.isSignalisationView = url.includes('signalisation-devices');
        this.isDeviceInDefaultView = url.includes('devices-in-default');

        this.body?.nativeElement.classList.add('offset-width-'+this.contentOffset);
        setTimeout(() => this.body?.nativeElement.classList.remove('offset-width-'+this.contentOffset), 350);

        this.initHeader();

      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
    this.wsSbr$?.unsubscribe();
  }

  isSignalisationDevice(): boolean {
    return this.device!.type_id===3
        || this.device!.type_id===4;
  }

  isDefaultDevice(): boolean {
    return this.device!.type_id===5
        || this.device!.type_id===6
        || this.device!.type_id===7
        || this.device!.type_id===8;
  }

  getPictoLevel(): string {
    switch (this.device!.type_id) {
      // alarme device
      case 1:
      case 2:
        return this.device!.alarm?.level?? '1';
      // signalisation device
      case 3:
      case 4:
        return this.device!.alarm?.level?? '1';
      // default device
      case 5:
      case 6:
      case 7:
      case 8:
        return this.device!.default?.level?? '1';
      default:
        return '1';
    }
  }

  private fetchData(): void {
    const token = localStorage.getItem('geoalarme-mobile-token');
    if(!token)this.router.navigate( ['/login'] );
    this.http_service.get(`${API_URL}site/${this.siteId}/device/${this.device?.id}`, {}, { Authorization: 'Bearer ' + JSON.parse(token!).token })
      .subscribe((response:any) => {
        this.initData(response)
      }, (error) => {
        // console.log(error)
      })
  }
  private initData(response:any): void {
    this.device = (response.data??response).device;
  }
  private handleWsMessage(data:any|null): void {
    if(data && data.change && data.change.object==='device_change'){
      if(this.device?.id===data.change.target_id){
        this.fetchData()
      }
    }
  }

  private initHeader(): void {
    this.page_header_service.update(this.wording['device_details'].charAt(0).toUpperCase() + this.wording['device_details'].slice(1));
  }

}
