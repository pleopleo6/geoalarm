import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from "./auth/auth.guard";
import { AppDatasResolver } from "./app.datas.resolver";
import { WordingResolverService } from "./resolver/WordingResolverService";
import { DevicesViewComponent } from "./view/devices-view/devices-view.component";
import { DeviceViewComponent } from "./component/device-view/device-view.component";
import { LoginViewComponent } from "./view/login-view/login-view.component";
import { LogsViewComponent } from "./view/logs-view/logs-view.component";
import { MapViewComponent } from "./view/map-view/map-view.component";
import { SitesViewComponent } from "./view/sites-view/sites-view.component";
import { SiteViewComponent } from "./component/site-view/site-view.component";
import { UsersViewComponent } from "./view/users-view/users-view.component";
import { UserViewComponent } from "./component/user-view/user-view.component";
import {ContactViewComponent} from "./view/contact-view/contact-view.component";

const routes: Routes = [

  {
    path: 'login',
    component: LoginViewComponent
  },

  {
    path: 'devices',
    canActivate: [ AuthGuard ],
    data: { api_path: 'devices', api_path_replacements: [] },
    resolve: { wording: WordingResolverService, data: AppDatasResolver },
    component: DevicesViewComponent,
    children: [
      {
        path: ':deviceId',
        canActivate: [ AuthGuard ],
        data: { api_path: 'devices/deviceId', api_path_replacements: ['deviceId'] },
        resolve: { wording: WordingResolverService, data: AppDatasResolver },
        component: DeviceViewComponent },
    ]
  },

  {
    path: 'map',
    canActivate: [ AuthGuard ],
    resolve: { wording: WordingResolverService/*, data: AppDatasResolver*/ },
    component: MapViewComponent,
    children: [
      {
        path: ':deviceId',
        canActivate: [ AuthGuard ],
        resolve: { wording: WordingResolverService/*, data: AppDatasResolver*/ },
        component: DeviceViewComponent },
    ]
  },

  {
    path: 'users',
    canActivate: [ AuthGuard ],
    data: { api_path: 'users', api_path_replacements: [] },
    resolve: { wording: WordingResolverService, data: AppDatasResolver },
    component: UsersViewComponent,
    children: [
      {
        path: 'create',
        canActivate: [ AuthGuard ],
        resolve: { wording: WordingResolverService },
        component: UserViewComponent },
      {
        path: ':userId',
        canActivate: [ AuthGuard ],
        data: { api_path: 'users/userId', api_path_replacements: ['userId'] },
        resolve: { wording: WordingResolverService, data: AppDatasResolver },
        component: UserViewComponent },
    ]
  },

  {
    path: 'logs',
    canActivate: [ AuthGuard ],
    resolve: { wording: WordingResolverService/*, data: AppDatasResolver*/ },
    component: LogsViewComponent
  },

  {
    path: 'contact',
    canActivate: [ AuthGuard ],
    resolve: { wording: WordingResolverService },
    component: ContactViewComponent
  },

  {
    path: 'sites',
    canActivate: [ AuthGuard ],
    data: { api_path: 'sites', api_path_replacements: [] },
    resolve: { wording: WordingResolverService, data: AppDatasResolver },
    component: SitesViewComponent,
    children: [
      {
        path: ':siteId',
        canActivate: [ AuthGuard ],
        data: { api_path: 'site/siteId', api_path_replacements: ['siteId'] },
        resolve: { wording: WordingResolverService, data: AppDatasResolver },
        component: SiteViewComponent,
        children: [
          {
            path: ':deviceId',
            data: { api_path: 'site/siteId/devices/deviceId', api_path_replacements: ['siteId','deviceId'] },
            resolve: { wording: WordingResolverService, data: AppDatasResolver },
            canActivate: [ AuthGuard ],
            component: DeviceViewComponent
          },
        ]
      },
    ]
  },

  { path: '', redirectTo: '/sites', pathMatch: 'full' },
  { path: '**', redirectTo: '/sites', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
