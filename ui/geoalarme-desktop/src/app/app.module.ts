import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DialogModule } from "@angular/cdk/dialog";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatTabsModule } from "@angular/material/tabs";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTableModule } from "@angular/material/table";
import { MatSortModule } from "@angular/material/sort";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatRippleModule } from "@angular/material/core";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";

import { AppDatasResolver } from "./app.datas.resolver";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SitesViewComponent } from './view/sites-view/sites-view.component';
import { DevicesViewComponent } from './view/devices-view/devices-view.component';
import { MapViewComponent } from './view/map-view/map-view.component';
import { UsersViewComponent } from './view/users-view/users-view.component';
import { LogsViewComponent } from './view/logs-view/logs-view.component';
import { LoginViewComponent } from './view/login-view/login-view.component';
import { ProfileViewComponent } from './view/profile-view/profile-view.component';
import { DeviceViewComponent } from './component/device-view/device-view.component';
import { LogsFilterViewComponent } from './component/logs-filter-view/logs-filter-view.component';
import { SearchViewComponent } from './component/search-view/search-view.component';
import { SiteViewComponent } from "./component/site-view/site-view.component";
import { UserViewComponent } from './component/user-view/user-view.component';
import { HeaderViewComponent } from './component/header-view/header-view.component';
import { GenericTableComponent } from './component/generic-table/generic-table.component';
import { WordingResolverService } from './resolver/WordingResolverService';
import { FirstLetterUppercasePipe } from "./util/pipe/first-letter-uppercase.pipe";
import { FooterViewComponent } from './component/footer-view/footer-view.component';
import { ContentBoxComponent } from './component/content-box/content-box.component';
import { ContactViewComponent } from './view/contact-view/contact-view.component';
import { LoadingSpinnerComponent } from './component/loading-spinner/loading-spinner.component';
import { UserRoleSelectorComponent } from './component/user-role-selector/user-role-selector.component';
import { ConfirmationDialogComponent } from './component/confirmation-dialog/confirmation-dialog.component';
import { DistributionListComponent } from './component/distribution-list/distribution-list.component';
import { DistributionListItemComponent } from './component/distribution-list-item/distribution-list-item.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/desktop/i18n/');
}

@NgModule({
  declarations: [
    AppComponent,
    SitesViewComponent,
    DevicesViewComponent,
    MapViewComponent,
    UsersViewComponent,
    LogsViewComponent,
    LoginViewComponent,
    ProfileViewComponent,
    SiteViewComponent,
    DeviceViewComponent,
    SearchViewComponent,
    UserViewComponent,
    LogsFilterViewComponent,
    HeaderViewComponent,
    FirstLetterUppercasePipe,
    GenericTableComponent,
    FooterViewComponent,
    ContentBoxComponent,
    ContactViewComponent,
    LoadingSpinnerComponent,
    UserRoleSelectorComponent,
    ConfirmationDialogComponent,
    DistributionListComponent,
    DistributionListItemComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatTabsModule,
    DialogModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'fr'
    }),
    MatTableModule,
    DragDropModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSelectModule,
    MatCheckboxModule,
  ],
  providers: [
    AppDatasResolver,
    WordingResolverService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
