import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { WordingService } from '../app.services';

@Injectable()
export class WordingResolverService implements Resolve<any> {

  constructor(private wording_service: WordingService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    if(!Object.keys(this.wording_service.getWording()).length){
      return this.wording_service.load().subscribe((response:any) => {
        this.wording_service.register(response)
        return true;
      });
    }else{
      return true;
    }
  }
}
