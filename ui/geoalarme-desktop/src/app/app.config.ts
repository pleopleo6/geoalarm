import { environment } from '../environments/environment';

export const DEFAULT_LANGUAGE = 'fr';
export const API_BASE: string = (environment.location.hostname === 'localhost') ? 'http://localhost:3333' : window.location.origin;
export const API_URL: string = API_BASE + '/api/v1/desktop/';
