import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { WordingService } from "../../app.services";
import { Device } from "../../model/device";
import { TableCateg } from "../../model/table-categ";

@Component({
  selector: 'devices-view',
  templateUrl: './devices-view.component.html',
  styleUrl: './devices-view.component.scss'
})
export class DevicesViewComponent {

  devicesTableStructure:TableCateg[] = [
    { id: 'name',           key: 'name',                  sortKey: 'name',            labelId: 'device_name' },
    { id: 'type',           key: 'type.name',             sortKey: 'type.name',       labelId: 'device_type' },
    { id: 'site_name',      key: 'siteName',              sortKey: 'siteName',        labelId: 'site_name' },
    { id: 'station_name',   key: 'stationName',           sortKey: 'stationName',     labelId: 'station_name' },
    { id: 'alarm',          key: 'alarm.description',     sortKey: 'alarm.level',     labelId: 'alarm_level' },
    { id: 'default',        key: 'default.description',   sortKey: 'default.level',   labelId: 'default_level' },
    { id: 'battery_level',  key: 'batteryLevel',          sortKey: 'batteryLevel',    labelId: 'battery_level' },
    { id: 'com_level',      key: 'comLevel',              sortKey: 'comLevel',        labelId: 'com_level' },
    { id: 'com_flag',       key: 'comFlag',               sortKey: 'comFlag',         labelId: 'com_flag' },
  ];

  data: Device[] = [];
  wording: Record<string, string> = {};

  constructor(private readonly route: ActivatedRoute,
              private readonly wording_service: WordingService){

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) this.wording = wording;
    });

    this.route.data.subscribe( (response: any) => {
      const noDataLabel = this.wording['no_data'].charAt(0).toUpperCase() + this.wording['no_data'].slice(1);
      this.data = response.data.map((item:Device) => ({
        ...item,
        batteryLevel: item.batteryLevel?.length? item.batteryLevel : noDataLabel,
        comLevel: item.comLevel?.length? item.comLevel : noDataLabel,
        comFlag: item.comFlag?.length? item.comFlag : noDataLabel,
      }));
    });

    this.data.sort((a:Device,b:Device) => ((a.name > b.name)? 1 : (a.name < b.name ? -1 : 0)));

  }

}
