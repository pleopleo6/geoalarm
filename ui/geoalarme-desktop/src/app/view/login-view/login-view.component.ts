import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MatButtonToggleChange } from "@angular/material/button-toggle";
import { TranslateService } from "@ngx-translate/core";
import { DEFAULT_LANGUAGE } from "../../app.config";
import { AuthService } from "../../auth/auth.service";
import { Languages } from "../../model/languages";
import { Language } from "../../model/language";
import { WordingService } from "../../app.services";

@Component({
  selector: 'login-view',
  templateUrl: './login-view.component.html',
  styleUrl: './login-view.component.scss'
})
export class LoginViewComponent implements OnInit {

  logingForm:         FormGroup;
  loging:             boolean = false;
  returnUrl: 	        string|undefined;

  languages:          Language[] = [];
  favoriteLanguage:   string;

  constructor(private readonly route: ActivatedRoute,
              private readonly router: Router,
              private readonly auth_service: AuthService,
              private readonly wording_service: WordingService,
              private readonly translate: TranslateService) {

    localStorage.removeItem('geoalarme-desktop-token');
    localStorage.removeItem('geoalarme-desktop-user');
    localStorage.removeItem('geoalarme-desktop-roles');

    this.wording_service.reset();

    this.logingForm = new FormGroup({
      email: new FormControl('',[Validators.required]),
      password: new FormControl('', [Validators.required]),
    });

    Object.keys(Languages).forEach((k: string) => {
      // @ts-ignore
      this.languages = [...this.languages, { id: k, label: Languages[k] }]
    })

    const favoriteLanguage = localStorage.getItem('geoalarme-desktop-favorite-language');
    this.favoriteLanguage = favoriteLanguage?? DEFAULT_LANGUAGE;
    this.translate.setDefaultLang(this.favoriteLanguage);
    this.translate.use(this.favoriteLanguage);

  }

  ngOnInit() {
    this.auth_service.logout();
    if(localStorage.getItem('geoalarme-desktop-token')){
      this.auth_service.logout().subscribe( data => { }, error => { console.log('failed to logout'); } );
      localStorage.removeItem('geoalarme-desktop-token');
      localStorage.removeItem('geoalarme-desktop-user');
      localStorage.removeItem('geoalarme-desktop-roles');
    }
    if(!localStorage.getItem('geoalarme-desktop-favorite-language')){
      localStorage.setItem('geoalarme-desktop-favorite-language', DEFAULT_LANGUAGE);
    }
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {

    if(!this.loging){

      this.auth_service.login({
        email: this.logingForm?.value.email,
        password: this.logingForm?.value.password
      }).subscribe( response => {
          if(response['token']){
            localStorage.setItem('geoalarme-desktop-token', JSON.stringify(response['token']));
            localStorage.setItem('geoalarme-desktop-user', JSON.stringify(response['user']));
            // localStorage.setItem('geoalarme-desktop-roles', JSON.stringify(response['rolesBySite']));
            // TMP
            localStorage.setItem('geoalarme-desktop-roles', 'administrator');
            // localStorage.setItem('geoalarme-desktop-roles', 'operator');
            // localStorage.setItem('geoalarme-desktop-roles', 'observer');
            this.router.navigate( [ this.returnUrl ] );
          }else{
            this.logingForm?.reset();
            this.loging = false;
          }
        }, error => {
          this.logingForm?.reset();
          this.loging = false;
        });

      this.loging = true;

    }
  }

  hangleChangeLanguage(e: MatButtonToggleChange) {
    localStorage.setItem('geoalarme-desktop-favorite-language', e.value);
    this.translate.use(e.value);
  }

}
