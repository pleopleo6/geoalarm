import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { WordingService } from "../../app.services";
import { Site } from "../../model/site";
import { TableCateg } from "../../model/table-categ";

@Component({
  selector: 'sites-view',
  templateUrl: './sites-view.component.html',
  styleUrl: './sites-view.component.scss'
})
export class SitesViewComponent {

  sitesTableStructure:TableCateg[] = [
    { id: 'name',       key: 'name',                  sortKey: 'name', labelId: 'site_name' },
    { id: 'city',       key: ['city','district'],     keySeparator:', ', sortKey: ['city','district'], labelId: 'site_location' },
    { id: 'alarm',      key: 'alarm.description',     sortKey: 'alarm.level', labelId: 'site_alarm_state' },
    { id: 'default',    key: 'default.description',   sortKey: 'default.level', labelId: 'site_default_state' },
    { id: 'status',     key: 'status.name',           sortKey: 'status.id', labelId: 'site_system_state' },
  ];

  data: Site[] = [];
  wording: Record<string, string> = {};

  constructor(private readonly route: ActivatedRoute,
              private readonly wording_service: WordingService,){

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
      }
    });

    this.route.data.subscribe( (response: any) => this.data = response.data);

  }

}
