import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { DialogService, WordingService } from "../../app.services";
import { TableCateg } from "../../model/table-categ";
import { UserResumed } from "../../model/user-resumed";
import { UserRoleBySite } from "../../model/user-role-by-site";
import { ConfirmationDialogComponent } from "../../component/confirmation-dialog/confirmation-dialog.component";
import { FirstLetterUppercasePipe } from "../../util/pipe/first-letter-uppercase.pipe";

@Component({
  selector: 'users-view',
  templateUrl: './users-view.component.html',
  styleUrl: './users-view.component.scss'
})
export class UsersViewComponent {

  usersTableStructure:TableCateg[] = [
    { id: 'firstname',  key: 'firstname', sortKey: 'firstname', labelId: 'user_firstname' },
    { id: 'lastname',   key: 'lastname',  sortKey: 'lastname', labelId: 'user_lastname' },
    { id: 'roles_by_site',   key: 'roles_by_site',  sortKey: 'roles_by_site', labelId: 'user_rights' },
    { id: 'actions',  key: '',  sortKey: '', labelId: 'table_actions', actions:[
      { name:'edit-user' , picto: 'edit', confirmAction: false },
      { name:'suppress-user' , picto: 'delete', confirmAction: true },
    ] },
  ];

  data: UserResumed[] = [];
  wording: Record<string, string> = {};

  dialogOpened: boolean | undefined;
  dialogPendingData: UserResumed | undefined;

  constructor(private readonly route: ActivatedRoute,
              private readonly wording_service: WordingService,
              private readonly dialog_service: DialogService,){

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
      }
    });

    this.route.data.subscribe( (response: any) => {
      this.data = response.data.map((item:UserResumed) => ({
        ...item,
        roles_by_site: this.siteRoleReducer(item.roles_by_site),
      }))
    });

      this.dialog_service.state$.subscribe((dialogState:{ state?:string|undefined|null, result?:string|undefined|null }) => {
          if(dialogState.state==='opened'){
              this.dialogOpened = true;
          }else{
              if(dialogState.result==='confirm' && this.dialogPendingData){
                  this.removeUser();
              }
              setTimeout(() => this.dialogOpened = false, 200);
          }
      });

  }

  handleAction(e:any) {
// console.log(e.name)
    switch (e.name) {
      case 'suppress-user':
        this.dialogPendingData = e.data;
        this.openConfirmationDialog();
        break;
    }
  }

  private openConfirmationDialog(): void {
      this.dialog_service.openDialog(
          ConfirmationDialogComponent,
          {
              width: '50%',
              maxWidth: '650px',
              autoFocus: 'dialog',
              closeOnNavigation: true,
              data: {
                  title: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_title']),
                  content: new FirstLetterUppercasePipe().transform(this.wording['confirm_user_suppression']),
                  actions: [
                      { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_infirm_label']), color:'neutral', action:'infirm' },
                      { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_confirm_label']), color:'warn', action:'confirm' },
                  ]
              },
          }
      );
  }

  private removeUser(): void {
    // console.log(this.dialogPendingData)
    // TODO > request suppression
    // ...
  }

  private siteRoleReducer(rolesBySite:UserRoleBySite[]): string{
    let result = '';
    rolesBySite.forEach((item:UserRoleBySite, index:number) => result += item.site_name + ' : ' + item.role_name + (index<(rolesBySite.length-1)? '\r\n':''))
    return result;
  }

}
