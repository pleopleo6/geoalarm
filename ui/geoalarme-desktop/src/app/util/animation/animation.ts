import { animate, AnimationTriggerMetadata, style, transition, trigger } from "@angular/animations";

export function FadeIn(timingIn: number, offsetTop: number = 0): AnimationTriggerMetadata  {
  return trigger('fadeIn', [
    transition(':enter', [
      style(offsetTop ? { opacity: 0 , marginTop: offsetTop+'px', } : { opacity: 0, }),
      animate(timingIn, style(offsetTop ? { opacity: 1, marginTop: 0 } : { opacity: 1, })),
    ]),
  ]);
}
export function FadeInOut(timingIn: number, timingOut: number, offsetTop: number = 0): AnimationTriggerMetadata  {
  return trigger('fadeInOut', [
    transition(':enter', [
      style(offsetTop ? { opacity: 0, height: '100%', transform: `translateY(${offsetTop}px` } : { opacity: 0, }),
      animate(timingIn, style(offsetTop ? { opacity: 1, height: '100%', transform: 'translateY(0)' } : { opacity: 1, })),
    ]),
    transition(':leave', [
      animate( timingOut, style(offsetTop ? { opacity: 0, height: '100%', transform: `translateY(${-offsetTop}px`, } : { opacity: 0, })),
    ])
  ]);
}

