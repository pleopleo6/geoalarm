export interface UserRoleBySite {
  uid?:       string;
  site_id:    number;
  site_name?: string;
  role_id:    number;
  role_name?: string;
}
