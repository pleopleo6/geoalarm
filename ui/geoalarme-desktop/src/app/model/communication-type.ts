/**
 *  CommunicationType interface
 */
export interface CommunicationType {
  id:         number,
  name:       string,
  label_name: string,
}
