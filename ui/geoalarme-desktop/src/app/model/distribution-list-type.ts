/**
 *  DistributionListType interface
 */
export interface DistributionListType {
  id:         number,
  level:      number,
  label_name: string,
  type_id:    number,
}
