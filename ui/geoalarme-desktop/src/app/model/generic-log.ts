import { LogType } from "./log-type";
import { Station } from "./station";

export interface GenericLog {
  id: number,
  log_station?: Station,
  log_type: LogType,
  log_data: string,
  log_timestamp: Date,
  actor: string,
}
