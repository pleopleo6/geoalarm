export interface SignalisationState {
  id:           number,
  description:  string,
  level:        string,
}
