import {TableAction} from "./table-action";

/**
 * TableCateg interface
 */
export interface TableCateg {
    id: string;
    key: string|string[];
    keySeparator?: string;
    sortKey: string|string[];
    labelId: string;
    actions?:TableAction[]
}
