/**
 * TableSortItem interface
 */
export interface TableSortItem {
    id: string;
    sortKey:string | string[];
}
