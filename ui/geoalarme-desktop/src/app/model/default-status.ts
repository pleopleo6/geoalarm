export interface DefaultStatus {
  id: number,
  description: string,
  level: string
}
