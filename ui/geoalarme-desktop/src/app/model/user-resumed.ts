import {UserRoleBySite} from "./user-role-by-site";

export interface UserResumed {
  id: number;
  firstname: string;
  lastname: string;
  roles_by_site: UserRoleBySite[];
}

