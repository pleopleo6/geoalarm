import { Position } from "./position";

export interface OtherDevice {
  id:           number,
  name:         string,
  device_id:    string
  latitude:     number,
  longitude:    number,
  altitude:     number,
  position:     Position,
  created_at:   Date,
  updated_at:   Date,
}
