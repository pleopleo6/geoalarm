export interface Alarm {
  id:           number,
  description:  string,
  level:        string,
}
