/**
 *  DistributionListItem interface
 */
export interface DistributionListItem {
  id?:          number,
  uid?:         string,
  userId?:      number,
  eventIds:     number[],
  messageIds:   number[],
}
