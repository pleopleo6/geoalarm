/**
 *  DialogAction interface
 */
export interface DialogAction {
  label:string,
  color:string,
  action:string,
}
