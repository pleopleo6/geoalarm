export enum Languages {
  fr = 'french',
  en = 'english',
  de = 'german',
  it = 'italian'
}
