import {UserRoleBySite} from "./user-role-by-site";

export interface User {
  id?: number;
  firstname: string;
  lastname: string;
  email: string;
  mobile_phone_number: string;
  phone_number: string;
  id_threema: string;
  active?: boolean;
  roles_by_site: UserRoleBySite[];
}

