export interface SiteStatus {
  id: number,
  name: string,
  description: string,
}
