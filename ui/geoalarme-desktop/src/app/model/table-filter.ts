/**
 * TableFilter interface
 */
export interface TableFilter {
    id: string;
    direction: string;
    labelId: string;
    sortKey: string | string[];
}
