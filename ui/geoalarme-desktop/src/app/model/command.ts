import {CommandValidationLevel} from "./command-validation-level";

export interface Command {
  id:                 number;
  inner_id:           string;
  name:               string;
  label_id:           string;
  description_id:     string;
  command:            string;
  params?:            any;
  priority:           number;
  validationLevelId:  CommandValidationLevel;
}
