import { CommunicationType } from "./communication-type";
import { DistributionListItem } from "./distribution-list-item";
import { DistributionListType } from "./distribution-list-type";
import { User } from "./user";

/**
 *  DistributionList interface
 */
export interface DistributionList {
  users:                User[],
  communication_types:  CommunicationType[],
  lists_by_type:        Record<string, DistributionListType[]>,
  data:                 DistributionListItem[],
}
