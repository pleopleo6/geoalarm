import Polygon from "polygon";
import { Alarm } from "./alarm";
import { Command } from "./command";
import { Default } from "./default";
import { Station } from "./station";

export interface Installation {
  id: number,
  name: string,
  area: Polygon,
  site_id: string,
  city: string,
  district: string,
  country: string,
  latitude: number,
  longitude: number,
  altitude: number,
  commands: Command[],
  alarm: Alarm,
  default: Default,
  stations: Station[]
  created_at: Date,
  updated_at: Date,
  last_connexion: Date,
}
