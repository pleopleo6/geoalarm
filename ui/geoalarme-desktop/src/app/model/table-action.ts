/**
 *  TableAction interface
 */
export interface TableAction {
  name: string;
  picto: string;
  confirmAction: boolean;
}
