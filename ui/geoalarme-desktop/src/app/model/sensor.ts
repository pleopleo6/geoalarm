import { SensorType } from "./sensor-type";

export interface Sensor {
  id:             number,
  type:           SensorType,
  sensor_id:      string,
  app_eui:        string,
  name:           string,
  latitude:       number,
  longitude:      number,
  altitude:       number,
  created_at:     Date,
  updated_at:     Date,
}
