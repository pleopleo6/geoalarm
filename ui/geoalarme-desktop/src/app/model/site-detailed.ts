import { AlarmStatus } from './alarm-status';
import { DefaultStatus } from './default-status';
import { SiteStatus } from "./site-status";
import { DistributionList } from "./distribution-list";
import { Site } from "./site";
import { Command } from "./command";
import { Station } from "./station";

export interface SiteDetailed {
  alarm: AlarmStatus,
  default: DefaultStatus,
  commands: Command[];
  distribution_list?: DistributionList;
  siteInfos: Site;
  siteStatus: SiteStatus,
  siteStations: Station[];
}
