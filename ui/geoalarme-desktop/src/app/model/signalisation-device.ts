import { Position } from "./position";
import {Alarm} from "./alarm";
import {Default} from "./default";
import {SignalisationState} from "./signalisation-state";

export interface SignalisationDevice {
  id:           number,
  name:         string,
  device_id:    string,
  batteryLevel?:  string,
  comLevel?:      string,
  comFlag?:      string,
  alarm:        Alarm,
  default:      Default,
  sign_state:   SignalisationState,
  latitude:     number,
  longitude:    number,
  altitude:     number,
  position:     Position,
  created_at:   Date,
  updated_at:   Date,
}
