import {DialogAction} from "./dialog-action";

/**
 *  DialogData interface
 */
export interface DialogData {
  title: string;
  content: string;
  actions: DialogAction[];
  data: any;
}
