import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Dialog, DialogConfig, DialogRef } from "@angular/cdk/dialog";
import { ComponentType } from "@angular/cdk/overlay";
import { BehaviorSubject, Observable } from 'rxjs';
import { API_BASE, DEFAULT_LANGUAGE } from "./app.config";
import { Wording } from './model/wording';

@Injectable({
  providedIn: 'root'
})
export class HTTPService {

  constructor(private http: HttpClient) { }

  request(api_url: string, query_obj?:Object, header_obj?:Object):Observable<Record<string, any>> {
    return this.http.request( api_url + this.getQueryString(query_obj), (header_obj? this.getHeader(header_obj) : null) );
  }

  get(api_url: string, query_obj?:Object, header_obj?:Object):Observable<Record<string, any>> {
    return this.http.get( api_url + this.getQueryString(query_obj) );
  }

  post(api_url: string, query_obj?:Object, header_obj?:Object, data?:Object):Observable<Record<string, any>> {
    return this.http.post( api_url + this.getQueryString(query_obj), data, (header_obj? this.getHeader(header_obj) : null) );
  }

  put(api_url: string, query_obj?:Object, header_obj?:Object, data?:Object):Observable<Record<string, any>> {
    return this.http.put( api_url + this.getQueryString(query_obj), data, (header_obj? this.getHeader(header_obj) : null) );
  }

  patch(api_url: string, query_obj?:Object, header_obj?:Object, data?:Object):Observable<Record<string, any>> {
    return this.http.patch( api_url + this.getQueryString(query_obj), data, (header_obj? this.getHeader(header_obj) : null) );
  }

  delete(api_url: string, query_obj?:Object, header_obj?:Object):Observable<Record<string, any>> {
    return this.http.delete( api_url + this.getQueryString(query_obj), (header_obj? this.getHeader(header_obj) : null) );
  }

  private getQueryString(query_obj:any):string{
    let k, p: string = '';
    for (k in query_obj){ p+= ('&' + k + '=' + query_obj[k]); };
    return ((p!=='')? ('?' + p) : '');
  }
  private getHeader(header_obj:any): any {
    return { headers: new HttpHeaders(header_obj) };
  }

}

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  loading$: BehaviorSubject<boolean>;
  private currentState: boolean;
  constructor(){
    this.currentState = false;
    this.loading$ = new BehaviorSubject<boolean>(this.currentState);
  }
  toggleLoading() {
    this.currentState = true;
    this.loading$.next(this.currentState);
  }
  toggleLoaded() {
    this.currentState = false;
    this.loading$.next(this.currentState);
  }
}

@Injectable({
  providedIn: 'root'
})
export class WordingService {
  private wording: Record<string, string> = {};
  map$: BehaviorSubject<Record<string, string>>;
  constructor(private readonly http: HttpClient){
    this.map$ = new BehaviorSubject<Record<string, string>>({});
  }
  load(): Observable<Object> {
    const favoriteLanguage = localStorage.getItem('geoalarme-desktop-favorite-language');
    return this.http.get(`${API_BASE}/api/v1/wording/desktop/${favoriteLanguage?? DEFAULT_LANGUAGE}`)
  }
  register(wording: Wording[]) {
    wording.forEach((item: Wording) => this.wording[item.name] = item.value )
    this.map$.next(this.wording);
  }
  reset() {
    Object.keys(this.wording).forEach(key => delete this.wording[key]);
  }
  getWording(): Record<string, string>{
    return this.wording;
  }
}

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  state$: BehaviorSubject<{ state?:string|undefined|null, result?:string|undefined|null }>;
  private dialogRef: DialogRef<string> | undefined | null;
  constructor(private readonly dialog: Dialog,){
    this.state$ = new BehaviorSubject<{ state?:string|undefined|null, result?:string|undefined|null }>({});
  }
  openDialog(component: ComponentType<unknown>, config?: DialogConfig<unknown, DialogRef<string, unknown>>) {
    this.dialogRef = this.dialog.open<string>(component, config);
    this.dialogRef?.closed.subscribe((result:string | undefined) => this.handleDialogClosed(result));
    this.state$.next({ state:'opened' });
  }
  private handleDialogClosed(result:string | undefined): void {
    this.state$.next({ state:'closed', result:result });
    this.dialogRef = null;
  }
}

