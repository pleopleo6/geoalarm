import { Injectable } from '@angular/core';

import { API_URL } from '../app.config';
import { HTTPService } from "../app.services";
import { UserCredentials } from "../model/user-credentials";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    constructor(private http_service:HTTPService) { }

    login(credentials:UserCredentials) {
      return this.http_service.post(`${API_URL}login`,{},{'Content-Type':'application/json'}, credentials);
    }

    logout() {
      return this.http_service.post(`${API_URL}logout`,{},{'Content-Type':'application/json'});
    }

    isLogged() {
      const desktopToken = localStorage.getItem('geoalarme-desktop-token');
      const storedData:any = !!desktopToken? JSON.parse(desktopToken) : null;
      // const tokenExpiration = this.jwt_helper.getTokenExpirationDate(storedData?.token);
      // const tokenExpired = tokenExpiration && (tokenExpiration.getTime() < new Date().getTime());
      // return storedData && storedData.token && storedData.token.split('.').length===3? !tokenExpired : false;
      return !!(storedData && storedData.token)
    }

}
