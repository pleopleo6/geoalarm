import { Component } from '@angular/core';
import { NavigationEnd, Router } from "@angular/router";
import { AuthService } from "./auth/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {

  userLogged:       boolean = false;

  constructor(private readonly router: Router,
              private readonly auth_service: AuthService){
    this.router.events.subscribe( e => {
      if(e instanceof NavigationEnd) {
        this.userLogged = this.auth_service.isLogged();
      }
    });
  }
}
