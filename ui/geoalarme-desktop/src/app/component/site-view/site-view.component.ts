import { Component, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { Dialog, DialogRef } from "@angular/cdk/dialog";
import { Subscription } from "rxjs";
import { FadeInOut } from "../../util/animation/animation";
import { WordingService } from "../../app.services";
import { ConfirmationDialogComponent } from "../confirmation-dialog/confirmation-dialog.component";
import { SiteDetailed } from "../../model/site-detailed";
import { Device } from "../../model/device";
import { Station } from "../../model/station";
import { TableCateg } from "../../model/table-categ";
import { UserRoleBySite } from "../../model/user-role-by-site";
import { FirstLetterUppercasePipe } from "../../util/pipe/first-letter-uppercase.pipe";

@Component({
  selector: 'site-view',
  templateUrl: './site-view.component.html',
  styleUrl: './site-view.component.scss',
  animations: [FadeInOut(200,200, 0 )]
})
export class SiteViewComponent implements OnDestroy {

  deviceTableStructure:TableCateg[] = [
    { id: 'name',           key: 'name',                  sortKey: 'name',            labelId: 'device_name' },
    { id: 'type',           key: 'type.name',             sortKey: 'type.name',       labelId: 'device_type' },
    { id: 'alarm',          key: 'alarm.description',     sortKey: 'alarm.level',     labelId: 'alarm_level' },
    { id: 'default',        key: 'default.description',   sortKey: 'default.level',   labelId: 'default_level' },
    { id: 'battery_level',  key: 'batteryLevel',          sortKey: 'batteryLevel',    labelId: 'battery_level' },
    { id: 'com_level',      key: 'comLevel',              sortKey: 'comLevel',        labelId: 'com_level' },
    { id: 'com_flag',       key: 'comFlag',               sortKey: 'comFlag',         labelId: 'com_flag' },
  ];

  data: SiteDetailed | undefined;
  all_devices: Device[] = [];
  wording: Record<string, string> = {};

  contentOffset = 0;

  dialogRef: DialogRef<string> | undefined | null;
  dialogContext: string | undefined;
  dialogPendingData: UserRoleBySite | any | undefined;

  routerSbr$: Subscription | undefined;

  @HostListener('window:keyup', ['$event']) onKeyDown = (e: KeyboardEvent) => {
    if(e.keyCode===27) {
      this.handleAction({ name:'close-overlay' });
    }
  };

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly dialog: Dialog,
              private readonly wording_service: WordingService){

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) this.wording = wording;
    });

    this.route.data.subscribe( (response: any) => {

      this.data = response.data;

      const noDataLabel = this.wording['no_data'].charAt(0).toUpperCase() + this.wording['no_data'].slice(1);

      response.data.siteStations.forEach((station: Station) => {
        this.all_devices = [
          ...this.all_devices,
          ...station.devices!.map((item:Device) => ({
            ...item,
            batteryLevel: item.batteryLevel?.length? item.batteryLevel : noDataLabel,
            comLevel: item.comLevel?.length? item.comLevel : noDataLabel,
            comFlag: item.comFlag?.length? item.comFlag : noDataLabel,
          }))
        ]
      });

      this.all_devices.sort((a:Device,b:Device) => ((a.name > b.name)? 1 : (a.name < b.name ? -1 : 0)));

    });

    const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
    this.contentOffset = urlParams.length - 2;

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {
        const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
        this.contentOffset = urlParams.length - 2;
      }
    });

  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
  }

  handleAction(e:any){
    switch(e.name){
      case 'suppress-user-from-distribution-list':
        this.dialogPendingData = e.data;
        this.openConfirmationDialog(e.name);
        break;
      case 'close-overlay':
        this.closeOverlay();
        break;
    }
  }

  private openConfirmationDialog(context:string): void {

    const title = new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_title']);
    let content:string = '';
    const actions = [
      { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_confirm_label']), color:'warn', action:'confirm' },
      { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_infirm_label']), color:'neutral', action:'infirm' },
    ]

    switch(context){
      case 'suppress-user-role':
        content = new FirstLetterUppercasePipe().transform(this.wording['confirm_user_role_suppression']);
        break;
      case 'suppress-user-from-distribution-list':
        content = new FirstLetterUppercasePipe().transform(this.wording['confirm_suppress_user_from_distribution_list']);
        break;
    }

    this.dialogContext = context;
    this.dialogRef = this.dialog.open<string>(ConfirmationDialogComponent, {
      width: '50%',
      maxWidth: '650px',
      autoFocus: 'dialog',
      closeOnNavigation: true,
      data: { title, content, actions },
    });

    this.dialogRef?.closed.subscribe((result:string | undefined) => {

      if(result==='confirm'){
        switch(this.dialogContext){
          case 'suppress-user-from-distribution-list':
            // TODO
            break;
        }
      }

      // timeout requis / close dialog on esc
      setTimeout(() => this.dialogRef = null, 200)

    });

  }

  private closeOverlay(): void {
    const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
    this.router.navigate([urlParams[0]]);
  }
}
