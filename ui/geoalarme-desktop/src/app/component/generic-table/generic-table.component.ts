import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { WordingService } from "../../app.services";
import { TableAction } from "../../model/table-action";
import { TableSortItem } from "../../model/table-sort-item";
import { TableCateg } from '../../model/table-categ';
import { TableFilter } from "../../model/table-filter";
import { FirstLetterUppercasePipe } from "../../util/pipe/first-letter-uppercase.pipe";

@Component({
  selector: 'generic-table',
  templateUrl: './generic-table.component.html',
  styleUrl: './generic-table.component.scss'
})
export class GenericTableComponent implements OnInit, OnChanges {

  @Input() structure: TableCateg[] = [];
  @Input() multiline: boolean = false;
  @Input() paginated: boolean = true;
  @Input() sortable: boolean = true;
  @Input() linked: boolean = true;
  @Input() withBottomLine: boolean = false;
  @Input() items: any[] | undefined;
  @Output() action: EventEmitter<{ name:string, data?:any }> = new EventEmitter<{ name:string, data?:any }>();
  @ViewChild(MatPaginator) paginator: MatPaginator | null = null;

  wording: Record<string, string> = {};

  dataSource: MatTableDataSource<any> | undefined;
  tableStructure: TableCateg[]  = [];
  tableColumns: string[] | undefined;

  sortCollection: TableFilter[] = [];
  orderedItems: any[]|undefined;

  constructor(private readonly router: Router,
              private readonly wording_service: WordingService,) {
    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) this.wording = wording;
    });
  }

  ngOnInit(): void {
    this.tableStructure = this.structure;
    this.tableColumns = this.tableStructure.map((item: TableCateg) => item.id);
  }

  ngOnChanges(): void{
    if(this.items){
      this.orderedItems = [...this.items];
      this.dataSource = new MatTableDataSource(this.orderedItems);
      setTimeout(() => (this.dataSource as MatTableDataSource<any>).paginator = this.paginator);
    }
  }

  resolveCellContent(key: string|string[], data: any, separator='.', keySeparator=''): string {
    let result = '', value:string;
    if(Array.isArray(key)){
      key.forEach((item:string, index:number) => {
        value = item.split(separator).reduce((prev: any, curr: string) => prev?.[curr], data);
        result += ((index > 0) ? keySeparator + value : value);
      });
    }else{
      result = key.split(separator).reduce((prev: any, curr: string) => prev?.[curr], data);
    }
    return new FirstLetterUppercasePipe().transform(result);
  }

  resolveCellClass(id: string, data: any): string {
    return id==='status'? `status-picto site-status-${data[id].id}` : ((id==='default'||id==='alarm')? `status-picto site-${id}-${data[id].level}` : '');
  }

  getSortDirection(labelId: string): string{
    return this.sortCollection.find((item: TableFilter) => item.labelId===labelId)?.direction || '';
  }

  handleSort(itemId: string): void{

    const filter = this.sortCollection.find((item: any) => item.id === itemId);

    if(!filter){
      const tableItem = this.tableStructure?.find((item: TableCateg) => item.id===itemId)!;
      this.sortCollection.push({
        id: itemId,
        direction: 'asc',
        sortKey: tableItem.sortKey,
        labelId: tableItem.labelId!
      });
    }else{
      if(filter.direction === 'asc'){
        filter.direction = 'desc';
      }else{
        this.sortCollection.splice(this.sortCollection.indexOf(filter), 1);
      }
    }

    this.updateOrder();

  }
  handleToggleFilter(filter: TableFilter): void{
    filter.direction = filter.direction==='asc'? 'desc' : 'asc';
    this.updateOrder();
  }
  handleRemoveFilter(filter: TableFilter): void{
    this.sortCollection.splice(this.sortCollection.indexOf(filter), 1);
  }
  handleDropFilter(e: CdkDragDrop<string[]>) {
    moveItemInArray(this.sortCollection, e.previousIndex, e.currentIndex);
    this.updateOrder();
  }
  handleSelection(data: any): void{
    this.router.navigate([`${this.router.url}`, data.id]);
  }
  handleExport(): void{
    const tsvContent = this.jsonToTSV();
    const blob = new Blob([tsvContent], { type: 'text/csv;charset=utf-8,' });
    const objUrl = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.setAttribute('href', objUrl);
    link.setAttribute('download', `Geoalarme-${new Date().toISOString().slice(0, 10)}.tsv`);
    link.style.display = 'none';
    document.querySelector('body')?.append(link);
    link.click();
    link.remove();
  }

  handleAction(e:MouseEvent, data:any, action:TableAction) {
    switch (action.name) {
      case 'suppress-user':
        e.stopPropagation();
        break;
    }
    this.action.emit({ name: action.name, data: data })
  }

  protected updateOrder() {

    const orderMap: TableSortItem[] = [];
    let dir: string;

    this.sortCollection.forEach((filterItem: TableFilter) => {
      dir = filterItem.direction==='desc'? '-':'';
      orderMap.push(
        {
          id: dir + this.tableStructure?.find((item: TableCateg) => item.labelId === filterItem.labelId)?.key,
          sortKey: filterItem.sortKey
        });
    });

    this.orderedItems = this.orderedItems?.sort(this.customSorter(orderMap));
    this.dataSource = new MatTableDataSource(this.orderedItems);
    (this.dataSource as MatTableDataSource<any>).paginator = this.paginator;

  }

  protected customSorter(fields: TableSortItem[]){
    return (a: any, b: any) => fields.map((o:{ id:string, sortKey:string|string[] }) => {
      let dir = 1;
      if (o.id[0] === '-') dir = -1;
      const aValue = this.resolveCellContent(o.sortKey,a);
      const bValue = this.resolveCellContent(o.sortKey,b);
      return aValue > bValue? (dir) : aValue < bValue? -(dir) : 0;
    }).reduce((p, n) => p ? p : n, 0);
  }

  protected jsonToTSV(): string{
    const keys = this.structure.map((item: TableCateg) => item.key);
    const keySeparators = this.structure.map((item: TableCateg) => item.keySeparator);
    const labels = this.structure.map((item: TableCateg) => item.labelId);
    const flatList = [ labels ];
    let result = '';
    this.orderedItems?.forEach((item: any) => {
      const values: string[] = [];
      keys.forEach((k: string|string[], index:number) => values.push(this.resolveCellContent(k, item,'.', keySeparators[index])));
      flatList.push(values);
    });
    flatList.forEach((item: string[], index: number) => result += (item.join('\t')) + (index===flatList.length-1? '':'\n'));
    return result;
  }

}
