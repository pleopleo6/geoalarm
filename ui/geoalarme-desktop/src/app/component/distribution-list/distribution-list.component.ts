import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { API_URL } from "../../app.config";
import { DialogService, HTTPService } from "../../app.services";
import { DistributionList } from "../../model/distribution-list";
import { User } from "../../model/user";
import { ConfirmationDialogComponent } from "../confirmation-dialog/confirmation-dialog.component";
import { FirstLetterUppercasePipe } from "../../util/pipe/first-letter-uppercase.pipe";
import { DistributionListItem } from "../../model/distribution-list-item";
import { NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
  selector: 'distribution-list',
  templateUrl: './distribution-list.component.html',
  styleUrl: './distribution-list.component.scss'
})
export class DistributionListComponent implements OnDestroy, OnInit{

  @Input() data: DistributionList | undefined;
  @Input() users: User[] | undefined = [];
  @Input() wording: Record<string, string> = {};
  @Output() action: EventEmitter<any> = new EventEmitter<any>();

  siteId: string | undefined;
  dialogOpened: boolean | undefined;
  dialogPendingData: DistributionListItem | undefined | null;

  routerSbr$: Subscription | undefined;

  constructor(private readonly router: Router,
              private readonly http_service: HTTPService,
              private readonly dialog_service: DialogService) {

    const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
    this.siteId = urlParams[1];

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {
        const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
        this.siteId = urlParams[1];
      }
    });

    this.dialog_service.state$.subscribe((dialogState:{ state?:string|undefined|null, result?:string|undefined|null }) => {
      if(dialogState.state==='opened'){
        this.dialogOpened = true;
      }else{
        if(dialogState.result==='confirm' && this.dialogPendingData){
          this.suppressDistributionItem();
        }
        setTimeout(() => this.dialogOpened = false, 200);
      }
    });

  }

  ngOnInit() {
    let timestamp = Date.now();
    this.data?.data.forEach((item:DistributionListItem) => item.uid = (timestamp++).toString(16));
  }

  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
  }

  handleAction(e:any){
    switch(e.name){
      case 'add-distribution-item':
        this.createDistributionItem();
        break;
      case 'update-distribution-item':
        this.updateDistributionItem(e.data);
        break;
      case 'suppress-distribution-item':
        this.dialogPendingData = e.data;
        this.openConfirmationDialog();
        break;
      case 'register-distribution-item':
        this.registerDistributionItem(e.data);
        break;
    }
  }

  private openConfirmationDialog(): void {
    this.dialog_service.openDialog(
      ConfirmationDialogComponent,
      {
        width: '50%',
        maxWidth: '650px',
        autoFocus: 'dialog',
        closeOnNavigation: true,
        data: {
          title: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_title']),
          content: new FirstLetterUppercasePipe().transform(this.wording['confirm_distribution_list_item_suppression']),
          actions: [
            { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_infirm_label']), color:'neutral', action:'infirm' },
            { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_confirm_label']), color:'warn', action:'confirm' },
          ]
        },
      }
    );
  }

  private createDistributionItem(): void {
    this.data?.data.push({
      eventIds: [],
      messageIds: []
    });
  }

  private registerDistributionItem(data:DistributionListItem): void {
    const desktopToken = localStorage.getItem('geoalarme-desktop-token');
    if(!desktopToken) this.router.navigate( ['/login'] );
    this.http_service.post(
      `${API_URL}site/${this.siteId}/distribution`,
      {},
      { Authorization: 'Bearer ' + JSON.parse(desktopToken!).token },
      { ...data, siteId:this.siteId })
    .subscribe(
      (response:any|undefined) => {
        this.data!.data = response.data;
        let timestamp = Date.now();
        this.data?.data.forEach((item:DistributionListItem) => item.uid = (timestamp++).toString(16));
      }, (error) => console.log(error))
  }
  private updateDistributionItem(data:DistributionListItem): void {
    const desktopToken = localStorage.getItem('geoalarme-desktop-token');
    if(!desktopToken) this.router.navigate( ['/login'] );
    this.http_service.put(
      `${API_URL}site/${this.siteId}/distribution/${data.id}`,
      {},
      { Authorization: 'Bearer ' + JSON.parse(desktopToken!).token },
      data)
    .subscribe(
      (response:any|undefined) => {
        this.data!.data = response.data;
        let timestamp = Date.now();
        this.data?.data.forEach((item:DistributionListItem) => item.uid = (timestamp++).toString(16));
      }, (error) => console.log(error))
  }
  private suppressDistributionItem(): void {
    if(this.dialogPendingData){
      const desktopToken = localStorage.getItem('geoalarme-desktop-token');
      if(!desktopToken) this.router.navigate( ['/login'] );
      this.http_service.delete(
        `${API_URL}site/${this.siteId}/distribution/${this.dialogPendingData.id}`,
        {},
        { Authorization: 'Bearer ' + JSON.parse(desktopToken!).token })
      .subscribe(
        (response:any|undefined) => {
          this.data!.data = response.data;
          let timestamp = Date.now();
          this.data?.data.forEach((item:DistributionListItem) => item.uid = (timestamp++).toString(16));
        }, (error) => console.log(error))
    }
  }

}
