import { Component } from '@angular/core';
import { WordingService } from "../../app.services";

@Component({
  selector: 'footer-view',
  templateUrl: './footer-view.component.html',
  styleUrl: './footer-view.component.scss'
})
export class FooterViewComponent {

  wording: Record<string, string> = {};

  constructor(private readonly wording_service: WordingService) {
    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
      }
    });
  }

}
