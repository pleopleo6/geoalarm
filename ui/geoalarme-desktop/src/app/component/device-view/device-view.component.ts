import { Component, ElementRef, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { WordingService } from "../../app.services";
import { Device } from "../../model/device";
import { Station } from "../../model/station";
import { TableCateg } from "../../model/table-categ";
import { FadeInOut } from "../../util/animation/animation";

@Component({
  selector: 'device-view',
  templateUrl: './device-view.component.html',
  styleUrl: './device-view.component.scss',
  animations: [FadeInOut(200,200, 0 )]
})
export class DeviceViewComponent implements OnInit, OnDestroy {

  implantationTableStructure:TableCateg[] = [
    { id: 'site_name',    key: 'site_name',      sortKey: 'site_name',    labelId: 'site_name' },
    { id: 'station_name', key: 'station_name',   sortKey: 'station_name', labelId: 'station_name' },
  ];

  deviceDetailsTableStructure:TableCateg[] = [
    { id: 'parameter_name',    key: 'parameter_name',    sortKey: 'parameter_name',  labelId: 'parameter_name' },
    { id: 'parameter_value',   key: 'parameter_value',   sortKey: 'parameter_value', labelId: 'parameter_value' },
  ];

  data: Device | undefined;
  implantationData: { site_name:string, station_name:string }[] = [];
  deviceDetailsData: { parameter_name:string, parameter_value:string }[] = [];
  wording: Record<string, string> = {};

  contentOffset = 0;

  routerSbr$: Subscription | undefined;

  @HostListener('window:keyup', ['$event']) onKeyDown = (e: KeyboardEvent) => {
    if(e.keyCode===27 && this.contentOffset===0) {
      this.handleAction({ name:'close-overlay' });
    }
  };

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly wording_service: WordingService,
              private el:ElementRef){

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) this.wording = wording;
    });

    this.route.data.subscribe( (response: any) => {
      this.data = response.data;
      response.data.sites.forEach((site:any) => {
        site.stations.forEach((station:Station) => {
          this.implantationData.push( { site_name:site.name, station_name: station.name } );
        })
      });
      response.data.data.forEach((data:any) => {
         this.deviceDetailsData.push( {
           parameter_name: data.dataType.name,
           parameter_value: data.data + data.dataType.unity,
         })
      });
    });

    const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
    this.contentOffset = urlParams.length - 2;

    this.routerSbr$ = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {
        const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
        this.contentOffset = urlParams.length - 2;
      }
    });

  }
  ngOnInit() {
    this.el.nativeElement.classList.add(`offset-${this.contentOffset}`);
  }
  ngOnDestroy(){
    this.routerSbr$?.unsubscribe();
  }

  handleAction(e:any){
    const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
    switch(e.name){
      case 'back':
        urlParams.pop()
        this.navigateTo(urlParams.join('/'));
        break;
      case 'close-overlay':
        this.navigateTo(urlParams[0]);
        break;
    }
  }

  private navigateTo(url:string): void {
    this.router.navigate([url]);
  }

}
