import { Component, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { FadeInOut } from "../../util/animation/animation";
import { TableCateg } from "../../model/table-categ";
import { DialogService, WordingService } from "../../app.services";
import { ConfirmationDialogComponent } from "../confirmation-dialog/confirmation-dialog.component";
import { Role } from "../../model/role";
import { Site } from "../../model/site";
import { User } from "../../model/user";
import { FirstLetterUppercasePipe } from "../../util/pipe/first-letter-uppercase.pipe";
import { UserRoleBySite } from "../../model/user-role-by-site";

@Component({
  selector: 'user-view',
  templateUrl: './user-view.component.html',
  styleUrl: './user-view.component.scss',
  animations: [FadeInOut(200,200, 0 )]
})
export class UserViewComponent {

  data: User | undefined;
  sites: Site[] = [];
  users: User[] = [];
  roles: Role[] = [];
  wording: Record<string, string> = {};

  userInfosFormGroup: FormGroup | undefined;
  userPasswordFormGroup: FormGroup | undefined;
  userTypeFormGroup: FormGroup | undefined;
  userRoleFormGroup: FormGroup | undefined | null;

  dataChanged = false;
  dataValidated = false;

  dialogOpened: boolean | undefined;
  dialogPendingData: UserRoleBySite | undefined | null;

  userRightsTableStructure:TableCateg[] = [
    { id: 'site',  key: 'site_name',  sortKey: 'site_name', labelId: 'site_name' },
    { id: 'role',  key: 'role_name',  sortKey: 'role_name', labelId: 'user_role' },
    { id: 'actions',  key: '',  sortKey: '', labelId: 'table_actions', actions:[
      { name:'suppress-user-role' , picto: 'delete', confirmAction: true }
    ] },
  ];

  @HostListener('window:keyup', ['$event']) onKeyDown = (e: KeyboardEvent) => {
    if(e.keyCode===27) {
      this.handleAction({ name:'close-overlay' });
    }
  };

  constructor(private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly formBuilder: FormBuilder,
              private readonly dialog_service: DialogService,
              private readonly wording_service: WordingService){

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) this.wording = wording;
    });

    this.route.data.subscribe( (response: any) => {

      let timestamp = Date.now();
      const roles_by_site:UserRoleBySite[]= [];

      this.data = response.data.user;
      this.data!.roles_by_site.forEach((item:UserRoleBySite) => {
        roles_by_site.push({...item, uid: timestamp.toString(16)});
        timestamp++;
      });
      this.data!.roles_by_site = roles_by_site;
      this.sites = response.data.sites;
      this.users = response.data.users;
      this.roles = response.data.roles;

      this.userInfosFormGroup = this.formBuilder.group({
        firstname: new FormControl(this.data?.firstname??'', [Validators.required]),
        lastname: new FormControl(this.data?.lastname??'', [Validators.required]),
        email: new FormControl(this.data?.email??'', [Validators.required]),
        phone_number: new FormControl(this.data?.phone_number??'', [Validators.required]),
        mobile_phone_number: new FormControl(this.data?.mobile_phone_number??'', [Validators.required]),
        id_threema: new FormControl(this.data?.id_threema??'', [Validators.required]),
      });

      this.userPasswordFormGroup = this.formBuilder.group({
        password: new FormControl('', [Validators.required]),
        passwordConfirm: new FormControl('', [Validators.required]),
      });

      // TODO > association rols / site
      this.userTypeFormGroup = this.formBuilder.group({
        userType: new FormControl('', [Validators.required]),
      });
    });

    this.dialog_service.state$.subscribe((dialogState:{ state?:string|undefined|null, result?:string|undefined|null }) => {
        if(dialogState.state==='opened'){
            this.dialogOpened = true;
        }else{
          if(dialogState.result==='confirm' && this.dialogPendingData){
            this.removeUserRole();
          }
          setTimeout(() => this.dialogOpened = false, 200);
        }
    });

  }

  handleUserInfosChange() {
    console.log(this.userInfosFormGroup?.value)
    // ...
  }

  handleAction(e:any){
    switch(e.name){
      case 'create-user-role':
        this.createUserRole();
        break;
      case 'validate-user-role':
        this.addUserRole();
        break;
      case 'suppress-user-role':
        this.dialogPendingData = e.data;
        this.openConfirmationDialog();
        break;
      case 'close-overlay':
        if(!this.dialogOpened) this.closeOverlay();
        break;
    }
  }

  private createUserRole(): void {
    this.userRoleFormGroup = this.formBuilder.group({
      site_id: new FormControl('', [Validators.required]),
      role_id: new FormControl('', [Validators.required]),
    });
  }
  private addUserRole(): void {

    const site_id = this.userRoleFormGroup?.get('site_id')?.value;
    const site_name = this.sites.find((item:Site) => item.id === site_id)?.name;
    const role_id = this.userRoleFormGroup?.get('role_id')?.value;
    const role_name = this.roles.find((item:Role) => item.id === role_id)?.name;

    const roles_by_site = this.data?.roles_by_site?? [];
    roles_by_site?.push( { site_id, site_name, role_id, role_name });

    if(this.data){
      this.data.roles_by_site = [...roles_by_site];
    }else{
      this.data = {
        firstname: this.userInfosFormGroup?.get('firstname')?.value,
        lastname: this.userInfosFormGroup?.get('lastname')?.value,
        email: this.userInfosFormGroup?.get('email')?.value,
        mobile_phone_number: this.userInfosFormGroup?.get('mobile_phone_number')?.value,
        phone_number: this.userInfosFormGroup?.get('phone_number')?.value,
        id_threema: this.userInfosFormGroup?.get('id_threema')?.value,
        roles_by_site
      }
    }

    this.userRoleFormGroup = null;
    this.dataChanged = true;

    this.validateData();

  }
  private removeUserRole(): void {
    const roles_by_site = this.data!.roles_by_site;
    const data = this.data!.roles_by_site.find((item:UserRoleBySite) => item.uid===this.dialogPendingData?.uid);
    if(data){
      roles_by_site.splice(roles_by_site.indexOf(data), 1);
      this.data!.roles_by_site = [...roles_by_site];
      this.dialogPendingData = null;
    }
  }

  private validateData(): void {
    // TODO > validate form datas
    // this.dataValidated = ...
  }

  private openConfirmationDialog(): void {
    this.dialog_service.openDialog(
      ConfirmationDialogComponent,
      {
          width: '50%',
          maxWidth: '650px',
          autoFocus: 'dialog',
          closeOnNavigation: true,
          data: {
            title: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_title']),
            content: new FirstLetterUppercasePipe().transform(this.wording['confirm_user_role_suppression']),
            actions: [
              { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_infirm_label']), color:'neutral', action:'infirm' },
              { label: new FirstLetterUppercasePipe().transform(this.wording['confirmation_dialog_confirm_label']), color:'warn', action:'confirm' },
            ]
          },
        }
    );
  }

  private closeOverlay(): void {
    const urlParams= this.router.url.split('/').filter((item: string) => item!=='');
    this.router.navigate([urlParams[0]]);
  }

}
