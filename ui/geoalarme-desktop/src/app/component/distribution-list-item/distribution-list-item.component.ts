import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { User } from "../../model/user";
import { CommunicationType } from "../../model/communication-type";
import { DistributionListType } from "../../model/distribution-list-type";
import { DistributionListItem } from "../../model/distribution-list-item";
import { Subscription } from "rxjs";

@Component({
  selector: 'distribution-list-item',
  templateUrl: './distribution-list-item.component.html',
  styleUrl: './distribution-list-item.component.scss'
})
export class DistributionListItemComponent implements OnInit, OnDestroy {

  @Input() data:                DistributionListItem | undefined;
  @Input() listsByType:         Record<string, DistributionListType[]> | undefined = {};
  @Input() communicationTypes:  CommunicationType[] | undefined = [];
  @Input() users:               User[] | undefined = [];
  @Input() wording:             Record<string, string> = {};
  @Output() action:             EventEmitter<any> = new EventEmitter<any>();

  formGroup: FormGroup | undefined;
  userFormControls: FormControl | undefined;
  distributionAlarmsFormControls: FormControl[] = [];
  distributionDefaultsFormControls: FormControl[] = [];
  communicationsFormControls: FormControl[] = [];

  formEditable = false;
  formEditing = false;
  formChanged = false;

  formChangeSbr$: Subscription | undefined;

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit() {

    this.formEditable = !this.data?.userId;

    this.formGroup = this.formBuilder.group({
      user: new FormControl(this.data?.userId?? '', [ Validators.required]),
      events: this.formBuilder.group(''),
      communications: this.formBuilder.array([]),
    });

    this.formChangeSbr$ = this.formGroup.valueChanges.subscribe(value => {
      this.formChanged = true;
    });

    Object.keys(this.listsByType!).forEach((distributionType:string) => {
      (this.formGroup?.get('events') as FormGroup).addControl(distributionType, this.formBuilder.array([]));
      this.listsByType![distributionType].forEach((item:DistributionListType) => {
        ((this.formGroup?.get('events') as FormArray).get(distributionType) as FormArray).push(new FormControl(this.data?.eventIds.includes(item.id)))
      })
    })

    this.communicationTypes!.forEach((item:CommunicationType) => {
      (this.formGroup?.get('communications') as FormArray).push(new FormControl(this.data?.messageIds.includes(item.id)))
    });

    this.userFormControls = this.formGroup!.controls['user'] as FormControl;
    this.distributionAlarmsFormControls = (this.formGroup!.controls['events'].get('distribution_list_alarms') as FormArray).controls as FormControl[];
    this.distributionDefaultsFormControls = (this.formGroup!.controls['events'].get('distribution_list_technical_defaults') as FormArray).controls as FormControl[];
    this.communicationsFormControls = (this.formGroup!.controls['communications'] as FormArray).controls as FormControl[];

    this.formEditing = !this.data?.userId;

    if(!this.formEditable){
      this.formGroup.disable();
    }

  }

  ngOnDestroy(): void {
      this.formChangeSbr$?.unsubscribe();
  }

  handleAction(e:any){
    switch(e.name){
      case 'toggle-editing':
        this.formGroup?.enable();
        this.formEditing = true;
        break;
      case 'update-distribution-item':
      case 'register-distribution-item':
      case 'suppress-distribution-item':
        this.formEditing = false;
        this.formGroup?.disable();
        this.action.emit({name:e.name, data:this.mapData()});
        break;
    }
  }

  private mapData(): DistributionListItem {

    const data:DistributionListItem = {
      id: this.data?.id,
      // uid: this.data?.uid,
      userId: this.formGroup?.value.user,
      eventIds: [],
      messageIds: []
    };

    let distributionTypeLength = 0, distributionTypeOffset = 0;
    Object.keys(this.listsByType!).forEach((distributionType:string) => {
      this.formGroup?.value.events[distributionType]?.forEach((item:boolean, index:number) => {
        if (item) data.eventIds.push(distributionTypeOffset + index + 1)
        distributionTypeLength++;
      })
      distributionTypeOffset += distributionTypeLength;
    })

    this.formGroup?.value.communications?.forEach((item:boolean, index:number) => {
      if (item) data.messageIds.push(index + 1)
    })

    return data;
  }

}
