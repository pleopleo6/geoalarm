import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributionListItemComponent } from './distribution-list-item.component';

describe('DistributionListItemComponent', () => {
  let component: DistributionListItemComponent;
  let fixture: ComponentFixture<DistributionListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistributionListItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DistributionListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
