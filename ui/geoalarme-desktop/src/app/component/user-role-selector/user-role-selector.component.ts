import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { Role } from "../../model/role";
import { Site } from "../../model/site";

@Component({
  selector: 'user-role-selector',
  templateUrl: './user-role-selector.component.html',
  styleUrl: './user-role-selector.component.scss'
})
export class UserRoleSelectorComponent {

  @Input() sites: Site[] = [];
  @Input() roles: Role[] = [];
  @Input() wording: Record<string, string> = {};
  @Input() formGroup: FormGroup | undefined;
  @Output() action: EventEmitter<any> = new EventEmitter<any>();

  handleSubmit(): void {
    this.action.emit({name:'validate-user-role'})
  }

}
