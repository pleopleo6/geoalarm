import {Component, HostListener, Inject} from '@angular/core';
import { DialogRef, DIALOG_DATA } from '@angular/cdk/dialog';
import { DialogData } from "../../model/dialog-data";

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrl: './confirmation-dialog.component.scss'
})
export class ConfirmationDialogComponent {

  @HostListener('window:keyup', ['$event']) onKeyDown = (e: KeyboardEvent) => {
    if(e.keyCode===27) {
      this.close();
    }
  };

  constructor(protected dialogRef: DialogRef<string>, @Inject(DIALOG_DATA) protected data: DialogData) {}

  private close(): void {
    this.dialogRef.close()
  }
}
