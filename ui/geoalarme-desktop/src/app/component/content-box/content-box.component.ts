import { Component, Input } from '@angular/core';

@Component({
  selector: 'content-box',
  templateUrl: './content-box.component.html',
  styleUrl: './content-box.component.scss'
})
export class ContentBoxComponent {
  @Input() title: string | undefined;
}
