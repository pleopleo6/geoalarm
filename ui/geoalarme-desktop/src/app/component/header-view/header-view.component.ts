import { Component } from '@angular/core';
import { NavigationEnd, Router } from "@angular/router";
import { DEFAULT_LANGUAGE } from "../../app.config";
import { WordingService } from "../../app.services";
import { Languages } from "../../model/languages";
import { Language } from "../../model/language";

export interface INavigationLink {
  labelId: string;
  url: string;
  toolbar: IToolbarItem[];
}
export interface IToolbarItem {
  pictoId: string;
  actionName: string;
}

@Component({
  selector: 'header-view',
  templateUrl: './header-view.component.html',
  styleUrl: './header-view.component.scss'
})
export class HeaderViewComponent {

  wording:            Record<string, string> = {};
  languages:          Language[] = [];
  favoriteLanguage:   string;

  publicSections:     Record<string, INavigationLink[]> = {
    'super-administrator': [
      {
        labelId: 'sites',
        url: '/sites',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'devices',
        url: '/devices',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'map',
        url: '/map',
        toolbar: [
          { pictoId: 'filter_list',  actionName: 'filter' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'users',
        url: '/users',
        toolbar: [
          { pictoId: 'add',  actionName: 'add' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'logs',
        url: '/logs',
        toolbar: [
          { pictoId: 'filter_list',  actionName: 'filter' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
    ],

    'administrator': [
      {
        labelId: 'sites',
        url: '/sites',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'devices',
        url: '/devices',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'map',
        url: '/map',
        toolbar: [
          { pictoId: 'filter_list',  actionName: 'filter' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'users',
        url: '/users',
        toolbar: [
          { pictoId: 'add',  actionName: 'add' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'logs',
        url: '/logs',
        toolbar: [
          { pictoId: 'filter_list',  actionName: 'filter' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
    ],

    'operator': [
      {
        labelId: 'sites',
        url: '/sites',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'devices',
        url: '/devices',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'map',
        url: '/map',
        toolbar: [
          { pictoId: 'filter_list',  actionName: 'filter' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      /*{
        labelId: 'users',
        url: '/users',
        toolbar: [
          { pictoId: 'add',  actionName: 'add' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },*/
      {
        labelId: 'logs',
        url: '/logs',
        toolbar: [
          {pictoId: 'filter_list', actionName: 'filter'},
          {pictoId: 'search', actionName: 'search'},
        ]
      }
    ],

    'observer': [
      /*{
        labelId: 'sites',
        url: '/sites',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },*/
      {
        labelId: 'devices',
        url: '/devices',
        toolbar: [
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'map',
        url: '/map',
        toolbar: [
          { pictoId: 'filter_list',  actionName: 'filter' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      /*{
        labelId: 'users',
        url: '/users',
        toolbar: [
          { pictoId: 'add',  actionName: 'add' },
          { pictoId: 'search',  actionName: 'search' },
        ]
      },
      {
        labelId: 'logs',
        url: '/logs',
          { pictoId: 'filter_list',  actionName: 'filter' },
          { pictoId: 'search',  actionName: 'search' },
      },*/
    ]
  };
  currentUserRole:    string | null;
  userSections:       INavigationLink[] = [];
  currentSection:     INavigationLink | undefined;

  constructor(private readonly router: Router,
              private readonly wording_service: WordingService) {

    // const userRolesBySite: Record<string, string> = JSON.parse(localStorage.getItem('geoalarme-mobile-roles')!);
    this.currentUserRole = localStorage.getItem('geoalarme-desktop-roles');
    this.userSections = this.publicSections[this.currentUserRole!];

console.log('currentUserRole:', this.currentUserRole, '| userSections:', this.userSections)

    const favoriteLanguage = localStorage.getItem('geoalarme-desktop-favorite-language');
    this.favoriteLanguage = favoriteLanguage?? DEFAULT_LANGUAGE;

    this.wording_service.map$.subscribe((wording: Record<string, string>) => {
      if(Object.keys(wording).length) {
        this.wording = wording;
        if(!this.languages.length){
          Object.keys(Languages).forEach((k: string) => {
            // @ts-ignore
            this.languages = [...this.languages, { id: k, label: this.wording[Languages[k]] }]
          });
        }
      }
    });

    const refUrl = '/' + this.router.url.split('/')[1];
    this.currentSection = this.userSections.find((item: INavigationLink) => item.url===refUrl)

    this.router.events.subscribe( e => {
      if(e instanceof NavigationEnd) {
        const refUrl = '/' + this.router.url.split('/')[1];
        this.currentSection = this.userSections.find((item: INavigationLink) => item.url===refUrl)
      }
    });

  }

  hangleChangeLanguage(language: Language) {
    localStorage.setItem('geoalarme-desktop-favorite-language', language.id);
    this.wording_service.load().subscribe((response:any) => {
      if(response.length){
        this.wording_service.register(response);
      }
      return true;
    });
  }
  handleLogout() {
    this.router.navigate(['login']);
  }
  handleToolbarAction(action: string){
    console.log('handleToolbarAction', action)
  }
  handleSharedAction(data:any) {
    switch (data.name) {
      case 'loading':

        break;
      case 'end-loading':

        break;
    }
  }
}
