import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsFilterViewComponent } from './logs-filter-view.component';

describe('LogsFilterViewComponent', () => {
  let component: LogsFilterViewComponent;
  let fixture: ComponentFixture<LogsFilterViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogsFilterViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LogsFilterViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
